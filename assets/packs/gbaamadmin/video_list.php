<!DOCTYPE html>
<html>
<head>
    <title>It's Brain - premium admin HTML template by Eugene Kopyov</title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){
            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                    $('#post_'+id).fadeOut();
                }

                return false;
            });
        });
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>


<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content">
        <div class="title"><h5>Edit Videos</h5></div>
        
        
        <!-- Static table with resizable columns -->
        <div class="widget">
            <div class="head"><h5 class="iFrames">Video list</h5></div>
            <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic resize">
                <thead>
                    <tr>
                      <td width="100">Upload Date</td>
                      <td width="500">Title</td>
                      <td width="100">Action</td>
                    </tr>
                </thead>
                <tbody>

                    <tr class="videopost" id="post_10">
                        <td>Jan 1st</td>
                        <td>
                            <a href="#" class="info">
                                <img class="thumb" src="http://www.gbaam.com/thumbs/damiduro.jpg">
                                <span class="name">Davio - Dami Duro</span>
                            </a>
                        </td>
                        <td><a href="#" class="deletepost" id="10">Delete</a> &middot; <a href="#">Edit</a></td>
                    </tr>

                    

                </tbody>
            </table>
        </div>

        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&copy; Copyright 2011. All rights reserved. It's Brain admin theme by <a href="#" title="">Eugene Kopyov</a></span>
    </div>
</div>

</body>
</html>
