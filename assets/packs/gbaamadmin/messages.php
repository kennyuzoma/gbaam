<!DOCTYPE html>
<html>
<head>
    <title>It's Brain - premium admin HTML template by Eugene Kopyov</title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>


<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content">
        <div class="title"><h5>Messages</h5></div>
        
        <!-- Static table with resizable columns -->
        <div class="widget">
            <div class="head"><h5 class="iFrames">Messages</h5></div>
            <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic resize">
                <thead>
                    <tr>
                      <td width="100">Date</td>
                      <td width="300">Title</td>
                      <td width="150">Name</td>
                      <td width="150">Action</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Jan 1st</td>
                        <td><a href="#">UltraServs</a></td>
                        <td><a href="#">Kenny Uzoma</a></td>
                        <td><a class="deletepost">Delete</a> &middot; <a href="#">Edit</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&copy; Copyright 2011. All rights reserved. It's Brain admin theme by <a href="#" title="">Eugene Kopyov</a></span>
    </div>
</div>

</body>
</html>
