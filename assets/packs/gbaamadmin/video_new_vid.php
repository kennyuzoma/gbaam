<!DOCTYPE html>
<html>
<head>
    <title>It's Brain - premium admin HTML template by Eugene Kopyov</title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){
            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                    $('#post_'+id).fadeOut();
                }

                return false;
            });
        });
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>




<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content" id="container">
    	<div class="title"><h5>New Video</h5></div>
        
        <!-- Form begins -->
        <form action="" class="mainForm" method="post">
        
        	<!-- Input text fields -->
            <fieldset>
                <div class="widget first">
                    <div class="head"><h5 class="iList">Enter Video Information</h5></div>
                        <div class="rowElem kenny">
                             
                            <div class="formLeft">
                                <input type="text" name="inputtext" placeholder="Artist Name"/>
                            </div>
                            <Br>

                            <div class="formLeft">
                                <input type="text" name="inputtext" placeholder="Video Title"/>
                            </div>
                            <Br>

                            <div class="formLeft">
                                <textarea type="text" name="inputtext" placeholder="Video Description"/></textarea>
                            </div>
                            <div class="fix"></div>

                        </div>

              
                  
                    <div class="floatright twoOne">
                   
                    <input type="submit" value="Submit form" class="greyishBtn submitForm" />
                    
                    </div>
                    <div class="fix"></div>
                </div>
            </fieldset> 
        </form>
        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<?php include('inc/footer.php'); ?>

</body>
</html>
