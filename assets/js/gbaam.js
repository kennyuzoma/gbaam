$(function() {

	$('.socialPop').click(function(event) {
    var width  = 575,
      height = 400,
      left   = ($(window).width()  - width)  / 2,
      top    = ($(window).height() - height) / 2,
      url    = this.href,
      opts   = 'status=1' +
      ',width='  + width  +
      ',height=' + height +
      ',top='    + top    +
      ',left='   + left;

      window.open(url, 'twitter', opts);

    return false;
  });

	/*$('.lbox').submit(function(e){

		e.preventDefault();

		$(this).submit();

		var username = $('.lusername').val();
		var password = $('.lpass').val();

		$.post("main/check_login", $(this).serialize(), function(result) {
				if(result == 'TRUE')
				{
					alert('TRUE');
				}
				else
				{
					$('.wrong').fadeIn();
				}
			});

		return false;


	});*/
	/*var testing = false;

    $('.loginbutton').on('click', function() {

    	var theform = $('.loginbutton').closest('form');

        $.ajax({
            //url: theform.attr('action'),
            url: 'main/ajax_login',
            type: 'post',
            data: theform.serialize(),
            contenttype: "application/json",
    		datatype: 'jsonp',
            async: false,
            success: function(data) {
            	newdata = JSON.parse(data);
                if (newdata.status == 'success')
                {
                    //testing = true;
                    //theform.attr('action');
                    //theform.submit();
                }
                else
                {
                   $('.wrong_login').fadeIn().html(newdata.message);
                }
            },
            error: function() {
                alert('There has been an error, please alert us immediately');
            }
        });

        return testing;
    });
/*
    $('.loginbutton').on('click', function() {

    	var theform = $(this).closest('form');
    	
        $.ajax({
             url: 'main/ajax_login',
            type: 'post',
            data: theform.serialize(),
            contenttype: "application/json",
    		datatype: 'jsonp',
            async: false,
            success: function(data) {
            	newdata = JSON.parse(data);
                if (newdata.status == 'success')
                {
                    testing = true;
                    theform.attr('action');
                    theform.submit();
                }
                else
                {
                   $('.wrong_login').fadeIn().html(newdata.message);
                }
            },
            error: function() {
                alert('There has been an error, please alert us immediately');
            }
        });

        return testing;
    });
*/
   /* $('.submitbutton').on('click', function() {

    	var theform = $(this).closest('form');
   	
        $.ajax({
             url: theform.attr('action'),
            type: 'post',
            data: theform.serialize(),
            contenttype: "application/json",
    		datatype: 'jsonp',
            async: false,
            success: function(data) {
            	newdata = JSON.parse(data);
                if (newdata.status == 'success')
                {
                    testing = true;
                    theform.attr('action');
                    theform.submit();
                }
                else
                {
                   $('.wrong_signup').fadeIn().html(newdata.message);

                }
            },
            error: function() {
                alert('There has been an error, please alert us immediately');
            }
        });

        return testing;
    });
*/
	$('.js_fb_click').live('click',function(){
                $('.area').replaceWith('<span>Logging into Facebook...</span>');//.replaceWith('<img src="http://gbaam.com/assets/img/ajax-loader.gif" />');
            });
	$('.js_twitter_click').live('click', function(){
		$('.area').replaceWith('<span>Logging into Twitter...</span>');
	});

	/* Search Stuff */
	$( "#search input[type='submit']" ).attr("disabled", "disabled");
	if($( "#search input[type='text']" ).val() == ''){
		$( "#search input[type='text']" ).val("Search");
	}
	else
	{
		$("#search input[type='text']").css("color","black");
		$( "#search input[type='submit']" ).removeAttr("disabled", "disabled");
	}
	
	$( "#search input[type='text']" ).hover(function(){
		if($( "#search input[type='text']" ).val() == 'Search'){
			$(this).val('').css("color","black");
			$( "#search input[type='submit']" ).removeAttr("disabled", "disabled");
		}
	});






	//Set Default State of each portfolio piece
	$(".paging").show();
	$(".paging a:first").addClass("active");

	//Get size of images, how many there are, then determin the size of the image reel.
	var imageWidth = $(".window").width();
	var imageSum = $(".image_reel img").size();
	var imageReelWidth = imageWidth * imageSum;

	//Adjust the image reel to its new size
	$(".image_reel").css({'width' : imageReelWidth});

	//Paging + Slider Function
	rotate = function(){
		var triggerID = $active.attr("rel") -1; //Get number of times to slide
		var image_reelPosition = triggerID * imageWidth; //Determines the distance the image reel needs to slide

		$(".paging a").removeClass('active'); //Remove all active class
		$active.addClass('active'); //Add active class (the $active is declared in the rotateSwitch function)

		//Slider Animation
		$(".image_reel").animate({
			left: -image_reelPosition
		}, 500 );


	};


	//Rotation + Timing Event
	rotateSwitch = function(){
		play = setInterval(function(){ //Set timer - this will repeat itself every X seconds
			$active = $('.paging a.active').next();
			if ( $active.length === 0) { //If paging reaches the end...
				$active = $('.paging a:first'); //go back to first
			}
			rotate(); //Trigger the paging and slider function
		}, 8000); //Timer speed in milliseconds
	};

	rotateSwitch(); //Run function on launch

	//On Hover
	$(".image_reel a").hover(function() {
		clearInterval(play); //Stop the rotation
	}, function() {
		rotateSwitch(); //Resume rotation
	});

	//On Click
	$(".paging a").click(function() {
		$active = $(this); //Activate the clicked paging
		//Reset Timer
		clearInterval(play); //Stop the rotation
		rotate(); //Trigger rotation immediately
		rotateSwitch(); // Resume rotation
		return false; //Prevent browser jump to link anchor
	});


	$('.loginclick').live('click',function (e) {
		$('#basic-modal-content').modal({
			overlayClose:true,
			opacity:70,
			overlayCss: {backgroundColor:"#000"},
			containerCss: {height:"340px"},
			position: ["10%","50%"]
		});

		return false;
	});

	$('.signupclick').on('click',function (e) {
		$('#basic-modal-content_signup').modal({
			overlayClose:true,
			opacity:70,
			overlayCss: {backgroundColor:"#000"},
			position: ["10%","50%"]
		});

		return false;
	});


	

	

});