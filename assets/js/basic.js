/*
 * SimpleModal Basic Modal Dialog
 * http://simplemodal.com
 *
 * Copyright (c) 2013 Eric Martin - http://ericmmartin.com
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

jQuery(function ($) {
	// Load dialog on page load
	//$('#basic-modal-content').modal();

	// Load dialog on click
	$('.loginclick').live('click',function (e) {
		$('#basic-modal-content').modal({
			overlayClose:true,
			opacity:60,
			overlayCss: {backgroundColor:"#000"},
			position: ["10%","33%"]
		});

		return false;
	});

	$('.signupclick').live('click',function (e) {
		$('#basic-modal-content_signup').modal({
			overlayClose:true,
			opacity:60,
			overlayCss: {backgroundColor:"#000"},
			position: ["10%","33%"]
		});

		return false;
	});
});