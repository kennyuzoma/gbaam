<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class MY_Exceptions extends CI_Exceptions {

    function __construct(){
        parent::__construct();
    }

    function show_404($page = '', $log_error = true)
    {   
        // error page logic
        header("HTTP/1.1 404 Not Found");

        // First, assign the CodeIgniter object to a variable
        $CI =& get_instance();

        // do what you want here, even db stuff or just 
        // load your template with a custom 404
        $d['title'] = "Oops! 404 page";
        $d['meta_desc'] = "404 not found.";

        $CI->load->view('error/error_404',$d);
        echo $CI->output->get_output();
        
        exit;
    }
}
?>