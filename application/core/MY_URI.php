<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class MY_URI extends CI_URI {


    function _filter_uri($str)
	{
		
		if ($str != '' && $this->config->item('permitted_uri_chars') != '' && $this->config->item('enable_query_strings') == FALSE)
		{
			// preg_quote() in PHP 5.3 escapes -, so the str_replace() and addition of - to preg_quote() is to maintain backwards
			// compatibility as many are unaware of how characters in the permitted_uri_chars will be parsed as a regex pattern
			if ( ! preg_match("|^[".str_replace(array('\\-', '\-'), '-', preg_quote($this->config->item('permitted_uri_chars'), '-'))."]+$|i", $str))
			{
				//show_error('The URI you submitted has disallowed characters.', 400);
				//redirect('error');
				$str = '';
				//$str = $str;
			}
		}

		// Convert programatic characters to entities
		$bad	= array('$',		'(',		')',		'%28',		'%29');
		$good	= array('&#36;',	'&#40;',	'&#41;',	'&#40;',	'&#41;');

		return $str;
	}

	public function _parse_routes()
	{
	    foreach ($this->uri->segments as &$segment)
	    {
	        $segment = strtolower($segment);
	    }

	    return parent::_parse_routes();
	}

}
