<?php

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

function youtube_id_from_url($url) {
   $pattern = '#^(?:https?://)?';    # Optional URL scheme. Either http or https.
    $pattern .= '(?:www\.)?';         #  Optional www subdomain.
    $pattern .= '(?:';                #  Group host alternatives:
    $pattern .=   'youtu\.be/';       #    Either youtu.be,
    $pattern .=   '|youtube\.com';    #    or youtube.com
    $pattern .=   '(?:';              #    Group path alternatives:
    $pattern .=     '/embed/';        #      Either /embed/,
    $pattern .=     '|/v/';           #      or /v/,
    $pattern .=     '|/watch\?v=';    #      or /watch?v=,    
    $pattern .=     '|/watch\?.+&v='; #      or /watch?other_param&v=
    $pattern .=   ')';                #    End path alternatives.
    $pattern .= ')';                  #  End host alternatives.
    $pattern .= '([\w-]{11})';        # 11 characters (Length of Youtube video ids).
    $pattern .= '(?:.+)?$#x';         # Optional other ending URL parameters.
    preg_match($pattern, $url, $matches);
    return (isset($matches[1])) ? $matches[1] : false;
 }

	function getYoutubeDuration($url)
	{
		//$url = "http://www.youtube.com/watch?v=C4kxS1ksqtw&feature=relate";
		parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
		$vid = $my_array_of_vars['v']; 

        $data=@file_get_contents('http://gdata.youtube.com/feeds/api/videos/'.$vid.'?v=2&alt=jsonc');
        if (false===$data) return false;

        $obj=json_decode($data);

        $seconds = $obj->data->duration;

        $new_duration = gmdate("H:i:s", $seconds);

        $pieces = explode(":", $new_duration);

		$h = $pieces[0]; // Hours
		$m = $pieces[1]; // Minutes
		$s = $pieces[2]; // Seconds

		//start duration variable
		$dur = '';

		//HOUR STUFF
		if($h == '00')
		{
			
				$dur .= '';
		}
		else
		{
			$firstCharOfHour = substr($h, 0, 1);
			if(($firstCharOfHour == '0'))
				$dur .= substr($h, 1, 1).':';
			else
				$dur .= $h.':';
		}

		//MIN STUFF
		$firstCharOfMin = substr($m, 0, 1);
		if(($firstCharOfMin == '0') && ($h =='00'))
			$dur .= substr($m, 1, 1).':';
		
		else
			$dur .= $m.":";


		$dur .= $s;

		echo $dur;
    }

    function youtube_meta_image($video_id)
	{
		$CI =& get_instance();
		$gv = $CI->Video_model->getVideo_row($video_id);

		if(($gv->thumb == '') and (strpos($gv->video_src,'youtu') !== false) )
		{
			return 'http://img.youtube.com/vi/'.youtube_id_from_url($gv->video_src).'/maxresdefault.jpg';
		}
		elseif(($gv->thumb != '') and (strpos($gv->video_src,'youtu') !== false))
		{
			$imageUrl = base_url().'thumbs/'.$gv->thumb;

			list($width, $height, $type, $attr) = getimagesize($imageUrl);

			if($height < 200 || $width < 200)
			{
				

				if(strpos($gv->video_src,'youtu') !== false)
					return 'http://img.youtube.com/vi/'.youtube_id_from_url($gv->video_src).'/maxresdefault.jpg';
				else
					return $imageUrl;
								
			}
		}
		else
		{
			return base_url().'assets/img/gbaamimagelg.jpg';
		}
		
	}