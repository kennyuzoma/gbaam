<?php

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

function is_localhost()
{
	if($_SERVER['HTTP_HOST'] == 'localhost')
		return TRUE;
	else
		return FALSE;
}

function is_loggedin()
{
	$CI = &get_instance();

	if($CI->session->userdata('logged_in'))
		return TRUE;
	else
		return FALSE;
}

function loggedin_user_id()
{
	$CI = &get_instance();

	return $CI->session->userdata('user_id');
}

function admin_logged_in()
	{
		$CI =& get_instance();
		$admin_logged_in = $CI->session->userdata('admin_logged_in');
		
		if(!isset($admin_logged_in) || $admin_logged_in != true)
		{
			redirect('admin/');
		}	
		return $admin_logged_in;
	}
 
function get_user($user_id)
{
	$CI = &get_instance();

	return $CI->User_model->get_user($user_id);
}

function current_time()
{
	// return the current time
	return date('Y-m-d H:i:s');
}

function login_redirect($location = 'main/login')
{
	$CI = &get_instance();

	if(!$CI->session->userdata('logged_in'))
		redirect($location.'?next='.uri_string());
}

function conv_date($date)
{
	$newDate = date("M j, Y", strtotime($date));

	return $newDate;
}

function look4links($tweetText)
{
	// URLs (from http://www.phpro.org/examples/URL-to-Link.html)
    $tweetText = preg_replace("/([\w]+:\/\/[\w-?&;#~=\.\/\@]+[\w\/])/i","<a target=\"_blank\" href=\"$1\" target=\"_blank\">$1</a>",$tweetText);
    
    /*
    $tweetText = preg_replace("/([\w-?&;#~=\.\/]+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?))/i","<A HREF=\"mailto:$1\">$1</A>",$tweetText);
    */
    // twitter handles
    $tweetText = preg_replace('/(@\S+)/i',"<a target=\"_blank\" href=\"http://twitter.com/$1\" target=\"_blank\">$1</a>",$tweetText);
    
    // hash tags map to search?q=#hash
    $tweetText = preg_replace('/(#)(\S+)/i',"<a target=\"_blank\" href=\"http://twitter.com/search?q=%23$2\" target=\"_blank\">$1$2</a>",$tweetText);   
		

    $tweetText = preg_replace('$(\s|^)(www\.[a-z0-9_./?=&-]+)(?![^<>]*>)$i',' <a target="_blank" href="http://$2"  target="_blank">$2</a> ',$tweetText);
    
    return $tweetText;
}

function clean_url_slug($str,$item_id = '')
{	
	#convert case to lower
	$str = strtolower($str);

	#remove special characters
	$str = preg_replace('/[^a-zA-Z0-9]/i',' ', $str);

	#remove white space characters from both side
	$str = trim($str);

	#remove double or more space repeats between words chunk
	$str = preg_replace('/\s+/', ' ', $str);

	#fill spaces with hyphens
	$str = preg_replace('/\s+/', '-', $str);

	// search the database if this permalink exists
	$CI = &get_instance();

	$sql = "SELECT articles.permalink, blog_posts.permalink, mixtapes.permalink from articles, blog_posts, mixtapes WHERE articles.permalink = '".$str."' OR blog_posts.permalink = '".$str."' OR mixtapes.permalink = '".$str."' LIMIT 1";
	
	$query = $CI->db->query($sql);

	// if this permalink is found then begin....
	if($query->num_rows() == 1)
	{

		// this variable is set by default to 1.
		// if we find that the item_id variable is entered, then we will check if this
		// is the selected post. and if this selected post has the same permalink then,
		// there will be no need to find a new permalink because nothing was changed.
		// (will edit this description later)
		$continue = 1;

		if($item_id != '') // only if $item_id is set....
		{
			$sql2 = "SELECT id, permalink, type
					FROM
					        (
					            SELECT id, permalink, 'articles' as type  FROM articles
					            UNION ALL
					            SELECT id, permalink, 'blog_posts' as type FROM blog_posts
					            UNION ALL
					            SELECT id, permalink, 'mixtapes' as type FROM mixtapes
					            UNION ALL
					            SELECT id, permalink, 'songs' as type FROM songs
					        ) s
					WHERE id = {$item_id} and permalink = '{$str}'
					ORDER BY ID";

			$q2 = $CI->db->query($sql2);

			if($q2->num_rows() == 1)
			{
				$continue = 0; // do not try and find a new permalink.
			}

		}

		// if continue is set 1, then this will continue
		if($continue == 1)
		{
			// find an available permalink...
			for ($i = 2; $i < 100;  $i++) 
			{
				$last2 = substr($str, -2, 2);

				if($last2 == '-'.$i)
					continue;

				if($i > 2)
				{
					$str = substr($str, 0, -1).$i;  
					break;
					
				}
				else
				{
					$str = $str.'-'.$i;
	    			break;
				}
	    		
		    }
		}

	}
    // return the string
	return $str;
}


function uname_avail($username)
{
	$CI = &get_instance();

	$sql = "SELECT * FROM users where username = '{$username}' LIMIT 1";
	$query = $CI->db->query($sql);

	if($query->num_rows() == 1)
		return FALSE;
	else
		return TRUE;
}
/**
 * This function finds creates an available username.
 * @param  [var] $username [username]
 * @return [string]           [returns the avail username.]
 */

function next_avail_username($username)
{
	if(uname_avail($username) == TRUE)
		return $username;
	else
	{
		// find an available username...
		for ($i = 1; $i < 100;  $i++) 
		{
			if(uname_avail($username.$i) == TRUE)
			{
				$username = $username.$i;
				break;
			}
		}
		return $username;
	}
	

}

if( !function_exists('get_image') )
{
	function get_image($item_type,$image_id,$img_hash,$type = '')
	{
		$CI = &get_instance();
		return $CI->Photo_model->show_image($item_type, $image_id, $img_hash, $type);

	}
}

function get_profile_image($item_type,$image_id,$img_hash,$type = '')
	{
		$CI = &get_instance();
		return $CI->Photo_model->show_profile_image($item_type, $image_id, $img_hash, $type);

	}

function is_this_a_reserved_username($username,$type = '')
{
	$CI = &get_instance();
	
	// load the config for reserved words.
	$CI->load->config('reserved_words');

	// convert the username to lower string for checking.
	$username = strtolower($username);

	// fetch both arrays
	$reserved_config = $CI->config->item('reserved_words');
	$reserved_db = explode(',', $CI->config->item('reserved_names'));

	// combine both arrays and convert to lower strings for checking.
	$reserved_usernames = array_merge($reserved_config, $reserved_db);
	$reserved_usernames = array_map('strtolower', $reserved_usernames);

	// reserved usernames from database
	if (in_array($username, $reserved_usernames))
	{
		// check if this is already in the admin database
		if($type != '')
		{
			$sql = "SELECT * FROM {$type} where username = '{$username}' LIMIT 1";
			$query = $CI->db->query($sql);
			$count = $query->num_rows();

			if($count == 1)
			{
				return FALSE; // username can be used since it was held for reservation.
			}
			else
			{
				return TRUE;
			}
		}
		else
		{
			return TRUE; // this username can not be used....
		}

	    
	}
}

function value_field($field, $default='') {
    return (isset($_POST[$field])) ? $_POST[$field] : $default;
} 

function convert_u_status($status_code)
{
	switch ($status_code)
	{
		case '0':
			return 'Confirm Acct';
			break;

		case '1':
			return 'Active';
			break;

		case '2':
			return 'Deactivated';
			break;

		case '3':
			return 'Banned';
			break;

		case '4':
			return 'Finish Signup';
			break;

	}
}

function is_this_my_post($type, $post_id)
{
	$CI = &get_instance();
	return $CI->Admin_model->is_this_my_post($type, $post_id);
}

function get_setting_updated_date($setting)
{
	$CI = &get_instance();

	$setting = $CI->Site_model->get_config_item($setting);
	
	$date = $setting->updated_date; 

	if($date == '0000-00-00 00:00:00')
		return FALSE;
	else
	{
		$newDate = date("M j, Y", strtotime($date));
		return $newDate;
	}
	
}

function admin_show_errors()
{
	$CI = &get_instance();
	
	if($CI->session->userdata('admin_user_id') == '1')
	{
		$CI->output->enable_profiler(TRUE);
		error_reporting(E_ALL);
		ini_set('display_errors', 1);
	}

}