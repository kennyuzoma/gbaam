<?php

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

function sendthatemail($template_name, $template_content, $message)
{
	require APPPATH.'libraries/Mandrill.php';

	// bring CI inside 
	$CI = &get_instance();

	// get key
	$api_key = $CI->config->item('mandrill_key');

	// new mandril 
	$mandrill = new Mandrill($api_key);

	// from
	$message['from_name'] = 'Gbaam';
	$message['from_email'] = 'noreply@Gbaam.com';

	

	// the other message stuff
	$message['global_merge_vars'][] = array(
                'name' => 'CURRENT_YEAR',
                'content' => date('Y')
            );

    $message['global_merge_vars'][] = array(
            	'name' => 'LIST_ADDRESS',
            	'content' => $CI->config->item('address')
            );

    $message['global_merge_vars'][] = array(
            	'name' => 'LIST_COMPANY',
            	'content' => 'Gbaam'
            );
	    


	
	$async = true;

	$response = $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async);
	//print_r($response);
}