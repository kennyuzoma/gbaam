<?php

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class LowerUrl {
    public function run() {
            $_SERVER['REQUEST_URI'] = strtolower($_SERVER['REQUEST_URI']);
    }
}
