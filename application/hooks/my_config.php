<?php

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

  	//Loads configuration from database into global CI config
	function load_config()
	{
		$CI =& get_instance();

		foreach($CI->Site_model->get_all_config() as $g)
		{
			$CI->config->set_item($g->setting,$g->value);
		}
	}
?>