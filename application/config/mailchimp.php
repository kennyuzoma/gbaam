<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Super-simple, minimum abstraction MailChimp API v2 wrapper
 * 
 * @author Drew McLellan <drew.mclellan@gmail.com> modified by Ben Bowler <ben.bowler@vice.com>
 * @version 1.0
 */

/**
 * api_key       
 * api_endpoint            
 */

$config['api_key'] = '393d8f1c5a5e7b69546163c5ee2778f0-us3';
$config['api_endpoint'] = 'https://us3.api.mailchimp.com/2.0/';
