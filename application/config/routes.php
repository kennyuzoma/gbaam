<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
if ($_SERVER['SERVER_NAME'] == 'blog.gbaam.com')
    $route['default_controller'] = 'blog/index';

else
	$route['default_controller'] = "main/index"; 

// 404 Routes
$route['404_override'] = 'error/index';


// Video Routes
$route['video/all'] = 'video/all/$1';
$route['video/all/(:any)'] = 'video/all/$1';
$route['video/embed/(:any)'] = 'video/embed/$1';
$route['video/embed_chall/(:any)/(:any)'] = 'video/embed_chall/$1/$2';
$route['video/add_vote/(:any)'] = 'video/add_vote/$1/$2';
$route['video/(:any)'] = 'video/view/$1';

// Gbaam TV Routes
$route['tv_all/(:any)'] = 'tv/all/$1';
$route['tv/v/(:any)'] = 'tv/v/$1';
$route['tv/view/(:any)'] = 'tv/view/$1';
$route['tv/all'] = 'tv/all';
$route['tv/all/(:any)'] = 'tv/all/$1';
$route['tv/(:any)'] = 'tv/index/$1';
$route['tv/all/(:num)'] = 'tv/all/$1';

$route['gbaamtv'] = 'tv';
$route['gbaamtv_all/(:any)'] = 'tv/all/$1';
$route['gbaamtv/v/(:any)'] = 'tv/v/$1';
$route['gbaamtv/view/(:any)'] = 'tv/view/$1';
$route['gbaamtv/all'] = 'tv/all';
$route['gbaamtv/all/(:any)'] = 'tv/all/$1';
$route['gbaamtv/(:any)'] = 'tv/index/$1';
$route['gbaamtv/all/(:num)'] = 'tv/all/$1';

// Editorial Routes
$route['editorial'] = 'editorial/index';
$route['editorial/index'] = 'editorial/index';
$route['editorial/index/(:num)'] = 'editorial/index/$1';
$route['editorial/home'] = 'editorial/home';
$route['editorial/home/(:num)'] = 'editorial/index/$1';
$route['editorial/view/(:any)'] = 'editorial/view/$1';
$route['editorial/author/'] = 'editorial/author/';
$route['editorial/author/(:any)'] = 'editorial/author/$1';
$route['editorial/(:any)'] = 'editorial/view/$1';

// Blog Routes
$route['blog'] = 'blog/index';
$route['blog/index'] = 'blog/index';
$route['blog/index/(:num)'] = 'blog/index/$1';
$route['blog/home'] = 'blog/home';
$route['blog/home/(:num)'] = 'blog/index/$1';
$route['blog/view/(:any)'] = 'blog/view/$1';
$route['blog/(:any)'] = 'blog/view/$1';

// Main Controller Routes
$route['contact'] = 'main/contact';
$route['about'] = 'main/about';
$route['advertise'] = 'main/advertise';
$route['writeforus'] = 'main/writeforus';
$route['writer-guidelines'] = 'main/writer_guidelines';
$route['main/video-agreement'] = 'main/video_agreement';
$route['main/video-guidelines'] = 'main/video_guidelines';

// Mixtapes Redirect
$route['mixtape'] = 'mixtapes'; 
$route['mixtape/(:any)'] = 'mixtapes'; 

// Mixtape Routes
$route['mixtapes/(:num)/(:any)'] = 'mixtapes/tape/$1/$2'; 

// Song Routes
$route['song'] = 'chunes';
$route['song(:any)'] = 'chunes';
$route['songs/(:any)'] = 'chunes';
$route['songs/(:num)/(:any)'] = 'chunes/song/$1/$2'; 
$route['chune'] = 'chunes';
$route['chunes/(:num)/(:any)'] = 'chunes/song/$1/$2'; 

// profile Routes
$route['profile'] = 'users/profile';
$route['profile/(:any)'] = 'users/profile/$1';

$route['admin/(:any)'] = 'admin/$1';



/* End of file routes.php */
/* Location: ./application/config/routes.php */