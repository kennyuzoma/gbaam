<?php 

if($_SERVER['HTTP_HOST'] == 'localhost')
{
	$config['image_library'] = 'GD2';
}
else
{
	$config['image_library'] = 'ImageMagick';
	$config['library_path'] = '/usr/bin/convert';
}
