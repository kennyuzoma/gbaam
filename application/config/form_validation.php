<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*******
*
*  ALL VALIDATION RULES!
*
********/
$config = array(
	'form_login' => array(
		array(
			'field' => 'username',
			'label' => 'Username',
			'rules' => 'trim|required|xss_clean'
			),
		array(
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'trim|required|xss_clean|min_length[6]|callback__check_login'
			)
		),
	'main/jslogin' => array(
		array(
			'field' => 'username',
			'label' => 'Username',
			'rules' => 'trim|required|xss_clean'
			),
		array(
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'trim|required|xss_clean|min_length[6]|callback__check_login'
			)
		),
	'main/signup' => array(
		array(
			'field' => 's_name',
			'label' => 'Name',
			'rules' => 'trim|required|xss_clean'
			),
		array(
			'field' => 's_username',
			'label' => 'Username',
			'rules' => 'trim|required|xss_clean|callback__doesUserExist_username'
			),
		array(
			'field' => 's_email',
			'label' => 'Email Address',
			'rules' => 'trim|required|xss_clean|valid_email|callback__doesEmailExist'
			),
		array(
			'field' => 's_password',
			'label' => 'Password',
			'rules' => 'trim|required|xss_clean|min_length[6]'
			)
		),

	'main/forgotpassword' => array(
		array(
			'field' => 'f_email',
			'label' => 'Email',
			'rules' => 'trim|required|xss_clean|callback__doesUserExist_forgot'
			)
		),
	'main/resetpw' => array(
		array(
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'trim|required|xss_clean|matches[conf_password]|md5|min_length[6]'
			),
		array(
			'field' => 'conf_password',
			'label' => 'Password Confirmation',
			'rules' => 'trim|required|xss_clean|min_length[6]'
			)

		),
	'main/twitter_finish' => array(
		array(
			'field' => 's_email',
			'label' => 'Email Address',
			'rules' => 'trim|required|xss_clean|valid_email|callback__doesEmailExist[twitter]'
			)
		),
	'subscribe' => array(
		array(
			'field' => 'name',
			'label' => 'Your Name',
			'rules' => 'trim|required|xss_clean'
			),
		array(
			'field' => 'email',
			'label' => 'Email Address',
			'rules' => 'trim|required|xss_clean|valid_email|callback__isEmailSubscribed'
			)
		),
	'main/contact' => array(
		array(
			'field' => 'person_name',
			'label' => 'Person Name',
			'rules' => 'trim|required|xss_clean'
			),
		array(
			'field' => 'email',
			'label' => 'Email',
			'rules' => 'trim|required|xss_clean|valid_email'
			),
		array(
			'field' => 'subject',
			'label' => 'Subject',
			'rules' => 'trim|xss_clean'
			),
		array(
			'field' => 'message',
			'label' => 'Message',
			'rules' => 'trim|required|xss_clean'
			)

		),
	'admin_login' => array(
		array(
			'field' => 'username',
			'label' => 'Username',
			'rules' => 'trim|required|xss_clean'
			),
		array(
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'trim|required|xss_clean|callback__login_check'
			)
		),
	'admin_video' => array(
		array(
			'field' => 'videotitle',
			'label' => 'Video Title',
			'rules' => 'trim|required|xss_clean'
			),
		array(
			'field' => 'artistname',
			'label' => 'Artist name',
			'rules' => 'trim|required|xss_clean'
			),
		array(
			'field' => 'video_src',
			'label' => 'Video Source',
			'rules' => 'trim|required|xss_clean'
			)
		),
	'admin_video' => array(
		array(
			'field' => 'videotitle',
			'label' => 'Video Title',
			'rules' => 'trim|required|xss_clean'
			),
		array(
			'field' => 'video_src',
			'label' => 'Video Source',
			'rules' => 'trim|required|xss_clean'
			)
		),
	'admin_mixtape' => array(
		array(
			'field' => 'mixtape_title',
			'label' => 'Mixtape Title',
			'rules' => 'trim|required|xss_clean'
			),
		array(
			'field' => 'artist_name',
			'label' => 'Artist name',
			'rules' => 'trim|required|xss_clean'
			)
		),
	'admin_song' => array(
		array(
			'field' => 'song_title',
			'label' => 'Song Title',
			'rules' => 'trim|required|xss_clean'
			),
		array(
			'field' => 'artist_name',
			'label' => 'Artist name',
			'rules' => 'trim|required|xss_clean'
			)
		),
	'admin_article' => array(
		array(
			'field' => 'title',
			'label' => 'Title',
			'rules' => 'trim|required|xss_clean'
			)
		),
	'admin_slide' => array(
		array(
			'field' => 'alt_text',
			'label' => 'Text',
			'rules' => 'trim|xss_clean'
			)
		),
	'settings' => array(
								/*array(
                                        'field' => 'firstname',
                                        'label' => 'First Name',
                                        'rules' => 'trim|required|xss_clean'
                                     ),
								array(
                                        'field' => 'lastname',
                                        'label' => 'Last Name',
                                        'rules' => 'trim|xss_clean'
                                        ),*/
		array(
			'field' => 'display_name',
			'label' => 'Display Name',
			'rules' => 'trim|xss_clean|required'
			),
		array(
			'field' => 'email',
			'label' => 'Email Address',
			'rules' => 'trim|required|xss_clean|valid_email|callback__doesEmailExist'
			),
		array(
			'field' => 'username',
			'label' => 'Username',
			'rules' => 'trim|required|xss_clean|callback__doesUserExist_username'
			),
		array(
			'field' => 'bio',
			'label' => 'Bio',
			'rules' => 'trim|xss_clean'
			),
		),
	'settings_password' => array(
		array(
			'field' => 'password1',
			'label' => 'Password',
			'rules' => 'trim|required|xss_clean|matches[password2]'
			),
		array(
			'field' => 'password2',
			'label' => 'Password Confirmation',
			'rules' => 'trim|required|xss_clean|min_length[6]'
			)

		)
);