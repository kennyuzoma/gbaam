

<!DOCTYPE html>
<html>
<head>

	<?php 
		//Load the reqiured Files View
		echo $this->load->view('inc/req_files'); 
	?>

</head>

<body class="singleVideo">
	<div id="fb-root"></div>

	<?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
<div id="wrapper">

  <?php 
    //Load the reqiured header view
    echo $this->load->view('inc/header'); 
  ?>

	<div class="vidmain">

		<div style="float:left;width:930px;height:200px;margin-bottom:15px;background: url(<?php echo base_url(); ?>assets/img/gbaam_blog.jpg); border-radius:10px;box-shadow: 0px 0px 10px black;"></div>
		
		<div id="featuredpg_full" >
      		<div class="storytitle">
            	<h1 class="title"><?php echo $title; ?></h1>
       	  		<span style="font-size:13px;float:left;width:100%;color:#aaa;">By <span style="color:#ddd;"><?php echo $author; ?></span> on <span style="color:#ddd;"><?php echo conv_date($date); ?></span> | <a href="#disqus_thread" style="color:#f90;" data-disqus-identifier="blog-<?php echo $id; ?>"></a></span> 

       		</div>

   	  		<!-- AddThis Button BEGIN -->
			<ul class="soci_actions addthis_toolbox addthis_default_style">
				<li style="margin-right:20px;">
					<div class="fb-share-button" data-href="<?php echo base_url().$this->uri->uri_string(); ?>" data-type="button_count"></div>
				</li>
				<li><a class="addthis_button_tweet"></a></li>
				<li><a class="addthis_button_google_plusone" g:plusone:size="medium"></a></li>
				<li><a class="addthis_counter addthis_pill_style"></a></li>
			</ul>
			<!-- AddThis Button END -->
			<?php if(($image != '') && (is_numeric($image))): ?>
			<img class="aimg" src="<?php echo get_image('article',$image,'',''); ?>" alt="<?php echo $title; ?>"/>
			<?php endif; ?>

           	<div class="edbody"><?php echo $body; ?></div>
           		
			<div id="disqus_thread" style="float:left;margin-top:10px;width:620px;margin-bottom:20px;padding:10px;background:#1c1c1c;"></div>
           	
           	<?php $this->load->view('inc/disqus_help'); ?>
            

            </div>

 

		<?php $this->load->view('inc/rightsidebar'); ?>
 
		<div style="clear:both;"></div>

	</div>
           


    	<?php echo $this->load->view('inc/footer'); ?>

    	
<script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
			<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f1e445964f67956"></script>

			<script type="text/javascript">
/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
var disqus_shortname = 'gbaam'; // required: replace example with your forum shortname

/* * * DON'T EDIT BELOW THIS LINE * * */
(function () {
var s = document.createElement('script'); s.async = true;
s.type = 'text/javascript';
s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
}());
</script>

<script type="text/javascript">
	(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>


</body>
</html>

