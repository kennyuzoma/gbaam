<!DOCTYPE html>
<html>
<head>

	<?php 
		//Load the reqiured Files View
		echo $this->load->view('inc/req_files'); 
	?>

</head>

<body class="singleVideo">
	<?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
<div id="wrapper">

  <?php 
    //Load the reqiured header view
    echo $this->load->view('inc/header'); 
  ?>

	<div class="vidmain" <?php /*style="-webkit-border-top-left-radius: 10px;
-webkit-border-top-right-radius: 10px;
-moz-border-radius-topleft: 10px;
-moz-border-radius-topright: 10px;
border-top-left-radius: 10px;
border-top-right-radius: 10px;"*/ ?>>

		<div style="float:left;width:930px;height:200px;margin-bottom:15px;background: url(<?php echo base_url(); ?>assets/img/gbaam_blog.jpg); border-radius:10px;box-shadow: 0px 0px 10px black;"></div>
		
		<div id="featuredpg_full">
        	<ul id="arthome"> 
        		<?php
        			foreach($get_articles as $g):
        		?>

        		<li>

		        	<div class="storytitle">
		            	<a class="title" style="font-weight:bold;line-height:1.1em;" href="<?php echo base_url(); ?>blog/<?php echo $g->permalink; ?>"><?php echo $g->title; ?></a>
		       	  		<span style="font-size:13px;float:left;width:100%;color:#aaa;">By <span style="color:#ddd;"><?php echo $g->author; ?></span> on <span style="color:#ddd;"><?php echo conv_date($g->date_created); ?></span> | <a href="<?php echo base_url(); ?>blog/<?php echo $g->permalink; ?>#disqus_thread" class="count-comments" data-disqus-identifier="blog-<?php echo $g->id; ?>"></a></span> 

		       		</div>


		   	  		<?php if(($g->image != '') && (is_numeric($g->image))): ?>
		   	  		<div class="crop">
		   	  			<a href="<?php echo base_url(); ?>blog/<?php echo $g->permalink; ?>">
							<img class="aimg" src="<?php echo get_image('article',$g->image,'',''); ?>" alt="<?php echo $g->title; ?>"/>
						</a>
					</div>
					<?php endif; ?>

		            <div class="edbody home">

		            	<?php 
		            		// remove images from preview
		            		$body = preg_replace("/<img[^>]+\>/i", "", $g->body); 

		            		// remove iframes from preview
		            		$body = preg_replace('/<iframe.*?\/iframe>/i','', $body);
		            		echo word_limiter($body,50); 
		            	?>
		            	<a class="orange" href="<?php echo base_url(); ?>blog/<?php echo $g->permalink; ?>">View more</a>
		            </div>
		        </li>

		        <?php 
		        	endforeach; 
		        ?>



		    </ul> 
		    <?php echo $pagi_links; ?>

		   </div>

           	

 

		<?php $this->load->view('inc/rightsidebar'); ?>
 
		<div style="clear:both;"></div>

	</div>
           


    	<?php echo $this->load->view('inc/footer'); ?>
<script type="text/javascript">
/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
var disqus_shortname = 'gbaam'; // required: replace example with your forum shortname

/* * * DON'T EDIT BELOW THIS LINE * * */
(function () {
var s = document.createElement('script'); s.async = true;
s.type = 'text/javascript';
s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
}());
</script>
</body>
</html>

