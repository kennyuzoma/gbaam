<!DOCTYPE html>
<html>
<head>

  <?php 
    //Load the reqiured Files View
    echo $this->load->view('inc/req_files'); 
  ?>
    
</head>

<body class="singleVideo">
  <?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
<div id="wrapper">

  <?php 
    //Load the reqiured header view
    echo $this->load->view('inc/header'); 
  ?>
    
    <div class="vidmain">
      <div style="float:left;width:100%;text-align:center;padding:40px 0;height:500px;">
          <h3 style="font-size:100px;color:#f90;">404!</h3><br>
			      	 <span style="font-size:20px;">Sorry, the page you were looking for is not available!<br><br> <a href="<?php echo base_url(); ?>">&laquo; Back Home</a></span>
			     
      </div>
        
  </div>
    
  <?php echo $this->load->view('inc/footer'); ?>

</body>
</html>