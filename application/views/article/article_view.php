

<!DOCTYPE html>
<html>
<head>

	<?php 
		//Load the reqiured Files View
		echo $this->load->view('inc/req_files'); 
	?>

</head>

<body class="singleVideo">
	<div id="fb-root"></div>

	<?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
<div id="wrapper">

  <?php 
    //Load the reqiured header view
    echo $this->load->view('inc/header'); 
  ?>

	<div class="vidmain">
		<?php if($status == '2'): ?>

		<div style="float:left;width:100%;margin-bottom:20px;text-align:center;font-size:25px;background:#dc9001;border-radius:5px;text-shadow: 0px 0px 3px black;">Unlisted</div>
		
		<?php elseif($status == '3'): ?>

		<div style="float:left;width:100%;margin-bottom:20px;text-align:center;font-size:25px;background:#dc9001;border-radius:5px;text-shadow: 0px 0px 3px black;">Private</div>
		
		<?php elseif($status == '4'): ?>
			
			<div style="float:left;width:100%;margin-bottom:10px;font-size:18px;border-radius:5px;text-shadow: 0px 0px 3px black;">
				<a href="<?php echo base_url() . $this->config->item('admin_location'); ?>/articles/edit/<?php echo $id;?>"><< Back to Edit Page</a>
			</div>

			<div style="float:left;width:100%;margin-bottom:20px;text-align:center;font-size:25px;background:#dc9001;border-radius:5px;text-shadow: 0px 0px 3px black;">REVIEW MODE</div>
		
		<?php endif; ?>

		<div id="featuredpg_full" style="margin-left: 5px;">
			
      		<div class="storytitle">
            	<h1 class="title"><?php echo $title; ?></h1>
       	  		<span style="font-size:13px;float:left;width:100%;color:#aaa;">
       	  			By
       	  			<a href="<?php echo base_url().'editorial/author/'.$this->Site_model->getAuthorLink($id); ?>" >
		       	  				<?php 
                        		echo $this->Site_model->getAuthor($id);
                        		?>
		       	  			</a> on <a href="<?php echo base_url(); ?>editorial/<?php echo $permalink; ?>" style="color:#ddd;"><?php echo conv_date($date); ?></a> | <a href="#disqus_thread" style="color:#f90;" data-disqus-identifier="editorial-<?php echo $id; ?>"></a></span> 

       		</div>
			
			
   	  		<!-- AddThis Button BEGIN -->
			<ul class="soci_actions addthis_toolbox addthis_default_style">
			<?php 
				// dont show in review mode
				if($status != '4'):
			?>
				<li style="margin-right:20px;margin-left:10px;">
					<div class="fb-share-button" data-href="<?php echo base_url().$this->uri->uri_string(); ?>" data-type="button_count"></div>
				</li>
				<li><a class="addthis_button_tweet"></a></li>
				<li><a class="addthis_button_google_plusone" g:plusone:size="medium"></a></li>
				<li><a class="addthis_counter addthis_pill_style"></a></li>
				<?php 
				endif; //end review mode
			?>
			</ul>
			<!-- AddThis Button END -->
			

			<?php if(($image != '') && (is_numeric($image))): ?>
				<span class="imgholder">
					<img class="aimg" src="<?php echo get_image('article',$image,'',''); ?>" alt="<?php echo $title; ?>"/>
				</span>
			<?php endif; ?>

           	<div class="edbody" ><?php echo $body; ?></div>
           	<div class="authorbio">

           		<?php 
           			if($author != 'Gbaam' && $author_id != '0'): 
           				$a = $this->Site_model->get_author($author_id);
           		?>

				<a href="<?php echo base_url(); ?>editorial/author/<?php echo $a->username; ?>" class="img">
					<?php if($a->profile_img == ''): ?>
					<img src="<?php echo base_url(); ?>assets/img/gholder_125.gif" />
					<?php else: ?>
					<img src="<?php echo get_image('admin_user',$a->profile_img,$a->img_hash,'125'); ?>" style="width:125px;" />
				<?php endif; ?>
				</a>
				<div class="right">
					<a href="<?php echo base_url(); ?>editorial/author/<?php echo $a->username; ?>" class="name">About <?php echo $a->firstname.' '.$a->lastname; ?></a>
					<span class="bio">

						<?php 
							if($a->bio == '')
								echo 'If you would like to write for Gbaam, Vist our <a href="<?php echo base_url(); ?>writeforus">Write for us</a> Page.</i>';
							else
							{
								echo $a->bio;
								if($guest == '1' || $a->admin_type == '6')
								{
									echo '<br>';
									echo '<i>This is a guest post. If you would like to write for Gbaam, Vist our <a href="'. base_url().'writeforus">Write for us</a> Page.</i>';
								}
							}
						?>

					</span>

					<ul class="social">
						<?php if($a->website != ''): ?>
						<li>
							<a href="<?php echo $a->website; ?>" target="_blank">
								<img src="<?php echo base_url(); ?>assets/img/social/www-variation.png" title="Visit Website" />
							</a>
						</li>
						<?php endif; ?>

						<?php if($a->facebook != ''): ?>
						<li>
							<a href="<?php echo $a->facebook; ?>" target="_blank">
								<img src="<?php echo base_url(); ?>assets/img/social/facebook-variation.png" title="Follow on Facebook" />
							</a>
						</li>
						<?php endif; ?>

						<?php if($a->twitter != ''): ?>
						<li>
							<a href="http://twitter.com/<?php echo $a->twitter; ?>" target="_blank">
								<img src="<?php echo base_url(); ?>assets/img/social/twitter-variation.png" title="Follow on Twitter" />
							</a>
						</li>
						<?php endif; ?>

						<?php if($a->googleplus != ''): ?>
						<li>
							<a href="<?php echo $a->googleplus; ?>" target="_blank">
								<img src="<?php echo base_url(); ?>assets/img/social/gplus-variation2.png" title="Follow on Google+" />
							</a>
						</li>
						<?php endif; ?>

						<?php if($a->instagram != ''): ?>
						<li>
							<a href="http://instagram.com/<?php echo $a->instagram; ?>" target="_blank">
								<img src="<?php echo base_url(); ?>assets/img/social/instagram-variation.png" title="Follow on Instagram" />
							</a>
						</li>
					<?php endif; ?>
					</ul>
				</div>

				<?php elseif($author == 'Gbaam'): ?>

				<a href="<?php echo base_url(); ?>editorial/author/gbaam" class="img">
					<img src="<?php echo base_url(); ?>assets/img/gholder_125.gif" />
				</a>
				<div class="right">
					<a href="<?php echo base_url(); ?>editorial/author/gbaam" class="name">
						Posted by Gbaam
					</a>
					<span class="bio">
						If you would like to write for Gbaam, Vist our <a href="<?php echo base_url(); ?>main/writeforus">Write for us</a> Page.
					</span>
					
				</div>

				<?php elseif($author_id == '0' && $author != 'Gbaam'): ?>

				<a href="<?php echo base_url(); ?>editorial/author/gbaam" class="img">
					<img src="<?php echo base_url(); ?>assets/img/gholder_125.gif" />
				</a>
				<div class="right">
					<a href="<?php echo base_url(); ?>editorial/author/gbaam" class="name">
						Posted by Guest Author - <?php echo $author; ?>
					</a>
					<span class="bio">
						This was a guest post. If you would like to write for Gbaam, Vist our <a href="<?php echo base_url(); ?>main/writeforus">Write for us</a> Page.
					</span>
					
				</div>

				<?php endif; ?>
           	</div>

		<div style="float:left;width:100%;margin:10px 0;text-align:center;">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Gbaam 2014 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:250px;height:250px"
     data-ad-client="ca-pub-5110726143954622"
     data-ad-slot="5544511172"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
		</div>
           		
           	<?php 
				// dont show in review mode
				if($status != '4'):
			?>
			<div id="disqus_thread" style="float:left;margin-top:10px;width:620px;margin-bottom:20px;padding:10px;background:#1c1c1c;"></div>
			<?php
				endif;
			?>
           	
           	<?php $this->load->view('inc/disqus_help'); ?>
            

        </div>

 

		<?php $this->load->view('inc/rightsidebar'); ?>
 
		<div style="clear:both;"></div>

	</div>
           


    	<?php echo $this->load->view('inc/footer'); ?>

    	
<script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
			<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f1e445964f67956"></script>

			<script type="text/javascript">
/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
var disqus_shortname = 'gbaam'; // required: replace example with your forum shortname

/* * * DON'T EDIT BELOW THIS LINE * * */
(function () {
var s = document.createElement('script'); s.async = true;
s.type = 'text/javascript';
s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
}());
</script>

<script type="text/javascript">
	(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>


</body>
</html>

