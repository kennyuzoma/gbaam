<!DOCTYPE html>
<html>
<head>

	<?php 
		//Load the reqiured Files View
		echo $this->load->view('inc/req_files'); 
	?>

</head>

<body class="singleVideo">
	<?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
<div id="wrapper">

  <?php 
    //Load the reqiured header view
    echo $this->load->view('inc/header'); 
  ?>

	<div class="vidmain">

		<div id="featuredpg_full" style="margin-left: 5px;">
			<?php if($this->uri->segment(2) != 'author'): ?>
			<?php /*<h2 class="cleanhead" style="font-size:30px;">Latest Stories</h2>*/ ?>
			<?php else: ?>

			<div class="authorbio" style="margin-bottom:30px;">

           		<?php 
           			$username = $this->uri->segment(3);
           			
           				$a = $this->Site_model->get_author_by_username($this->uri->segment(3));
           		?>

           		<?php if($username == 'gbaam'): ?>

				<span class="img">
					<img src="<?php echo base_url(); ?>assets/img/gholder_125.gif" />
				</span>
				<div class="right">
					<span class="name">
						Posts by Gbaam.com
					</span>
					<span class="bio">
						If you would like to write for Gbaam, Vist our <a href="<?php echo base_url(); ?>writeforus">Write for us</a> Page.
					</span>
					
				</div>

				<?php else: ?>

				<span class="img">
					<?php if($a->profile_img == ''): ?>
					<img src="<?php echo base_url(); ?>assets/img/gholder_125.gif" />
					<?php else: ?>
					<img src="<?php echo get_image('admin_user',$a->profile_img,$a->img_hash,'125'); ?>" style="width:125px;" />
				<?php endif; ?>
				</span>
				<div class="right">
					<span class="name">Posts by <?php echo $a->firstname.' '.$a->lastname; ?></span>
					<span class="bio">

						<?php 
							if($a->bio == '')
								echo 'If you would like to write for Gbaam, Vist our <a href="<?php echo base_url(); ?>writeforus">Write for us</a> Page.</i>';
							else
								echo $a->bio;
						?>

						<?php /*if($a->admin_type == '6'): ?>
							<i>
							If you would like to write for Gbaam, Vist our <a href="<?php echo base_url(); ?>main/writeforus">Write for us</a> Page.</i>
						<?php endif;*/ ?>

					</span>

					<ul class="social">
						<?php if($a->website != ''): ?>
						<li>
							<a href="<?php echo $a->website; ?>" target="_blank">
								<img src="<?php echo base_url(); ?>assets/img/social/www-variation.png" title="Visit Website" />
							</a>
						</li>
						<?php endif; ?>

						<?php if($a->facebook != ''): ?>
						<li>
							<a href="<?php echo $a->facebook; ?>" target="_blank">
								<img src="<?php echo base_url(); ?>assets/img/social/facebook-variation.png" title="Follow on Facebook" />
							</a>
						</li>
						<?php endif; ?>

						<?php if($a->twitter != ''): ?>
						<li>
							<a href="http://twitter.com/<?php echo $a->twitter; ?>" target="_blank">
								<img src="<?php echo base_url(); ?>assets/img/social/twitter-variation.png" title="Follow on Twitter" />
							</a>
						</li>
						<?php endif; ?>

						<?php if($a->googleplus != ''): ?>
						<li>
							<a href="<?php echo $a->googleplus; ?>" target="_blank">
								<img src="<?php echo base_url(); ?>assets/img/social/gplus-variation2.png" title="Follow on Google+" />
							</a>
						</li>
						<?php endif; ?>

						<?php if($a->instagram != ''): ?>
						<li>
							<a href="http://instagram.com/<?php echo $a->instagram; ?>" target="_blank">
								<img src="<?php echo base_url(); ?>assets/img/social/instagram-variation.png" title="Follow on Instagram" />
							</a>
						</li>
					<?php endif; ?>
					</ul>
				</div>

				<?php endif; ?>

           	</div>
           <?php endif; ?>
			<ul id="arthome"> 
			<?php foreach($get_articles as $g): ?>
				
					<li>
		      		<div class="storytitle">
		            	<h1 class="title">
		            		<a href="<?php echo base_url(); ?>editorial/<?php echo $g->permalink; ?>">
		            			<?php echo $g->title; ?>
		            		</a>
		            	</h1>

		       	  		<span style="font-size:13px;float:left;width:100%;color:#aaa;">
		       	  			By
		       	  			<a href="<?php echo base_url().'editorial/author/'.$this->Site_model->getAuthorLink($g->id); ?>">
		       	  				<?php 
                        		echo $this->Site_model->getAuthor($g->id);
                        		?>
		       	  			</a> on <a href="<?php echo base_url(); ?>editorial/<?php echo $g->permalink; ?>" style="color:#ddd;"><?php echo conv_date($g->date_created); ?></a> | <a href="<?php echo base_url(); ?>editorial/<?php echo $g->permalink; ?>#disqus_thread" style="color:#f90;" data-disqus-identifier="editorial-<?php echo $g->id; ?>"></a></span> 

		       		</div>

		   	  		
					<?php if(($g->image != '') && (is_numeric($g->image))): ?>
						<a href="<?php echo base_url(); ?>editorial/<?php echo $g->permalink; ?>" class="imgholder">
							<img class="aimg" src="<?php echo get_image('article',$g->image,'',''); ?>" alt="<?php echo $g->title; ?>"/>
						</a>
					<?php endif; ?>

		           	<div class="edbody home">

		            	<?php 
		            		// remove images from preview
		            		$body = preg_replace("/<img[^>]+\>/i", "", $g->body); 

		            		// remove iframes from preview
		            		$body = preg_replace('/<iframe.*?\/iframe>/i','', $body);
		            		echo word_limiter($body,50); 
		            	?>
		            	<a class="orange" href="<?php echo base_url(); ?>editorial/<?php echo $g->permalink; ?>">View more</a>
		            </div>
           			</li>
        		<?php  endforeach;  ?>
        	
        </ul>

        <?php echo $pagi_links; ?>

        </div>



		<?php $this->load->view('inc/rightsidebar'); ?>
 
		<div style="clear:both;"></div>

	</div>
           


    	<?php echo $this->load->view('inc/footer'); ?>
<script type="text/javascript">
/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
var disqus_shortname = 'gbaam'; // required: replace example with your forum shortname

/* * * DON'T EDIT BELOW THIS LINE * * */
(function () {
var s = document.createElement('script'); s.async = true;
s.type = 'text/javascript';
s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
}());
</script>
</body>
</html>

