<?php $page = 'homepage'; ?>

<!DOCTYPE html>
<html>
<head>
    <meta name="google-site-verification" content="WUA_0V1PIepm8pxZJaEUNiczkS5-8mA-UPAdg6-K9vg" />

	<?php 
		//Load the reqiured Files View
		echo $this->load->view('inc/req_files'); 
	?>

    
    
</head>

<body class="singleVideo">
    <?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
<div id="wrapper">

	<?php 
		//Load the reqiured header view
		echo $this->load->view('inc/header'); 
	?>

   
    <div id="sliderFrame" style="margin-bottom:20px;width:955px;padding-left:5px;padding-bottom:5px">
        <div id="slider" style="width:950px;">
            <?php foreach($getSlides as $gs): ?>
                        <a href="<?php echo $gs->href; ?>">
                          <img alt="<?php echo $gs->alt_text; ?>" src="<?php echo base_url() . $gs->image; ?>" />
                        </a>
                        <?php endforeach; ?> 
        </div>
    </div>

    <div id="middle">
        
        <div class="left">
            <ul class="art">

                <?php


                    for ($i=1; $i <= 3; $i++):

                    $g = $this->Site_model->getFeatArticle_slot('article',$i);

                		if($this->Site_model->getFeatArticle_slot_count('article',$i) == 0)
                			continue;

                ?>

                <li>
                    <a href="<?php echo base_url(). 'editorial/'.$g->permalink; ?>">
                        <img width="50" height="50" src="<?php echo get_image('article',$g->image,'','60'); ?>" alt="<?php echo $g->title; ?>"  />
                        <span class="title" style="font-size:13px;margin-bottom:5px;font-weight:bold;"><?php echo character_limiter($g->title,40); ?></span>
                        <span class="date" style="font-size:12px;">
                        	By <?php 
                        		echo $this->Site_model->getAuthor($g->id);
                        		?>
                        		
                        </span>
                    </a>
                </li>

                <?php 
                    endfor;
                ?>
                
            </ul>
        </div>

        <div class="right">
            <ul class="links">
            	<li><a  class="newchal" href="<?php echo base_url(); ?>main/submit/video">Submit Music Video</a></li>
            	<li><a  class="newchal" href="<?php echo base_url(); ?>main/submit/gbaamtv">Submit GbaamTV Video</a></li>
                <li><a class="newchal" href="<?php echo base_url(); ?>main/submit/challenge">Submit Challenge Video</a></li>  
            </ul>
        </div>

    </div>


   

    

    <div id="midway" style="background:#333;margin-top:0;padding-top:10px;padding-bottom:10px;">
	    <span style="font-size:24px;padding-bottom:3px;margin-bottom:10px;text-align:center;border-bottom:1px solid #5C5C5C;width:950px;float:left;">Featured Videos</span>
	    <ul id="videothumbs">

            <?php 

                for ($i=1; $i <= $this->config->item('feat_vid_homepage_count'); $i++):

                	if($this->Video_model->getFeatVideo_slot_count('five',$i) == 0)
                		continue;


                    $g = $this->Video_model->getFeatVideo_slot('five',$i);

            ?>
	    	<li>
	            <a title="<?php echo $g->artist_name.' - '.$g->title; ?>" href="<?php echo base_url(); ?>video/<?php echo hashids_encrypt($g->type_id); ?>" style="font-size:15px;">
                	<img src="<?php 

                	if($g->thumb == '')
                	{
                		if($g->yt == 1){

							echo 'http://img.youtube.com/vi/'.youtube_id_from_url($g->video_src).'/mqdefault.jpg';
						}
					}
						else
							{

                				echo base_url() . 'thumbs/' . $g->thumb; 
                			}
                	

                	?>" alt="<?php echo $g->artist_name.' - '.$g->title; ?>" /><BR>
                    <?php echo character_limiter($g->artist_name.' - '.$g->title,14); ?>
	            </a>
            </li>
            
	    	<?php 
                endfor; 
            ?>

	    </ul>

    	<a class="viewalink" href="<?php echo base_url(); ?>video">View All Videos</a>
    </div>
        <?php
        	//Load the reqiured Footer view 
            $d['css'] = 'margin-bottom:30px;';

        	echo $this->load->view('inc/footer',$d); 
        ?>


   
   
</body>
</html>
