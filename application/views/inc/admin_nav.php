<?php
	
	$action_pages = array('video','editorial','tv','challenge','mixtapes','songs','homepage');

	$seg1 = $this->uri->segment(1);
	$seg2 = $this->uri->segment(2);
	$seg3 = $this->uri->segment(3);
	$seg4 = $this->uri->segment(4);

	if(isset($getSlides)) // if this is the home page
	{
		$seg1 = 'homepage';
		$seg2 = 'homepage';
	}

	// get the admin information from the session
	$uid = $this->session->userdata('admin_user_id');

	if($uid != ''):	
		$me = $this->Admin_model->getAdmin($uid);
?>
<link href="<?php echo base_url(); ?>assets/packs/gbaamadmin/css/topnav.css" rel="stylesheet" type="text/css" />
<div id="topNav">
    <div class="fixed">
        <div class="wrapper">

            <div class="welcome">
            	<a href="<?php echo base_url() . $this->config->item('admin_location') .'/'; ?>">
            	<?php if($me->profile_img == ''): ?>
            		<img src="<?php echo base_url(); ?>assets/packs/gbaamadmin/images/userPic.png" alt="" />
				<?php else: ?>
					<img src="<?php echo get_image('admin_user',$me->profile_img,$me->img_hash,'50'); ?>" style="width:22px;" />
				<?php endif; ?>
            	<span>Welcome, <?php echo $me->firstname; ?>!</span>
            	</a>

            </div>

            <div class="userNav">
                <ul>
                	<?php 

						if(in_array($seg1,$action_pages))
						{
							if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),$seg1) == TRUE)
							{
								if($seg1 == 'video' )
								{
									$link = 'videos/mv/new/';
									$title = 'Music Video';
								}
								if($seg1 == 'editorial' )
								{
									$link = 'articles/new';
									$title = 'Editorial Story';
								}
								if($seg1 == 'tv' )
								{
									$link = 'videos/gbaamtv/new';
									$title = 'GbaamTV Video';
								}
								if($seg1 == 'challenge' )
								{
									$link = 'videos/challenge/new';
									$title = 'Challenge Video';
								}
								if($seg1 == 'mixtapes' )
								{
									$link = 'mixtape/new';
									$title = 'Mixtape';
								}
								if($seg1 == 'songs' )
								{
									$link = 'songs/new';
									$title = 'Song';
								}
								if($seg1 == 'homepage' )
								{
									$link = 'slides/new';
									$title = 'Slide';
								}

					?>


					<li>
						<a href="<?php echo base_url(). $this->config->item('admin_location') . '/'.$link; ?>" title="Add New <?php echo $title; ?>">
							<img src="<?php echo base_url(); ?>assets/packs/gbaamadmin/images/icons/topnav/subAdd.png" alt="" />
							<span >
								Add New <?php echo $title; ?>
							</span>
						</a>
					</li>

					<?php 
							}
						}
					?>

					<?php 

						if((in_array($seg1,$action_pages) && ($seg2 != '')))
						{
							if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),$seg1) == TRUE)
							{
								if($seg1 == 'video' && isset($videoid) && (is_this_my_post('videos',$videoid) == TRUE))
								{
									$link2 = 'videos/mv/edit/'.$videoid;
									$title = 'Music Video';
								}
								if($seg1 == 'editorial' && isset($id) && (is_this_my_post('articles',$id) == TRUE) )
								{
									$link2 = 'articles/edit/'.$id;
									$title = 'Story';
								}
								if($seg1 == 'tv' && isset($videoid) && (is_this_my_post('videos',$videoid) == TRUE))
								{
									$link2 = 'videos/gbaamtv/edit/'.$videoid;
									$title = 'GbaamTV Video';
								}
								if($seg1 == 'challenge' && isset($videoid) && (is_this_my_post('videos',$videoid) == TRUE))
								{
									$link2 = 'videos/challenge/edit/'.$videoid;
									$title = 'Challenge Video';
								}
								if(($seg1 == 'mixtapes')  && (isset($mixtapeid)) && (is_this_my_post('mixtapes',$mixtapeid) == TRUE))
								{
									$link2 = 'mixtape/edit/'.$mixtapeid;
									$title = 'Mixtape';
								}
								if($seg1 == 'songs'  && isset($songid) && (is_this_my_post('songs',$songid) == TRUE))
								{
									$link2 = 'songs/edit/'.$songid;
									$title = 'Song';
								}
								if($seg1 == 'homepage')
								{
									$link2 = 'slides/list';
									$title = 'Slideshow';
								}


								// this is just a conditional so that the "is_this_my_post" function can work
								if(isset($link2)):
					?>

							
								<li>
									<a href="<?php echo base_url(). $this->config->item('admin_location') . '/'.$link2; ?>" title="Add New <?php echo $title; ?>">
										<img src="<?php echo base_url(); ?>assets/packs/gbaamadmin/images/icons/topnav/subAdd.png" alt="" />
										<span style="color:#f90;">
											<b>Edit this <?php echo $title; ?></b>
										</span>
									</a>
								</li>

					<?php 
								endif;
							}
						}
					?>
                    <li>
                    	<a href="<?php echo $this->config->item('admin_location'); ?>/users/<?php echo $uid; ?>" title="">
                    		<img src="<?php echo base_url(); ?>assets/packs/gbaamadmin/images/icons/topnav/profile.png" alt="" />
                    		<span>Profile</span>
                    	</a>
                    </li>
                    <?php /*
                    <li class="dd"><a title=""><img src="<?php echo base_url(); ?>assets/packs/gbaamadmin/images/icons/topnav/messages.png" alt="" /><span>Messages</span></a>
                        <ul class="menu_body">
                            <li><a href="#" title="" class="sAdd">new message</a></li>
                            <li><a href="#" title="" class="sInbox">inbox</a></li>
                            <li><a href="#" title="" class="sOutbox">outbox</a></li>
                            <li><a href="#" title="" class="sTrash">trash</a></li>
                        </ul>
                    </li>
					*/ ?>
                    <li><a href="<?php echo $this->config->item('admin_location'); ?>/settings/general" title=""><img src="<?php echo base_url(); ?>assets/packs/gbaamadmin/images/icons/topnav/settings.png" alt="" /><span>Settings</span></a></li>
                    <li><a href="<?php echo base_url() . $this->config->item('admin_location'); ?>/logout" title=""><img src="<?php echo base_url(); ?>assets/packs/gbaamadmin/images/icons/topnav/logout.png" alt="" /><span>Logout</span></a></li>
                </ul>
            </div>
            <div class="fix"></div>
        </div>
    </div>
</div>
<?php
	endif;
?>