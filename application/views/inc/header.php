   	
   <script type="text/javascript">
   	$(function(){
		$('.menupop').hover(function(){
   			$(this).children('.ab-sub-wrapper').slideToggle("fast");
   		});
   	});
   		
   	</script>

   	<?php if($_SERVER['HTTP_HOST'] == 'localhost'): ?>
   	<a href="http://gbaam.com/<?php echo $this->uri->uri_string(); ?>" target="_blank" style="width:100%;float:left;text-align:center;display:block;font-size:20px;margin:10px 0;font-weight:bold;">Visit LIVE site!</a>
   	<?php endif; ?>

    <h1 id="logo">
        <a href="<?php echo base_url();?>">
            <img alt="Gbaam.com Logo" src="<?php echo $this->config->item('logo_url'); ?>" />
        </a>
    </h1>



    <div id="search_container">
       

        <form id="search" action="<?php echo base_url(); ?>search/pre_search" method="post" style="margin-top:10px;">
            <?php
                if(isset($term)):
            ?>
                <input type="text" name="term" value="<?php echo $term; ?>" />
            <?php
                else:
            ?>
                <input type="text" name="term" />
            <?php
                endif;
            ?>
            <input type="submit" name="submit" value="" />
        </form>

    </div>

    <div style="float:left;width:30px;margin-top:25px;margin-left:20px;">
        <a href="http://twitter.com/<?php echo $this->config->item('twitter_acct'); ?>" target="_blank">
                    <img style="width:30px;" class="toptwitter" src="<?php echo base_url(); ?>assets/img/social/twitter-variation.png" title="Follow us on Twitter!" alt="Follow us on Twitter!" />
                </a>
                <a href="https://www.facebook.com/pages/Gbaam/300011193363037" target="_blank">
                    <img style="width:30px;" class="topfb" src="<?php echo base_url(); ?>assets/img/social/facebook-variation.png" title="Follow us on Facebook" alt="Follow us on Facebook" />
                </a>
    </div>

<?php 
    if($this->session->userdata('logged_in') == FALSE)
        $margin = 'margin-top:40px;';
    else
        $margin = 'margin-top:22px;';
?>

    <div id="loginbox" style="<?php echo $margin; ?>">
		<?php
			if($this->session->userdata('logged_in') == TRUE):

  					$user_id = $this->session->userdata('user_id');

  					$display_name = $this->User_model->get_user_setting('display_name',$user_id);

                        $my_user_id = $this->session->userdata('user_id');
                        $u = $this->User_model->get_user($my_user_id);

                        // if display name is blank
                        if($display_name == '')
                            $name = $u->username;
                        
                        else
                            $name = $display_name;
                        

                        $profile_img = get_profile_image('user',$u->profile_img,$u->img_hash,'50');

                        echo '<img class="profile_img" alt="'.$name.'" width="40" height="40" src="'.$profile_img.'"/> Welcome, '.$name;
                        echo '<br>';
                        
                        echo '<a style="font-size:11px;" href="'. base_url().'main/logout?next='.uri_string().'">Log out</a>';
   
			else: 
        ?>

		<div class="area" style="text-align:center;">
			<a class="link loginclick" href="#">Log In</a> or <a href="#" class="signupclick link">Sign Up!</a>
	    </div>

		<?php endif; ?>
		

    </div>

    <script type="text/javascript">
    $(function(){
    	$('.js_resend_conf').click(function(){
    		$(this)
					.css('text-decoration','none')
					.css('cursor','auto')
					.text('Email Successfully Resent!')
					.removeClass(this);
			$.get("<?php echo base_url(); ?>email/resend_conf/");
		});

		$('.js_close').click(function(){
			$(this).parent().fadeOut(200);
			$.get("<?php echo base_url(); ?>users/close_conf/");
		});

		$('.conf_box.green').delay(4000).fadeOut();
    });
    </script>

	<?php
		if($this->session->userdata('logged_in') == TRUE):
			if($this->session->userdata('u_status') == 0):

				if($this->session->userdata('close_conf') != 'TRUE'):

	?>
    <div class="conf_box yellow" style="text-align:Center;margin-top:7px;">
    	<div style="float:left;width:98%">
	    	<div class="row" >
	    		Confirm your email address to access all of Gbaam's features. A confirmation message was sent to <b><?php echo $this->session->userdata('email'); ?></b>.

	    	</div>
	    	<span class="button js_resend_conf">Resend Confirmation Email</span>
	    </div>
    	<span class="js_close">x</span>
    </div>
    <?php
    			endif;
    		endif;
    	endif;
    ?>

	<?php
		if($this->session->flashdata('user_banned') == 'TRUE'):
	?>
    <div class="conf_box red" style="text-align:Center;margin-top:7px;">
    	<div style="float:left;width:98%">
	    	<div class="row" >
	    		Your account was Banned for violating our Terms of Service. <br>You do not have access to this account anymore. If you feel this is an error please <a href="#">click here</a>.

	    	</div>
	    </div>
    	<?php /*<span class="js_close">x</span>*/ ?>
    </div>
    <?php
    	endif;
   	?>

   	<?php
		if($this->session->flashdata('user_confirm') == 'TRUE'):
	?>
    <div class="conf_box green" style="text-align:Center;margin-top:7px;">
    	<div style="float:left;width:98%">
	    	<div class="row" style="margin-bottom:0px;">
	    		Your account has been successfully confirmed! Welcome to Gbaam!
	    	</div>
	    </div>
    	<?php /*<span class="js_close">x</span>*/ ?>
    </div>
    <?php
    	endif;
   	?>

   		<?php
		if($this->session->flashdata('user_resetpwd') == 'TRUE'):
	?>
    <div class="conf_box green" style="text-align:Center;margin-top:7px;">
    	<div style="float:left;width:98%">
	    	<div class="row" >
	    		Your password has been successfully reset!

	    	</div>
	    </div>
    	<?php /*<span class="js_close">x</span>*/ ?>
    </div>
    <?php
    	endif;
   	?>

    



    <div id="nav">
	    <ul>
          	<?php
				$page = $this->uri->segment(1);
				$gbtv = $this->uri->segment(2);
			?>

	    	<li>
                <a <?php echo $page == '' ? 'class="selected"' : '' ?> href="<?php echo base_url(); ?>" title="Home">Home</a>
            </li>

	    	<li>
                <a <?php echo $page == 'video' && $gbtv != 'gbtv' ? 'class="selected"' : '' ?> href="<?php echo base_url();?>video" title="Music Videos"> Music Videos</a>
            </li>

            <li>
                <a <?php echo $page == 'tv' || $page == 'gbaamtv_all' || $gbtv == 'gbtv' ? 'class="selected"' : '' ?> href="<?php echo base_url();?>tv" title="GbaamTV">GbaamTV</a>
            </li>
<?php /*
            <li>
                <a <?php echo $page == 'gbaamchallenge' || $page == 'challenge' ? 'class="selected"' : '' ?> href="<?php echo base_url();?>challenge" title="Challenge">Challenge</a>
            </li>
*/ ?>         
            <li>
                <a <?php echo $page == 'mixtapes' ? 'class="selected"' : '' ?> href="<?php echo base_url();?>mixtapes" title="Mixtapes">Mixtapes</a>
            </li>

            <?php //if($_SERVER['HTTP_HOST'] == 'localhost'): ?>
            <li>
                <a <?php echo $page == 'chunes' ? 'class="selected"' : '' ?> href="<?php echo base_url();?>chunes" title="Chunes">Chunes</a>
            </li>
        	<?php //endif; ?>

            <li>
                <a <?php echo $page == 'editorial' ? 'class="selected"' : '' ?> href="<?php echo base_url();?>editorial">Editorials</a>
            </li>

	    </ul>

    </div>

    <?php $this->load->view('inc/modal.php'); ?>
