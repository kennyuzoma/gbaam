<?php
    if(isset($sidebarIndexModels)){

        $chal_id = $this->Video_model->get_latest_challenge();

        $get_side_videos = $this->Challenge_model->get_videos_by_cat($chal_id);

        $page = $chal_id;

    }
?>

<div id="gcback">

     
        <select class="sectitle" style="">
            <?php foreach($get_cats as $c): ?>
            <option value="<?php echo base_url() . 'challenge/cat/'. $c->id; ?>" <?php echo $page == $c->id ? 'selected="selected"' : '' ?>>
                <?php echo $c->display_name; ?>
            </option>
            <?php endforeach; ?>
        </select>
       
 <? /*  
        <ul class="gclist">

            <ul class="section">

            <?php foreach($get_side_videos as $g): ?>
                <li>
                    <a title="<?php echo $g->title; ?>"  href="<?php echo base_url(); ?>challenge/view/<?php echo $g->id; ?>">
                        <?php if($g->thumb == ''){ ?>
                            <img style="height:80px;width:120px;margin-right:10px;" src="<?php echo base_url(); ?>thumbs/Gbaam3.jpg">
                        <?php } else { ?>
                            <img style="height:80px;width:120px;margin-right:10px;" src="<?php echo base_url(); ?>thumbs/<?php echo $g->thumb;?>" />
                        <?php } ?>
                       <?php echo $g->title; ?>
                    </a>
                </li>
            <?php endforeach; ?>

            </ul>

        </ul> 
*/ ?>
        <?php /*
        <ul class="gclist">
            <?php 
                $b = $this->Challenge_model->scores($cat);
                foreach($b as $k=>$v):
                    foreach($this->Challenge_model->get_video($k) as $g):
                        if($this->Challenge_model->get_vote_count($g->id) > 9)
                        {
                        
                    ?>
            
              <li>
                    <a title="<?php echo $g->title; ?>" href="<?php echo base_url(); ?>challenge/view/<?php echo $g->id; ?>">
                        <?php if($g->thumb == ''){ ?>
                            <img style="height:80px;width:120px;" src="<?php echo base_url(); ?>thumbs/Gbaam3.jpg">
                        <?php } else { ?>
                            <img style="height:80px;width:120px;" src="<?php echo base_url(); ?>thumbs/<?php echo $g->thumb;?>" />
                        <?php } ?>
                        <?php echo character_limiter($g->title,30); ?>
                    </a>
                </li>
            
            <?php 
                        }
                    endforeach;
                endforeach; 
            ?>

            </ul>
            <ul class="snailplace">
                <li>1st place</li>
                <li>2nd place</li>
                <li>3rd place</li>
                <li>4th place</li>
                <li>5th place</li>
            </ul>  */ ?>
    
     <span class="title">
        Leaderboard
        
    </span>
     <ul class="gclist_place">

                <?php 
                $i = 1;

                $num = array(
                    1 => array(
                            'ord' => 'st',
                            'full' => 'first'
                        ),
                    2 => array(
                            'ord' => 'nd',
                            'full' => 'second'
                        ),
                    3 => array(
                            'ord' => 'rd',
                            'full' => 'third'
                        ),
                    4 => array(
                            'ord' => 'th',
                            'full' => 'fourth'
                        ),
                    5 => array(
                            'ord' => 'th',
                            'full' => 'fifth'
                        )

                );

                $b = $this->Challenge_model->scores($cat);
                if($b == FALSE)
                {
              
                    for ($i=$i; $i <= 5; $i++) { 
                        ?>
                            <li>
                        <span class="place <?php echo $num[$i]['full']; ?>"><?php echo $i.$num[$i]['ord']; ?></span>
                        <?php /*<a title="Azonto Dance From Ghana"  href="http://gbaam.com/challenge/view/184">*/ ?>
                            <img alt="This spot has not been earned yet." class="img" style="margin-right:10px;" src="http://gbaam.com/assets/img/Gbaam_Experience.png" />
                            <span class="text" style="float:left;width:110px;margin-top:15px;margin-left:5px;font-size:13px;line-height:17px;font-style:italic;color:#ddd;">
                                This spot has not been earned yet.
                            </span>
                        <?php /*
                            Holder
                        </a> */ ?>
                    </li>
                        <?
                    }
                }
                else
                {

                    foreach($b as $k=>$v):
                        foreach($this->Challenge_model->get_video($k) as $g):
                            if($this->Challenge_model->get_vote_count($g->id) < 9)
                            {
                                // set the num to 0 because there was a vote registered but we will only show if there are more then 9 votes
                                $the_num = 0;
                            }
                            else
                            {
                                $the_num = $i++
                            
                        ?>
                    <li class="">
                        <span class="place <?php echo $num[$the_num]['full']; ?>"><?php echo $the_num.$num[$the_num]['ord']; ?></span>

                        <a title="<?php echo $g->title; ?>" href="<?php echo base_url(); ?>challenge/view/<?php echo $g->id; ?>">
                            <?php if($g->thumb == ''){ ?>
                                <img alt="<?php echo $g->title; ?>"class="img" src="<?php echo base_url(); ?>assets/img/Gbaam_Experience.png">
                            <?php } else { ?>
                                <img alt="<?php echo $g->title; ?>" class="img" src="<?php echo base_url(); ?>thumbs/<?php echo $g->thumb;?>" />
                            <?php } ?>
                            <span class="text" style="float:left;width:100px;margin-top:15px;margin-left:10px;font-size:14px;line-height:17px;">
                                <?php echo character_limiter($g->title,30); ?>
                            </span>
                        </a>
                    </li>

                    <?php 
                            }
                        endforeach;
                    endforeach; 

                    // add plus one sine we are referencing the number
                    for ($i=$the_num+1; $i <= 5; $i++) { 
                        ?>
                            <li>
                        <span class="place <?php echo $num[$i]['full']; ?>"><?php echo $i.$num[$i]['ord']; ?></span>
                            <img alt="This spot has not been earned yet." class="img" style="margin-right:10px;" src="http://gbaam.com/assets/img/Gbaam_Experience.png" />
                            <span class="text" style="float:left;width:110px;margin-top:15px;margin-left:5px;font-size:13px;line-height:17px;font-style:italic;color:#ddd;">
                                This spot has not been earned yet.
                            </span>
                    </li>
                        <?
                    }

                }
            ?>

                
                
            

        </ul>   
        <div class="fb" style="float:left;width:270px;margin-top:5px;">
            
                <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fpages%2FGbaam%2F300011193363037&amp;width=275&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=false&amp;header=false" scrolling="No" frameborder="0" style="border:none; overflow:hidden; width:275px; height:258px;background:white;" allowtransparency="true"></iframe>
        </div>

    </div>