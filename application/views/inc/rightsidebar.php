<?php
	$seg1 = $this->uri->segment(1); 
	$seg2 = $this->uri->segment(2); 
?>

<div class="rightside" style="height:auto;">
	
	<?php if(is_localhost()): ?>
	
		<?php 
			// show by default.
			$show_sub_box = 1;

			if(is_loggedin())
			{
				$user_sub_status = $this->User_model->get_user_setting('email_gbaam_news');

				if($user_sub_status == '0') 
					$show_sub_box = 1;

				elseif($user_sub_status == '1')
					$show_sub_box = 0;
			}

			if($show_sub_box == 1):
		?>
	
	<div class="subscribe">
		<div class="top">
			<img src="<?php echo $this->config->item('logo_url'); ?>" class="logo" />
			<h3>Newsletter</h3>
		</div>
		<p>Get weekly updates on the latest Music Videos, Editorials, Mixtapes, Songs, and giveaways!</p>
		
		<?php if(!is_loggedin()): ?>
		<form method="POST" action="<?php echo base_url(); ?>main/subscribe">
			<input type="text" name="name" class="inptxt" placeholder="Your Name">
			<br>
			<input type="text" name="email" class="inptxt" placeholder="Email Address">
			<br>
			<input type="submit" value="Subscribe" name="submit" class="textshad submit">
		</form>
		<?php else: ?>
		<a href="<?php echo base_url(); ?>main/subscribe/button" class="textshad submit">Subscribe</a>
		<?php endif; ?>
	</div>

		<?php endif; ?>

	<?php endif; ?>

	<?php $this->load->view('inc/google_250'); ?>
	
	<?php 
		if($seg1 != 'editorial'):
			if(($seg1 == 'mixtapes') && (is_numeric($seg2) || ($seg2 == 'all'))){

			}else{ /*
	?>
	<span class="gbsmall">
    	<a href="<?php echo base_url(); ?>challenge"><img alt="The Gbaam Challenge" width="250" src="http://gbaam.com/assets/img/Gbaam_Experience.png" /></a>
    </span>
	<?php */	
			}
		endif; 
	?>




	<?php 
		if(($seg1 == 'mixtapes') && (is_numeric($seg2) || ($seg2 == 'all'))): 
		$hide = TRUE;
	?>
	<div class="fb">

   		<div class="abox">
    		<span class="head">Hot Mixtapes</span>
    		<ul>
	      		<?php
	                for ($i=1; $i <= 3; $i++):
	                $gm = $this->Mixtape_model->getHotMixtape_by_slot($i);

	            		if($this->Mixtape_model->getHotMixtape_count($i) == 0)
                			continue;
	            ?>

            	<li>
	                <a href="<?php echo base_url(); ?>mixtapes/<?php echo $gm->id.'/'.$gm->permalink; ?>">
	                    <img  alt="<?php echo $gm->title; ?>" width="30" height="30" src="<?php echo get_image('mixtape',$gm->front_cover,$gm->img_hash,'50'); ?>" />
	                      <span class="title2"><?php echo character_limiter($gm->title,30); ?></span>
	                </a>
            	</li>

	            <?php 
	                endfor;
	            ?>
          	</ul>
        </div>

        <div class="abox">
		    <span class="head">Latest Mixtapes</span>
		    <ul>
		      <?php 
	            $mix  = $this->Mixtape_model->getAllMixtapes_2013(5);
	            foreach($mix['result'] as $g): ?>

	              <li>
	                <a href="<?php echo base_url(); ?>mixtapes/<?php echo $g->id.'/'.$g->permalink; ?>">
	                    <img alt="<?php echo $g->title; ?>" width="30" height="30" src="<?php echo get_image('mixtape',$g->front_cover,$g->img_hash,'50'); ?>" />
	                      <span class="title2"><?php echo character_limiter($g->title,30); ?></span>
	                </a>
	            </li>

	              <?php endforeach; ?>
	            <li class="more"><a href="<?php echo base_url(); ?>mixtapes/all">View All Mixtapes</a></li>
		    </ul>
        </div>

    </div>
    <?php endif; ?>




	<?php if($seg1 == 'editorial'): ?>
	<div class="fb">

    	<div class="abox">
			<span class="head">Featured Editorials</span>
			<ul>
			
            	<?php
            		for ($i=1; $i <= 3; $i++):
            		$g = $this->Site_model->getFeatArticle_slot('article',$i);
            	
            			if($this->Site_model->getFeatArticle_slot_count('article',$i) == 0)
                			continue;
        		?>

		        <li>
		            <a href="<?php echo base_url(). 'editorial/'.$g->permalink; ?>">
		                <img  width="30" height="30" src="<?php echo get_image('article',$g->image,'','60'); ?>" alt="<?php echo $g->title; ?>" alt="<?php echo $g->title; ?>" />
		                    <span class="title2"><?php echo character_limiter($g->title,50); ?></span>
		            </a>
		        </li>

		        <?php 
		            endfor;
		        ?>
        	</ul>
      	</div>

      	<div class="abox">
			<span class="head">Latest Editorials</span>
			<ul>
				<?php foreach($this->Site_model->getArticles(5) as $g): ?>

	            <li>
	                <a href="<?php echo base_url(). 'editorial/'.$g->permalink; ?>">
	                    <img alt="<?php echo $g->title; ?>" width="30" height="30" src="<?php echo get_image('article',$g->image,'','60'); ?>" alt="<?php echo $g->title; ?>" />
	                    <span class="title2"><?php echo character_limiter($g->title,50); ?></span>
	                </a>
	            </li>

	            <?php endforeach; ?>
	            <?php if($this->Site_model->getArticlesCount() > 5): ?>
	            <li class="more"><a href="<?php echo base_url(); ?>editorial">View All Editorials</a></li>
	        	<?php endif; ?>
	        </ul>
      </div>
  	</div>
  	<?php endif; ?>

	<?php if($_SERVER['HTTP_HOST'] != 'localhost'): ?>
  	<div class="fb">
        <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fpages%2FGbaam%2F300011193363037&amp;width=270&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=false&amp;header=false" scrolling="No" frameborder="0" style="border:none; overflow:hidden; width:250px; height:258px;margin:10px;" allowtransparency="true"></iframe>
    </div>
	<?php endif; ?>

    <?php $this->load->view('inc/google_250'); ?>

</div>