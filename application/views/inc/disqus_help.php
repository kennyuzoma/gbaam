<?php
			
// disqus stuff
$data['id'] = $this->session->userdata('user_id');
$data['username'] = $this->session->userdata('username');
$data['email'] = $this->session->userdata('email');

if($data['username'] == '')
	$data['url'] = base_url().'profile/'.$data['id'];
else
	$data['url'] = base_url().'profile/'.$data['username'];

if($data['username'] == '')
{
	if($this->session->userdata('provider') == 'facebook')
	{
		$ud = $this->facebook->api('/me');
		$data['username'] = $ud['name'];
		$data['email'] = $ud['email'];
	}
	if($this->session->userdata('provider') == 'twitter')
	{
		$twiter_user = $this->User_model->get_user($this->session->userdata('user_id'));
		$data['username'] = $twiter_user->display_name;
	}
}


if($this->session->userdata('logged_in') == TRUE):

    $my_user_id = $this->session->userdata('user_id');
    $u = $this->User_model->get_user($my_user_id);

    /// if profile pic is blank
    if($u->profile_img == '')
        $profile_img = base_url().'assets/img/default.png';
    else
    {
    	if($this->session->userdata('provider') == 'facebook')
    	{
    		$profile_img = 'http://graph.facebook.com/'.$u->facebook_id.'/picture?type=large';
    	}
    	else //twitter
    	{
    		$profile_img = get_profile_image('user',$u->profile_img,$u->img_hash,'200');
    	}
    	
    }
        

    $data['avatar'] = $profile_img;

endif;
 
function dsq_hmacsha1($data, $key) {
    $blocksize=64;
    $hashfunc='sha1';
    if (strlen($key)>$blocksize)
        $key=pack('H*', $hashfunc($key));
    $key=str_pad($key,$blocksize,chr(0x00));
    $ipad=str_repeat(chr(0x36),$blocksize);
    $opad=str_repeat(chr(0x5c),$blocksize);
    $hmac = pack(
                'H*',$hashfunc(
                    ($key^$opad).pack(
                        'H*',$hashfunc(
                            ($key^$ipad).$data
                        )
                    )
                )
            );
    return bin2hex($hmac);
}
 
$message = base64_encode(json_encode($data));
$timestamp = time();
$hmac = dsq_hmacsha1($message . ' ' . $timestamp, $this->config->item('DISQUS_SECRET_KEY'));
?>
<?php
	if($_SERVER['HTTP_HOST'] != 'localhost'):
?>
			<script type="text/javascript">
			var disqus_config = function() {
			    this.page.remote_auth_s3 = "<?php echo "$message $hmac $timestamp"; ?>";
			    this.page.api_key = "<?php echo $this->config->item('DISQUS_PUBLIC_KEY'); ?>";

			    this.sso = {
			          name:   "Gbaam",
			          button:  "<?php echo base_url(); ?>assets/img/gbaam-sso-login-button.png",
			          icon:    "<?php echo base_url();?>assets/img/favicon.ico",
			          url:     "<?php echo base_url();?>main/jslogin/",
			          logout:  "<?php echo base_url();?>main/logout?next=<?php echo uri_string(); ?>",
			          width:   "800",
			          height:  "400"
			    };
			}
			</script>

			<script type="text/javascript">
			    /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
			    var disqus_shortname = 'gbaam'; 
				var disqus_identifier = '<?php echo $my_identifier; ?>';
				var disqus_url = '<?php echo $disqus_url; ?>';
				var disqus_disable_mobile = false;
			    /* * * DON'T EDIT BELOW THIS LINE * * */
			    (function() {
			    	$(window).bind("load", function() {
			        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = false;
			        dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
			        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
			        });
			    })();
			</script>
<?php 
	endif;
?>