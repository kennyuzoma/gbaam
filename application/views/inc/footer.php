    <?php
		$page = $this->uri->segment(1);
		$page2 = $this->uri->segment(2);
	?>
    <div id="footer" 
    <?php
        if( isset($css) )
            echo 'style="'.$css.'"';
    ?>
    >
        <span style="float:left;width:100%;text-align:center;">A <?php /*<a href="http://peppdigital.com" target="_blank">*/ ?>Pepp Digital Media<?php /*</a>*/ ?> Company.</span>
        <br><br>
        <ul id="navbottom">

            <li><a <?php echo $page == '' ? 'class="selected"' : '' ?> href="<?php echo base_url(); ?>">Home</a></li>
            <li><a <?php echo $page == 'about' ? 'class="selected"' : '' ?> href="<?php echo base_url();?>about">About</a></li>
           <?php /*
            <li><a href="<?php echo base_url();?>video">Music Videos</a></li>
            <li><a href="<?php echo base_url();?>gbaamtv">GbaamTV</a></li>
            <li><a href="<?php echo base_url();?>challenge">Challenge</a></li>
            <li><a href="<?php echo base_url();?>mixtapes">Mixtapes</a></li>
            <li><a href="<?php echo base_url();?>editorial">Editorials</a></li>
            */ ?>
            <li><a href="http://blog.gbaam.com">Blog</a></li>

            <li><a <?php echo $page == 'contact' ? 'class="selected"' : '' ?> href="<?php echo base_url();?>contact">Contact Us</a></li>

            <li><a <?php echo ($page == 'writeforus' || $page == 'writer-guidelines') ? 'class="selected"' : '' ?> href="<?php echo base_url();?>writeforus">Write For Us</a></li>

            <li><a <?php echo $page == 'advertise' ? 'class="selected"' : '' ?> href="<?php echo base_url();?>advertise">Advertise With Us</a></li>

            <li><a <?php echo $page == 'terms' ? 'class="selected"' : '' ?> href="<?php echo base_url();?>main/terms">Terms</a></li>
            
            <li><a <?php echo $page == 'privacy' ? 'class="selected"' : '' ?> href="<?php echo base_url();?>main/privacy">Privacy</a></li>

            <?php
            /*
            Future
            ------

            Help
            Resources
            Safety

             */
            ?>
            
            <? /*<li><a href="<?php echo base_url();?>audio">Audio</a></li>
            <li><a href="<?php echo base_url();?>artists">Artists</a></li>*/ ?>
            <? /*<li><a href="<?php echo base_url();?>store">Store</a></li>*/ ?>
            
        </ul>
        <div id="lastline">
            &copy; <?php echo date('Y'); ?> Gbaam. All rights reserved.
        </div>

    </div>

    <div style="clear:both;"></div>
</div>

<?php

    $js_files = array(
        'gbaam.js',
        'js-image-slider.js',
        'jquery.tipsy.js',
        'jquery.simplemodal.js',
        'jquery.lazyload.min.js',
        'jquery.fancybox.pack.js',
        'jquery.validate.js',
        'readmore.min.js'/*,

        // mixtape
        'swfobject.js',
        'jquery.apPlaylistManager.min.js',
        'jquery.apTextScroller.min.js',
        'jquery.html5audio.min.js',
        'jquery.html5audio.func.js',
        'jquery.html5audio.settings_playlist_selector.js'*/
        
    );

    $this->minify->js($js_files); 
     
    echo $this->minify->deploy_js(true); 

?>

<script type='text/javascript'>
	var rotation_speed = 500;

    $.fn.tipsy.defaults = {
        delayIn: 0,      // delay before showing tooltip (ms)
        delayOut: 0,     // delay before hiding tooltip (ms)
        fade: false,     // fade tooltips in/out?
        fallback: '',    // fallback text to use when no tooltip text
        gravity: 'n',    // gravity
        html: false,     // is tooltip content HTML?
        live: false,     // use live event support?
        offset: 0,       // pixel offset of tooltip from element
        opacity: 1.0,    // opacity of tooltip
        title: 'title',  // attribute/callback containing tooltip text
        trigger: 'hover' // how tooltip is triggered - hover | focus | manual
    };
  $(function() {
    
  	$('.js_fb_click').on('click',function(){

		$(this).hide();
		$('.js_fb_cont').append('<span style="float:left;width:100%;text-align:Center"><img style="margin-right:5px;" src="http://gbaam.com/assets/packs/gbaamadmin/images/loaders/loader.gif" /><span style="font-size:14px;">Logging In to Facebook...</span></span>');
	});

	$('.js_twitter_click').on('click',function(){

		$(this).hide();
		$('.js_twitter_cont').append('<span style="float:left;width:100%;text-align:Center"><img style="margin-right:5px;" src="http://gbaam.com/assets/packs/gbaamadmin/images/loaders/loader.gif" /><span style="font-size:14px;">Logging In to Twitter...</span></span>');
	});


    $('.toptwitter').tipsy({gravity: 'n'});

    $('.topfb').tipsy({gravity: 's'});

    $('.south').tipsy({gravity: 's'});

    
    $('.act').tipsy({gravity: 'n'});

    $('.act').on('click', function(){

		$('.act').data('tooltipsy').hide();
	});

    $('.authorbio .social li img').tipsy({gravity: 'n'});

    $("img.lazy").lazyload({
        effect : "fadeIn",
        placeholder: "data:image/jpg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMtaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6N0JCQkZFREQ3QzlGMTFFM0EzOUFBMUFFMkJCMDlCOTMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6N0JCQkZFREU3QzlGMTFFM0EzOUFBMUFFMkJCMDlCOTMiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDowQ0ZFMjE3RTdDOTQxMUUzQTM5QUExQUUyQkIwOUI5MyIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo3QkJCRkVEQzdDOUYxMUUzQTM5QUExQUUyQkIwOUI5MyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pv/uAA5BZG9iZQBkwAAAAAH/2wCEAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQECAgICAgICAgICAgMDAwMDAwMDAwMBAQEBAQEBAgEBAgICAQICAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDA//AABEIAAEAAQMBEQACEQEDEQH/xABKAAEAAAAAAAAAAAAAAAAAAAAKAQEAAAAAAAAAAAAAAAAAAAAAEAEAAAAAAAAAAAAAAAAAAAAAEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwAQ4P/Z",
        threshold : 200
    });
});
</script>




