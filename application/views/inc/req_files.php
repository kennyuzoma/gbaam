<?php
	// get the segments
	$seg1 = $this->uri->segment(1);
	$seg2 = $this->uri->segment(2);

	if(!isset($page_type))
		$page_type = '';
?>
<title>

	<?php 
	if(!$seg1)
		echo "Gbaam - ".$this->config->item('site_slogan');
	
	elseif(!isset($title))
		echo "Gbaam";
	
	elseif(isset($title))
	{
		if($seg1 == 'gbaamtv' && $seg2 != '')
			echo $title." | GbaamTV";
		
		else
			echo $title." | Gbaam";
	}
	?>

</title>

<?php /*<link href='http://fonts.googleapis.com/css?family=Open+Sans:600,400' rel='stylesheet' type='text/css'>*/ ?>

<?php
	$meta_title = $title;

	if($title == '')
		$title = "Gbaam - ".$this->config->item('site_slogan');

	if($seg1 == 'gbaamtv' && $seg2 != '')
		$meta_title = $title.' | GbaamTV';

	$meta_url = base_url().$this->uri->uri_string();
	$meta_image = base_url().'assets/img/gbaamimagelg.jpg';
	$meta_keywords = $this->config->item('meta_keywords');
	
	$fb_type = 'website';
	$tw_type = 'summary';

	if($page_type == 'video')
	{
		$meta_desc = htmlentities(strip_tags(character_limiter(preg_replace('/\s+/', ' ', $video_desc),300)));
		$meta_image = youtube_meta_image($videoid); //hardcoded url from the controller
		$meta_vid_src = $this->config->item('player_url').'video/embed/'.$videoid;
		//$fb_type = 'video'; // hide for now untill we find a html5 flash player

		// $tw_type = 'player';  // need to wait for a https url
	}
	elseif($page_type == 'mixtape')
	{
		$meta_image = get_image('mixtape',$front_cover,$img_hash,'');
		$meta_desc = htmlentities(strip_tags(character_limiter(preg_replace('/\s+/', ' ', $desc),300)));
		$tw_type = 'summary_large_image';	

	}
	elseif($page_type == 'song')
	{
		$meta_image = get_image('song',$front_cover,$img_hash,'');
		$meta_desc = htmlentities(strip_tags(character_limiter(preg_replace('/\s+/', ' ', $desc),300)));
		$tw_type = 'summary_large_image';	

	}
	elseif($page_type == 'article')
	{
		$meta_image = get_image('article',$image,'','');
		$meta_desc = htmlentities(strip_tags(character_limiter(preg_replace('/\s+/', ' ', $body),300)));
		$meta_published_time = conv_date($date);
		$fb_type = 'article';
	}



?>


<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="<?php echo $meta_desc; ?>" />
<meta name="keywords" itemprop="keywords" content="<?php echo $meta_keywords; ?>" />


<meta property="og:site_name" content="Gbaam">
<meta property="og:url" content="<?php echo $meta_url; ?>">
<meta property="og:title" content="<?php echo $meta_title; ?>">
<meta property="og:image" content="<?php echo $meta_image; ?>">
<meta property="og:description" content="<?php echo $meta_desc; ?>">
<meta property="og:type" content="<?php echo $fb_type; ?>">

<?php /*if($page_type == 'video'): ?>

<meta property="og:video" content="<?php echo $meta_vid_src; ?>">
<meta property="og:video:type" content="text/html">
<meta property="og:video:width" content="1280">
<meta property="og:video:height" content="720">

<?php endif; */ ?>

<?php if($page_type == 'article'): ?>

<meta name="Author" content="<?php echo $author; ?>" />
<meta property="article:published_time" content="<?php echo $meta_published_time; ?>">

<?php endif; ?>

<meta property="fb:app_id" content="248373775244670">

<meta name="twitter:card" content="<?php echo $tw_type; ?>">
<meta name="twitter:site" content="@<?php echo $this->config->item('twitter_acct'); ?>">
<meta name="twitter:url" content="<?php echo $meta_url; ?>">
<meta name="twitter:description" content="<?php echo $meta_desc; ?>">

<?php if($page_type == 'mixtape'): ?>
<meta name="twitter:image:src" content="<?php echo $meta_image; ?>">
<?php else: ?>
<meta name="twitter:image" content="<?php echo $meta_image; ?>">
<?php endif; ?>



<?php 

// Not yet
// // Only for players
// Needs to be an HTTPS url
	
/*

<meta name="twitter:player" content="https://www.youtube.com/embed/AGlKJ8CHAg4">
<meta name="twitter:player:width" content="1280">
<meta name="twitter:player:height" content="720">

<meta name="twitter:app:name:iphone" content="YouTube">
<meta name="twitter:app:id:iphone" content="544007664">
<meta name="twitter:app:name:ipad" content="YouTube">
<meta name="twitter:app:id:ipad" content="544007664">
<meta name="twitter:app:url:iphone" content="vnd.youtube://watch/AGlKJ8CHAg4">
<meta name="twitter:app:url:ipad" content="vnd.youtube://watch/AGlKJ8CHAg4">
<meta name="twitter:app:name:googleplay" content="YouTube">
<meta name="twitter:app:id:googleplay" content="com.google.android.youtube">
<meta name="twitter:app:url:googleplay" content="http://www.youtube.com/watch?v=AGlKJ8CHAg4">

*/
?>

<?php

// add css files
$css_files = array(
	'reset.css',
	'style.css',
	'basic.css',
	'tipsy.css',
	'js-image-slider.css',

	// from the mixtape page
	'jquery.fancybox.css',
	'jquery.selectbox.css',
	'html5audio_default.css',
	'html5audio_playlist_selector.css',
	'admin-bar-min.css'
);

$this->minify->css($css_files); 

// echo the link rel
echo $this->minify->deploy_css(false); 


?>

<link rel="icon" type="image/x-icon" href="<?php echo base_url();?>assets/img/favicon.ico" />

<?php if(!isset($pagi_links)): ?>
<meta name="robots" content="index, follow"/>
<link rel="canonical" href="<?php echo base_url($this->uri->uri_string()); ?>" />
<?php endif; ?>

<script type="text/javascript">
  var base_url = "<?php print base_url(); ?>";
</script>

<script type="text/javascript" src="<?php echo $this->config->item('jquery_file'); ?>" ></script>

<?php if(($seg1 = 'video') && ($seg2 != '')) : ?>
<script type="text/javascript" src="<?php echo $this->config->item('jqueryui_file'); ?>" ></script>
<?php endif; ?>



