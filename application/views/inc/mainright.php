<div id="mainright">

	<ul>
		<li><a href="http://blog.gbaam.com">Blog</a></li>

		<li><a href="<?php echo base_url(); ?>about" <?php if($page == 'about') echo 'class="sel"'; ?>>About Us</a></li>
<?php /*
		<li><a href="<?php echo base_url(); ?>main/advertise" <?php if($page == 'advertise') echo 'class="sel"'; ?>>Advertise with Us</a></li>
		*/ ?>

		<li><a href="<?php echo base_url(); ?>contact" <?php if($page == 'contact') echo 'class="sel"'; ?>>Contact Us</a></li>

		<li><a href="<?php echo base_url(); ?>main/terms" <?php if($page == 'terms') echo 'class="sel"'; ?>>Terms</a></li>
		
		<li><a href="<?php echo base_url(); ?>main/privacy" <?php if($page == 'privacy') echo 'class="sel"'; ?>>Privacy</a></li>

		<li><a href="<?php echo base_url(); ?>main/video-agreement" <?php if($page == 'vsa') echo 'class="sel"'; ?>>Submission Agreement</a></li>
	</ul>

</div>