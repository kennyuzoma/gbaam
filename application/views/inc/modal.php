<?php

$next = '';

// if this was a login from another page calling this modal,
// set the uri string to be that page..
if(uri_string() != NULL)
	$_GET['next'] = uri_string();

if(isset($_GET['next']))
	$next = '?next=' . $_GET['next'];
 
?>

<div id="basic-modal-content" class="modalbox" style="height:300px;">

	<h3>Log In to Gbaam</h3>
	<div class="social">
		<span class="js_fb_cont" style="float:left;width:100%;margin-bottom:10px;">
			<a href="<?php echo base_url() . 'oauth/login/facebook'.$next; ?>" class="js_fb_click">
				<img alt="Connect with Facebook" style="margin-left:10px;float:left;width:auto;" src="<?php echo base_url(); ?>assets/img/Small_and_Long_200x24.png" />
			</a>
		</span>

		<br>

		<span class="js_twitter_cont" style="float:left;width:100%;">
			<a href="<?php echo base_url().'oauth/login/twitter'.$next; ?>" class="js_twitter_click">
				<img alt="Connect with Twitter" style="float:left;margin-left:30px;width:auto;margin-top:5px " src="<?php echo base_url(); ?>assets/img/sign-in-with-twitter-gray.png">
			</a>
		</span>
	</div>
	<form class="lbox" method="POST" action="<?php echo base_url() . 'main/login'.$next; ?>">

		<input type="hidden" name="next" value="<?php echo uri_string(); ?>" />

		<span class="wrong wrong_login"></span>

		<div class="row">
			<label for="username">Email or Username</label>
			<Br>
				<input type="text" name="username" class="lusername" />
			</div>

			<div class="row">
				<label for="password">Password</label>
				<Br>
					<input type="password" name="password" class="lpass" />
				</div>

				<div class="row">
					<input type="submit" name="login" value="Login" class="loginbutton" />
				</div>

				<div class="row">
					<a href="<?php echo base_url(); ?>main/forgotpassword">Forgot Password?</a>
				</div>

			</form>


		</div>

		<!-- preload the images -->
		<div style='display:none'>
			<img src='<?php echo base_url(); ?>assets/img/x.png' alt='Close' />
		</div>


		<?php

//
//
// Sign Up
// 
// 

		?>
		<div id="basic-modal-content_signup" class="modalbox" >

			<?php /* <h3>Sign Up with Gbaam</h3> */ ?>
			<div class="social">
				<span class="subhead orange">Sign Up with...</span>
				<span class="js_fb_cont" style="float:left;width:100%;margin-bottom:10px;">
					<a href="<?php echo base_url() . 'oauth/login/facebook' . $next; ?>" class="js_fb_click">
						<img alt="Connect with Facebook" style="margin-left:10px;float:left;width:auto;" src="<?php echo base_url(); ?>assets/img/Small_and_Long_200x24.png" />
					</a>
				</span>

				<br>

				<span class="js_twitter_cont" style="float:left;width:100%;">
					<a href="<?php echo base_url().'oauth/login/twitter' . $next; ?>" class="js_twitter_click">
						<img alt="Connect with Twitter" style="float:left;margin-left:30px;width:auto;margin-top:5px " src="<?php echo base_url(); ?>assets/img/sign-in-with-twitter-gray.png">
					</a>
				</span>
			</div>
			<form class="lbox" method="POST" action="<?php echo base_url(); ?>main/signup" autocomplete="off">

				<input type="hidden" name="next" value="<?php echo uri_string(); ?>" />

				<span class="subhead orange">...or Register!</span>

				<span class="wrong wrong_signup"></span>
				
				<input name="s_holder" type="text" class="theholder" />
				
				<div class="row">
					<label for="s_name">Name</label>
					<Br>
					<input type="text" name="s_name" />
				</div>


				<div class="row">
					<label for="s_username">Username</label>
					<Br>
					<input type="text" name="s_username" />
				</div>

				<div class="row">
					<label for="s_email">Email</label>
					<Br>
					<input type="text" name="s_email" />
				</div>

				<div class="row">
					<label for="s_password">Password</label>
					<Br>
					<input type="password" name="s_password" />
				</div>
				<div class="row">
					<span class="text">By creating an account, I accept Gbaam's 
					<br>
					<a href="<?php echo base_url(); ?>main/terms">Terms of Service</a> and <a href="<?php echo base_url(); ?>main/privacy">Privacy Policy</a>.</span>
				</div>
				<div class="row">
					<input type="submit" name="signup" value="Sign Up" class="submitbutton" />
				</div>

			</form>


		</div>

						<!-- preload the images -->
						<div style='display:none'>
							<img src='<?php echo base_url(); ?>assets/img/x.png' alt='Close' />
						</div>