<?php 
	$type = $this->uri->segment(2);

	if($type == '')
		$type = 'home';

	// get the user
	$user_id = $this->session->userdata('user_id');
	$user = $this->User_model->get_user($user_id);

	// turn the user settings into variables.
	extract($this->User_model->get_all_user_settings($user_id));

?>

<!DOCTYPE html>
<html>
<head>

	<?php 
	//Load the reqiured Files View
	echo $this->load->view('inc/req_files'); 
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/settings.css">
	<script type="text/javascript">
		$(function(){

			$('.mini_nav li').on('click', function(){

				var type = $(this).attr('id');

				$('.mini_nav li').siblings().removeClass('sel');
				$(this).addClass('sel');

				if(type == 'all')
					$('.content_boxes .row').show();
				else
				{
					$('.content_boxes .row').hide();
					$('.js_box_'+type).show();
				}

			});

			$('.js_remove_profilepic').on('click', function(){

				$.post( base_url+"ajax/js_remove_profilepic", function( ) {

					var default_photo = base_url+'assets/img/default.png';

					$('.profile_img, .proimg').attr('src', default_photo);
					//$( ".result" ).html( data );
				});

			});

			var timeoutReference;
			$('.js_username').keyup(function(){

				var unametext = $('.js_username_text');
				unametext.text('Checking username...');

				var username = $('.js_username').val();

				if (timeoutReference) clearTimeout(timeoutReference);
		        timeoutReference = setTimeout(function() {

		            $.ajax({  
					type: "GET",  
					url: base_url+"ajax/js_check_username?username="+username, 
					success: function(response) {
						if(response == 'empty')
						{
							unametext.html('Please type a username');
						}
						if (response == 'taken')
						{
							unametext.html('<b>'+username+'</b> is <span class="bad"><b>Unavailable</b></span>');
						}
						if(response == 'free')
						{
							unametext.html('<b>'+username+'</b> is <span class="good"><b>Available!</b></span>');
						}
					}
				});  

		        }, 500);
				
				return false;

			});

			$('.statusbox').delay(4000).fadeOut(500);

		});

	</script>


</head>

<body class="singleVideo">
	<?php echo $this->load->view('inc/admin_nav'); ?>
	<?php echo $this->load->view('inc/analytics'); ?>
	<div id="wrapper">

		<?php 
		//Load the reqiured header view
		echo $this->load->view('inc/header'); 
		?>

		<div class="vidmain">

			<div class="settingsmenu">
				<ul>
					<li>
						<a href="<?php echo base_url(); ?>settings" 
						<?php echo ($type == 'home' ? 'class="sel"' : ''); ?>
						>General Settings</a>
					</li>
					<li>
						<a href="<?php echo base_url(); ?>settings/password"
						<?php echo ($type == 'password' ? 'class="sel"' : ''); ?>
						>Password</a>
					</li>
					<li>
						<a href="<?php echo base_url(); ?>settings/notifications"
						<?php echo ($type == 'notifications' ? 'class="sel"' : ''); ?>
						>Notifications</a>
					</li>
					<li>
						<a href="<?php echo base_url(); ?>settings/social"
						<?php echo ($type == 'social' ? 'class="sel"' : ''); ?>
						>Social Networks</a>
					</li>
				</ul>
			</div>
			
			<div class="settingsbox">
				
				<h1>
					<span class="text"><?php echo $title; ?></span>
				</h1>
				
				<?php
	            if(isset($validation))
	            {
	            	if($validation == FALSE)
	                	echo '<div class="statusbox error">'.$message.'</div>';
	              	
	              	else
	              		echo '<div class="statusbox success">'.$message.'</div>';
	            }
	         	?>

				<form action="<?php echo base_url() . $this->uri->uri_string(); ?>" method="POST" enctype="multipart/form-data">
				
					<?php if($type == 'home'): ?>
						<?php /*
						<div class="row">
							<label for="firstname">First Name</label>
							<input type="text" name="firstname" class="jsname inptxt" value="<?php echo set_value('firstname', $user->firstname); ?>"/>
						</div>

						<div class="row">
							<label for="firstname">Last Name</label>
							<input type="text" name="lastname" class="jsname inptxt" value="<?php echo set_value('lastname', $user->lastname); ?>"/>
						</div>
						*/ ?>
						
						<div class="row">
							<label for="display_name">Profile Image</label>

							<div style="float:left;width:200px;text-align:center;">
								<img src="<?php echo get_profile_image('user',$user->profile_img,$user->img_hash,'200'); ?>" class="proimg" style="margin-bottom:10px;"/>

								<span class="js_remove_profilepic">Remove Profile Photo?</span>
							</div>

							<ul class="box">
								<li><input type="file" name="pro_pic" class="uploadphotobutton" value="Upload Photo" /></li>
								
								<?php if((isset($facebook_id) && $facebook_id != '') || (isset($twitter_id) && $twitter_id != '')): ?>
								<li> - OR - </li>
								<?php endif; ?>
								<?php if(isset($facebook_id) && $facebook_id != ''): ?>
								<li class="button" style="background:#48649F;">
									Import from <span style="font-weight:bold;">Facebook</span>
								</li>
								<?php endif; ?>
								<?php if(isset($twitter_id) && $twitter_id != ''): ?>
								<li class="button" style="background:#55ACEE;">
									Import from <span style="font-weight:bold;">Twitter</span>
								</li>
								<?php endif; ?>
							</ul>
						</div>

						<hr />

						<div class="row">
							<label for="display_name">Display Name</label>
							<input type="text" name="display_name" class="jsname inptxt" value="<?php echo set_value('display_name', isset($display_name) ? $display_name : ''); ?>"/>
						</div>
						<div class="row">
							<label for="firstname">Email Address</label>
							<input type="text" name="email" class="jsname inptxt" value="<?php echo set_value('email', $user->email); ?>"/>
						</div>

						<hr />


						
						<div class="row">
							<label for="firstname">Username</label>
							<input type="text" name="username" class="jsname js_username inptxt" value="<?php echo set_value('username', $user->username); ?>"/>
							<span class="desc js_username_text">
	                        	<?php if($user->username != ''): ?>
	                        	Your current username is <span class="good"><b><?php echo $user->username; ?>
								<?php endif; ?>
	                        	</b></span>
							</span>
						</div>

						<hr />

						<div class="row">
							<label for="firstname">Bio</label>
							<textarea name="bio" class="jsname inptxt"><?php echo set_value('bio', isset($bio) ? $bio : ''); ?></textarea>
						</div>
					<?php endif; ?>




					<?php if($type == 'password'): ?>
						<div class="row">
							<label for="password1">Current Password</label>
							<input type="password" name="password1" class="jsname inptxt" />
						</div>

						<div class="row">
							<label for="password2">New Password</label>
							<input type="password" name="password2" class="jsname inptxt" />
						</div>
					<?php endif; ?>



					<?php if($type == 'notifications'): ?>
						<?php /*<div class="row check">
							<legend>Email me when</legend>
							<div class="cont">
								<div class="checkrow">
									<input type="checkbox" id="test" name="test" class="jscheck" />
									<label name="test">My Tweets are Popping</label>
								</div>
								<div class="checkrow">
									<input type="checkbox" name="test" class="jscheck" />
									<label name="test">My Tweets are Popping</label>
								</div>
							</div>
						</div>

						<hr />
						*/ ?>
						<div class="row check">
							<legend>Gbaam News</legend>
							<div class="cont">
								<div class="checkrow">
									<input type="hidden" name="user[email_gbaam_news]" value="0" />
									<input type="checkbox" id="test2" name="user[email_gbaam_news]" class="jscheck" value="1"
									<?php echo (isset($email_gbaam_news) && $email_gbaam_news == '1') ? 'checked="checked"' : ''; ?> />
									
									<label for="test2">Email Me about GBaam News.</label>
								</div>

							</div>
						</div>
					<?php endif; ?>







					<?php if($type == 'social'): ?>
						<div class="row">
							<label for="password1">Facebook:</label>
							<?php if(isset($facebook_id) && $facebook_id != ''): ?>
								<span class="desc" style="margin-top:8px;float:left;"><b class="good">
								CONNECTED</b> as <?php echo $this->User_model->get_facebook_name($facebook_id); ?>
								<?php
									if((isset($twitter_id) || ($user->email != '' && $user->pass != ''))):
								?>
								 - <a href="#" style="font-size:13px;">(Disconnect)</a>
								 <?php
								 	endif;
								 ?>
								</span>
							<?php else: ?>
							<a href="#">
								<img alt="Connect with Facebook" style="margin-left:10px;float:left;width:auto;" src="<?php echo base_url(); ?>assets/img/Small_and_Long_200x24.png" />
							</a>
							<?php endif; ?>
						</div>

						<div class="row">


							<label for="password1">Twitter:</label>

							<?php if(isset($twitter_id) && $twitter_id != ''): ?>
								<span class="desc" style="margin-top:8px;float:left;"><b class="good">
								CONNECTED</b> as <?php echo $this->User_model->get_twitter_screen_name($twitter_id); ?>
								

								<?php
									if((isset($facebook_id) || ($user->email != '' && $user->pass != ''))): 
								?>
								 - <a href="#" style="font-size:13px;">(Disconnect)</a>
								<?php 
									endif;
								?>

								</span>
							<?php else: ?>
								<a href="#">
									<img alt="Connect with Twitter" style="float:left;margin-left:30px;width:auto;margin-top:5px " src="<?php echo base_url(); ?>assets/img/sign-in-with-twitter-gray.png">
								</a>
							<?php endif; ?>

						

						</div>
					<?php endif; ?>


					<hr />

					<div class="row" style="text-align: center;">
						<input type="submit" class="textshad submit" value="Save Changes" name="submit" />
					</div>

				</form>
			
			</div>
			
		</div>

		<?php echo $this->load->view('inc/footer'); ?>

	</body>
	</html>