<!DOCTYPE html>
<html>
<head>

  <?php 
  
  	$d['meta_desc'] = 'Coming Soon!';

    //Load the reqiured Files View
    echo $this->load->view('inc/req_files',$d); 
  ?>
    
</head>

<body class="singleVideo">
  <?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
<div id="wrapper">

  <?php 
    //Load the reqiured header view
    echo $this->load->view('inc/header'); 
  ?>
    
    <div class="vidmain">
      <div style="float:left;width:100%;text-align:center;padding:40px 0;height:500px;">
          <h3 style="font-size:50px;color:#f90;">Coming Soon!</h3><br>
			      	 <span style="font-size:20px;">This page is not available. Check back soon!<br><br> <a href="<?php echo base_url(); ?>">&laquo; Back Home</a></span>
			     
      </div>
        
  </div>
    
  <?php echo $this->load->view('inc/footer'); ?>

</body>
</html>