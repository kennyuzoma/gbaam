<!DOCTYPE html>
<html>
<head>

  <?php 
    //Load the reqiured Files View
    echo $this->load->view('inc/req_files'); 
  ?>
    
</head>

<body class="singleVideo">
  <?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
<div id="wrapper">

  <?php 
    //Load the reqiured header view
    echo $this->load->view('inc/header'); 
  ?>
    
    <div class="vidmain">
      <form class="big_search">
        <input type="text" name="search">
        <input type="submit" name="submit" Value="Search" />
      </form>
    </div>
  
    
  <?php echo $this->load->view('inc/footer'); ?>

</body>
</html>