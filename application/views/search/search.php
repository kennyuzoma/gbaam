<!DOCTYPE html>
<html>
<head>

	<?php 
    //Load the reqiured Files View
	echo $this->load->view('inc/req_files'); 
	?>

</head>

<body class="singleVideo">
	<?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
	<div id="wrapper">

		<?php 
    //Load the reqiured header view
		echo $this->load->view('inc/header'); 
		?>

		<div class="vidmain">
			<div style="float:left;width:640px;">
				<?php
				if($getSearch_video == FALSE && $getSearch_gbaamtv == FALSE && $getSearch_song == FALSE && $getSearch_mixtape == FALSE && $getSearch_articles == FALSE)
				{
					echo '<span style="float:left;width:640px;font-size:28px;padding-bottom:3px;margin-bottom:3px;border-bottom:1px solid #5C5C5C;text-align:center;margin-top:10px;" class="cleanhead">
					There are no results for "'.$term.'"
					</span>';
				}
				elseif($term == '')
				{
					echo '<span style="float:left;width:640px;font-size:28px;padding-bottom:3px;margin-bottom:3px;border-bottom:1px solid #5C5C5C;text-align:center;margin-top:10px;" class="cleanhead">
					Please enter a search term.
					</span>';
				}



				else{ ?>

				<span style="float:left;width:640px;font-size:28px;padding-bottom:3px;margin-bottom:3px;border-bottom:1px solid #5C5C5C;" class="cleanhead">
					<?php echo $pg_title; ?>
				</span>


				<?php if($getSearch_video != ''): ?>
				<span  class="cleanhead_blk">Video Results (<?php echo $getSearch_video_count; ?>)</span>
				<ul id="searchlist">
					<?php foreach($getSearch_video as $g): 

					?>
					<li>
						<a title="<?php echo $g->artist_name . ' - '.$g->title; ?>" href="<?php echo base_url() ?>video/<?php echo hashids_encrypt($g->id); ?>">
							<img src="<?php 

		  					if($g->thumb == '')
		  					{
		  						if($g->yt == 1){

		  							echo 'http://img.youtube.com/vi/'.youtube_id_from_url($g->video_src).'/mqdefault.jpg';
		  						}
		  						else
		  						{
		  							echo base_url()."thumbs/Gbaam3.jpg";
		  						}
		  					}
		  					else
		  					{

		  						echo base_url() . 'thumbs/' . $g->thumb; 
		  					}

		  					?>" alt="<?php echo $g->title; ?>" />
							<span class="name"><?php echo $g->artist_name . ' - '.$g->title; ?></span>
						</a>
					</li>
					<?php endforeach; 

					

					$v_count = count($getSearch_video);

					if($v_count >= 3 && $this->uri->segment(4) != 'video')
						{ ?>
					<li class="moreresults"><a href="<?php echo base_url(); ?>search/srchsing/<?php echo $term; ?>/video">View All <?php echo $getSearch_video_count.' '; ?>Video Results</a></li>
					<?php } ?>
				</ul> 
			<?php endif; ?>



				<?php if($getSearch_gbaamtv != ''): ?>
				<span class="cleanhead_blk">GbaamTV Results (<?php echo $getSearch_gbaamtv_count; ?>)</span>
				<ul id="searchlist">
					<?php foreach($getSearch_gbaamtv as $g): 

					?>
					<li>
						<a title="<?php echo $g->title; ?>" href="<?php echo base_url() ?>tv/v/<?php echo hashids_encrypt($g->id); ?>">
							<img src="<?php 

		  					if($g->thumb == '')
		  					{
		  						if($g->yt == 1){

		  							echo 'http://img.youtube.com/vi/'.youtube_id_from_url($g->video_src).'/mqdefault.jpg';
		  						}
		  						else
		  						{
		  							echo base_url()."thumbs/Gbaam3.jpg";
		  						}
		  					}
		  					else
		  					{

		  						echo base_url() . 'thumbs/' . $g->thumb; 
		  					}

		  					?>" alt="<?php echo $g->title; ?>" />
							<span class="name"><?php echo $g->title; ?></span>
						</a>
					</li>
					<?php endforeach; 


					$v_count = count($getSearch_gbaamtv);

					if($v_count >= 3 && $this->uri->segment(4) != 'gbaamtv')
						{ ?>
					<li class="moreresults"><a href="<?php echo base_url(); ?>search/srchsing/<?php echo $term; ?>/gbaamtv">View All <?php echo $getSearch_gbaamtv_count.' '; ?>GbaamTV Results</a></li>
					<?php } ?>
				</ul> 
			<?php endif; ?>




				<?php /*if($getSearch_chal != ''): ?>
				<span class="cleanhead_blk">Challenge Results (<?php echo $getSearch_chal_count; ?>)</span>
				<ul id="searchlist">
					<?php foreach($getSearch_chal as $g): 

					?>
					<li>
						<a title="<?php echo $g->title; ?>" href="<?php echo base_url() ?>video/view/<?php echo hashids_encrypt($g->id); ?>">
							<img src="<?php 

		  					if($g->thumb == '')
		  					{
		  						if($g->yt == 1){

		  							echo 'http://img.youtube.com/vi/'.youtube_id_from_url($g->video_src).'/mqdefault.jpg';
		  						}
		  						else
		  						{
		  							echo base_url()."thumbs/Gbaam3.jpg";
		  						}
		  					}
		  					else
		  					{

		  						echo base_url() . 'thumbs/' . $g->thumb; 
		  					}

		  					?>" alt="<?php echo $g->title; ?>" />
							<span class="name"><?php echo $g->title; ?></span>
						</a>
					</li>
					<?php endforeach; 

					endif;

					$v_count = count($getSearch_chal);

					if($v_count >= 3 && $this->uri->segment(4) != 'chal')
						{ ?>
					<li class="moreresults"><a href="<?php echo base_url(); ?>search/srchsing/<?php echo $term; ?>/chal">View All <?php echo $getSearch_chal_count.' '; ?>Challenge Results</a></li>
					<?php } ?>
				</ul> 

				<?php*/ if($getSearch_mixtape != ''): ?>
				<span class="cleanhead_blk">Mixtape Results (<?php echo $getSearch_mixtape_count; ?>)</span>
				<ul id="searchlist">
					<?php foreach($getSearch_mixtape as $g): 

					?>
					<li>
						<?php
						$mixtape_url = base_url().'mixtapes/'.$g->id.'/'.$g->permalink;
						?>
						<a title="<?php echo $g->artist . ' - '.$g->title; ?>" href="<?php echo $mixtape_url ?>">
							<?php 
							if($g->front_cover == ''){
								?> 
								<img alt="<?php echo $g->artist . ' - '.$g->title; ?>" src="<?php echo base_url(); ?>thumbs/Gbaam3.jpg" /> 
								<?php
							} 
							else{ ?>
							<img alt="<?php echo $g->artist . ' - '.$g->title; ?>" src="<?php echo get_image('mixtape',$g->front_cover,'','125'); ?>" alt="<?php echo $g->title; ?>" alt="<?php echo $g->title; ?>" /> 
							<?php } ?>
							<span class="name"><?php echo $g->artist . ' - '.$g->title; ?></span>
							<span class="desc"><?php echo character_limiter($g->desc,200); ?></span>
						</a>
					</li>
					<?php endforeach; 

					

					$v_count = count($getSearch_mixtape);

					if($v_count >= 3 && $this->uri->segment(4) != 'mixtape')
						{ ?>
					<li class="moreresults"><a href="<?php echo base_url(); ?>search/srchsing/<?php echo $term; ?>/mixtape">View All <?php echo $getSearch_mixtape_count.' '; ?>Mixtape Results</a></li>
					<?php } ?>
				</ul> 
			<?php endif; ?>

			<?php if($getSearch_song != ''): ?>
				<span class="cleanhead_blk">Song Results (<?php echo $getSearch_song_count; ?>)</span>
				<ul id="searchlist">
					<?php foreach($getSearch_song as $g): 

					?>
					<li>
						<?php
						$song_url = base_url().'songs/'.$g->id.'/'.$g->permalink;
						?>
						<a title="<?php echo $g->artist . ' - '.$g->title; ?>" href="<?php echo $song_url ?>">
							<?php 
							if($g->front_cover == ''){
								?> 
								<img alt="<?php echo $g->artist . ' - '.$g->title; ?>" src="<?php echo base_url(); ?>thumbs/Gbaam3.jpg" /> 
								<?php
							} 
							else{ ?>
							<img alt="<?php echo $g->artist . ' - '.$g->title; ?>" src="<?php echo get_image('song',$g->front_cover,'','125'); ?>" alt="<?php echo $g->title; ?>" alt="<?php echo $g->title; ?>" /> 
							<?php } ?>
							<span class="name"><?php echo $g->artist . ' - '.$g->title; ?></span>
							<span class="desc"><?php echo character_limiter($g->desc,200); ?></span>
						</a>
					</li>
					<?php endforeach; 

					

					$v_count = count($getSearch_song);

					if($v_count >= 3 && $this->uri->segment(4) != 'song')
						{ ?>
					<li class="moreresults"><a href="<?php echo base_url(); ?>search/srchsing/<?php echo $term; ?>/song">View All <?php echo $getSearch_song_count.' '; ?>Song Results</a></li>
					<?php } ?>
				</ul> 
			<?php endif; ?>



				<?php if($getSearch_articles != ''): ?>
				<span class="cleanhead_blk">Editorial Results (<?php echo $getSearch_articles_count; ?>)</span>
				<div id="featuredpg">
					<ul id="searchlist"> 
						<?php foreach($getSearch_articles as $g): ?>
						<li>
							<div class="storytitle">
								<a class="title" href="<?php echo base_url(); ?>editorial/<?php echo $g->permalink; ?>"><?php echo $g->title;?></a> <br />

								<span style="font-size:13px;">
									By <?php echo $g->author; ?> on <?php echo conv_date($g->date_created); ?> | 
									<a href="<?php echo base_url(); ?>editorial/<?php echo $g->permalink; ?>" class="count-comments" data-disqus-url="<?php echo base_url(); ?>editorial/<?php echo $g->permalink; ?>"></a>
								</span>
							</div>

							<a href="<?php echo base_url(); ?>editorial/<?php echo $g->permalink; ?>">
								<img alt="<?php echo $g->title; ?>" class="aimg" src="<?php echo get_image('article',$g->image,'','150'); ?>" alt="<?php echo $g->title; ?>" alt="<?php echo $g->title; ?>" />
							</a>
							<div class="edbody home">
								<?php 
                    // remove images from preview
								$body = preg_replace("/<img[^>]+\>/i", "", $g->body); 

                    // remove iframes from preview
								$body = preg_replace('/<iframe.*?\/iframe>/i','', $body);
								echo word_limiter($body,50); 
								?>
								<a class="orange" href="<?php echo base_url(); ?>editorial/<?php echo $g->permalink; ?>">View more</a>
							</div>
						</li>

						<?php 
							endforeach; 

						

						$v_count = count($getSearch_articles);

						if($v_count >= 3 && $this->uri->segment(4) != 'editorials'):
								 
						?>
						<li class="moreresults" style="">
							<a style="font-size:16px;" href="<?php echo base_url(); ?>search/srchsing/<?php echo $term; ?>/articles">View All <?php echo $getSearch_articles_count.' '; ?>
								Editorial Results
							</a>
						</li>
						<?php 
						endif; 
						?>
					</ul> 
				</div>
				<?php

					endif;
				?>

				<?php }?>
			</div>
			<?php $this->load->view('inc/rightsidebar'); ?>


			</div>
			<?php echo $this->load->view('inc/footer'); ?>
</div>
		</body>
		</html>