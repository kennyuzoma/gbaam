<!DOCTYPE html>
<html>
<head>

	<?php 
    //Load the reqiured Files View
	echo $this->load->view('inc/req_files'); 
	?>

</head>

<body class="singleVideo">
	<?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
	<div id="wrapper">

		<?php 
    //Load the reqiured header view
		echo $this->load->view('inc/header'); 
		?>

		<div class="vidmain">
			<div style="float:left;width:640px;">


				<span style="float:left;width:640px;font-size:28px;padding-bottom:3px;margin-bottom:3px;border-bottom:1px solid #5C5C5C;" class="cleanhead">
					<?php echo $pg_title; ?>
				</span>

				<span style="float:left;width:640px;font-size:17px;padding-bottom:5px;margin-bottom:1px;" class="cleanhead">
					<a href="<?php echo base_url(); ?>search/srch/<?php echo $term; ?>">&laquo; Back to all results</a>
				</span>


				<?php
				if($this->uri->segment(4) == 'video'):
					if($getSearch_video != ''): ?>
				<span class="cleanhead_blk">Video Results (<?php echo $getSearch_video_count; ?>)</span>
				<ul id="searchlist">
					<?php foreach($getSearch_video as $g): 

					?>
					<li>
						<a title="<?php echo $g->artist_name . ' - '.$g->title; ?>" href="<?php echo base_url() ?>video/<?php echo hashids_encrypt($g->id); ?>">
							<img src="<?php 

		  					if($g->thumb == '')
		  					{
		  						if($g->yt == 1){

		  							echo 'http://img.youtube.com/vi/'.youtube_id_from_url($g->video_src).'/mqdefault.jpg';
		  						}
		  						else
		  						{
		  							echo base_url()."thumbs/Gbaam3.jpg";
		  						}
		  					}
		  					else
		  					{

		  						echo base_url() . 'thumbs/' . $g->thumb; 
		  					}

		  					?>" alt="<?php echo $g->title; ?>" />
							<span class="name"><?php echo $g->artist_name . ' - '.$g->title; ?></span>
						</a>
					</li>
				<?php endforeach; ?>


				</ul> 

				<?php
					endif;
				endif;
				?>











			<?php 
			if($this->uri->segment(4) == 'gbaamtv'):
				if($getSearch_gbaamtv != ''): ?>
			<span class="cleanhead_blk">GbaamTV Results (<?php echo $getSearch_gbaamtv_count; ?>)</span>
			<ul id="searchlist">
				<?php foreach($getSearch_gbaamtv as $g): 

				?>
				<li>
					<a title="<?php echo $g->title; ?>" href="<?php echo base_url() ?>tv/v/<?php echo hashids_encrypt($g->id); ?>">
						<img src="<?php 

		  					if($g->thumb == '')
		  					{
		  						if($g->yt == 1){

		  							echo 'http://img.youtube.com/vi/'.youtube_id_from_url($g->video_src).'/mqdefault.jpg';
		  						}
		  						else
		  						{
		  							echo base_url()."thumbs/Gbaam3.jpg";
		  						}
		  					}
		  					else
		  					{

		  						echo base_url() . 'thumbs/' . $g->thumb; 
		  					}

		  					?>" alt="<?php echo $g->title; ?>" />
						<span class="name"><?php echo $g->title; ?></span>
					</a>
				</li>
			<?php endforeach; ?>


			</ul> 

		<?php
			endif;
		endif;
		?>










		<?php 
		if($this->uri->segment(4) == 'chal'):
			if($getSearch_chal != ''): ?>
		<span class="cleanhead_blk">Challenge Results (<?php echo $getSearch_chal_count; ?>)</span>
		<ul id="searchlist">
			<?php foreach($getSearch_chal as $g): 

			?>
			<li>
				<a title="<?php echo $g->title; ?>" href="<?php echo base_url() ?>challenge/view/<?php echo hashids_encrypt($g->id); ?>">
					<img src="<?php 

		  					if($g->thumb == '')
		  					{
		  						if($g->yt == 1){

		  							echo 'http://img.youtube.com/vi/'.youtube_id_from_url($g->video_src).'/mqdefault.jpg';
		  						}
		  						else
		  						{
		  							echo base_url()."thumbs/Gbaam3.jpg";
		  						}
		  					}
		  					else
		  					{

		  						echo base_url() . 'thumbs/' . $g->thumb; 
		  					}

		  					?>" alt="<?php echo $g->title; ?>" />
					<span class="name"><?php echo $g->title; ?></span>
				</a>
			</li>
		<?php endforeach; ?>


		</ul> 

		<?php
			endif;
		endif;
		?>









		<?php 
		if($this->uri->segment(4) == 'mixtape'):
			if($getSearch_mixtape != ''): ?>
		<span class="cleanhead_blk">Mixtape Results (<?php echo $getSearch_mixtape_count; ?>)</span>
		<ul id="searchlist">
			<?php foreach($getSearch_mixtape as $g): 

			?>
			<li>
				<?php
				$mixtape_url = base_url().'mixtapes/'.$g->id.'/'.$g->permalink;
				?>
				<a title="<?php echo $g->artist . ' - '.$g->title; ?>" href="<?php echo $mixtape_url ?>">
					<?php 
					if($g->front_cover == ''){
						?> 
						<img alt="<?php echo $g->artist . ' - '.$g->title; ?>" src="<?php echo base_url(); ?>thumbs/Gbaam3.jpg" /> 
						<?php
					} 
					else{ ?>
					<img alt="<?php echo $g->artist . ' - '.$g->title; ?>" src="<?php echo get_image('mixtape',$g->front_cover,'','125'); ?>" alt="<?php echo $g->title; ?>" alt="<?php echo $g->title; ?>" /> 
					<?php } ?>
					<span class="name"><?php echo $g->artist . ' - '.$g->title; ?></span>
					<span class="desc"><?php echo character_limiter($g->desc,200); ?></span>
				</a>
			</li>
		<?php endforeach; ?>


		</ul> 

		<?php
			endif;
		endif;
		?>



		<?php 
		if($this->uri->segment(4) == 'song'):
			if($getSearch_song != ''): ?>
		<span class="cleanhead_blk">song Results (<?php echo $getSearch_song_count; ?>)</span>
		<ul id="searchlist">
			<?php foreach($getSearch_song as $g): 

			?>
			<li>
				<?php
				$song_url = base_url().'songs/'.$g->id.'/'.$g->permalink;
				?>
				<a title="<?php echo $g->artist . ' - '.$g->title; ?>" href="<?php echo $song_url ?>">
					<?php 
					if($g->front_cover == ''){
						?> 
						<img alt="<?php echo $g->artist . ' - '.$g->title; ?>" src="<?php echo base_url(); ?>thumbs/Gbaam3.jpg" /> 
						<?php
					} 
					else{ ?>
					<img alt="<?php echo $g->artist . ' - '.$g->title; ?>" src="<?php echo get_image('song',$g->front_cover,'','125'); ?>" alt="<?php echo $g->title; ?>" alt="<?php echo $g->title; ?>" /> 
					<?php } ?>
					<span class="name"><?php echo $g->artist . ' - '.$g->title; ?></span>
					<span class="desc"><?php echo character_limiter($g->desc,200); ?></span>
				</a>
			</li>
		<?php endforeach; ?>


		</ul> 

		<?php
			endif;
		endif;
		?>





<?php 
if($this->uri->segment(4) == 'articles'):
	if($getSearch_articles != ''): 
		?>
	<span class="cleanhead_blk">Editorial Results (<?php echo $getSearch_articles_count; ?>)</span>
	<div id="featuredpg">
		<ul id="searchlist"> 
			<?php foreach($getSearch_articles as $g): 

			?>
			<li>
				<div class="storytitle">
					<a class="title" href="<?php echo base_url(); ?>editorial/<?php echo $g->permalink; ?>"><?php echo $g->title;?></a> <br />

					<span style="font-size:13px;">
						By <?php echo $g->author; ?> on <?php echo conv_date($g->date_created); ?> | 
						<a href="<?php echo base_url(); ?>editorial/<?php echo $g->permalink; ?>" class="count-comments" data-disqus-url="<?php echo base_url(); ?>editorial/<?php echo $g->permalink; ?>"></a>
					</span>
				</div>

				<a href="<?php echo base_url(); ?>editorial/<?php echo $g->permalink; ?>">
					<img alt="<?php echo $g->title; ?>" class="aimg" src="<?php echo get_image('article',$g->image,'','150'); ?>" alt="<?php echo $g->title; ?>" alt="<?php echo $g->title; ?>" />
				</a>
				<div class="edbody home">
					<?php 
                    // remove images from preview
					$body = preg_replace("/<img[^>]+\>/i", "", $g->body); 

                    // remove iframes from preview
					$body = preg_replace('/<iframe.*?\/iframe>/i','', $body);
					echo word_limiter($body,50); 
					?>

					<a class="orange" href="<?php echo base_url(); ?>editorial/<?php echo $g->permalink; ?>">View more</a></div>
				</li>
			<?php endforeach; ?>


		</ul> 
	</div>

	<?php
	endif;
	endif;
	?>

	<div style="float:left;width:100%;margin-top:10px;">
		<?php echo $pagi_links; ?>
	</div>
</div>
	<?php $this->load->view('inc/rightsidebar'); ?>
		
	</div>

<?php echo $this->load->view('inc/footer'); ?>
</body>
</html>