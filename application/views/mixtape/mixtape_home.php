<!DOCTYPE html>
<html>
<head>

  <?php 
    //Load the reqiured Files View
    echo $this->load->view('inc/req_files'); 
  ?>
    
</head>

<body class="singleVideo">
    <?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
<div id="wrapper">

  <?php 
    //Load the reqiured header view
    echo $this->load->view('inc/header'); 
  ?>
    
    <div class="vidmain">
    
      <div style="float:left;width:640px;padding-bottom:10px;"> 
      <?php if($this->Mixtape_model->isMOTWavail() == TRUE): ?>
        <span class="cleanhead">Mixtape Of The Week</span>
      	<div id="mixtapetwo2" style="margin-bottom:10px;">
        <?php foreach($getMotw as $gm): 

        $mixtape_url = base_url().'mixtapes/'.$gm->id.'/'.$gm->permalink;
        ?>
      	<a title="<?php echo $gm->title.' - '.$gm->artist; ?>" href="<?php echo $mixtape_url; ?>">
            <img alt="<?php echo $gm->title.' - '.$gm->artist; ?>"class="mxcover" src="<?php echo get_image('mixtape',$gm->front_cover,$gm->img_hash,'200'); ?>" />
        </a>
        <span class="mxtitle">
            <a href="<?php echo $mixtape_url; ?>">
                <?php echo $gm->title.' - '.$gm->artist; ?> 
            </a>
            </span>
			<p class="mxdesc" style="height:110px;"><?php echo nl2br(character_limiter($gm->desc,370)); ?></p>
        <?php endforeach; ?>
        <div style="float:right;width:425px;border-bottom:1px solid #5C5C5C;border-top:1px solid #5C5C5C;padding:5px 0;">
	        <span class="mxrating">Rating:  
            <?php if($isScore == FALSE) { ?>
                <b class="score">None</b>
                <?php } else { ?>
                <b class="score<?php if($final_score <= 2) 
													{
														echo 1;
													} else {
														echo $final_score;
													} ?>"> <?php echo $final_score; ?>/10</b>
                <?php } ?>
            </span>
	        <!-- AddThis Button BEGIN -->
			<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style"  
    addthis:url="<?php echo $mixtape_url; ?>"
    addthis:description="<?php echo character_limiter($gm->desc,430); ?>"
>
<a class="addthis_button_preferred_1"></a>
<a class="addthis_button_preferred_2"></a>
<a class="addthis_button_preferred_3"></a>
<a class="addthis_button_preferred_4"></a>
<a class="addthis_button_compact"></a>
<a class="addthis_counter addthis_bubble_style"></a>
</div>
			<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4f1e445964f67956"></script>
			<!-- AddThis Button END --> 
        </div>
        
        
        </div>
        <?php endif; ?>
        <span class="cleanhead" style="margin-bottom:0;margin-bottom:10px;">Hot Mixtapes</span>
		<ul id="featmixtapes" style="margin-bottom:20px;">
        	
            <?php 
            if($getHotMixtapes_count == 0):
            
                echo '<span style="color:#eee;">There are no Hot Mixtapes available yet!</span>';
            
            else:

                for ($i=1; $i <= 3; $i++):

                            $gm = $this->Mixtape_model->getHotMixtape_by_slot($i);

                        	if($this->Mixtape_model->getHotMixtape_count($i) == 0)
                        		continue;
                        
                             ?>
                <li>
                	<a title="<?php echo $gm->title.' - '.$gm->artist; ?>" href="<?php echo base_url(); ?>mixtapes/<?php echo $gm->id.'/'.$gm->permalink; ?>">
                    	<img alt="<?php echo $gm->title.' - '.$gm->artist; ?>" style="width:180px;height:180px;" src="<?php echo get_image('mixtape',$gm->front_cover,$gm->img_hash,'200'); ?>" />
                    </a>
                    <a href="<?php echo base_url(); ?>mixtapes/<?php echo $gm->id.'/'.$gm->permalink; ?>" class="title"><?php echo character_limiter($gm->title,24); ?></a>
                    <a href="<?php echo base_url(); ?>mixtapes/<?php echo $gm->id.'/'.$gm->permalink; ?>" class="artistname"><?php echo $gm->artist; ?></a>
                    <span class="rating">Rating: <b>
                                <?php

                                $getmix = $this->Mixtape_model->get_vote_final($gm->id);
                                $isScore = $getmix['isScore'];
                                
                                    if($isScore == FALSE) { 
                                        echo 'None';
                                    } else {
                                        $final_score = $getmix['final_score'];
                                        if($final_score <= 2) 
                                        {
                                            echo 1;
                                        } else {
                                            echo $final_score.'/10';
                                        }
                                    } ?>
                                    </b></span>
                </li> 
                <?php 
                endfor; //result
            endif; //count
            ?>
        </ul>
        <span class="cleanhead" style="margin-bottom:0;">Latest Mixtapes</span>
        <ul id="latestmtlist">
        
        	<?php 

            if($getMixtapesCount == 0):
            
                echo 'There are no mixtapes available yet';
            
            else:

                foreach($getMixtapes as $g): ?>
            	<li>
    		            <a title="<?php echo $g->title.' - '.$g->artist; ?>" href="<?php echo base_url(); ?>mixtapes/<?php echo $g->id.'/'.$g->permalink; ?>">
    		            	<img alt="<?php echo $g->title.' - '.$g->artist; ?>" style="width:125px;height:125px;" src="<?php echo get_image('mixtape',$g->front_cover,$g->img_hash,'125'); ?>" />
    	                
    	               		<span class="mtitle"><?php echo character_limiter($g->title,15); ?></span>
    	                </a>
    	                <span class="maname"><?php echo character_limiter($g->artist,20); ?></span>


    	                <span class="rating">Rating: <b>
                                <?php

                                $getmix = $this->Mixtape_model->get_vote_final($g->id);
                                $isScore = $getmix['isScore'];
                                
                                    if($isScore == FALSE) { 
                                        echo 'None';
                                    } else {
                                        $final_score = $getmix['final_score'];
                                        if($final_score <= 2) 
                                        {
                                            echo 1;
                                        } else {
                                            echo $final_score.'/10';
                                        }
                                    } ?>
                                    </b></span>
                        <span class="views">Views: <b><?php echo number_format($g->v_count); ?></b></span>
                </li>
                <?php 
                endforeach; 

            endif;
            ?>
            
            
        </ul>
       <a class="viewalink" href="<?php echo base_url(); ?>mixtapes/all">View All Mixtapes</a>

      </div>
        <?php $this->load->view('inc/rightsidebar'); ?>
  </div>
    
   
    
    	<?php echo $this->load->view('inc/footer'); ?>

</body>
</html>