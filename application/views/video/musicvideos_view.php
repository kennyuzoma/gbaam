<!DOCTYPE html>
<html>
<head>

	<?php 
		//Load the reqiured Files View
		echo $this->load->view('inc/req_files'); 
	?>

<script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#async=1#pubid=ra-4f1e445964f67956"></script>

<script type="text/javascript">
	function initAddThis() 
     {
          addthis.init()
     }
     // After the DOM has loaded...
     $(window).bind("load", function() {
     initAddThis();
 	});
</script>
<style>

.morelink{
  cursor:pointer;
  font-weight:bold;
}
.morelink:hover{
  cursor:pointer;
}
.morecontent span {
  display: none;
}

.readmore-js-toggle{
	float: right;
	width: 620px;
	text-align: center;
	font-weight: normal;
	font-size: inherit;
	color: #f90;
	font-size: 13px;
	font-weight:bold;
	padding-top:5px;
	box-shadow: 0px -4px 2px -3px rgba(50, 50, 50, 0.75);

}
.video_main_box{
float:left;width:650px;
}
.main-wider{
	width:100%;
}
.vid-wider{
	width: 854px;
	height: 480px;
	margin-left: 42px;
	box-shadow: 0px 0px 10px black;
	margin-bottom: 11px;
}

.vid-reg{
	width:650px;
	height:366px;
}

.pushdown{
	margin-top:0px;
}

.pushup{
	margin-top:-398px;
}
.rightsidecont{
	float:right;width:280px;
}
.gtvdescbox{width:630px; }

.thedesctxt{display:none;}

.colororange{background:#f90;}

</style>

    <script>
	$(function() {

		if($('.act').hasClass( "colororange" ))
		{
			var act_on = 0;
		}
		else
		{
			var act_on = 1;
		}

		$('.act').on('click', function(){

			$(".rightsidecont").toggleClass('pushup',100).delay(200).toggleClass('pushdown',100);
			$(this).toggleClass('colororange');
			$('.video_main_box').delay(200).toggleClass('main-wider',200);
			$("#vidload").delay(200).toggleClass('vid-reg',200).toggleClass('vid-wider',200);

			$(this).tipsy("hide");

			if(act_on == 1)
			{
				$.post( "<?php echo base_url(); ?>ajax/video_preference/long" );

				$(this).attr('title','Enter Regular Mode');
				$(this).attr('alt','Enter Regular Mode');
				act_on = 0;
				console.log('act_on 0');

				
			}
			else
			{
				$.post( "<?php echo base_url(); ?>ajax/video_preference/regular" );

				$(this).attr('title','Enter Theater Mode');
				$(this).attr('alt','Enter Theater Mode');
				act_on = 1;
				console.log('act_on 1');
			}
			
		});

		$('.thedesctxt').show();
				
		$('.thedesctxt_cont').readmore({
		  	speed: 75,
		  	maxHeight: 40,
		  	embedCss: false,
		  	sectionCSS: '',
		  	moreLink: '<a href="#">Show More</a>',
			lessLink: '<a href="#">Show Less</a>'
		});



		
		$(document).on('click', '.loginclick2', function (e) {
		    $('#basic-modal-content').modal({
				overlayClose:true,
				opacity:60,
				overlayCss: {backgroundColor:"#000"},
				containerCss: {height:"340px"},
				position: ["10%","50%"]
			});

		    e.preventDefault();
		});

		$(document).on('click', '.signupclick2', function (e) {
	        $('#basic-modal-content_signup').modal({
	        overlayClose:true,
	        opacity:60,
	        overlayCss: {backgroundColor:"#000"},
			position: ["10%","50%"]
	      });

	        e.preventDefault();
	    });

		 $(document).on('click', '.closeit', function (e) {
        $('.warningMessage').fadeOut();

        e.preventDefault();
    });

		$('.vb').on('click', function() {

			if(!$(this).parent().hasClass('hov'))
			{

				if($('#vote li a').hasClass('vb'))
				{
					var videoid = $('.videoid').val();
					var choice = $(this).attr('id');
					var value = $('#choice'+choice).text();

					$.ajax({
						url: "<?php echo base_url(); ?>video/add_vote/"+videoid+"/"+choice,
						type: 'get',
						contenttype: "application/json",
						datatype: 'jsonp',
						async: false,
						success: function(dat) {
							newdata = JSON.parse(dat);
							if(newdata.status == 'good')
							{
								if(value == '')
								{

								}
								else
								{
									var old_value = $('#vote2 .hov').text();
									if(old_value == 1)
									{
										$('#vote2 .hov').text(' ');
									}
									else
									{
										$('#vote2 .hov').text(--old_value).show();
									}
									

									value = parseInt(value, 10) || 0; // Use a radix!!!
									$('#choice'+choice).text(++value).show();

									
								}

								$('#vote li, #vote2 li').removeClass('hov');

								$('.space').prepend('<div class="result"><b>Thanks for your vote!</b></div>').show('fast');
								$('.result').delay(100).slideDown("fast").delay(3000).slideUp('fast');

								$('#choice'+choice).addClass('hov');
								$('#vote #vi'+choice).addClass('hov');
							}
							else
							{
								$('.space').prepend('<div class="warningMessage"><b>You must be logged in to vote! <a href="#" class="loginclick2">Log in</a> or <a href="#" class="signupclick2">Sign up</a> </b><span class="closeit">x</span></div>').show('fast');
								$('.warningMessage').delay(100).slideDown("fast");


							}
			            },
			            error: function() {
			                alert('There has been an error, please alert us immediately');
			            }
			        });
				}
				$('#vote li a').removeClass('vb');
			}

			return false;
		});



		$('body').load(function () {
		  if (typeof callback == 'function') {
		    callback($('body', this.contentWindow.document).html());
		  }
		  setTimeout(function () {$('iframe').remove();}, 50);
		});

	


	});

	</script>


</head>

<body class="singleVideo">
	<?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
<div id="wrapper">

  <?php 
    //Load the reqiured header view
    echo $this->load->view('inc/header'); 
  ?>
    <div style="float:left;width:950px;padding:5px 5px 0px 5px;background:#222222;">
    <div class="vidmain" style="float:left;width:940px;background:#111111;padding:5px;margin-bottom: 5px;">

    	<?php if($status == '2'): ?>

		<div style="float:left;width:100%;margin-bottom:20px;text-align:center;font-size:25px;background:#dc9001;border-radius:5px;text-shadow: 0px 0px 3px black;">Unlisted</div>
		
		<?php elseif($status == '3'): ?>

		<div style="float:left;width:100%;margin-bottom:20px;text-align:center;font-size:25px;background:#dc9001;border-radius:5px;text-shadow: 0px 0px 3px black;">Private</div>
		
		<?php endif; ?>
		
		<div class="video_main_box <?php if($video_pref == 'long') echo 'main-wider'; ?>">

    		<h1 style="border:0;margin-bottom:6px;padding-bottom:0;font-size:20px;float:left;width: 94%;"><?php echo $title; ?></h1>

    		<span class="act <?php if($video_pref == 'long') echo 'colororange'; ?>" style="float: right; cursor: pointer; user-select: none; margin-top:3px;border-radius: 5px;" title="Enter Theater Mode" alt="Enter Theater Mode">
    			<img style="float: left;" width="34" src="<?php echo base_url(); ?>assets/img/theater-icon.png" />
    		</span>
			<?php
				if($_SERVER['HTTP_HOST'] == 'localhost')
					$auto = 'sadsd';
				else
					$auto = 'auto';
			?>
			<iframe id="vidload" class="<?php if($video_pref == 'long') echo 'vid-wider'; else echo 'vid-reg'; ?>" width="650" height="366" src="<?php echo $this->config->item('player_url').'video/embed/'.hashids_encrypt($videoid).'/'.$auto; ?>" frameborder="0" scrolling="no" allowfullscreen></iframe>
		</div>

    	<div style="float:left;width:650px;">
		
			

    	<div id="video">
			<div class="video_info_box" style="float:left;width:650px;">
	    		<div style="float:left;width:620px;" class="space"></div>
	    		<input type="hidden" value="<?php echo $videoid; ?>" class="videoid" />
	            <div style="float:left;width:405px;">
	            <ul id="vote" class="voteone">

					<?php

					if($this->session->userdata('logged_in') == TRUE)
					{
						$did_i_vote = $this->Video_model->did_i_vote($videoid);

						$num_rows = $did_i_vote['num_rows'];
						$row = $did_i_vote['row'];

						if($num_rows == 1)
							$class = 'hov';	
						else
							$class = '';
					}
					else
					{
						// just so i dont have to write more code
						$num_rows = 0;
					}
					

	                $i = 0;
						while ($i <= 4):

							if($i == 0)
								$word = 'Seriously?';
							
							if($i == 1)
								$word = 'Hmmmmm';

							if($i == 2)
								$word = 'Not Bad';

							if($i == 3)
								$word = 'Niceee';

							if($i == 4)
								$word = 'Gbaam!';

						

					?>
	                <li id="vi<?php echo $i; ?>"
						<?php
							if($num_rows == 1)
								if($row->choice == $i)
									echo 'class="hov"';
						?>
	                	>
	                	<a href="#" class="vb" id="<?php echo $i; ?>">
							<?php echo $word; ?>
	                	</a>
	                </li>
	                <?php
						$i++;
						endwhile;
					?>
	                
	            </ul>

	            <div id="heatcont">
	            	<img title="Vote now!" src="<?php echo base_url(); ?>assets/img/heat.png" />
	            </div>

	            <ul id="vote2">
	    			<?php
	                $i = 0;
						while ($i <= 4):
					?>
	                	<li id="choice<?php echo $i; ?>" style="height:15px;"

							<?php
							if($num_rows == 1)
								if($row->choice == $i)
									echo 'class="hov"';
							?>
	                		>
	                    	<?php echo $this->Video_model->get_votes($videoid,$i); ?>
	                    </li>
					<?php
						$i++;
						endwhile;
					?>
	            </ul> 
	            </div>
	            <div style="float:left;width:245px;;margin-top:3px;text-align:center;background: rgb(61,61,61);
	background: -moz-linear-gradient(top,  rgba(61,61,61,1) 0%, rgba(14,14,14,1) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(61,61,61,1)), color-stop(100%,rgba(14,14,14,1)));
	background: -webkit-linear-gradient(top,  rgba(61,61,61,1) 0%,rgba(14,14,14,1) 100%);
	background: -o-linear-gradient(top,  rgba(61,61,61,1) 0%,rgba(14,14,14,1) 100%);
	background: -ms-linear-gradient(top,  rgba(61,61,61,1) 0%,rgba(14,14,14,1) 100%);
	background: linear-gradient(to bottom,  rgba(61,61,61,1) 0%,rgba(14,14,14,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3d3d3d', endColorstr='#0e0e0e',GradientType=0 );

	">
	            	<div style="float:left;width:104px;height: 46px;border-left:1px solid black;border-right:1px solid black;">
	            		<span style="font-size:12px;float:left;width:104px;margin-top:5px;">Gbaam Rated</span>
	                	<?php if($isScore == FALSE) { ?>
	                	<div class="ratedgbaam" >None</div>
	                	<?php } else { ?>
	                	<div class="ratedgbaam score<?php if($css_score < 2)
														{
															echo 1;
														} else {
															echo $css_score;
														} ?>"><?php echo $final_score; ?>%</div>
	                	<?php } ?>
	                </div>

	                <div style="float:left;width:139px;">
						<span style="font-size:12px;float:left;width:139px;margin-top:5px;display:block;">Views</span>
						<span style="font-size:20px;"><?php echo $v_count; ?></span>
	                </div>

	            </div>
	            <div class="gtvdescbox" style="margin-bottom:0;">
	            	<div class="thedesctxt_cont" style="float:left;width:620px;min-height:30px;">
	              <b>Posted on <?php echo $date; ?></b> <br>
	              
	              <span class="thedesctxt">
	              <?php echo nl2br(look4links($video_desc)); ?>
	          		</span>
          		</div>
          	</div>

              



            </div>
            <?php
		        $title=urlencode($title.' | GBAAM');
		        $url = urlencode(base_url().$this->uri->uri_string());
		        $summary=urlencode(strip_tags(character_limiter($video_desc,300)));
		        $image=urlencode(youtube_meta_image($videoid));
		    ?>
       
	       <ul class="soci_actions3 addthis_toolbox addthis_default_style" style="margin:10px 0px 20px 0px;">
	          <li class="">

	            <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>&amp;p[images][0]=<?php echo $image;?>" class="socialPop">
	              <img src="<?php echo base_url(); ?>assets/img/social/facebook-variation.png" />
	            </a>
	          </li>
	          <li class="">
	            <a href="https://twitter.com/intent/tweet?text=<?php echo $title; ?>&url=<?php echo $url; ?>&related=" class="socialPop">
	              <img src="<?php echo base_url(); ?>assets/img/social/twitter-variation.png" />
	            </a>
	          </li>
	          <li class="">
	            <a href="https://plus.google.com/share?url=<?php echo $url; ?>&t=<?php echo $title; ?>" class="socialPop">
	              <img src="<?php echo base_url(); ?>assets/img/social/gplus-variation2.png" />
	            </a>
	          </li>
	          <li class="share"><a class="addthis_counter addthis_pill_style"></a></li>

	        </ul>
			

            <div id="fivefeat" class="voteone">
            <span style="float:left;width:620px;padding:5px 10px 5px 10px;font-size:16px ;">Featured Videos</span>
			<ul>
  			
  			<?php 

		                for ($i=1; $i <= $this->config->item('feat_vid_reg_count'); $i++):

		                	if($this->Video_model->getFeatVideo_slot_count('five',$i) == 0)
		                		continue;

		                    $g = $this->Video_model->getFeatVideo_slot('five',$i);

		            ?>
			    	<li id="vi1" <?php if($videoid == $g->type_id) echo 'class="selected"'; ?>>
			            <a title="<?php echo $g->artist_name.' - '.$g->title; ?>" href="<?php echo base_url(); ?>video/<?php echo hashids_encrypt($g->type_id); ?>">
		                	<img height="65" src="<?php 

		                	if($g->thumb == '')
		                	{
		                		if($g->yt == 1){

									echo 'http://img.youtube.com/vi/'.youtube_id_from_url($g->video_src).'/mqdefault.jpg';
								}
							}
								else
									{

		                				echo base_url() . 'thumbs/' . $g->thumb; 
		                			}
		                	

		                	?>" alt="<?php echo $g->artist_name.' - '.$g->title; ?>"/><BR>
		                    <?php echo character_limiter($g->artist_name.' - '.$g->title,15); ?>
			            </a>
		            </li>
		            
			    	<?php 
		                endfor; 
		            ?>
				
            </ul>
            </div>
           
            
            </div>
            

            <div id="disqus_thread" style="float:left;width:630px;background:#222;padding:10px;margin-top:10px;"></div>

		</div>
		
		<div class="rightsidecont <?php if($video_pref == 'long') echo 'pushdown'; else echo 'pushup'; ?>" >
			<div class="rightside"  style="background:#222;">

				<div style="float:left;width:100%;">
				<?php $this->load->view('inc/google_250'); ?>
				</div>

		    </div>
			<div id="videoscroll">
	  	    	<ul> 

					<?php  
						$i = 0;
						$final_date = array();

						$get_videos = $this->Video_model->getAllVideos('',10,'vdate_created DESC');

						foreach($get_videos as $gv): 

						$mysql_date = $gv->vdate_created;
	              		$time = strtotime($mysql_date);
						$new_date = date('M d, Y', $time);
						$final_date[$i] = $new_date;

						if($i > 0){
							if ($final_date[$i] != $final_date[$i-1])
							{
						?>

							<li class="thedate">
								<a title="<?php echo $new_date; ?>" href="#" id="<?php echo $gv->date_created; ?>">
								<?php echo $new_date; ?>
								</a>
							</li>

						<?php
							}
						} else {
						   ?>
						   	<li class="thedate">
								<a title="<?php echo $new_date; ?>" href="#" id="<?php echo $gv->date_created; ?>">
									<?php echo $new_date; ?>
								</a>
							</li>
						   <?php
						}
						  
					?>

					

	              	<li>
	                  	<a title="<?php echo $gv->artist_name.' - '.$gv->title; ?>" href="<?php echo base_url(); ?>video/<?php echo hashids_encrypt($gv->id); ?>" class="img">
	                    	<?php 
							if($gv->thumb == ''){

								if($gv->yt == 1){
									echo '<img class="lazy" alt="'.$gv->artist_name.' - '.$gv->title.'"  data-original="http://img.youtube.com/vi/'.youtube_id_from_url($gv->video_src).'/mqdefault.jpg" width="107" height="60" />';
								}
								else
								{


							?>
	                      	<img class="lazy" data-original="<?php echo base_url(); ?>thumbs/Gbaam3.jpg"  alt="<?php echo $gv->artist_name.' - '.$gv->title; ?>" width="107" height="60" />  
	                        <?php
	                        	}
							}
							else{ ?>
							<img class="lazy" data-original="<?php echo base_url(); ?>thumbs/<?php echo $gv->thumb; ?>"  alt="<?php echo $gv->artist_name.' - '.$gv->title; ?>" width="107" height="60" /> 
							<?php } ?>
	                      </a>
	                      <a title="<?php echo $gv->title; ?>"  href="<?php echo base_url(); ?>video/<?php echo hashids_encrypt($gv->id); ?>" class="title">
	                      <?php echo $gv->artist_name.' - '.character_limiter($gv->title,40); ?>
	                      
	                      </a>
	                  </li>
	                  <?php 
	                      $i++;
	                      endforeach; 
	                  ?>
	              </ul> 
	                  
				<a style="text-align:center;background:#f90;border-radius:5px;float:right;width:100%;font-size:20px;text-shadow:0px 0px 5px black;padding:5px 0;" href="<?php echo base_url(); ?>video/all">View All Videos</a>
		  	</div>
		</div>

		</div>

    	
    </div>


    	<?php echo $this->load->view('inc/footer'); ?>

<?php $this->load->view('inc/disqus_help'); ?>


   <?php /*
   <script>
    $(window).bind("load", function() {
       $.getScript('<?php echo base_url(); ?>assets/js/social.js', function() {});
    });
</script>*/ ?>
</body>
</html>
