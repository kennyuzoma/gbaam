<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title><?php echo $video_title; ?></title>
	<link type="text/css" rel="stylesheet" href="https://vjs.zencdn.net/4.5.1/video-js.css" />
	<style type="text/css">
	.vjs-default-skin div.vjs-big-play-button{
		top: 50%!important; 
		left: 50%!important; 
		width:120px!important; 
		height:80px!important; 
		margin: -40px 0px 0px -60px!important; 
		}
	</style>
	<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
	<!-- For the twitter icon. -->
    <link href="https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">  

    <style>
      .vjs-control.vjs-tweet-button:before {
        font-family: FontAwesome;
        content: "\f081";
      }
      </style>
      <script src="https://vjs.zencdn.net/4.5.1/video.js"></script>

  <script src="<?php echo base_url(); ?>assets/packs/youtube.js"></script>
	
</head>

<body style="margin:0;padding:0;" >
	

	<video id="vid1" src="" class="video-js vjs-default-skin" 
	<?php 

		if($auto == 'auto') 
		{ 
			echo 'autoplay="autoplay" ';
			echo 'preload="auto" ';
    	} 
	
    ?>
	 controls width="100%" height="100%">

	  </video>
<script>
      videojs.Tweet = videojs.Button.extend({
      /** @constructor */
        init: function(player, options){
          videojs.Button.call(this, player, options);
          this.on('click', this.onClick);
        }
      });

      videojs.Tweet.prototype.onClick = function() {
        alert("TWEET!");
      };

      // Note that we're not doing this in prototype.createEl() because
      // it won't be called by Component.init (due to name obfuscation).
      var createTweetButton = function() {
        var props = {
            className: 'vjs-tweet-button vjs-control',
            innerHTML: '<div class="vjs-control-content"><span class="vjs-control-text">' + ('Tweet') + '</span></div>',
            role: 'button',
            'aria-live': 'polite', // let the screen reader user know that the text of the button may change
            tabIndex: 0
          };
        return videojs.Component.prototype.createEl(null, props);
      };

      var tweet;
      videojs.plugin('tweet', function() {
        var options = { 'el' : createTweetButton() };
        tweet = new videojs.Tweet(this, options);
        this.controlBar.el().appendChild(tweet.el());
      });

    </script>
	  <script>
  videojs('vid1', { "techOrder": ["youtube"], "src": "<?php echo $video_src; ?>" }, {
        plugins : { tweet : {} }
      }
  )/*.ready(function() {
    // Cue a video using ended event
	// Most video.js events are supported
    this.one('ended', function() {
      this.src('http://www.youtube.com/watch?v=jofNR_WkoCE');
	  this.play();
    });
  })*/;
  </script>


</body>
</html>
