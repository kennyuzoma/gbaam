<!DOCTYPE html>
<html>
<head>

	<?php 
		//Load the reqiured Files View
		echo $this->load->view('inc/req_files'); 
	?>

</head>

<body class="singleVideo">
	<?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
<div id="wrapper">

  <?php 
    //Load the reqiured header view
    echo $this->load->view('inc/header'); 
  ?>

	<div id="vidwrap">
		
		<div class="left">
			
			<?php 
				$seg2 = $this->uri->segment(2);
				if($seg2 != 'all'): 
			?>
			<div style="float:left;width:630px;background:black;padding:10px;margin-bottom:10px;border-radius:10px;">
			<h2 style="border-bottom:1px solid #555;amargin-bottom:10px;padding-bottom:10px;">Featured Videos</h2>

			<ul class="slots">
				<?php 

		                for ($i=1; $i <= $this->config->item('top_vid_count'); $i++):

		                	if($this->Video_model->getFeatVideo_slot_count('two',$i) == 0)
                				continue;

		                    $g = $this->Video_model->getFeatVideo_slot('two',$i);

		            ?>
			    	<li>
			            <a title="<?php echo $g->artist_name.' - '.$g->title; ?>" href="<?php echo base_url(); ?>video/<?php echo hashids_encrypt($g->type_id); ?>">
		                	<img class="img" style="border:1px solid white;" src="<?php 

		                	if($g->thumb == '')
		                	{
		                		if($g->yt == 1){

									echo 'http://img.youtube.com/vi/'.youtube_id_from_url($g->video_src).'/mqdefault.jpg';
								}
							}
								else
									{

		                				echo base_url() . 'thumbs/' . $g->thumb; 
		                			}
		                	

		                	?>" alt="<?php echo $g->artist_name.' - '.$g->title; ?>" /><BR>
		                    <?php echo character_limiter($g->artist_name.' - '.$g->title,35); ?>
			            </a>
		            </li>
		            
			    	<?php 
		                endfor; 
		            ?>

			</ul>
			
			<div id="five">
				<ul>
			    	<?php 

		                for ($i=1; $i < 6; $i++):

		                	if($this->Video_model->getFeatVideo_slot_count('five',$i) == 0)
                				continue;

		                    $g = $this->Video_model->getFeatVideo_slot('five',$i);



		            ?>
			    	<li id="vi1">
			            <a title="<?php echo $g->artist_name.' - '.$g->title; ?>" href="<?php echo base_url(); ?>video/<?php echo hashids_encrypt($g->type_id); ?>">
		                	<img height="65" src="<?php 

		                	if($g->thumb == '')
		                	{
		                		if($g->yt == 1){

									echo 'http://img.youtube.com/vi/'.youtube_id_from_url($g->video_src).'/mqdefault.jpg';
								}
							}
								else
									{

		                				echo base_url() . 'thumbs/' . $g->thumb; 
		                			}
		                	

		                	?>" alt="<?php echo $g->artist_name.' - '.$g->title; ?>" /><BR>
		                    <?php echo character_limiter($g->artist_name.' - '.$g->title,15); ?>
			            </a>
		            </li>
		            
			    	<?php 
		                endfor; 
		            ?>
				</ul>
			</div>
		</div>

		<?php endif; ?>
			<h2 style="float:left;width:640px;text-align:Center;margin-left:10px;<?php if($seg2 == 'all') echo 'margin-top:10px;'; ?>">All Music Videos</h2>
			<ul id="vidfeed"> 
               
						
                  <?php 
                  	/*
                  	echo '<pre>';
                  	echo print_r($getVideos);
                  	echo '</pre>';
*/
					foreach($getVideos as $d): 

						$mysql_date = $d->n_date_created;
                  		$time = strtotime($mysql_date);
						$new_date = date('F d, Y', $time);

						

				?>
						<li class="thedate">
							<a title="<?php echo $new_date ?>" id="<?php echo $d->n_date_created; ?>" class="date"><?php echo $new_date ?></a>
						</li>

					<?php  foreach($this->Video_model->fsv_dategroup($d->n_date_created) as $gv): ?>
	              	<li class="vid">
	                  	<a title="<?php echo $gv->artist_name.' - '.$gv->title; ?>" href="<?php echo base_url(); ?>video/<?php echo hashids_encrypt($gv->id); ?>" class="img">
                        	<?php 
							if($gv->thumb == ''){

								if($gv->yt == 1){
									echo '<img class="lazy" alt="'.$gv->artist_name.' - '.$gv->title.'"  data-original="http://img.youtube.com/vi/'.youtube_id_from_url($gv->video_src).'/mqdefault.jpg" width="200"
height="112" />';
								}
								else
								{


							?>
	                      	<img class="lazy" data-original="<?php echo base_url(); ?>thumbs/Gbaam3.jpg"  alt="<?php echo $gv->artist_name.' - '.$gv->title; ?>" width="200"
height="112" />  
                            <?php
                            	}
							}
							else{ ?>
							<img class="lazy" data-original="<?php echo base_url(); ?>thumbs/<?php echo $gv->thumb; ?>"  alt="<?php echo $gv->artist_name.' - '.$gv->title; ?>" width="200"
height="112" /> 
							<?php } ?>
	                      </a>
	                      <a title="<?php echo $gv->title; ?>"  href="<?php echo base_url(); ?>video/<?php echo hashids_encrypt($gv->id); ?>" class="title">
	                      	<?php
	                      		$the_title = $gv->artist_name.' - '.$gv->title;
	                      	?>

	                      <?php echo character_limiter($the_title,47); ?>
	                      </a>
                          
	                      <span class="views"><?php echo number_format($gv->v_count); ?> Views</span>
	  					
	  					
	                  </li>
                      <?php endforeach; ?>
                      <li class="prevdate" id="">
						  <?php endforeach; ?>
					</li>
	        </ul> 

	        <?php if(isset($pagi_links)) echo $pagi_links; ?>

		</div>

		<?php $this->load->view('inc/rightsidebar'); ?>

		<div style="clear:both;"></div>

	</div>
           


    	<?php echo $this->load->view('inc/footer'); ?>

</body>
</html>
