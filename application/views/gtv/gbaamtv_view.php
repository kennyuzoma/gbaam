
<!DOCTYPE html>
<html>
<head>

	<?php 
    //Load the reqiured Files View
	echo $this->load->view('inc/req_files'); 
	?>

<?php /*
  <meta property="og:title" content="GbaamTV | <?php echo $title; ?>" />
  <meta property="og:type" content="video" />
  <meta property="og:url" content="<?php echo base_url().$this->uri->uri_string(); ?>" />
  <meta property="og:image" content="<?php 

                        if($thumb == '')
                        {
                          if($yt == 1){

                            echo 'http://img.youtube.com/vi/'.youtube_id_from_url($video_src).'/3.jpg';
                          }
                          else
                          {
                            echo base_url()."thumbs/Gbaam3.jpg";
                          }
                        }
                        else
                          {

                              echo base_url() . 'thumbs/' . $thumb; 
                            }
                        

                        ?>" />
  <meta property="og:description" content="<?php echo htmlentities($video_desc,ENT_QUOTES); ?>" /> 
  <meta property="og:site_name" content="Gbaam" />
*/ ?>

<style>
.readmore-js-toggle{
	float: left;
	width: 500px;
	text-align: center;
	font-weight: normal;
	font-size: inherit;
	color: #f90;
	font-size: 13px;
	font-weight:bold;
	margin-top:5px;

}

.thetxtt{display:none;}

</style>
<script>

$(function(){


	$('.thetxtt').show();

	$('.thedesctxt_cont').readmore({
		speed: 75,
		maxHeight: 35,
		embedCss: false,
		sectionCSS: '',
		moreLink: '<a href="#">Show More</a>',
		lessLink: '<a href="#">Show Less</a>'
	});


	$('.morevids').on('click', function(){
		$(this).addClass('tabsel');
		$('.commentstab').removeClass('tabsel');
		$('#disqus_thread').hide();
		$('.more_section').show();
	});

	$('.commentstab').on('click', function(){
		$(this).addClass('tabsel');
		$('.morevids').removeClass('tabsel');
		$('#disqus_thread').show();
		$('.more_section').hide();
	});

});

</script>
<script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#async=1#pubid=ra-4f1e445964f67956"></script>
<script type="text/javascript">
function initAddThis() 
{
	addthis.init()
}
     // After the DOM has loaded...
     $(window).bind("load", function() {
     	initAddThis();
     });
     </script>

 </head>

 <body class="singleVideo">
 	<?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>

 	<div id="wrapper">

 		<?php 
    //Load the reqiured header view
 		echo $this->load->view('inc/header'); 
 		?>

 		<div class="vidmain">

 			<?php if($status == '2'): ?>

			<div style="float:left;width:100%;margin-bottom:20px;text-align:center;font-size:25px;background:#dc9001;border-radius:5px;text-shadow: 0px 0px 3px black;">Unlisted</div>
			
			<?php elseif($status == '3'): ?>

			<div style="float:left;width:100%;margin-bottom:20px;text-align:center;font-size:25px;background:#dc9001;border-radius:5px;text-shadow: 0px 0px 3px black;">Private</div>
			
			<?php endif; ?>

 			<iframe id="vidload" style="-webkit-box-shadow: 0px 0px 11px rgba(0, 0, 0, 0.78);
 			-moz-box-shadow:    0px 0px 11px rgba(0, 0, 0, 0.78);
 			box-shadow:         0px 0px 11px rgba(0, 0, 0, 0.78); float:left;margin-bottom:10px;margin-left:43px;margin-top:7px;" width="854" height="480" src="<?php echo $this->config->item('player_url').'video/embed/'.$videoid.'/auto/tv'; ?>" frameborder="0" scrolling="no" allowfullscreen></iframe>

       <? /* <ul class="soci_actions2 addthis_toolbox addthis_default_style" style="width:930px;margin:8px 0px 11px 0px;">
              <li class="fb">
                <div class="fb-share-button" data-href="<?php echo base_url().$this->uri->uri_string(); ?>" data-type="button_count" style="float:left;"></div>
              </li>
              <li class="twit">
                 <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo base_url().$this->uri->uri_string(); ?>" data-text="<?php echo $title; ?>">Tweet</a>
              </li>
              <li class="g">
                <div class="g-plusone" data-size="medium" data-href="<?php echo base_url().$this->uri->uri_string(); ?>"></div>
              </li>
              <li claass="share"><a class="addthis_counter addthis_pill_style"></a></li>
            </ul>
        */ ?>
        <?php
        $share_title=urlencode($title. ' | GBAAM');
        $url = urlencode(base_url().$this->uri->uri_string());
        $summary=urlencode(strip_tags(character_limiter($video_desc,300)));
        $image=urlencode(youtube_meta_image($videoid));
        ?>

        <ul class="soci_actions3 addthis_toolbox addthis_default_style" style="width:930px;margin:10px 0px 30px 0px;">
        	<li class="">

        		<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>&amp;p[images][0]=<?php echo $image;?>" class="socialPop">
        			<img src="<?php echo base_url(); ?>assets/img/social/facebook-variation.png" />
        		</a>
        	</li>
        	<li class="">
        		<a href="https://twitter.com/intent/tweet?text=<?php echo $share_title; ?>&url=<?php echo $url; ?>&related=" class="socialPop">
        			<img src="<?php echo base_url(); ?>assets/img/social/twitter-variation.png" />
        		</a>
        	</li>
        	<li class="">
        		<a href="https://plus.google.com/share?url=<?php echo $url; ?>&t=<?php echo $share_title; ?>" class="socialPop">
        			<img src="<?php echo base_url(); ?>assets/img/social/gplus-variation2.png" />
        		</a>
        	</li>
        	<li class="share"><a class="addthis_counter addthis_pill_style"></a></li>
        </ul>
        <div class="secndwrapper">
        	<!-- <h2>GbaamTV - The Verdict</h2> -->



			<div class="gtvinfobox">
	        	<h1 class="gggtitle" style="margin-top:0px;"><?php echo $title; ?></h1>
	        	<div class="gtvdescbox">
	        		<div style="float:left;width:500px;margin-right:15px;">
	        			<div class="thedesctxt_cont" style="float:left;width:500px;">
	        				<b>Posted on <?php echo $date; ?></b> <br>
	        				<span class="thetxtt">
	        					<?php echo nl2br(look4links($video_desc)); ?>
	        					<noscript>
	        						<?php echo nl2br(look4links($video_desc)); ?>
	        					</noscript>
	        				</span>




	        			</div>
	        		</div>
	        		<div style="float:left;width:100px;">
	        			<div style="float:right:width:124px;background:black;text-align:center;padding:5px 0px;border-radius:5px;margin-bottom:10px;">
	        				<span style="font-size:12px;float:left;width:100%;">Views</span><span style="font-size:17px;width:130px;margin:0 8px;"><?php echo $v_count; ?></span>
	        			</div>

	        		</div>

	        	</div>
	        </div>


        
        <div class="gtvtabs" >
        	<a href="#disqus_thread" data-disqus-identifier="<?php echo $my_identifier; ?>" style="width:285px;font-size:20px;padding:5px 0px"  class="tab tabsel commentstab">0 Comments</a>
        	<a style="width:284px;font-size:20px;padding:5px 0px" class="tab morevids">More Videos</a>

        </div>
        <div id="disqus_thread" style="float:left;width:620px;background:#1c1c1c;padding:10px;margin-top:10px;"></div>
        <?php /*<div id="disqus_thread" style="float:left;width:630px;"></div>*/ ?>

        <div class="more_section" style="display:none;">

        	<ul>

        		<span class="sixfortyheader">More Videos</span>
        		<ul id="gtvvids" class="gtvfeat" >
        			<?php /*<a href="#" class="clickme">Click me</a>*/ ?>
        			<?php foreach($getLatestVideos as $v): ?>
        			<li id="<?php echo $v->id; ?>">
        				<a title="<?php echo $v->title; ?>"  href="<?php echo base_url(); ?>tv/v/<?php echo hashids_encrypt($v->id); ?>">
        					<img src="<?php 

  					if($v->thumb == '')
  					{
  						if($v->yt == 1){

  							echo 'http://img.youtube.com/vi/'.youtube_id_from_url($v->video_src).'/mqdefault.jpg';
  						}
  						else
  						{
  							echo base_url()."thumbs/Gbaam3.jpg";
  						}
  					}
  					else
  					{

  						echo base_url() . 'thumbs/' . $v->thumb; 
  					}


  					?>" alt="<?php echo $v->title; ?>"/>

        					<?php echo character_limiter($v->title,17); ?>
        				</a>
        			</li>
        		<?php endforeach; ?>

        	</ul>
        	<div class="pagination" style="margin-bottom:10px;">
        		<?php
        		$page_count = $this->Video_model->create_ajax_pagination_links($gtv_cat);
        		if($page_count > 1){
        			echo '<a href="#" class="pagination_link sel" id="1">1</a>';
        			for ($i = 2; $i <= $page_count; $i++){

        				echo '<a href="#" class="pagination_link" id="'.$i.'">'. $i.'</a>';
        			}
        		}
        		?>
        	</div>
        </ul>

    </div>


</div>

<?php $this->load->view('inc/rightsidebar'); ?>

</div>

<?php echo $this->load->view('inc/footer'); ?>

<script>
$(function() {
	var base_url = '<?php echo base_url(); ?>';
	var page = '<?php echo $gtv_cat; ?>';

	$('.pagination_link').on('click', function(){

		var pagi_pos = $(this).attr('id');

        //remove prev select
        $('.pagination_link').removeClass('sel');

        $(this).addClass('sel');

        $('.gtvfeat li').hide();

        $.post(base_url+"ajax/load_gtv_videos/"+pagi_pos+"/"+page, $(this).serialize(), function(data) {

        	$(data).hide().appendTo(".gtvfeat").slideDown(200);
        });
        return false;
    });
});
</script>

<?php $this->load->view('inc/disqus_help'); ?>

<script type="text/javascript">
/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
var disqus_shortname = 'gbaam'; // required: replace example with your forum shortname

/* * * DON'T EDIT BELOW THIS LINE * * */
(function () {
	var s = document.createElement('script'); s.async = true;
	s.type = 'text/javascript';
	s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
	(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
}());
</script>
</body>

</html>