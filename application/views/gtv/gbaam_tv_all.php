<!DOCTYPE html>
<html>
<head>

  <?php 
    //Load the reqiured Files View
    echo $this->load->view('inc/req_files'); 
  ?>
    
</head>

<body class="singleVideo">
  <?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
<div id="wrapper">

  <?php 
    //Load the reqiured header view
    echo $this->load->view('inc/header'); 
  ?>
    <div class="vidmain">
      <div style="float:left;width:640px;">
      
       <div class="gtvtabs">
             <?php 
                $seg = $this->uri->segment(3);
                if(is_numeric($seg))
                  $seg = '';
              ?>
		   <a href="<?php echo base_url(); ?>tv/all/" class="tab <?php echo $seg == '' || '' ? 'tabsel' : '' ?>">All Videos</a>
             <a href="<?php echo base_url(); ?>tv/all/variety" class="tab <?php echo $seg == 'variety' || '' ? 'tabsel' : '' ?>">Variety</a>
            <a href="<?php echo base_url(); ?>tv/all/interviews" class="tab <?php echo $seg == 'interviews' ? 'tabsel' : '' ?>">Interviews</a>
		      <a href="<?php echo base_url(); ?>tv/all/shortfilms" class="tab <?php echo $seg == 'shortfilms' ? 'tabsel' : ''?>">Short Films</a>
		      <a href="<?php echo base_url(); ?>tv/all/reality" class="tab <?php echo $seg == 'reality' ? 'tabsel' : '' ?>">Reality Shows</a>
		      <a href="<?php echo base_url(); ?>tv/all/documentary" class="tab <?php echo $seg == 'documentary' ? 'tabsel' : '' ?>">Documentary</a>
		      
            </div>
        <ul id="GbaamTVlist">
        	<?php foreach($getVideos as $v): ?>  
            	<li>
                	<a title="<?php echo $v->title; ?>" href="<?php echo base_url(); ?>tv/v/<?php echo hashids_encrypt($v->vid); ?>">
                	
						<img src="<?php 

  					if($v->thumb == '')
  					{
  						if($v->yt == 1){

  							echo 'http://img.youtube.com/vi/'.youtube_id_from_url($v->video_src).'/mqdefault.jpg';
  						}
  						else
  						{
  							echo base_url()."thumbs/Gbaam3.jpg";
  						}
  					}
  					else
  					{

  						echo base_url() . 'thumbs/' . $v->thumb; 
  					}


  					?>" alt="<?php echo $v->title; ?>" style="width:120px;"/>

	                    <span class="name"><?php echo $v->title; ?> </span> 
		            </a>
                    <span class="desc">
						<?php if($v->description != '') { 
							  	echo word_limiter($v->description,17); 
								?> <a href="<?php echo base_url(); ?>tv/v/<?php echo hashids_encrypt($v->vid); ?>">more</a>
                        <?php } ?></span>
                </li>
            <?php endforeach; ?>


           
		</ul>
        <div style="float:left;width:100%;margin-top:10px;margin-bottom:20px;">
        <?php echo $pagi_links; ?>
        </div>
      </div>
      
      <?php $this->load->view('inc/rightsidebar'); ?>

  </div>
  <?php echo $this->load->view('inc/footer'); ?>
    
   

</body>
</html>