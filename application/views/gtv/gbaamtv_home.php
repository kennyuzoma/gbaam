<!DOCTYPE html>
<html>
<head>

	<?php 
    //Load the reqiured Files View
	echo $this->load->view('inc/req_files'); 
	?>

</head>

<body class="singleVideo">
	<?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
	<div id="fb-root"></div>

	<div id="wrapper">

		<?php 
    //Load the reqiured header view
		echo $this->load->view('inc/header'); 
		?>

		<div class="vidmain">
			<?php
			if($this->uri->segment(2) != '')
			{
				$auto = 'noauto';
			}
			else
				$auto = 'noauto';
				//$auto = 'auto';
			?>

			<iframe id="vidload" style="-webkit-box-shadow: 0px 0px 11px rgba(0, 0, 0, 0.78);
			-moz-box-shadow:    0px 0px 11px rgba(0, 0, 0, 0.78);
			box-shadow:         0px 0px 11px rgba(0, 0, 0, 0.78);float:left;margin-bottom:20px;margin-left:43px;margin-top:7px;" width="854" height="480" src="<?php echo $this->config->item('player_url').'video/embed/'.hashids_encrypt($videoid).'/'.$auto.'/tv'; ?>" frameborder="0" scrolling="no" allowfullscreen></iframe>
			<div class="secndwrapper">

				<div class="gtvinfobox">
					<h1 class="gggtitle" style="margin-top:0px;"><?php echo $title; ?></h1>

					<div class="gtvdescbox">

						<div style="float:left;width:520px;line-height:15px;">
							<b><u><?php echo $date; ?></u></b>: <?php echo $video_desc; ?>
						</div>

					</div>
				</div>

  <?php 
  if($this->uri->segment(1) != 'tv'){
  	?>
  	<div class="gtvtabs">
  		<a href="#" style="width:280px;font-size:20px;padding:5px 0px" class="tab tabsel morevids">More Videos</a>
  		<a href="#" style="width:280px;font-size:20px;padding:5px 0px"  class="tab comments">Comments</a>
  	</div>
  	<?php } ?>

  	<?php
  	if($this->uri->segment(1) == 'tv'){
  		?>
  		<div class="gtvtabs">
  			<?php $seg = $this->uri->segment(2); ?>
  			<a href="<?php echo base_url(); ?>tv/" class="tab <?php echo $seg == '' ? 'tabsel' : '' ?>" title="Featured Videos">Featured Videos</a>

  			<a style="padding:5px 10px !important;" href="<?php echo base_url(); ?>tv/variety" class="tab <?php echo $seg == 'variety' ? 'tabsel' : '' ?>" title="Variety">Must See</a>
  			<a href="<?php echo base_url(); ?>tv/interviews" class="tab <?php echo $seg == 'interviews' ? 'tabsel' : '' ?>" title="Interviews">Interviews</a>
  			<a href="<?php echo base_url(); ?>tv/shortfilms" class="tab <?php echo $seg == 'shortfilms' ? 'tabsel' : ''?>" title="Short Films">Short Films</a>
  			<a href="<?php echo base_url(); ?>tv/reality" class="tab <?php echo $seg == 'reality' ? 'tabsel' : '' ?>" title="Reality Shows">Reality Shows</a>
  			<a href="<?php echo base_url(); ?>tv/documentary" class="tab <?php echo $seg == 'documentary' ? 'tabsel' : '' ?>" title="Documentary">Documentary</a>
  		</div>
  		<?php } else { ?>
  		<div class="gtvtabs">
  			<?php 
  			$seg = $gtv_cat; 

  			?>
  			<a href="<?php echo base_url(); ?>tv/" class="tab <?php echo $gtv_cat == '' ? 'tabsel' : '' ?>" title="Featured Videos">Featured Videos</a>
  			<a style="padding:5px 10px !important;" href="<?php echo base_url(); ?>tv/variety" class="tab <?php echo $gtv_cat == 'variety' ? 'tabsel' : '' ?>" title="Variety">Must See</a>
  			<a href="<?php echo base_url(); ?>tv/interviews" class="tab <?php echo $seg == 'interview' ? 'tabsel' : '' ?>" title="Interviews">Interviews</a>
  			<a href="<?php echo base_url(); ?>tv/shortfilms" class="tab <?php echo $seg == 'shortfilms' ? 'tabsel' : ''?>" title="Short Films">Short Films</a>
  			<a href="<?php echo base_url(); ?>tv/reality" class="tab <?php echo $seg == 'reality' ? 'tabsel' : '' ?>" title="Reality Shows">Reality Shows</a>
  			<a href="<?php echo base_url(); ?>tv/documentary" class="tab <?php echo $seg == 'documentary' ? 'tabsel' : '' ?>" title="Documentary">Documentary</a>
  		</div>
  		<?php } ?>
  		<span class="sixfortyheader" title="Featured Videos">Featured Videos</span>
  		<ul id="gtvvids">

  			<?php 
  			$count = $this->Video_model->getFeatVideoTypeCount($type);
  			for ($i=1; $i <= $count; $i++):

  				$g = $this->Video_model->getFeatVideo_slot($type,$i);

  					if($this->Video_model->getFeatVideo_slot_count($type,$i) == 0)
  						continue;

  			?>
  			<li>
  				<a title="<?php echo $g->title; ?>" href="<?php echo base_url(); ?>tv/v/<?php echo hashids_encrypt($g->type_id); ?>">
  					<img src="<?php 

  					if($g->thumb == '')
  					{
  						if($g->yt == 1){

  							echo 'http://img.youtube.com/vi/'.youtube_id_from_url($g->video_src).'/mqdefault.jpg';
  						}
  						else
  						{
  							echo base_url()."thumbs/Gbaam3.jpg";
  						}
  					}
  					else
  					{

  						echo base_url() . 'thumbs/' . $g->thumb; 
  					}


  					?>" alt="<?php echo $g->title; ?>" /><BR>
  					<?php echo character_limiter($g->title,17); ?>
  				</a>
  			</li>

  			<?php 
  			endfor; 
  			?>


  		</ul>

  		<span class="sixfortyheader">Latest Videos</span>
  		<ul id="gtvvids" class="gtvfeat" >
  			<?php /*<a href="#" class="clickme">Click me</a>*/ ?>
  			<?php foreach($getLatestVideos as $v): ?>
  			<li id="<?php echo $v->id; ?>">
  				<a title="<?php echo $v->title; ?>"  href="<?php echo base_url(); ?>tv/v/<?php echo hashids_encrypt($v->id); ?>">
  					
  					<img src="<?php 

  					if($v->thumb == '')
  					{
  						if($v->yt == 1){

  							echo 'http://img.youtube.com/vi/'.youtube_id_from_url($v->video_src).'/mqdefault.jpg';
  						}
  						else
  						{
  							echo base_url()."thumbs/Gbaam3.jpg";
  						}
  					}
  					else
  					{

  						echo base_url() . 'thumbs/' . $v->thumb; 
  					}


  					?>" alt="<?php echo $v->title; ?>"/>
  					

  					<?php echo character_limiter($v->title,17); ?>
  				</a>
  			</li>
  		<?php endforeach; ?>

  	</ul>
  	<div class="pagination" style="margin-bottom:10px;">
  		<?php
  		$page_count = $this->Video_model->create_ajax_pagination_links($seg);
  		if($page_count > 1){
  			echo '<a href="#" class="pagination_link sel" id="1">1</a>';
  			for ($i = 2; $i <= $page_count; $i++){

  				echo '<a href="#" class="pagination_link" id="'.$i.'">'. $i.'</a>';
  			}
  		}
  		?>
  	</div>
  	<a class="viewalink gtvviewall" href="<?php echo base_url(); ?>tv/all/<?php echo $seg; ?>">View All Videos</a>
  </div>

  <?php $this->load->view('inc/rightsidebar'); ?>

</div>

<?php echo $this->load->view('inc/footer'); ?>
<script>
$(function() {
	$('a[href=#disqus_thread]').on('click', function(event) {

      // This will prevent the default action of the anchor
      event.preventDefault();

      // Failing the above, you could use this, however the above is recommended
      return false;

  });

	var base_url = '<?php echo base_url(); ?>';
	var page = '<?php echo $this->uri->segment(2); ?>';

	$('.pagination_link').on('click', function(){

		var pagi_pos = $(this).attr('id');

				//remove prev select
				$('.pagination_link').removeClass('sel');

				$(this).addClass('sel');

				$('.gtvfeat li').hide();

				$.post(base_url+"ajax/load_gtv_videos/"+pagi_pos+"/"+page, $(this).serialize(), function(data) {

					$(data).hide().appendTo(".gtvfeat").slideDown(200);
				});
				return false;
			});
});
</script>
</body>

</html>