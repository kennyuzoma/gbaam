<!DOCTYPE html>
<html>
<head>

	<?php 
	//Load the reqiured Files View
	echo $this->load->view('inc/req_files'); 
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/users.css">
	<script type="text/javascript">
		$(function(){

			$('.mini_nav li').on('click', function(){

				var type = $(this).attr('id');

				$('.mini_nav li').siblings().removeClass('sel');
				$(this).addClass('sel');

				if(type == 'all')
					$('.content_boxes .row').show();
				else
				{
					$('.content_boxes .row').hide();
					$('.js_box_'+type).show();
				}

			});

			$('.fancybox').fancybox();

		})
	</script>
</head>

<body class="singleVideo">
	<?php echo $this->load->view('inc/admin_nav'); ?>
	<?php echo $this->load->view('inc/analytics'); ?>
	<div id="wrapper">

		<?php 
		//Load the reqiured header view
		echo $this->load->view('inc/header'); 
		?>

		<div class="vidmain" id="users">

			<div id="left">

				<div class="breadcrumbs">
					<a href="<?php echo base_url() ;?>" style="padding-left:10px;">Home</a> / <span class="name"><?php echo $display_name; ?></span>

				</div>

				<div class="profile-box">

					<div class="left">
						<a class="fancybox" rel="gallery1" href="<?php echo get_image('user',$user->profile_img,$user->img_hash,'500'); ?>">
							<img class="propic" src="<?php echo get_image('user',$user->profile_img,$user->img_hash,'200'); ?>">
						</a>
						<br><br>
						<span class="proview">Profile Views: <?php echo $user->views; ?></span>
					</div>

					<div class="right">
						<span class="name"><?php echo $display_name; ?></span>
						<div class="info">
							<div class="row">
								<span class="q">Sex:</span>
								<span class="a">Male</span>
							</div>
							<div class="row">
								<span class="q">Bio:</span>
								<span class="a"><?php echo $bio; ?></span>
							</div>
						</div>
					</div>

				</div>

				<ul class="navtabs">
					<li class="sel south" alt="Coming Soon!" original-title="Coming Soon!">Favorites</li>
					<li class="south" alt="Coming Soon!" original-title="Coming Soon!">Comments</li>
					<li class="south" alt="Coming Soon!" original-title="Coming Soon!">Likes</li>
					<li class="south" alt="Coming Soon!" original-title="Coming Soon!">Friends</li>
					<?php
					/*
					<li class="south subtab">
						<a href="<?php echo base_url(); ?>main/submit/video">Submit Content!</a>
					</li>
					*/ ?>
				</ul>

				<div class="content_boxes">
					<span style="text-align:center;width:100%;float:left;">Coming soon!</span>
<?php /*
					<div class="inbox fav">

						<ul class="mini_nav">
							<li id="all" class="sel">All</li>
							<li id="mixtapes">Mixtapes</li>
							<li id="songs">Songs</li>
							<li id="videos">Videos</li>
						</ul>

						<div class="row js_box_mixtapes">
							<div class="heading">
								<h3>Mixtapes (20)</h3><a href="#" class="viewall">View All</a>
							</div>
							<ul>
								<li>
									<a href="#">
										<img src="http://gbaam.com/data/uploaded_images/mixtape/166_7e7757b1e12abcb736ab9a754ffb617a_200.jpg">
										<span class="text">February Stories - Kelechi</span>
									</a>
								</li>

								<li>
									<a href="#">
										<img src="http://gbaam.com/data/uploaded_images/mixtape/166_7e7757b1e12abcb736ab9a754ffb617a_200.jpg">
										<span class="text">February Stories - Kelechi</span>
									</a>
								</li>

								<li>
									<a href="#">
										<img src="http://gbaam.com/data/uploaded_images/mixtape/166_7e7757b1e12abcb736ab9a754ffb617a_200.jpg">
										<span class="text">February Stories - Kelechi</span>
									</a>
								</li>

								<li>
									<a href="#">
										<img src="http://gbaam.com/data/uploaded_images/mixtape/166_7e7757b1e12abcb736ab9a754ffb617a_200.jpg">
										<span class="text">February Stories - Kelechi</span>
									</a>
								</li>

								<li>
									<a href="#">
										<img src="http://gbaam.com/data/uploaded_images/mixtape/166_7e7757b1e12abcb736ab9a754ffb617a_200.jpg">
										<span class="text">February Stories - Kelechi</span>
									</a>
								</li>

							</ul>
						</div>

						<div class="row js_box_songs">
							<div class="heading">
								<h3>Songs (20)</h3><a href="#" class="viewall">View All</a>
							</div>
							<ul>
								<li>
									<a href="#">
										<img src="http://gbaam.com/data/uploaded_images/mixtape/166_7e7757b1e12abcb736ab9a754ffb617a_200.jpg">
										<span class="text">February Stories - Kelechi</span>
									</a>
								</li>

								<li>
									<a href="#">
										<img src="http://gbaam.com/data/uploaded_images/mixtape/166_7e7757b1e12abcb736ab9a754ffb617a_200.jpg">
										<span class="text">February Stories - Kelechi</span>
									</a>
								</li>

								<li>
									<a href="#">
										<img src="http://gbaam.com/data/uploaded_images/mixtape/166_7e7757b1e12abcb736ab9a754ffb617a_200.jpg">
										<span class="text">February Stories - Kelechi</span>
									</a>
								</li>

								<li>
									<a href="#">
										<img src="http://gbaam.com/data/uploaded_images/mixtape/166_7e7757b1e12abcb736ab9a754ffb617a_200.jpg">
										<span class="text">February Stories - Kelechi</span>
									</a>
								</li>
							</ul>
						</div>

						<div class="row video js_box_videos">
							<div class="heading">
								<h3>Music Videos (20)</h3><a href="#" class="viewall">View All</a>
							</div>
							<ul>
								<li>
									<a href="#">
										<img src="http://img.youtube.com/vi/vDHBe1BA-Nk/mqdefault.jpg">
										<span class="text">Dorobucci - Maveb</span>
									</a>
								</li>

								<li>
									<a href="#">
										<img src="http://img.youtube.com/vi/vDHBe1BA-Nk/mqdefault.jpg">
										<span class="text">Dorobucci - Maveb</span>
									</a>
								</li>

								<li>
									<a href="#">
										<img src="http://img.youtube.com/vi/vDHBe1BA-Nk/mqdefault.jpg">
										<span class="text">Dorobucci - Maveb</span>
									</a>
								</li>

								<li>
									<a href="#">
										<img src="http://img.youtube.com/vi/vDHBe1BA-Nk/mqdefault.jpg">
										<span class="text">Dorobucci - Maveb</span>
									</a>
								</li>


							</ul>
						</div>

					</div>	
*/ ?>
				</div>

			</div>

			<?php $this->load->view('inc/rightsidebar'); ?>

		</div>

		<?php echo $this->load->view('inc/footer'); ?>

	</body>
	</html>