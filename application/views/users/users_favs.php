<!DOCTYPE html>
<html>
<head>

	<?php 
	//Load the reqiured Files View
	echo $this->load->view('inc/req_files'); 
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/users.css">
	<script type="text/javascript">
		$(function(){
<?php /*
			$('.mini_nav li').on('click', function(){

				var type = $(this).attr('id');

				$('.mini_nav li').siblings().removeClass('sel');
				$(this).addClass('sel');

				if(type == 'all')
					$('.content_boxes .row').show();
				else
				{
					$('.content_boxes .row').hide();
					$('.js_box_'+type).show();
				}

			});
*/ ?>
		});
	</script>
</head>

<body class="singleVideo">
	<?php echo $this->load->view('inc/admin_nav'); ?>
	<?php echo $this->load->view('inc/analytics'); ?>
	<div id="wrapper">

		<?php 
		//Load the reqiured header view
		echo $this->load->view('inc/header'); 
		?>

		<div class="vidmain" id="users">

			<div id="left">

				<div class="breadcrumbs">
					<a href="<?php echo base_url() ;?>" style="padding-left:10px;">Home</a> / <a href="#" class="name">Kenny Uzoma</a> / Favorites - Mixtapes

				</div>

				<ul class="navtabs">
					<li class="sel">Favorites</li>
					<li class="south" alt="Coming Soon!" original-title="Coming Soon!">Comments</li>
					<li class="south" alt="Coming Soon!" original-title="Coming Soon!">Likes</li>
					<li class="south" alt="Coming Soon!" original-title="Coming Soon!">Friends</li>
					<?php
					/*
					<li class="south subtab">
						<a href="<?php echo base_url(); ?>main/submit/video">Submit Content!</a>
					</li>
					*/ ?>
				</ul>

				<div class="content_boxes">

					<div class="inbox fav">

						<ul class="mini_nav">
							<li id="mixtapes" class="sel">
								<a href="#">Mixtapes</a>
							</li>
							<li id="songs">
								<a href="#">Songs</a>
							</li>
							<li id="videos">
								<a href="#">Videos</a>
							</li>
						</ul>

						<div class="row js_box_mixtapes long">
							<div class="heading">
								<h3>Mixtapes (20)</h3>
							</div>
							<ul>
								<li>
									<a href="#">
										<img src="http://gbaam.com/data/uploaded_images/mixtape/166_7e7757b1e12abcb736ab9a754ffb617a_200.jpg">
										<div class="right">
											<span class="text">February Stories - Kelechi</span>
											<span class="desc">Presenting The February Stories EP from Bay Area singer-songwriter Kelechi. Produced by TOCHI with its elixir of RnB, Hip Hop and AfroBeat, this EP is sure to nourish your mind soul and body. Enjoy. God…</span>
										</div>
									</a>
								</li>

								

							</ul>

							<div class="pagination2">&nbsp;<strong>1</strong>&nbsp;<a href="http://localhost/Projects/gbaam/mixtapes/all/16">2</a>&nbsp;<a href="http://localhost/Projects/gbaam/mixtapes/all/16">&gt;</a>&nbsp;</div>
						</div>

					</div>	

				</div>

			</div>

			<?php $this->load->view('inc/rightsidebar'); ?>

		</div>

		<?php echo $this->load->view('inc/footer'); ?>

	</body>
	</html>