<!DOCTYPE html>
<html>
<head>

  <?php 
    //Load the reqiured Files View
    echo $this->load->view('inc/req_files'); 
  ?>

    <script>
	$(function() {


        $('a[href=#disqus_thread]').on('click', function(event) {

        // This will prevent the default action of the anchor
        event.preventDefault();

        // Failing the above, you could use this, however the above is recommended
        return false;

});
        
        $('.morevids').on('click', function(){
            $(this).addClass('tabsel');
            $('.commentstab').removeClass('tabsel');
            $('#disqus_thread').hide();
            $('.more_section').show();
        });

        $('.commentstab').on('click', function(){
            $(this).addClass('tabsel');
            $('.morevids').removeClass('tabsel');
            $('#disqus_thread').show();
            $('.more_section').hide();
        });

        $('.js_c_body').click(function(){

            $('.submit').show();
            $('.js_c_body').val('');

        });

        $('#postbox form').submit(function(){

            // if there is no comments, remove that text!
            no_comments = $('.no_comments');
            if( no_comments.length ){
                no_comments.remove();
            }

            $.post("/comments/submit", $(this).serialize(), function(data) {
                $('#comments').prepend(data);
                //increase the value of the comments
            $('.js_c_count').text( parseInt($('.js_c_count').text()) + 1 );
                

            c_body = $('.js_c_body').val();
            });

            

            /*$('#comments').prepend('<li class="comment"><img class="img" src="http://s.ytimg.com/yt/img/no_videos_140-vfl5AhOQY.png" /><span class="right"><a href="#" class="name">Kenny Uzoma</a><span class="body">'+c_body+'</span></span></li>');*/

            // put everything back to normal
            this.reset();
            $('.submit').hide();

            return false;


        });

	
		$('.open .vb').click( function() {
			
			if($('#vote li a').hasClass('vb'))
			{
				var videoid = $('.videoid').val();
				var choice = $(this).attr('id');
				var value = $('#choice'+choice).text();
				
				$('.space').prepend('<di class="result"><b>Thanks for your vote!</b></div>').show('fast'); 
				$('.result').slideDown("fast").delay(2000).slideUp('fast');
				
				 $('#choice'+choice).addClass('hov');
				 $('#vote #vi'+choice).addClass('hov');
				 
				if(value == '')
				{
					
				}
				else
				{
					value = parseInt(value, 10) || 0; // Use a radix!!!
					$('#choice'+choice).text(++value).show();
				}
				 
				$.ajax({
				  url: "<?php echo base_url(); ?>challenge/add_vote/"+videoid+"/"+choice,
				  success: function(){
				  }
				});
			}
			//$('#vote li a').removeClass('vb');
			
			return false;
		}); 

        $(".sectitle").change(function(){
            window.location=this.value;
        });
	}); 
	</script>
    
</head>

<body>

    <?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
<div id="wrapper">

  <?php 
    //Load the reqiured header view
    echo $this->load->view('inc/header'); 
  ?>

  <div class="vidmain">
  	
    
  	<?php 
        //Load challenge Sidebar View
        echo $this->load->view('inc/chal_sidebar'); 
    ?>

    <div class="secndwrapper gcwrapper" style="float:right;width:640px;">
       <!-- <h2>GbaamTV - The Verdict</h2> --> 
        
         	<span class="challtitle"><?php echo $title; ?></span>

        	<iframe id="vidload" width="640" height="360" src="<?php echo base_url(); ?>video/embed/<?php echo $videoid; ?>/auto" frameborder="0" allowfullscreen></iframe>
            
            <?php if($active_chal == FALSE): ?>
            <div style="float:left;width:620px;padding:5px 10px;margin-top:10px;background:#d86b00;text-align:center;">Voting is closed</div>
            <?php endif; ?>

            <div style="float:left;width:620px;display:none;" class="space"></div>
    		<input type="hidden" value="<?php echo $videoid; ?>" class="videoid" />

            <div style="float:left;width:405px;">
            <ul id="vote" class="voteone <?php if($active_chal == TRUE) echo 'open'; ?>">

                <li id="vi0">
                    <a href="#" class="vb" id="0">
                        <? /*<img src="<?php echo base_url(); ?>assets/img/e1.png" height="47" /> */ ?>
                       Seriously?
                    </a>

                </li>
                <li id="vi1">
                    <a href="#" class="vb" id="1">
                        <? /*<img src="<?php echo base_url(); ?>assets/img/d1.png" height="47" /> */ ?>
                       Hmmmmm
                    </a>
                </li>
                <li id="vi2">
                    <a href="#" class="vb"  id="2">
                        <? /*<img src="<?php echo base_url(); ?>assets/img/c1.png" height="47" /> */ ?>
                        Not Bad

                    </a>
                </li>
                <li id="vi3">
                    <a href="#" class="vb" id="3">
                        <? /*<img src="<?php echo base_url(); ?>assets/img/b1.png" height="47" /> */ ?>
                        Niceee
                    </a>
                </li>

                <li id="vi4">
                    <a href="#" class="vb" id="4">
                        <? /*<img src="<?php echo base_url(); ?>assets/img/a1.png" height="47" /> */ ?>
                        Gbaam!
                    </a>
                </li> 
            </ul>

            <div style="float:left;width:405px;background:url(<?php echo base_url(); ?>assets/img/heat.png); height:5px;">
                <img title="Vote now!" style="float:left;width:405px;height:5px;" src="<?php echo base_url(); ?>assets/img/heat.png" />
            </div>

            <ul id="vote2">
                <?php
                $i = 0;
                    while ($i <= 4):
                ?>
                    <li id="choice<?php echo $i; ?>" style="height:15px;">
                        <?php echo $this->Challenge_model->get_votes($videoid,$i); ?>
                    </li>
                <?php
                    $i++;
                    endwhile;
                ?>
            </ul>
            </div>

          <div style="float:left;width:235px;margin-top:3px;padding-bottom:4px;text-align:center;background: rgb(61,61,61);
background: -moz-linear-gradient(top,  rgba(61,61,61,1) 0%, rgba(14,14,14,1) 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(61,61,61,1)), color-stop(100%,rgba(14,14,14,1)));
background: -webkit-linear-gradient(top,  rgba(61,61,61,1) 0%,rgba(14,14,14,1) 100%);
background: -o-linear-gradient(top,  rgba(61,61,61,1) 0%,rgba(14,14,14,1) 100%);
background: -ms-linear-gradient(top,  rgba(61,61,61,1) 0%,rgba(14,14,14,1) 100%);
background: linear-gradient(to bottom,  rgba(61,61,61,1) 0%,rgba(14,14,14,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3d3d3d', endColorstr='#0e0e0e',GradientType=0 );

">
                <div style="float:left;width:104px;border-left:1px solid black;border-right:1px solid black;">
                    <span style="font-size:12px;float:left;width:104px;margin-top:5px;">Gbaam Rated</span>
                    <?php if($isScore == FALSE) { ?>
                    <div class="ratedgbaam" >None</div>
                    <?php } else { ?>
                    <div class="ratedgbaam score<?php if($css_score < 2)
                                                    {
                                                        echo 1;
                                                    } else {
                                                        echo $css_score;
                                                    } ?>"><?php echo $final_score; ?>%</div>
                    <?php } ?>
                </div>

                <div style="float:left;width:124px">
                <span style="font-size:12px;float:left;width:124px;margin-top:5px;margin-bottom:3px;">Views</span><span style="font-size:17px;width:130px;margin:0 8px;"><?php echo $v_count; ?></span>
                </div>

            </div>
       
       
            <div class="gtvtabs" style="float:right;margin-top:10px;">
          <a href="#disqus_thread" style="width:280px;font-size:20px;padding:5px 0px;"  class="tab tabsel commentstab"></a>
            <a style="width:280px;font-size:20px;padding:5px 0px" class="tab morevids">More Videos</a>
            
        </div>
        <?php $this->load->view('inc/disqus_help'); ?>
        <div id="disqus_thread" style="float:right;width:620px;background:#1c1c1c;padding:10px;margin-top:10px;"></div>
        <?php /*<div id="disqus_thread" style="float:left;width:630px;"></div>*/ ?>
      <script type="text/javascript">
          /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
          var disqus_shortname = 'gbaam'; // required: replace example with your forum shortname
      
          /* * * DON'T EDIT BELOW THIS LINE * * */
          (function() {
              var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
              dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
              (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
          })();
      </script>

        <ul id="vidfeed" class="more_section" style="display:none;width:640px;float:right;"> 
                        
                 <?php  foreach($this->Challenge_model->get_videos_by_cat($cat) as $gv): ?>
                    <li class="vid">
                        <a title="<?php echo $gv->title; ?>" href="<?php echo base_url(); ?>challenge/view/<?php echo $gv->id; ?>" class="img">
                            <?php 
                            if($gv->thumb == ''){

                                if($gv->yt == 1){
                                    echo '<img src="http://img.youtube.com/vi/'.youtube_id_from_url($gv->video_src).'/3.jpg" alt="'.$gv->title.'" />';
                                }
                                else
                                {


                            ?>
                            <img src="<?php echo base_url(); ?>thumbs/Gbaam3.jpg" alt="<?php echo $gv->title; ?>" />  
                            <?php
                                }
                            }
                            else{ ?>
                            <img  src="<?php echo base_url(); ?>thumbs/<?php echo $gv->thumb; ?>" alt="<?php echo $gv->title; ?>" /> 
                            <?php } ?>
                          </a>
                          <a title="<?php echo $gv->title; ?>"  href="<?php echo base_url(); ?>challenge/view/<?php echo $gv->id; ?>" class="title">
                            <?php
                                $the_title = $gv->title;
                            ?>

                          <?php echo character_limiter($the_title,47); ?>
                          </a>
                          
                          <span class="views"><?php echo number_format($gv->v_count); ?> Views</span>
                        
                        
                      </li>
                      <?php endforeach; ?>
            </ul> 
            
 </div>
        
        
        	
   </div>
    
   	    <?php echo $this->load->view('inc/footer'); ?>
 
</body>

</html>