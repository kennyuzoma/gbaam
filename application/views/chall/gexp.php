<?php $default = 'The Azonto'; ?>
<!DOCTYPE html>
<html>
<head>

    <?php 
        //Load the reqiured Files View
        echo $this->load->view('inc/req_files'); 
    ?>


    <script>
	$(function() {

		$('.vb').click( function() {
			
			if($('#vote li a').hasClass('vb'))
			{
				var videoid = $('.videoid').val();
				var choice = $(this).attr('id');
				var value = $('#choice'+choice).text();
				
				$('.space').prepend('<di class="result"><b>Thanks for your vote!</b></div>').show('fast'); 
				$('.result').slideDown("fast").delay(2000).slideUp('fast');
				
				 $('#choice'+choice).addClass('hov');
				 $('#vote #vi'+choice).addClass('hov');
				 
				if(value == '')
				{
					
				}
				else
				{
					value = parseInt(value, 10) || 0; // Use a radix!!!
					$('#choice'+choice).text(++value).show();
				}
				 
				$.ajax({
				  url: "<?php echo base_url(); ?>video/add_vote/"+videoid+"/"+choice,
				  success: function(){
				  }
				});
			}
			$('#vote li a').removeClass('vb');
			
			return false;
		}); 

		 $(".sectitle").change(function(){
            window.location=this.value;
        });
	}); 
	</script>
    
</head>

<body class="singleVideo">
    <?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
<div id="wrapper">

    <?php 
        //Load the reqiured header view
        echo $this->load->view('inc/header'); 
    ?>
    
    <div class="vidmain">
  	
    <?php 
        //Load challenge Sidebar View
        echo $this->load->view('inc/chal_sidebar'); 
    ?>
      
    <div class="secndwrapper gcwrapper" style="float:right;width:640px;">
       <!-- <h2>GbaamTV - The Verdict</h2> --> 
       
         	<span class="challtitle">Welcome to The Gbaam Challenge</span>

        	<div style="text-align:center;" class="chalinfo">

				<p>
					The Internet has produced countless stars and flubs from Justin Beiber to King Bach and the list goes on and on.  Every now and then, a fad sweeps the web and we are entertained with the goofs, the talented and the flat out confused all wanting a piece of that 15 minutes of fame or perhaps they truly enjoy the experience. Nonetheless the goal of “The Gbaam Challenge” is to present a challenge every week where the person with the highest positive score wins a $100 prize automatically***. We do not have a say in the results as long as your video crosses the threshold of 1000 votes, it becomes automatically eligible for a prize.
				</p>
				
				<p>
					<i>*** For our first month of operation this will be extended through the whole month of January for a $500 prize with a 2500 vote threshold. </i>
				</p>

				<p>
					<b><u>CORE Rules</u></b><br>
					1.	Be Creative<br>
					2.	Be Original<br>
					3.	Be Respectful<Br>
					4.	Be Extraordinary<Br>
				</p>
				
				<p>
					<b><u>How to Compete</u></b><br>
					1.	Record a video no more than 120 seconds (2 Minutes).<br>
					2.	Send us the video via a file transfer site such as wetransfer or sendspace to <a href="mailto:tv@gbaam.com">tv@gbaam.com</a>.<br>
					3.	Submit the download link through our <a href="http://www.gbaam.com/main/submit/challenge">“Gbaam Challenge Video” submission form</a>.<br>
					4.	Your video will go live within 24hrs of submission if it meets all the guidelines.<br>
				</p>
        	</div>

			
			<span class="head">
				Current Challenge: <?php echo $cat_info->display_name; ?>
			</span>
			
			<ul id="vidfeed" style="width:640px;float:right;"> 
				<?php
					if($this->Challenge_model->get_videos_by_cat_count($cat) == 0):
				?>

				<li style="text-align:center;float:left;width:100%;font-size:13px;font-style: italic;">
					There are no Gbaam Challenge videos yet. <a style="text-decoration:underline;" href="http://www.gbaam.com/main/submit/challenge">Submit a video.</a>
				</li>
				
				<?php
					else:
				?>
						
                 <?php  foreach($this->Challenge_model->get_videos_by_cat($cat) as $gv): ?>
	              	<li class="vid">
	                  	<a title="<?php echo $gv->title; ?>" href="<?php echo base_url(); ?>challenge/view/<?php echo $gv->id; ?>" class="img">
                        	<?php 
							if($gv->thumb == ''){

								if($gv->yt == 1){
									echo '<img src="http://img.youtube.com/vi/'.youtube_id_from_url($gv->video_src).'/3.jpg" alt="'.$gv->title.'"/>';
								}
								else
								{


							?>
	                      	<img src="<?php echo base_url(); ?>thumbs/Gbaam3.jpg" alt="<?php echo $gv->title; ?>" />  
                            <?php
                            	}
							}
							else{ ?>
							<img  src="<?php echo base_url(); ?>thumbs/<?php echo $gv->thumb; ?>" alt="<?php echo $gv->title; ?>" /> 
							<?php } ?>
	                      </a>
	                      <a title="<?php echo $gv->title; ?>"  href="<?php echo base_url(); ?>challenge/view/<?php echo $gv->id; ?>" class="title">
	                      	<?php
	                      		$the_title = $gv->title;
	                      	?>

	                      <?php echo character_limiter($the_title,47); ?>
	                      </a>
                          
	                      <span class="views"><?php echo number_format($gv->v_count); ?> Views</span>
	  					
	  					
	                  </li>
                      <?php endforeach; ?>
                <?php endif; ?>
	        </ul> 
	       


        </div>

        
   
		
        	
   </div>
    
   	    <?php echo $this->load->view('inc/footer'); ?>

		
</body>

</html>