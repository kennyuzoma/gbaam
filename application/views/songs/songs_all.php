<!DOCTYPE html>
<html>
<head>

  <?php 
    //Load the reqiured Files View
    echo $this->load->view('inc/req_files'); 
  ?>
    
</head>

<body class="singleVideo">
	<?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
<div id="wrapper">

  <?php 
    //Load the reqiured header view
    echo $this->load->view('inc/header'); 
  ?>
    
    <div class="vidmain">
    
      <div style="float:left;width:640px;"> 
      
        <span class="cleanhead" style="margin-bottom:0;">All Chunes</span>
        <ul id="latestmtlist">
        <?php foreach($getSongs as $g): ?>
        	<li>
                    <a title="<?php echo $g->title.' - '.$g->artist; ?>" href="<?php echo base_url(); ?>chunes/<?php echo $g->id.'/'.$g->permalink; ?>">
                      <img alt="<?php echo $g->title.' - '.$g->artist; ?>" style="width:125px;height:125px;" src="<?php echo get_image('song',$g->front_cover,$g->img_hash,'200'); ?>" />
                      
                        <span class="mtitle"><?php echo character_limiter($g->title,15); ?></span>
                      </a>
                      <span class="maname"><?php echo character_limiter($g->artist,20); ?></span>
                      <span class="rating">Rating: <b>
                                <?php

                                $getmix = $this->Songs_model->get_vote_final($g->id);
                                $isScore = $getmix['isScore'];
                                
                                    if($isScore == FALSE) { 
                                        echo 'None';
                                    } else {
                                        $final_score = $getmix['final_score'];
                                        if($final_score <= 2) 
                                        {
                                            echo 1;
                                        } else {
                                            echo $final_score.'/10';
                                        }
                                    } ?>
                                    </b></span>
                        <span class="views">Views: <b><?php echo number_format($g->v_count); ?></b></span>
                </li>
        <?php endforeach; ?>
       
            
        </ul>
        <?php echo $pagi_links; ?>

      </div>
      <?php $this->load->view('inc/rightsidebar'); ?>
  </div>
    
   
    
    	<?php echo $this->load->view('inc/footer'); ?>

</body>
</html>