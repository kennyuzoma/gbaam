<!DOCTYPE html>
<html>
<head>
	<?php 
	    //Load the reqiured Files View
	echo $this->load->view('inc/req_files'); 
	?>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/packs/clean-jplayer-skin/style.css">
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/clean-jplayer-skin/jquery.jplayer.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/clean-jplayer-skin/jplayer.cleanskin.js"></script>


	<style>

		.morelink {
			cursor:pointer;
			font-weight:bold;
		}
		.morelink:hover {
			cursor:pointer;
		}

		.morecontent span {
			display: none;
		}

		.readmore-js-toggle {
			float: right;
			width: 425px;
			text-align: center;
			font-weight: normal;
			font-size: inherit;
			color: #f90;
			font-size: 13px;
			font-weight:bold;
			margin-bottom:5px;
			padding-top:5px;
			box-shadow: 0px -4px 2px -3px rgba(50, 50, 50, 0.75);

		}

		.mxdesc{
			display:none;
		}

	</style>



	<script type="text/javascript">


		$(document).ready(function(){
			$('#panel').remove();
			$('#backToTop').remove();
			$('.content').css('padding', '0');
			$(document).find('.webPlayer').each(function() { $('#'+this.id).videoPlayer({ solution: 'flash, html' }); });

			$('.mxdesc').show();
			$('.mxdesc').readmore({
				speed: 75,
				maxHeight: 70,
				embedCss: false,
				sectionCSS: '',
				moreLink: '<a href="#">Show More</a>',
				lessLink: '<a href="#">Show Less</a>'
			});


			$('.fancybox').fancybox();


			$(document).on('click', '.loginclick2', function (e) {
				$('#basic-modal-content').modal({
					overlayClose:true,
					opacity:60,
					overlayCss: {backgroundColor:"#000"},
					containerCss: {height:"340px"},
					position: ["10%","50%"]
				});

				e.preventDefault();
			});

			$(document).on('click', '.signupclick2', function (e) {
				$('#basic-modal-content_signup').modal({
					overlayClose:true,
					opacity:60,
					overlayCss: {backgroundColor:"#000"},
					position: ["10%","50%"]
				});

				e.preventDefault();
			});

			$(document).on('click', '.closeit', function (e) {
				$('.warningMessage').fadeOut();

				e.preventDefault();
			});

			$('.vb').on('click', function() {

				if(!$(this).parent().hasClass('hov'))
				{

					if($('#vote li a').hasClass('vb'))
					{
						var songid = $('.songid').val();
						var choice = $(this).attr('id');
						var value = $('#choice'+choice).text();

						$.ajax({
							url: "<?php echo base_url(); ?>chunes/add_vote/"+songid+"/"+choice,
							type: 'get',
							contenttype: "application/json",
							datatype: 'jsonp',
							async: false,
							success: function(dat) {
								newdata = JSON.parse(dat);
								if(newdata.status == 'good')
								{
									if(value == '')
									{

									}
									else
									{
                  value = parseInt(value, 10) || 0; // Use a radix!!!
                  $('#choice'+choice).text(++value).show();
              }

              $('#vote li, #vote2 li').removeClass('hov');

              $('.space').prepend('<div class="result" style="margin-bottom:10px;border-radius5px;"><b>Thanks for your vote!</b></div>').show('fast');
              $('.result').delay(100).slideDown("fast").delay(3000).slideUp('fast');

              $('#choice'+choice).addClass('hov');
              $('#vote #vi'+choice).addClass('hov');
          }
          else
          {
          	$('.space').prepend('<div class="warningMessage" style="margin-bottom:10px;border-radius5px;"><b>You must be logged in to vote! <a href="#" class="loginclick2">Log in</a> or <a href="#" class="signupclick2">Sign up</a> </b><span class="closeit">x</span></div>').show('fast');
          	$('.warningMessage').delay(100).slideDown("fast");


          }
      },
      error: function() {
      	alert('There has been an error, please alert us immediately');
      }
  });
}
$('#vote li a').removeClass('vb');
}

return false;
});




});
</script>

<script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#async=1#pubid=ra-4f1e445964f67956"></script>
<script type="text/javascript">
	function initAddThis() 
	{
		addthis.init()
	}
     // After the DOM has loaded...
     $(window).bind("load", function() {
     	initAddThis();
     });
 </script>
        
    </head>

    <body class="singleVideo">
    	<?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
    	<div id="wrapper">

    		<?php 
    //Load the reqiured header view
    		echo $this->load->view('inc/header'); 
    		?>
    		<div class="vidmain">

	       	  		


    			<?php if($status == '2'): ?>

    				<div style="float:left;width:100%;margin-bottom:20px;text-align:center;font-size:25px;background:#dc9001;border-radius:5px;text-shadow: 0px 0px 3px black;">Unlisted</div>

    			<?php elseif($status == '3'): ?>

    				<div style="float:left;width:100%;margin-bottom:20px;text-align:center;font-size:25px;background:#dc9001;border-radius:5px;text-shadow: 0px 0px 3px black;">Private</div>

    			<?php endif; ?>
    			
    			<div style="float:left;width:640px;text-align:center;" class="space"></div>
    			<div style="float:left;width:640px;padding-bottom:7px;"> 

    				<div id="mixtapetwo2">
    					
    					<input type="hidden" value="<?php echo $songid; ?>" class="songid"  />
    					<?php 
    					if($front_cover != ''):
    						?>
    					<a class="fancybox" rel="gallery1" href="<?php echo get_image('song',$front_cover,$img_hash,'500'); ?>" title="Artwork">
    						<img alt="<?php echo $title; ?>" class="mxcover" src="<?php echo get_image('song',$front_cover,$img_hash,'200'); ?>" />
    					</a>
    					<?php 
    					else:
    						?>
    					<img alt="<?php echo $title; ?>" class="mxcover" src="<?php echo get_image('song',$front_cover,$img_hash,'200'); ?>" />
    					<?php
    					endif;
    					?>

    					<h1 class="mxtitle"><?php echo $title; ?></h1>
    					<div class="descwrap" style="float:right;width:425px;min-height:100px;">
    					<?php /*
							<span style="font-size:13px;float:left;width:425px;margin-bottom:10px;color:#aaa;">
	       	  			Posted By
	       	  			<a href="<?php echo base_url().'editorial/author/'.$this->Site_model->getAuthorLink($songid,'song'); ?>" >
	       	  				<?php 
                    		echo $this->Site_model->getAuthor($songid,'song');
                    		?>
	       	  			</a> on <span style="color:#ddd;"><?php echo conv_date($date_created); ?></span> | <a href="#disqus_thread" style="color:#f90;" data-disqus-identifier="song-<?php echo $songid; ?>"></a>
			       	 </span> */ ?>
    						<p class="mxdesc"><?php echo nl2br(look4links($desc)); ?></p></div>

    					<div style="float:right;width:425px;border-bottom:1px solid #5C5C5C;border-top:1px solid #5C5C5C;padding:3px 0;">
    						<div style="float:left;width:425px;" class="mxvote">
    							<ul id="vote" class="voteone" style="width:425px;">

    								<?php

    								if($this->session->userdata('logged_in') == TRUE)
    								{
    									$did_i_vote = $this->Songs_model->did_i_vote($songid);

    									$num_rows = $did_i_vote['num_rows'];
    									$row = $did_i_vote['row'];

    									if($num_rows == 1)
    										$class = 'hov'; 
    									else
    										$class = '';
    								}
    								else
    								{
          								// just so i dont have to write more code
    									$num_rows = 0;
    								}
    								

    								$i = 0;
    								while ($i <= 4):

    									if($i == 0)
    										$word = 'Seriously?';
    									
    									if($i == 1)
    										$word = 'Hmmmmm';

    									if($i == 2)
    										$word = 'Not Bad';

    									if($i == 3)
    										$word = 'Niceee';

    									if($i == 4)
    										$word = 'Gbaam!';

    									

    									?>
    									<li id="vi<?php echo $i; ?>"
    										<?php
    										if($num_rows == 1)
    											if($row->choice == $i)
    												echo 'class="hov"';
    											?>
    											>
    											<a href="#" class="vb" id="<?php echo $i; ?>">
    												<?php echo $word; ?>
    											</a>
    										</li>
    										<?php
    										$i++;
    										endwhile;
    										?>
    										
    									</ul>
    									
    									<div style="float:left;width:425px;background:url(<?php echo base_url(); ?>assets/img/heat.png); height:10px;">
    										<img title="Vote now!" style="float:left;width:425px;height:10px;" src="<?php echo base_url(); ?>assets/img/heat.png" />
    									</div>

    								</div> 
    								
    							</div>

    						</div>

      
        
        <?php
        $title_soc=urlencode($title.' | GBAAM');
        $url = urlencode(base_url().$this->uri->uri_string());
        $summary=urlencode(strip_tags(character_limiter($desc,300)));
        $image=urlencode(get_image('song',$front_cover,$img_hash));
        ?>
        
        <ul class="soci_actions3 addthis_toolbox addthis_default_style" style="margin:10px 0px 20px 0px;">
        	<li class="">

        		<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>&amp;p[images][0]=<?php echo $image;?>" class="socialPop">
        			<img src="<?php echo base_url(); ?>assets/img/social/facebook-variation.png" />
        		</a>
        	</li>
        	<li class="">
        		<a href="https://twitter.com/intent/tweet?text=<?php echo $title_soc; ?>&url=<?php echo $url; ?>&related=" class="socialPop">
        			<img src="<?php echo base_url(); ?>assets/img/social/twitter-variation.png" />
        		</a>
        	</li>
        	<li class="">
        		<a href="https://plus.google.com/share?url=<?php echo $url; ?>&t=<?php echo $title_soc; ?>" class="socialPop">
        			<img src="<?php echo base_url(); ?>assets/img/social/gplus-variation2.png" />
        		</a>
        	</li>
        	<li class="share"><a class="addthis_counter addthis_pill_style"></a></li>
        	<?php if($songid == '59'): ?>
        		<li>
        			<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" style="
        			/* float: left; */
        			width: 100px;
        			height: auto !important;
        			">
        			<input type="hidden" name="cmd" value="_s-xclick">
        			<input type="hidden" name="hosted_button_id" value="EANT6PR8GWQX6">
        			<input type="submit" class="css here" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" value="Donate to Artist!" style="
        			border: 0;
        			border-radius: 5px;
        			color: white;
        			box-shadow: 0px 0px 5px black;
        			font-size: 17px;
        			margin: 0;
        			text-shadow: 0px 0px 2px black;
        			background-color: orange;
        			cursor:pointer;
        			margin-top: 3px;
        			">
        			<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
        		</form>


        	</li>

        	
        <?php endif; ?>
        
    </ul>

	<div class="box">
			<div id="uniquePlayer-6" class="webPlayer audioPlayer">
				<div id="uniqueContainer-6" class="videoPlayer"></div>

				<div style="display:none;" class="playerData">
					{
					"name": "<?php echo $title; ?>",
					"size": {
					"width": "640px" },
					"media": {
						"<?php echo $song_ext; ?>": "<?php echo $song_file; ?>"
						}
					}
				</div>
			</div>
		</div>
    

    <div id="mixmenu">
    	<div class="bx" style="-webkit-border-top-left-radius: 5px;
    	-webkit-border-bottom-left-radius: 5px;
    	-moz-border-radius-topleft: 5px;
    	-moz-border-radius-bottomleft: 5px;
    	border-top-left-radius: 5px;
    	border-bottom-left-radius: 5px;">
	    	<span class="name">Views</span>
	    	<span class="num"><?php echo number_format($v_count); ?> </span>
	    </div>
	    <div class="bx">
	    	<span class="name">Downloads</span>
	    	<span class="num"><?php echo $d_count; ?> </span>
	    </div>
	    <div class="bx" style="border:0;-webkit-border-top-right-radius: 5px;
	    -webkit-border-bottom-right-radius: 5px;
	    -moz-border-radius-topright: 5px;
	    -moz-border-radius-bottomright: 5px;
	    border-top-right-radius: 5px;
	    border-bottom-right-radius: 5px;">
		    <span class="name">Rating</span> 
		    <?php if($isScore == FALSE) { ?>
		    <span class="num score">None</span>
		    <?php } else { ?>
		    <span class="num score<?php if($final_score <= 2) 
		    {
		    	echo 1;
		    } else {
		    	echo $final_score;
		    } ?>"> <?php echo $final_score; ?>/10</span>
		    <?php } ?>
		</div> 
		
		<?php
			if(!$this->agent->is_mobile()):
		?>
		<div >  
			<form class="dwnld" style="height: 41px;border-radius: 5px;"action="<?php echo base_url(); ?>chunes/d" method="post">
				<input type="hidden" name="song" value="<?php echo $song_hash; ?>">
				<?php /*<span class="downloadbutton" title="Download now!">Download</span>*/ ?>
				<button type="submit" class="downloadbutton">Download</button>
			</form>
		</div>
		<?php
			else:
		?>
		<div class="dwnld" style="height:41px;border-radius:5px;">
			<a href="<?php echo base_url(); ?>chunes/d/<?php echo $song_hash; ?>" target="_blank" class="downloadbutton" title="Download now!">Download</span>
		</div>
		<?php
			endif; 
		?>

	</div>

<?php /*
	<div style="float:left;width:100%;margin:25px 0 10px 0;text-align:center;">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- Gbaam 2014 -->
		<ins class="adsbygoogle"
		style="display:inline-block;width:250px;height:250px"
		data-ad-client="ca-pub-5110726143954622"
		data-ad-slot="5544511172"></ins>
		<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
	</div>
*/ ?>
	<?php $this->load->view('inc/disqus_help'); ?>
	<div id="disqus_thread" style="float:left;width:620px;margin-top:10px;background:#1c1c1c;padding:10px;"></div>




</div>

<?php $this->load->view('inc/rightsidebar'); ?> 
</div>



<?php echo $this->load->view('inc/footer'); ?>

<script type="text/javascript">
/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
var disqus_shortname = 'gbaam'; // required: replace example with your forum shortname

/* * * DON'T EDIT BELOW THIS LINE * * */
(function () {
var s = document.createElement('script'); s.async = true;
s.type = 'text/javascript';
s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
}());
</script>

<script type="text/javascript">
	(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
</body>
</html>