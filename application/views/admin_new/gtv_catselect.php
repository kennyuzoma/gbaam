<!DOCTYPE html>
<html>
<head> 
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){
            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                     $.post('<?php echo base_url().$this->config->item('admin_location'); ?>/hotmixtapes/remove/'+id, function() {
                        $('#post_'+id).fadeOut();
                    });
                }

                return false;
            });
        });
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>


<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

    <!-- Content -->
    <div class="content">
        <div class="title"><h5><?php echo $title; ?></h5></div>
        
        
        <!-- Static table with resizable columns -->
        <div class="widget">
            <div class="head"><h5 class="iFrames"><?php echo $title; ?></h5></div>
            <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic resize">
                <thead>
                    <tr>
                        <td width="50">Slot</td>
                        <td width="100">Added date</td>
                        <td width="450">Title</td>
                        <td width="100">Action</td>
                    </tr>
                </thead>
                <tbody>

                    <tr> 
                        <td colspan="100%">
                            <b>
                                <a href="<?php echo $this->config->item('admin_location'); ?>/gtvfeatvideos/list/gtv_main">
                                    GbaamTV Front Page
                                </a>
                            </b>
                        </td>
                    </tr>

                    <?php
                        foreach($gtv_cats as $g):
                    ?>
                        <tr>
                            
                            <td colspan="100%">
                                <b>
                                    <a href="<?php echo $this->config->item('admin_location'); ?>/gtvfeatvideos/list/<?php echo $g->name; ?>">
                                        <?php echo $g->name; ?>
                                    </a>
                                </b>
                            </td>
                        </tr>
                    <?php
                        endforeach;
                    ?>


                </tbody>
            </table>
        </div>

        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
    <div class="wrapper">
        <span>&copy; Copyright 2011. All rights reserved. It's Brain admin theme by <a href="#" title="">Eugene Kopyov</a></span>
    </div>
</div>

</body>
</html>
