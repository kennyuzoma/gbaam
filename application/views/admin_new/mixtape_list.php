<!DOCTYPE html>
<html>
<head> 
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){

            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                    $.post('<?php echo base_url().$this->config->item('admin_location'); ?>/mixtape/delete/'+id, function() {
                        $('#post_'+id).fadeOut();
                    });
                    
                }

                return false;
            });

            $('.makemotw').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure you want to make this mixtape of the week?")) {
                    $.post('<?php echo base_url().$this->config->item('admin_location'); ?>/mixtape/make_motw/'+id, function() {
                        $('#post_'+id).css('background','gold');
                    });
                    
                }

                return false;
            });

        });
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>


<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content">
        <div class="title"><h5>All Mixtapes</h5></div>

       
        
        
        <!-- Static table with resizable columns -->
        <div class="widget">
            <div class="head"><h5 class="iFrames">Mixtape list</h5></div>
            <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic resize">
                <thead>
                    <tr>
                      <td width="100">Upload Date</td>
                      <td width="500">Title</td>
                      <td width="100">Action</td>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach($get_mixtapes as $g): ?>

                    <tr class="videopost" id="post_<?php echo $g->id; ?>">
                        <td align="center"><?php echo conv_date($g->added); ?></td>
                        <td>
                            <a href="<?php echo $this->config->item('admin_location'); ?>/mixtape/edit/<?php echo $g->id; ?>" class="info">
                                <img class="thumb" src="<?php echo get_image('mixtape',$g->front_cover,$g->img_hash,'125'); ?>" />
                                <span class="name"><?php echo $g->title.' - '.$g->artist; ?></span>
                            </a>
                        </td>
                        <td>
                        	<?php 
								$my_type = $this->session->userdata('admin_type');
								if(($my_type > '4') && ($this->session->userdata('admin_user_id') != $g->poster_id)): 

							?>
								<a href="mixtapes/<?php echo $g->id.'/'.$this->Mixtape_model->getMixtapeUrl($g->id); ?>" target="_blank" >View Mixtape</a>

							<?php else: ?>
								<a href="#" class="deletepost" id="<?php echo $g->id; ?>">Delete</a> &middot; 
								<a href="<?php echo $this->config->item('admin_location'); ?>/mixtape/edit/<?php echo $g->id; ?>">Edit</a> &middot;<br> 
								<a href="mixtapes/<?php echo $g->id.'/'.$this->Mixtape_model->getMixtapeUrl($g->id); ?>" target="_blank">View Mixtape</a><br> 
								
								<?php if($my_type <= '4'): ?>
								<b><a href="#" class="makemotw" id="<?php echo $g->id; ?>">Make MOTW</a></b>
								<?php endif; ?>

	                        	
	                        <?php endif; ?>

                        </td>
                    </tr>

                    <?php endforeach; ?>

                </tbody>
            </table>
        </div>

        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&copy; Copyright 2011. All rights reserved. It's Brain admin theme by <a href="#" title="">Eugene Kopyov</a></span>
    </div>
</div>

</body>
</html>
