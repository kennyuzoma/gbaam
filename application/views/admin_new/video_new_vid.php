<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){

            $( "#datepicker" ).datepicker();

            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                    $('#post_'+id).fadeOut();
                }

                return false;
            });
        });
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>




<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content" id="container">
    	<div class="title"><h5><?php echo $title; ?></h5></div>

    	<?php
            if(isset($validation))
            {
            	if($validation == FALSE)
            	{
            		echo '<span class="wrong">'.$message.'</span>';
            	}
            }
        ?>
        
        <!-- Form begins -->
        <form action="<?php echo base_url() . $this->uri->uri_string(); ?>" class="mainForm" method="post" enctype="multipart/form-data">
            
            <input type="hidden" name="video_type" value="<?php echo $video_type; ?>" />
        
        	<!-- Input text fields -->
            <fieldset>
                <div class="widget first">
                    <div class="head"><h5 class="iList">Enter Video Information</h5></div>
                        <div class="rowElem kenny">

                            <?php if($video_type == 'challenge'): ?>
                            <div class="rowElem">
                                <label class="formLeft">Challenge: </label>
                                <div class="formRight noSearch">
                                    <select id="m" name="chall_cat" class="styledselect-month chzn-select" style="width:250px;">
                                        <?php foreach($this->Video_model->get_g_ch_cat() as $c): ?>
                                            <option value="<?php echo $c->id; ?>" <?php echo set_select('chall_cat', $c->id); ?>><?php echo $c->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="fix"></div>
                            </div>
                            <?php endif; ?>

                             <?php if($video_type == 'gbaamtv'): ?>
                            <div class="rowElem">
                                <label class="formLeft">Category: </label>
                                <div class="formRight noSearch">
                                    <select name="tv_cat" class="chzn-select" style="width:250px;">
                                        <?php foreach($this->Video_model->get_gtv_cat() as $c): ?>
                                            <option value="<?php echo $c->id; ?>" <?php echo set_select('tv_cat', $c->id); ?>><?php echo $c->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="fix"></div>
                            </div>
                            <?php endif; ?>

                            <div class="rowElem">
                                <label class="formLeft">Date:</label>
                                <div class="formRight"><input type="text" class="maskDate" id="datepicker" name="date" value="<?php echo set_value("date", date('m/d/Y')); ?>" /></div>
                                <div class="fix"></div>
                            </div>
                            
                            <?php if($video_type == 'mv'): ?>
                            <div class="rowElem">
                                <label class="formLeft">Artist Name:</label>
                                <div class="formRight">
                                    <input type="text" placeholder="Artist Name" name="artistname" value="<?php echo set_value('artistname', ''); ?>" />
                                </div>
                            </div>
                            <?php endif; ?>
                   
                            
                            <div class="rowElem">
                                <label class="formLeft">Title:</label>
                                <div class="formRight">
                                    <input type="text" placeholder="Video Title" name="videotitle" value="<?php echo set_value('videotitle', ''); ?>"/>
                                </div>
                            </div>
                           
                            <div class="rowElem">
                                <label class="formLeft">Description:</label>
                                <div class="formRight">
                                    <textarea rows="8" cols="" name="description" class="auto" style="overflow: hidden;" placeholder="Video Description"><?php echo set_value('description', ''); ?></textarea>
                                </div>
                            </div>

                            <div class="rowElem">
                            	<label class="formLeft">
                            		<label for="tags">Tags:</label>
                            	</label>
                            	<div class="formRight">
                            		<input type="text" id="tags" name="tags" class="tags" value="<?php echo set_value('tags', ''); ?>" />
                            	</div>
                            	<div class="fix"></div>
                    		</div>

                            <div class="fix"></div>

                            <div class="rowElem">
                                <label class="formLeft">Video Source:</label>
                                <div class="formRight">
                                    <input type="text" placeholder="Video Source" name="video_src" value="<?php echo set_value('video_src', ''); ?>" />
                                </div>
                            </div>
                            <div class="rowElem">
                                <label class="formLeft">Add Thumbnail:</label>
                                <div class="formRight">
                                    <input type="file" name="lg_img">
                                </div>
                            </div>

                        </div>

              
                  
                    <div class="floatright twoOne">
                   
                    <input type="submit" name="submit" value="Submit form" class="greyishBtn submitForm" />
                    
                    </div>
                    <div class="fix"></div>
                </div>
            </fieldset> 
        </form>
        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<?php include('inc/footer.php'); ?>

</body>
</html>
