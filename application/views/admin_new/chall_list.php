<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title> 

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){
            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                     $.post('<?php echo base_url().$this->config->item('admin_location'); ?>/challenge/delete/'+id, function() {
                        $('#post_'+id).fadeOut();
                    });
                }

                return false;
            });
        });
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>


<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content">
        <div class="title"><h5>Edit Challenges</h5></div>
        
        
        <!-- Static table with resizable columns -->
        <div class="widget">
            <div class="head"><h5 class="iFrames">Challenges list</h5></div>
            <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic resize">
                <thead>
                    <tr>
                      <td width="100">Added Date</td>
                      <td width="100">Status</td>
                      <td width="400">Title</td>
                      <td width="100">Action</td>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach($get_challenges as $g): ?>

                    <tr class="videopost" id="post_<?php echo $g->id; ?>">
                        <td align="center"><?php echo conv_date($g->date); ?></td>
                        <td align="center">
                            <?php 
                            if($g->active == 1){
                                echo '<b>Active</b>';
                            } else {
                                echo '<b>Hidden</b>';
                            }
                            ?>
                            
                        </td>
                        <td align="center">
                            <a href="<?php echo $this->config->item('admin_location'); ?>/challenge/edit/<?php echo $g->id; ?>" class="info">
                                
                                <span class="name" style="margin:0;"><?php echo $g->display_name; ?></span>
                            </a>
                        </td>
                       
                        <td><a href="#" class="deletepost" id="<?php echo $g->id; ?>">Delete</a> &middot; 
                       <?php 
                       //Add unhide soon
                       /*
                        <a href="#" class="hidepost" id="<?php echo $g->id; ?>">Hide</a> &middot;<br>
                        */ 
                       ?> <a href="<?php echo $this->config->item('admin_location'); ?>/challenge/edit/<?php echo $g->id; ?>">Edit</a> </td>
                    </tr>

                    <?php endforeach; ?>

                </tbody>
            </table>
        </div>

        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&copy; Copyright 2011. All rights reserved. It's Brain admin theme by <a href="#" title="">Eugene Kopyov</a></span>
    </div>
</div>

</body>
</html>
