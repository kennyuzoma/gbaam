<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){

            $( "#datepicker" ).datepicker();

            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                    $('#post_'+id).fadeOut();
                }

                return false;
            });

            var timer = null;
            var permalink_typed = 0;

            $('.js_permalink').keyup(function(){
                permalink_typed = 1;
            });

            if(permalink_typed == 0 )
            {
                $('.js_challengetitle').keyup(function() {
                    clearTimeout(timer);
                    timer = setTimeout(doneTyping, 1500);
                });
            }
        });

        function doneTyping () {
        $.post( "<?php echo $this->config->item('admin_location'); ?>/pretty_url/", { url: $('.js_challengetitle').val() }, function( data ) {
     
            //alert( data);
            $('.js_permalink').val(data);
        });
    }
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>




<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content" id="container">
    	<div class="title"><h5>Challenge</h5></div>
        
        <!-- Form begins -->
        <form action="<?php echo base_url() . $this->uri->uri_string(); ?>" class="mainForm" method="post">

        	<!-- Input text fields -->
            <fieldset>
                <div class="widget first">
                    <div class="head"><h5 class="iList">New Challenge</h5></div>
                        <div class="rowElem kenny">

                            <div class="rowElem">
                                <label>Radio :</label> 
                                <div class="formRight">
                                    <input type="radio" name="question1" value="1" id="radioOne" checked="checked"/>
                                    <label for="radioOne">Active</label>
                                    <input type="radio" name="question1" value="0" id="radioTwo"/>
                                    <label for="radioTwo" >Hidden</label>
                                </div>
                                <div class="fix"></div>
                            </div>
           
                            <div class="rowElem">
                                <label class="formLeft">Challenege Title:</label>
                                <div class="formRight">
                                    <input type="text" name="display_name" class="js_challengetitle" placeholder=""/>
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Url Name:</label>
                                <div class="formRight">
                                    <input type="text" name="name" class="js_permalink" placeholder=""/>
                                </div>
                            </div>

                    <div class="floatright twoOne">
                   
                    <input type="submit" name="submit" value="Submit form" class="greyishBtn submitForm" />
                    
                    </div>
                    <div class="fix"></div>
                </div>
            </fieldset> 
        </form>
        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<?php include('inc/footer.php'); ?>

</body>
</html>
