<!DOCTYPE html>
<html>
<head> 
    <title>Gbaam Admin</title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>


<!-- Content wrapper -->
<div class="wrapper">
	
	<!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>
    
    <!-- Content -->
    <div class="content">

    	<div class="title"><h5>Dashboard</h5></div>
        
        <!-- Statistics -->
        <div class="stats">
        	<ul>
            	<li>
            		<a href="<?php echo $this->config->item('admin_location'); ?>/site_users/list" class="count grey" title="">
            			<?php
            				$users = $this->User_model->get_users();
            				echo $users['count'];
            			?>
            		</a>
            		<span><a href="<?php echo $this->config->item('admin_location'); ?>/site_users/list">Registered Users</a></span>
            	</li>
                
            </ul>
            <div class="fix"></div>
        </div>
                
        <!-- Charts -->
        <div class="widget first">
            <div class="head"><h5 class="iGraph">Charts</h5></div>
            <div class="body">
                <div class="chart"></div>
            </div>
        </div>
        
        <!-- Calendar -->
        <div class="widget">
        	<div class="head"><h5 class="iDayCalendar">Schedule</h5></div>
            <div id="calendar"></div>
        </div>
        
        <!-- Full width tabs --> 
        <div class="widget">      
            <ul class="tabs">
                <li><a href="#tab3">Tab Active</a></li>
                <li><a href="#tab4">Tab Inactive</a></li>
            </ul>
            
            <div class="tab_container">
                <div id="tab3" class="tab_content">
                	<h4 class="aligncenter red pt10">Nice looking tables with custom widgets, file manager and contact list!</h4>
                    <p><a href="http://themes.kopyov.com/itsbrain" title="">It's Brain</a> is a new unique admin template with lots of <a href="form_elements.html" title="">features</a>, detailed documentation and quick support.. It includes <span class="red">19 html pages</span> and 10 .psd files, loaded with 20+ of plugins and comes with <span class="red">5 pattern backgrounds</span>. Hope you'll find this theme interesting and useful for your next project!</p>
                </div>
                <div id="tab4" class="tab_content">This tab is active now</div>
            </div>	
            <div class="fix"></div>	 
        </div>
        
        <!-- Dynamic table -->
        <div class="table">
            <div class="head"><h5 class="iFrames">Users</h5></div>
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                <thead>
                    <tr>
                    	<th>ID</th>
                        <th>Username</th>
                        <th>Display Name</th>
                        <th>User Status</th>
                        <th>SignUp Date</th>
                    </tr>
                </thead>
                <tbody>
					<?php 
						$users = $this->User_model->get_users('desc');
						foreach($users['result'] as $u):
					?>
                    <tr class="gradeA">
                    	<td><?php echo $u->id; ?></td>
                        <td><?php echo $u->username; ?></td>
                        <td><?php echo $u->display_name; ?></td>
                        <td class="center"><?php echo convert_u_status($u->u_status); ?></td>
                        <td class="center"><?php echo conv_date($u->date); ?></td>
                    </tr>
                    <?php
                    	endforeach; 
                    ?>
                    
                </tbody>
            </table>
        </div>
        
        <?php /*
        <!-- Widgets -->
        <div class="widgets">
            <div class="left">
            
                <!-- Search -->
                <div class="searchWidget">
                	<form action="">
                    	<input type="text" name="search" id="ac" placeholder="Enter search text..." />
                        <input type="submit" name="find" value="" />
                    </form>
                </div>
                
                <!-- Statistics -->
                <div class="widget">
                    <div class="head"><h5 class="iChart8">Website statistic</h5><div class="num"><a href="#" class="blueNum">+245</a></div></div>
                    <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic">
                        <thead>
                            <tr>
                              <td width="21%">Amount</td>
                              <td>Description</td>
                              <td width="21%">Changes</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td align="center"><a href="#" title="" class="webStatsLink">980</a></td>
                                <td>returned visitors</td>
                                <td><span class="statPlus">0.32%</span></td>
                            </tr>
                            <tr>
                                <td align="center"><a href="#" title="" class="webStatsLink">1545</a></td>
                                <td>new registrations</td>
                                <td><span class="statMinus">82.3%</span></td>
                            </tr>
                            <tr>
                                <td align="center"><a href="#" title="" class="webStatsLink">457</a></td>
                                <td>new affiliates registrations</td>
                                <td><span class="statPlus">100%</span></td>
                            </tr>
                            <tr>
                                <td align="center"><a href="#" title="" class="webStatsLink">9543</a></td>
                                <td>new visitors</td>
                                <td><span class="statPlus">4.99%</span></td>
                            </tr>
                            <tr>
                                <td align="center"><a href="#" title="" class="webStatsLink">354</a></td>
                                <td>new pending comments</td>
                                <td><span class="statMinus">9.67%</span></td>
                            </tr>
                        </tbody>
                    </table>                    
                </div>
            	
                <!-- Latest orders -->
                <div class="widget">
                    <div class="head"><h5 class="iMoney">Latest orders</h5><div class="num"><a href="#" class="blueNum">+245</a></div></div>
                    
                    <div class="supTicket nobg">
                    	<div class="issueType">
                        	<span class="issueInfo"><a href="#" title="">VPS Basic</a></span>
                            <span class="issueNum"><a href="#" title="">[ #21254 ]</a></span>
                            <div class="fix"></div>
                        </div>
                        
                        <div class="issueSummary">
                       		<a href="#" title="" class="floatleft"><img src="<?php echo base_url(); ?>assets/packs/gbaamadmin/images/user.png" alt="" /></a>	
                            <div class="ticketInfo">
                            	<ul>
                                	<li>Current order status:</li>
                                    <li class="even"><strong class="green">[ pending ]</strong></li>
                                    <li>User email:</li>
                                    <li class="even"><a href="#" title="">user@company.com</a></li>
                                </ul>
                                <div class="fix"></div>
                            </div>
                            <div class="fix"></div>
                        </div> 
                    </div>
                    
                    <div class="supTicket">
                    	<div class="issueType">
                        	<span class="issueInfo"><a href="#" title="">VPS Basic</a></span>
                            <span class="issueNum"><a href="#" title="">[ #21254 ]</a></span>
                            <div class="fix"></div>
                        </div>
                        
                        <div class="issueSummary">
                       		<a href="#" title="" class="floatleft"><img src="<?php echo base_url(); ?>assets/packs/gbaamadmin/images/user.png" alt="" /></a>	
                            <div class="ticketInfo">
                            	<ul>
                                	<li>Current order status:</li>
                                    <li class="even"><strong class="green">[ pending ]</strong></li>
                                    <li>User email:</li>
                                    <li class="even"><a href="#" title="">user@company.com</a></li>
                                </ul>
                                <div class="fix"></div>
                            </div>
                            <div class="fix"></div>
                        </div> 
                    </div> 
                    
                    <div class="supTicket">
                    	<div class="issueType">
                        	<span class="issueInfo"><a href="#" title="">VPS Basic</a></span>
                            <span class="issueNum"><a href="#" title="">[ #21254 ]</a></span>
                            <div class="fix"></div>
                        </div>
                        
                        <div class="issueSummary">
                       		<a href="#" title="" class="floatleft"><img src="<?php echo base_url(); ?>assets/packs/gbaamadmin/images/user.png" alt="" /></a>	
                            <div class="ticketInfo">
                            	<ul>
                                	<li>Current order status:</li>
                                    <li class="even"><strong class="green">[ pending ]</strong></li>
                                    <li>User email:</li>
                                    <li class="even"><a href="#" title="">user@company.com</a></li>
                                </ul>
                                <div class="fix"></div>
                            </div>
                            <div class="fix"></div>
                        </div> 
                    </div>                    
                </div>
                
                <div class="fix"></div>                 
            </div>
            
            
            <!-- Right widgets -->
            <div class="right">
            	
                <!-- Support tickets widget -->
                <div class="widget">
                    <div class="head"><h5 class="iHelp">Support ticket widget</h5><div class="num"><a href="#" class="redNum">+128</a></div></div>
                    
                    <div class="supTicket nobg">
                    	<div class="issueType">
                        	<span class="issueInfo">General financial issue</span>
                            <span class="issueNum">[ #21254 ]</span>
                            <div class="fix"></div>
                        </div>
                        
                        <div class="issueSummary">
                       		<a href="#" title="" class="floatleft"><img src="<?php echo base_url(); ?>assets/packs/gbaamadmin/images/user.png" alt="" /></a>	
                            <div class="ticketInfo">
                            	<ul>
                                	<li><a href="#" title="">Eugene Kopyov</a></li>
                                    <li class="even"><strong class="red">[ High priority ]</strong></li>
                                    <li>Status: <strong class="green">[ Pending ]</strong></li>
                                    <li class="even">Oct 25, 2011  23:12</li>
                                </ul>
                                <div class="fix"></div>
                            </div>
                            <div class="fix"></div>
                        </div> 
                    </div>
                    
                    <div class="supTicket">
                    	<div class="issueType">
                        	<span class="issueInfo">General financial issue</span>
                            <span class="issueNum">[ #21254 ]</span>
                            <div class="fix"></div>
                        </div>
                        
                        <div class="issueSummary">
                       		<a href="#" title="" class="floatleft"><img src="<?php echo base_url(); ?>assets/packs/gbaamadmin/images/user.png" alt="" /></a>	
                            <div class="ticketInfo">
                            	<ul>
                                	<li><a href="#" title="">Eugene Kopyov</a></li>
                                    <li class="even"><strong class="red">[ High priority ]</strong></li>
                                    <li>Status: <strong class="green">[ Pending ]</strong></li>
                                    <li class="even">Oct 25, 2011  23:12</li>
                                </ul>
                                <div class="fix"></div>
                            </div>
                            <div class="fix"></div>
                        </div> 
                    </div>
                    
                    <div class="supTicket">
                    	<div class="issueType">
                        	<span class="issueInfo">General financial issue</span>
                            <span class="issueNum">[ #21254 ]</span>
                            <div class="fix"></div>
                        </div>
                        
                        <div class="issueSummary">
                       		<a href="#" title="" class="floatleft"><img src="<?php echo base_url(); ?>assets/packs/gbaamadmin/images/user.png" alt="" /></a>	
                            <div class="ticketInfo">
                            	<ul>
                                	<li><a href="#" title="">Eugene Kopyov</a></li>
                                    <li class="even"><strong class="red">[ High priority ]</strong></li>
                                    <li>Status: <strong class="green">[ Pending ]</strong></li>
                                    <li class="even">Oct 25, 2011  23:12</li>
                                </ul>
                                <div class="fix"></div>
                            </div>
                            <div class="fix"></div>
                        </div> 
                    </div>                    
                </div>
                
                <!-- Tabs widget -->
                <div class="widget">       
                    <ul class="tabs">
                        <li><a href="#tab1">Tab 1</a></li>
                        <li><a href="#tab2">Tab 2</a></li>
                    </ul>
                    
                    <div class="tab_container">
                        <div id="tab1" class="tab_content">Active tab</div>
                        <div id="tab2" class="tab_content">Now this one is active</div>
                    </div>	
                    <div class="fix"></div>		 
                </div>
                
                <!-- User widget -->
                <div class="widget">
                    <div class="head">
                        <div class="userWidget">
                            <form action="">
                                <input type="checkbox" id="check1" name="chbox" checked="checked" />
                            </form>
                        <a href="#" title="" class="userLink">Eugene Kopyov</a>
                        </div>
                        <div class="num"><span>Balance:</span><a href="#" class="greenNum">+128</a></div>
                    </div>
                    
                    <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic">
                        <tbody>
                            <tr class="noborder">
                                <td width="50%">Current package:</td>
                                <td align="right"><strong class="red">VPS Ultimate</strong></td>
                            </tr>
                            <tr>
                                <td>Paid until:</td>
                                <td align="right">Oct 25, 2011 &nbsp; 23:12</td>
                            </tr>
                            <tr>
                                <td>User email:</td>
                                <td align="right"><a href="#" title="">user@company.com</a></td>
                            </tr>
                            <tr>
                                <td>Support tickets:</td>
                                <td align="right"><a href="#" class="green">no pending tickets</a></td>
                            </tr>
                            <tr>
                                <td>Expiring domains:</td>
                                <td align="right"><span class="expire">12 domains</span></td>
                            </tr>
                        </tbody>
                    </table> 
                                       
                </div>
            </div>
        </div>
        */ ?>

    </div> <!-- End Content -->
    
    <div class="fix"></div>
</div>

    <!-- Footer -->
    <div id="footer">
	<div class="wrapper">
    	<span>&copy; Copyright Gbaam <?php echo date('Y'); ?>. All rights reserved. </span>
    </div>
</div>

</body>
</html>
