<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){

            $( "#datepicker" ).datepicker();

            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                    $('#post_'+id).fadeOut();
                }

                return false;
            });

            /*var timer = null;
            var permalink_typed = 0;

            $('.js_permalink').keyup(function(){
                permalink_typed = 1;
            });

            if(permalink_typed == 0 )
            {
                $('.mixtape_title').keyup(function() {
                    clearTimeout(timer);
                    timer = setTimeout(doneTyping, 1500);
                });
            }
            
*/

        
        
        });

      /*  function doneTyping () {
        $.post( "<?php echo $this->config->item('admin_location'); ?>/pretty_url/", { url: $('.mixtape_title').val() }, function( data ) {
     
            //alert( data);
            $('.js_permalink').val(data);
        });


}*/
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>




<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content" id="container">
    	<div class="title"><h5><?php echo $title; ?></h5></div>
        
        <?php
            if(isset($validation))
            {
            	if($validation == FALSE)
            	{
            		echo '<span class="wrong">'.$message.'</span>';
            	}
            }
        ?>
        
        <!-- Form begins -->
        <form action="<?php echo base_url() . $this->uri->uri_string(); ?>" class="mainForm" method="post" enctype='multipart/form-data'>
        	<!-- Input text fields -->
            <fieldset>
                <div class="widget first">
                    <div class="head"><h5 class="iList">Enter Mixtape Information</h5></div>
                        <div class="rowElem kenny">

                            <div class="rowElem">
                                <label class="formLeft">Date:</label>
                                <div class="formRight"><input name="date" type="text" class="maskDate" id="datepicker" value="<?php echo set_value('date', date('m/d/Y')); ?>" /></div>
                                <div class="fix"></div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Mixtape Title:</label>
                                <div class="formRight">
                                    <input type="text" name="mixtape_title" class="mixtape_title" placeholder="Mixtape Title" value="<?php echo set_value('mixtape_title', ''); ?>"/>
                                </div>
                            </div>


                            
                            <div class="rowElem">
                                <label class="formLeft">Artist:</label>
                                <div class="formRight">
                                    <input type="text" name="artist_name" placeholder="Artist Name" value="<?php echo set_value('artist_name', ''); ?>"/>
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Dj:</label>
                                <div class="formRight">
                                    <input type="text" name="dj_name" placeholder="Dj name" value="<?php echo set_value('dj_name', ''); ?>" />
                                </div>
                            </div>


                   
                            
                            
                           
                            <div class="rowElem">
                                <label class="formLeft">Description:</label>
                                <div class="formRight">
                                    <textarea rows="8" cols="" name="description" class="auto" style="overflow: hidden;" placeholder="Description"/><?php echo set_value('description', ''); ?></textarea>
                                </div>
                            </div>

                            <div class="rowElem">
                            	<label class="formLeft">
                            		<label for="tags">Tags:</label>
                            	</label>
                            	<div class="formRight">
                            		<input type="text" id="tags" name="tags" class="tags" value="<?php echo set_value('tags', ''); ?>" />
                            	</div>
                            	<div class="fix"></div>
                    		</div>
                    		
    <?php /*
                            <div class="rowElem">
                                <label class="formLeft">Permalink (Pretty URL for SEO):</label>
                                <div class="formRight">
                                    <span class="">Its best to just leave this alone</span>
                                    <input type="text" name="permalink" placeholder="Permalink" class="js_permalink" value="" />
                                </div>
                            </div>
*/ ?>
                            <div class="fix"></div>

                            <div class="rowElem">
                                <label class="formLeft">Front Cover:</label>
                                <div class="formRight">
                                    <input type="file" name="ft_cover">
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Back Cover (optional):</label>
                                <div class="formRight">
                                    <input type="file" name="bk_cover">
                                </div>
                            </div>

                        </div>

              
                  
                    <div class="floatright twoOne">
                   
                    <input type="submit" name="submit" value="Add Files" class="greyishBtn submitForm" />
                    
                    </div>
                    <div class="fix"></div>
                </div>
            </fieldset> 
        </form>
        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<?php include('inc/footer.php'); ?>

</body>
</html>
