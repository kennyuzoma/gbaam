<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>




    <script type="text/javascript">
        $(function(){

        	// date picker
            $( "#datepicker" ).datepicker();

            // on delete post
            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                    $('#post_'+id).fadeOut();
                }

                return false;
            });

            // the approval box
            $('.fake_deny').click(function(){
            	$(this).remove();
            	$('.real_deny_button').show();
            	$('.reasons_box').show();
            });

            // editor
            $('.wysiwyg').wysiwyg('removeFormat');

              //$('input[type="checkbox"]').on('click',function() {
              $('#posted_by_gbaam').on('click',function(){
			   	$('.selectorwrap').toggle();

            	if($('.author_select').val() == '0')
            	{
            		$('.authorbox').val(saved_other_author).toggle();
            	}
			});

            var saved_other_author = $('.saved_other_author').val();
            
            $('.author_select').change(function(){

			  if($(this).val() == '0'){ // or this.value == 'volvo'
			    if($('.authorbox').css('display') === 'none')
            	{

            		$('.authorbox').val(saved_other_author).show();

            	}
			  }
			  else
			  {
			  	$('.authorbox').val('');
			  	$('.authorbox').hide();
			  }
			});

			$('.editpermalinkbutton').on('click',function(){

        		$(this).hide();
        		$('.okayclick').show();

        		ptextval = $('.permtxt').text();

        		$('.permtxt').html('<input type="text" value="'+ptextval+'"  name="js_ptextinput"  style="font-size: 14px !important;" class="js_ptextinput" />');

        	});

        	$('.okayclick').on('click',function(){

        		ptextval = $('.js_ptextinput').val();

        		$.post( "<?php echo $this->config->item('admin_location'); ?>/pretty_url/", { url: ptextval }, function( data ) {

	        		$('.okayclick').hide();
	        		$('.editpermalinkbutton').show();
	        		$('.js_ptextinput').hide();
	        		$('.js_permalink').val(data);
	        		$('.permtxt').html(data);

	        	});

        	});

        });


    </script>
    <script src="<?php echo base_url(); ?>assets/packs/ckeditor/ckeditor.js"></script>
    <script type="text/javascript">
    	CKEDITOR.replace( 'ckeditor', {
    filebrowserBrowseUrl: '/browser/browse.php',
    filebrowserUploadUrl: '/uploader/upload.php'
});
    </script>

   

<?php /*
    <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>

    <script type="text/javascript">
tinymce.init({
    mode : "specific_textareas",
    editor_selector : "myTextEditor",
    theme: "modern",
    plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor"
    ],
    <?php if($_SERVER['HTTP_HOST'] == 'www.gbaam.com' || $_SERVER['HTTP_HOST'] == 'gbaam.com'): ?>
    external_plugins: {
        "jbimages": "/assets/packs/tinymce/plugins/jbimages/plugin.js"
    },
    <?php else: ?>
    external_plugins: {
        "jbimages": "/Projects/gbaam/assets/packs/tinymce/plugins/jbimages/plugin.js"
    },
    <?php endif; ?>
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar2: "jbimages print preview media | forecolor backcolor emoticons",
    image_advtab: true,
    paste_as_text: true,
    relative_urls : false,
remove_script_host : false,
convert_urls : true
    
});
</script>
*/?>
<style>
		.permtxt{
			background:#fffbcc;
			font-weight:Bold;
		}

		.js_ptextinput{
			width: 200px !important;
			
		}

		.jsbutton
		{
padding: 0 8px 1px;
font-size: 11px;
			color: #555;
border-color: #ccc;
background: #f7f7f7;
-webkit-box-shadow: inset 0 1px 0 #fff,0 1px 0 rgba(0,0,0,.08);
box-shadow: inset 0 1px 0 #fff,0 1px 0 rgba(0,0,0,.08);
/*vertical-align: top;*/
display: inline-block;
text-decoration: none;
font-size: 13px;

/*line-height: 26px;
height: 28px;*/
margin: 0;
padding: 0 10px 1px;
cursor: pointer;
border-width: 1px;
border-style: solid;
-webkit-appearance: none;
-webkit-border-radius: 3px;
border-radius: 3px;
white-space: nowrap;
-webkit-box-sizing: border-box;
-moz-box-sizing: border-box;
box-sizing: border-box;
		}
	</style>
</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>




<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content" id="container">
    	<div class="title"><h5>Editorial</h5></div>

    	<?php
            if(isset($validation))
            {
            	if($validation == FALSE)
            	{
            		echo '<span class="wrong">'.$message.'</span>';
            	}
            }
        ?>

    	<a style="
		    margin-top: 15px;
		    width:100%;
		    font-weight: bold;
		    font-size: 18px;
		" href="<?php echo $this->config->item('admin_location') . '/'.$this->uri->segment(2).'/list'; ?>">&laquo; Go Back</a>
	<div style="clear:both;width:100%;float:left;"></div>
        <?php foreach($get_article as $g): ?>

<?php
if(($g->author_id == '0') && ($g->author == '')){
?>
	<style>
		.selectorwrap:first-child{
			display:none;
		}
	</style>
<?php
}
?>
        <!-- Form begins -->
        <form action="<?php echo base_url() . $this->uri->uri_string(); ?>" class="mainForm" method="post" enctype="multipart/form-data">

        <input type="hidden" name="saved_other_author" class="saved_other_author" value="<?php echo $g->author; ?>" />

        	<!-- Input text fields -->
            <fieldset>
                <div class="widget first">
                    <div class="head">
                    	<h5 class="iList">
                    		<?php 
                    			if($g->status == '4')
                    				echo '<b>Review</b>';
                    			else
                    				echo 'Edit';
                    		?>
                    		Article
                    	</h5>
                    </div>
                    <a href="<?php echo base_url(). 'editorial/'.$g->permalink; ?>" style="float:left;width:100%;text-align:Center;margin:10px 0 30px 0;font-weight:bold;font-size:20px;">View  Article on Gbaam</a>
                       
                        <div class="rowElem kenny">
							
							<?php if($g->author_id != '0' && $this->session->userdata('admin_type') == '1'): ?>
                        	<div class="rowElem">
                                <label class="formLeft" style="margin-top:10px;">Submitted By:</label>
                                <div class="formRight">
                                <?php
								    // get the admin information from the session
								    $me = $this->Admin_model->getAdmin($g->author_id);
								?>
                                	<span style="font-size:18px;">
                                    	 <b><?php echo $me->display_name; ?></b>
                                   	</span>
                                </div>
                            </div>
                        	<?php endif; ?>

                        	<div class="rowElem">
                                <label class="formLeft">Author:</label>
                                <div class="formRight searchDrop">

                                <?php
                                	$my_admin_type = $this->session->userdata('admin_type');
								?>
                                <?php if($my_admin_type == '5' || $my_admin_type == '6'): ?>
                                	<span style="font-size:18px;">
                                    	 <b><?php echo $this->Site_model->getAuthor($g->id); ?></b>
                                   	</span>
                                <?php else: ?>
									<div class="selectorwrap">
									<select name="author_id" class="author_select chzn-select" style="width:250px;">
                                    	<?php 
                                    	foreach($this->Admin_model->get_users('','byalpha') as $p) {
                                    		echo '<option value="'.$p->aid.'"';
												if($p->aid == $g->author_id)
													echo ' selected="selected"';
                                    		echo '>'.$p->firstname.' '.$p->lastname;
                                    		if($p->admin_type == '6')
                                    			echo ' (Guest Author)';
                                    		echo '</option>';
                                    	} ?>
                                    	<option value="0"
											<?php 
												if('0' == $g->author_id)
													echo ' selected="selected"';
											?>
                                    	>Other Author</option>
                                    </select>
                                    </div>


                                    <input type="text" name="author" placeholder="Other Author" value="<?php echo $g->author; ?>" style="<?php 
												if('0' != $g->author_id)
													echo 'display:none;';
												if(($g->author_id == '0') && ($g->author == ''))
													echo 'display:none;';

											?> margin-top:10px;" class="pbgbaam authorbox" />
                                    
                                    

                                
	                                <div style="">
	                                	<input type="checkbox" id="posted_by_gbaam" name="posted_by_gbaam" <?php
	                                						if(($g->author_id == '0') && ($g->author == ''))
	                                							echo 'checked';
	                                						?>> 
	                                	<label style="margin-top:7px;margin-left:10px;font-size:18px;" for="posted_by_gbaam" class="">Post as <b>Gbaam</b></label>
	                                </div>
	                            <?php endif; ?>
                                </div>
                            </div>
							

							<?php if($g->reviewed_by != '0'): ?>
                        	<div class="rowElem">
                                <label class="formLeft" style="margin-top:10px;">Reviewed By:</label>
                                <div class="formRight">
                                <?php
								    // get the admin information from the session
								    $me = $this->Admin_model->getAdmin($g->reviewed_by);
								?>
                                	<span style="font-size:18px;">
                                    	 <b><?php echo $me->display_name; ?></b>
                                   	</span>
                                </div>
                            </div>
                        	<?php endif; ?>
							
							<?php 
								if($g->status == '4'):
									echo '<input type="hidden" name="status" value="4">';
								
									if($this->session->userdata('admin_type') == '5'):

							?>
							<div class="rowElem">
                        	
                                <label class="formLeft">Status: </label>
                                <div class="formRight">
									<input type="hidden" name="status" value="4">
									<span style="font-size:18px;">
                                    	 <b>In Review</b>
                                   	</span>
								</div>
							</div>
							<?php 
									endif;
								endif;

								if($this->session->userdata('admin_type') == '1'): 
									if($g->status != '4'):
							?>
                        	<div class="rowElem">
                        	
                                <label class="formLeft">Visibility: </label>
                                <div class="formRight noSearch">
                                    <select name="status" class="chzn-select" style="width:250px;">  
                                    	<option value="1" <?php if($g->status == 1) echo "selected"; ?>>
                                        	Public
                                        </option>

                                        <option value="2" <?php if($g->status == 2) echo "selected"; ?>>
                                        	Unlisted
                                        </option>

                                        <option value="3" <?php if($g->status == 3) echo "selected"; ?>>
                                        	Private
                                        </option>
                                        
                                    </select>
                                </div>
                                <div class="fix"></div>
                            </div>
                        	<?php 
                        			endif;
                        		endif; 
                        	?>
           
                            <div class="rowElem">
                                <label class="formLeft">Current Image:</label>
                                <div class="formRight">
                                    <img style="max-width:80%;" src="<?php echo get_image('article',$g->image,'',''); ?>" />
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Change Image? <br>(640 x 360):</label>
                                <div class="formRight">
                                    <input type="file" name="lg_img">
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Title:</label>
                                <div class="formRight">
                                    <input type="text" name="title" placeholder="Title" value="<?php echo set_value('title', $g->title); ?>" />
                                </div>

								<input type="hidden" name="permalink" class="js_permalink" placeholder="Permalink" value="<?php echo set_value('permalink', $g->permalink); ?>" />

                                <span class="ptext" style="margin-bottom:10px;font-size:14px;float:left;width:100%">
                                    <b>Permalink:</b> <?php echo base_url(); ?>editorial/<b><span class="permtxt"><?php echo $g->permalink; ?></span>
										<span class="editpermalinkbutton jsbutton">Edit</span>
										<span class="okayclick jsbutton" style="display:none;">OK</span>
                                    </b>
                                </span>

                            </div>

                            <div class="rowElem">
                            	<label class="formLeft">
                            		<label for="tags">Tags:</label>
                            	</label>
                            	<div class="formRight">
                            		<input type="text" id="tags" name="tags" class="tags" value="<?php echo set_value('tags', $g->tags); ?>" />
                            	</div>
                            	<div class="fix"></div>
                    		</div>

                            

                            

                    <div class="fix"></div>
                </div>
            </fieldset> 
             <fieldset>
                <div class="widget">    
                    <div class="head"><h5 class="iPencil">Content</h5></div>
                    <textarea rows="20" name="body" class="ckeditor" id="ckeditor" cols=""><?php echo set_value('body', $g->body); ?></textarea>       
                            
                </div>

                <div class="widget">
                	<?php
                		if($g->status == '4' && $this->session->userdata('admin_type') <= '4'):
                	?>
                	<div class="rowElem kenny reasons_box" style="display:none;float:left;border:1px solid red;">
	                   <div class="rowElem">
	                        <label class="formLeft" style="">Please choose a reason for <b>Denial of this article</b>:</label>
	                        <div class="formRight">
	                            <span style="font-size:18px;">
	                            	<?php foreach($this->Admin_model->get_reasons('deny') as $g): ?>
	                                
	                                <div style="float:left;width:100%;margin-bottom:10px;">
	                                	<input style="float;left;" type="checkbox" name="deny_reasons[]" value="<?php echo $g->id; ?>">
	                                	<b style="float:left;margin-left:10px;margin-top:8px;"><?php echo $g->display_name; ?></b>
	                            	
	                            	</div>

	                            	<?php endforeach; ?>
	                            </span>
	                        </div>
	                   	</div>

	                    <div class="rowElem">
	                        <label class="formLeft" style="">Add a custom reason:</label>
	                        <div class="formRight">
	                            	<textarea rows="8" cols="" name="deny_reasons_other" class="auto" style="overflow: hidden;" placeholder=""/></textarea>
	                        </div>
	                    </div>
	                    <div class="fix"></div>

                        </div>
                  <div class="rowElem kenny">
                   <div style="float:left;margin-top:30px;width:100%;text-align:center;">

					<div class="real_deny_button" style="display:none;">
					<input type="submit" name="submit_deny" value="Deny" class="think red" />
					</div>
					<span  class="think red fake_deny" style="font-size:20px;cursor:pointer;">Deny</span>
                    <input type="submit" name="submit" value="Approve" class="think green" />

                    
                   </div>
					


	                    
	                <div class="fix"></div>

                    </div>

                	<?php 
                		else:
                	?>
                    <div class="floatright twoOne">
                   
                    <input type="submit" name="submit" value="Submit form" class="greyishBtn submitForm" />
                    
                    </div>
                    <?php 
                    	endif;
                    ?>
                    <div class="fix"></div>
                </div>
            </fieldset>
        </form>
        <?php endforeach; ?>
        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<?php include('inc/footer.php'); ?>

</body>
</html>
