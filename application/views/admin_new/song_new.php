<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>
	
    <script type="text/javascript">
        $(function(){
        	//$('form').areYouSure();


            $( "#datepicker" ).datepicker();

            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                    $('#post_'+id).fadeOut();
                }

                return false;
            });

            //$('input[type="checkbox"]').on('click',function() {
            $('#posted_by_gbaam').on('click',function(){
			   	$('.selectorwrap').toggle();

            	if($('.author_select').val() == '0')
            	{
            		$('.authorbox').val(saved_other_author).toggle();
            	}
			});

            var saved_other_author = $('.saved_other_author').val();
            
            $('.author_select').change(function(){

			  if($(this).val() == '0'){ // or this.value == 'volvo'
			    if($('.authorbox').css('display') === 'none')
            	{
            		$('.authorbox').val(saved_other_author).show();
            	}
			  }
			  else
			  {
			  	$('.authorbox').val('');
			  	$('.authorbox').hide();
			  }
			});

			$('form').on('submit',function(){

				$('.js_mp3file').hide();
				$('.mp3file').append('<img src="http://gbaam.com/assets/packs/gbaamadmin/images/loaders/loader.gif" /><b>Uploading...</b>');


			});

            /*var timer = null;
            var permalink_typed = 0;

            $('.js_permalink').keyup(function(){
                permalink_typed = 1;
            });

            if(permalink_typed == 0 )
            {
                $('.mixtape_title').keyup(function() {
                    clearTimeout(timer);
                    timer = setTimeout(doneTyping, 1500);
                });
            }
            
*/

        
        
        });

      /*  function doneTyping () {
        $.post( "<?php echo $this->config->item('admin_location'); ?>/pretty_url/", { url: $('.mixtape_title').val() }, function( data ) {
     
            //alert( data);
            $('.js_permalink').val(data);
        });


}*/
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>




<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content" id="container">
    	<div class="title"><h5><?php echo $title; ?></h5></div>

    	<?php
            if(isset($validation))
            {
            	if($validation == FALSE)
            	{
            		echo '<span class="wrong">'.$message.'</span>';
            	}
            }
            if(isset($error))
            {
            	echo '<span class="wrong">'.$error.'</span>';
            }

        ?>
        
        <!-- Form begins -->
        <form action="<?php echo base_url() . $this->uri->uri_string(); ?>" class="mainForm" method="post" enctype='multipart/form-data'>
        	<!-- Input text fields -->
            <fieldset>
                <div class="widget first">
                    <div class="head"><h5 class="iList">Enter Song Information</h5></div>
                        <div class="rowElem kenny">
							
							<div class="rowElem">
                                <label class="formLeft">Author:</label>
                                <div class="formRight searchDrop">

                                <?php
								    // get the admin information from the session
								    $uid = $this->session->userdata('admin_user_id');
								    $me = $this->Admin_model->getAdmin($uid);
								    $admin_type = $this->session->userdata('admin_type');
								?>
                                <?php if(($admin_type == '5') || ($admin_type == '6')): ?>
                                	<span style="font-size:18px;">
                                    	 <b><?php echo $me->display_name; ?></b>
                                   	</span>
                                   	<input type="hidden" name="author_id" value="<?php echo $uid; ?>" />
                                <?php elseif($this->session->userdata('admin_type') == '1'): ?>
                                	<div class="selectorwrap">
									<select name="author_id" class="author_select chzn-select" style="width:250px;">
                                    	<?php 
                                    	foreach($this->Admin_model->get_users('','byalpha') as $g){
                                    		echo '<option value="'.$g->aid.'"';
												if($g->aid == $this->session->userdata('admin_user_id'))
													echo ' selected="selected"';
                                    		echo '>'.$g->firstname.' '.$g->lastname;
                                    		if($g->admin_type == '6')
                                    			echo ' (Guest Author)';
                                    		echo '</option>';
                                    	} ?>
                                    	<option value="0">Other Author</option>
                                    </select>
                                    </div>


                                    <input type="text" name="author" placeholder="Other Author" value="" style="display:none;margin-top:10px;" class="pbgbaam authorbox" />
                                    
                                   
	                                <div style="">
	                                	<input type="checkbox" id="posted_by_gbaam" name="posted_by_gbaam"> 
	                                	<label style="margin-top:7px;margin-left:10px;font-size:18px;" for="posted_by_gbaam" class="">Post as <b>Gbaam</b></label>
	                                </div>
	                                 <?php endif; ?>
                                </div>

                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Release Date:</label>
                                <div class="formRight"><input name="date" type="text" class="maskDate" id="datepicker" value="<?php echo set_value('date', date('m/d/Y')); ?>" /></div>
                                <div class="fix"></div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Song Title:</label>
                                <div class="formRight">
                                    <input type="text" name="song_title" class="song_title" placeholder="Song Title" value="<?php echo set_value('song_title', ''); ?>" />
                                </div>
                            </div>
                            
                            <div class="rowElem">
                                <label class="formLeft">Artist(s):</label>
                                <div class="formRight">
                                    <input type="text" name="artist_name" placeholder="Davido -or- Davido Ft. Ice Prince" value="<?php echo set_value('artist_name', ''); ?>" />
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Song Description:</label>
                                <div class="formRight">
                                    <textarea rows="8" cols="" name="description" class="auto" style="overflow: hidden;" placeholder="Song Description" /><?php echo set_value('description'); ?></textarea>
                                </div>
                            </div>

                            <div class="rowElem">
                            	<label class="formLeft">
                            		<label for="tags">Tags:</label>
                            	</label>
                            	<div class="formRight">
                            		<input type="text" id="tags" name="tags" class="tags" value="<?php echo set_value('tags', ''); ?>"  />
                            	</div>
                            	<div class="fix"></div>
                    		</div>

                            <div class="fix"></div>

                            <div class="rowElem">
                                <label class="formLeft">Song Artwork:</label>
                                <div class="formRight">
                                    <input type="file" name="ft_cover">
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Mp3/Wav File:</label>
                                <div class="formRight mp3file">
                                	<span class="js_mp3file">
                                    	<input type="file"  name="mp3_file">
                                    </span>
                                </div>
                            </div>

                        </div>

              
                  
                    <div class="floatright twoOne">
                   
                    <input type="submit" name="submit" value="Add Files" class="greyishBtn submitForm" />
                    
                    </div>
                    <div class="fix"></div>
                </div>
            </fieldset> 
        </form>
        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<?php include('inc/footer.php'); ?>


</body>
</html>
