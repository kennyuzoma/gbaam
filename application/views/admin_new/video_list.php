<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){
            $('.deletepost').on('click',function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                     $.post('<?php echo base_url().$this->config->item('admin_location'); ?>/videos/mv/delete/'+id, function() {
                        $('#post_'+id).fadeOut();
                    });
                }

                return false;
            });
        });
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>


<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content">
        <div class="title"><h5>Edit Videos</h5></div>
        
        
        <!-- Static table with resizable columns -->
        <div class="widget">
            <div class="head"><h5 class="iFrames">Video list</h5></div>
            <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic resize">
                <thead>
                    <tr>
                        <?php $style = ''; ?>
                        <?php if($video_type == 'challenge' || $video_type == 'gbaamtv'):
                                $style = 'style="width:200px;"';
                         ?>

                        <td width="100">Upload Date</td>
                        <td width="80">Category</td>
                        <td width="420">Title</td>
                        <td width="100">Action</td>

                        <?php else: ?>

                        <td width="100">Upload Date</td>
                        <td width="500">Title</td>
                        <td width="100">Action</td>

                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach($get_videos as $gv): ?>

                    <tr class="videopost" id="post_<?php echo $gv->vid; ?>">
                        <td align="center"><?php echo conv_date($gv->date_created); ?></td>

                        <?php if($video_type == 'challenge' || $video_type == 'gbaamtv'): ?>
                        <td align="center"><b>
                            <?php 
                            // returning the challenge name...
                            // WIll fix sql so that it will say
                            // chal_name.. etc.
                            echo $gv->display_name; 
                            ?></b>
                        </td>
                        <?php endif; ?>

                        <td>
                            <a href="<?php echo $this->config->item('admin_location'); ?>/videos/<?php echo $video_type; ?>/edit/<?php echo $gv->vid; ?>" class="info">
                                <img class="thumb" src="<?php 

                                        if($gv->thumb == '')
                                        {
                                            if($gv->yt == 1){

                                                echo 'http://img.youtube.com/vi/'.youtube_id_from_url($gv->video_src).'/3.jpg';
                                            }
                                            else
                                              {
                                                echo base_url()."thumbs/Gbaam3.jpg";
                                              }

                                        }
                                            else
                                                {

                                                    echo base_url() . 'thumbs/' . $gv->thumb; 
                                                }
                                        

                                        ?>" />
                                <span class="name" <?php echo $style; ?>>
                                    <?php 
                                        if($gv->artist_name == ''){ 
                                            echo $gv->title;
                                        } else {
                                           echo $gv->artist_name.' - '. $gv->title;
                                        }
                                    ?>
                                </span> 
                            </a>
                        </td>
                        <td>
                            <a  class="deletepost" id="<?php echo $gv->vid; ?>">Delete</a> &middot;<br> 
                            <a href="<?php echo $this->config->item('admin_location'); ?>/videos/<?php echo $video_type; ?>/edit/<?php echo $gv->vid; ?>">Edit</a> &middot;<br> 
                            <a href="<?php echo $viewlink.$gv->vid; ?>" target="_blank">View Video</a></td>
                    </tr>

                    <?php endforeach; ?>

                    

                </tbody>
            </table>
        </div>

        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&copy; Copyright 2011. All rights reserved. It's Brain admin theme by <a href="#" title="">Eugene Kopyov</a></span>
    </div>
</div>

</body>
</html>
