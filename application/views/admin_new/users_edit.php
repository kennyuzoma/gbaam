<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){

            $( "#datepicker" ).datepicker();

            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                    $('#post_'+id).fadeOut();
                }

                return false;
            });

            $('.js_admin_type').on('change', function() {

            	var admin_type = $(this).val();

            	if(admin_type == 1 || admin_type == 2)
            	{
            		$('.js_access_div').hide();
            	}
            	else
            	{
            		$('.js_access_div').show();
            	}

            	if(admin_type > 4)
            	{
            		// hide the songs area
            		$('.js_chk_slides').hide();

            	}
            	else
            		$('.js_chk_slides').show();
				//alert( this.value ); // or $(this).val()
				
			});

            var elem = $("#chars");
			$('textarea').limiter(500, elem);


        });

       (function($) {
		    $.fn.extend( {
		        limiter: function(limit, elem) {
		            $(this).on("keyup focus", function() {
		                setCount(this, elem);
		            });
		            function setCount(src, elem) {
		                var chars = src.value.length;
		                if (chars > limit) {
		                    src.value = src.value.substr(0, limit);
		                    chars = limit;
		                }
		                elem.html( limit - chars );
		            }
		            setCount($(this)[0], elem);
		        }
		    });
		})(jQuery);
    </script>
</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>




<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content" id="container">
    	<div class="title"><h5>Users</h5></div>

		<?php
            if(isset($validation))
            {
              if($validation == FALSE)
              {
                echo '<span class="wrong">'.$message.'</span>';
              }
            }
          ?>

          <a style="
		    margin-top: 15px;
		    float: left;
		    width:100%;
		    font-weight: bold;
		    font-size: 18px;
		" href="<?php echo $this->config->item('admin_location') . '/'.$this->uri->segment(2).'/list'; ?>">&laquo; Go Back</a>

        <!-- Form begins -->
        <form action="<?php echo base_url() . $this->uri->uri_string(); ?>" class="mainForm" method="post" enctype="multipart/form-data">
			
			<input type="hidden" value="<?php echo $user_id; ?>" name="user_id">

        	<!-- Input text fields -->
            <fieldset>
                <div class="widget first">
                    <div class="head"><h5 class="iList">Edit User</h5></div>
                        <div class="rowElem kenny">
							<?php 
                                $admin_type = $this->session->userdata('admin_type');

                               
                            ?>

                            

                        	<div class="rowElem">
                                <label class="formLeft">
                                	<?php 
                                		if($admin_type == '1')
                                			echo 'Admin';
                                		else
                                			echo 'User';
                                	?>
                                	 Type: 
                                </label>
                                <div class="formRight noSearch">

                                	<?php if($admin_type == '1'): ?>
                                    <select name="admin_type" class="js_admin_type chzn-select" style="width:250px;">
                                        <?php foreach($this->Admin_model->get_admin_types() as $c): ?>
                                            <option value="<?php echo $c->id; ?>"
                                                <?php if($c->id == $g->admin_type) echo "selected"; ?>><?php echo $c->type_name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <?php else: ?>
                                	<span style="font-size:18px;">
                                    	 <b><?php echo $g->type_name; ?></b>
                                   	</span>
                                   	<?php  endif; 	?>
                                </div>
                                <div class="fix"></div>
                            </div>

                            <div class="rowElem js_access_div">
                                <label class="formLeft">
                                	Access to:
                                </label>
                                <div class="formRight searchDrop">

                                	<?php if($admin_type == '1'): ?>
                                        <?php foreach($this->Admin_model->get_admin_types('access') as $c): ?>
											<div style="float:left;width:100%;margin-bottom:5px;" class="js_chk_<?php echo underscore($c->type); ?>">

	                                        	<input type="checkbox" id="<?php echo $c->type; ?>" name="access[<?php echo $c->id; ?>]" style="float;left;" value="<?php echo $c->id; ?>"
													<?php 
														if($this->Admin_model->get_user_access_other($user_id,$c->id) == TRUE) 
															echo 'checked';
													?>

													<?php 
														if($g->admin_type == '1') 
															echo 'disabled';
													?>
	                                        	 />
	                                        	<label for="<?php echo $c->type; ?>" style="float:left;margin-left:10px;margin-top:8px;<?php 
														if($g->admin_type == '1') 
															echo 'color:#777';
													?>">
	                                        		<?php echo ucfirst($c->type); ?>
	                                        	</label>
	                                        </div>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                	<span style="font-size:18px;">
                                    	 <b><?php echo $g->type_name; ?></b>
                                   	</span>
                                   	<?php  endif; 	?>
                                </div>
                                <div class="fix"></div>
                            </div>

                           

                             <div class="rowElem">
                                <label class="formLeft">Add Your Photo:</label>
                                <img src="<?php echo get_image('admin_user',$g->profile_img,$g->img_hash,'200'); ?>" />
                                <div class="formRight">
                                    <input type="file" name="lg_img">
                                </div>
                            </div>
                            

							<div class="rowElem">
                                <label class="formLeft">First Name:</label>
                                <div class="formRight">
                                    <input type="text" name="firstname" placeholder="First Name" value="<?php echo set_value('firstname', $g->firstname); ?>" />
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Last Name:</label>
                                <div class="formRight">
                                    <input type="text" name="lastname" placeholder="Last Name" value="<?php echo set_value('lastname', $g->lastname); ?>" />
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Username:</label>
                                <div class="formRight">
                                    <input class="noselect" type="text" name="username" placeholder="Username" value="<?php echo set_value('username', $g->username); ?>" autocomplete='off' />
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Email:</label>
                                <div class="formRight">
                                    <input type="text" name="email" placeholder="Email Address" value="<?php echo set_value('email', $g->email); ?>" autocomplete='off' />
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Bio<br>(<span id="chars">500</span> Characters Left):</label>
                                <div class="formRight">
                                    <textarea rows="8" cols="" name="bio" class="auto" style="overflow: hidden;" placeholder="Author Bio"/><?php echo set_value('bio', $g->bio); ?></textarea>
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Website:</label>
                                <div class="formRight">
                                    <input type="text" name="website" placeholder="Website" value="<?php echo set_value('website', $g->website); ?>" autocomplete='off' />
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Facebook URL:</label>
                                <div class="formRight">
                                    <input type="text" name="facebook" placeholder="Facebook URL" value="<?php echo set_value('facebook', $g->facebook); ?>" autocomplete='off' />
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Twitter Username:</label>
                                <div class="formRight">
                                    <input type="text" name="twitter" placeholder="Twitter Username" value="<?php echo set_value('twitter', $g->twitter); ?>" autocomplete='off' />
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Google+ URL:</label>
                                <div class="formRight">
                                    <input type="text" name="googleplus" placeholder="Google+ URL" value="<?php echo set_value('googleplus', $g->googleplus); ?>" autocomplete='off' />
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Instagram Username:</label>
                                <div class="formRight">
                                    <input type="text" name="instagram" placeholder="Instagram Username" value="<?php echo set_value('instagram', $g->instagram); ?>" autocomplete='off' />
                                </div>
                            </div>

                         
								
                            

                            <div class="rowElem">
                            	<b style="float:left;width:100%;font-size:20px;text-align:Center;margin-top:20px;margin-bottom:10px;color:#3a9d23;">Only if you need to change the password</b>
                                <label class="formLeft">Password:</label>
                                <div class="formRight">
                                    <input type="password" name="password" placeholder="" value="" autocomplete='off'/>
                                </div>
                            </div>


                            <div class="rowElem">
                                <label class="formLeft">Password Again:</label>
                                <div class="formRight">
                                    <input type="password" name="password2" placeholder="" value="" autocomplete='off'/>
                                </div>
                            </div>

                    <div class="floatright twoOne">
                   
                    <input type="submit" name="submit" value="Submit form" class="greyishBtn submitForm" />
                    
                    </div>
                    <div class="fix"></div>
                </div>
            </fieldset> 
        </form>
        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<?php include('inc/footer.php'); ?>

</body>
</html>
