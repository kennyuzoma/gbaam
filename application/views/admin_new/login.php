<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title; ?></title>

    <!-- Required files -->
    <?php $this->load->view('admin_new/inc/req_files.php'); ?>

    <style>
</style>
</head>

<body>

<!-- Top navigation bar -->
<div id="topNav">
    <div class="fixed">
        <div class="wrapper">
            <?php /*
            <div class="backTo"><a href="#" title=""><img src="http://www.gbaam.com/assets/img/Gbaam_logo270.png" alt="" height="30"/></a></div>
            */ ?>
            <div class="userNav">
                <ul>
 
                    <li><a href="mailto:<?php echo $this->config->item('contact_admin_email'); ?>" title=""><img src="<?php echo base_url(); ?>assets/packs/gbaamadmin/images/icons/topnav/contactAdmin.png" alt="" /><span>Contact admin</span></a></li>
                    
                </ul>
            </div>
            <div class="fix"></div>
        </div>
    </div>
</div>


<span style=""><?php if(isset($message)) echo $message; ?></span>
<?php 
	if(isset($_GET['success'] )):
		if($_GET['success'] == 'true'): ?>
    <div class="js_success" style="float:left;width:100%;background:#e3ffd1;border:1px solid #8cbd75;border-radius:5px;text-align:center;font-size:16px;padding:6px 0;margin-bottom:10px;font-weight:bold;">
		Thank you. You may now login now!
    </div>
<?php 
		endif; 
	endif;
?>
<!-- Login form area -->
<div class="loginWrapper">

 
    
	<div class="loginLogo"><img src="http://www.gbaam.com/assets/img/Gbaam_logo270.png" alt="" /></div>
    <div class="loginPanel">
        <div class="head"><h5 class="iUser">Administrator Login</h5></div>
        <form action="<?php echo base_url() . $this->uri->uri_string(); ?>" id="valid" class="mainForm" method="POST">
            <fieldset>
                <div class="loginRow noborder">
                    <label for="req1">Username:</label>
                    <div class="loginInput"><input type="text" name="username" value="<?php echo $this->input->post('username'); ?>" class="validate[required]" id="req1" /></div>
                    <div class="fix"></div>
                </div>
                
                <div class="loginRow">
                    <label for="req2">Password:</label>
                    <div class="loginInput"><input type="password" name="password" class="validate[required]" id="req2" /></div>
                    <div class="fix"></div>
                </div>
                
                <div class="loginRow">
                    <div class="rememberMe"><input type="checkbox" id="check2" name="chbox" /><label for="check2">Remember me</label></div>
                    <input type="submit" name="submit_login" value="LOGIN" class="greyishBtn submitForm" />
                    <div class="fix"></div>
                </div>
            </fieldset>
        </form>
    </div>
</div>

    <!-- Footer -->
    <?php $this->load->view('admin_new/inc/footer.php'); ?>

</body>
</html>
