<!DOCTYPE html>
<html>
<head> 
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){
            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                     $.post('<?php echo base_url().$this->config->item('admin_location'); ?>/hotmixtapes/remove/'+id, function() {
                        $('#post_'+id).fadeOut();
                    });
                }

                return false;
            });
        });
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>


<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

    <!-- Content -->
    <div class="content">
        <div class="title"><h5>Edit Slides</h5></div>
        
        
        <!-- Static table with resizable columns -->
        <div class="widget">
            <div class="head"><h5 class="iFrames">Slide List</h5></div>
            <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic resize">
                <thead>
                    <tr>
                        <td width="50">Slot</td>
                        <td width="100">Added date</td>
                        <td width="450">Title</td>
                        <td width="100">Action</td>
                    </tr>
                </thead>
                <tbody>
                
                    <?php 
                        if($this->Mixtape_model->getHotMixtape_count(1) == 0):
                    ?>
                        <tr>
                            <td>1</td>
                            <td></td>
                            <td>Empty... <a href="<?php echo $this->config->item('admin_location'); ?>/hotmixtapes/add/1">Add mixtape to Slot 1</a></td>
                            <td></td>
                        </tr>
                    <?php
                        else:
                    ?>
                        <tr class="videopost" id="post_<?php echo $hmixtape1->id; ?>">
                            <td align="center"><b><?php echo $hmixtape1->slot; ?></b></td>
                            <td align="center"><?php echo conv_date($hmixtape1->datestart); ?></td>
                            <td>
                                 <a href="<?php echo $this->config->item('admin_location'); ?>/mixtape/edit/<?php echo $hmixtape1->id; ?>" class="info">
                                    <img class="thumb" src="<?php echo get_image('mixtape',$hmixtape1->front_cover,$hmixtape1->img_hash,'125'); ?>" />
                                    <span class="name"><?php echo $hmixtape1->title.' - '.$hmixtape1->artist; ?></span>
                                </a>
                                
                            </td>
                            <td align="center"><?php /*<a href="#" class="deletepost" id="<?php echo $hmixtape1->id; ?>">Remove From Slot</a> &middot;<br> */ ?><a href="<?php echo $this->config->item('admin_location'); ?>/hotmixtapes/add/1">Replace</a> </td>
                        </tr>

                    <?php
                        endif;
                    ?>
                    
                    <?php 
                        if($this->Mixtape_model->getHotMixtape_count(2) == 0):
                    ?>
                        <tr>
                            <td>2</td>
                            <td></td>
                            <td>Empty... <a href="<?php echo $this->config->item('admin_location'); ?>/hotmixtapes/add/2">Add mixtape to Slot 2</a></td>
                            <td></td>
                        </tr>
                    <?php
                        else:
                    ?>
                        <tr class="videopost" id="post_<?php echo $hmixtape2->id; ?>">
                            <td align="center"><b><?php echo $hmixtape2->slot; ?></b></td>
                            <td><?php echo conv_date($hmixtape2->datestart); ?></td>
                            <td>
                                 <a href="<?php echo $this->config->item('admin_location'); ?>/mixtape/edit/<?php echo $hmixtape2->id; ?>" class="info">
                                    <img class="thumb" src="<?php echo get_image('mixtape',$hmixtape2->front_cover,$hmixtape2->img_hash,'125'); ?>" />
                                    <span class="name"><?php echo $hmixtape2->title.' - '.$hmixtape2->artist; ?></span>
                                </a>
                                
                            </td>
                            <td  align="center"><?php /*<a href="#" class="deletepost" id="<?php echo $hmixtape2->id; ?>">Remove From Slot</a> &middot;<br> */ ?><a href="<?php echo $this->config->item('admin_location'); ?>/hotmixtapes/add/2">Replace</a> </td>
                        </tr>

                    <?php
                        endif;
                    ?>
                    
                    
                    <?php 
                        if($this->Mixtape_model->getHotMixtape_count(3) == 0):
                    ?>
                        <tr>
                            <td>3</td>
                            <td></td>
                            <td>Empty... <a href="<?php echo $this->config->item('admin_location'); ?>/hotmixtapes/add/3">Add mixtape to Slot 3</a></td>
                            <td></td>
                        </tr>
                    <?php
                        else:
                    ?>
                        <tr class="videopost" id="post_<?php echo $hmixtape3->id; ?>">
                            <td align="center"><b><?php echo $hmixtape3->slot; ?></b></td>
                            <td><?php echo conv_date($hmixtape3->datestart); ?></td>
                            <td>
                                 <a href="<?php echo $this->config->item('admin_location'); ?>/mixtape/edit/<?php echo $hmixtape3->id; ?>" class="info">
                                    <img class="thumb" src="<?php echo get_image('mixtape',$hmixtape3->front_cover,$hmixtape3->img_hash,'125'); ?>" />
                                    <span class="name"><?php echo $hmixtape3->title.' - '.$hmixtape3->artist; ?></span>
                                </a>
                                
                            </td>
                            <td align="center"><?php /*<a href="#" class="deletepost" id="<?php echo $hmixtape3->id; ?>">Remove From Slot</a> &middot;<br> */ ?><a href="<?php echo $this->config->item('admin_location'); ?>/hotmixtapes/add/3">Replace</a> </td>
                        </tr>

                    <?php
                        endif;
                    ?>
                    

                </tbody>
            </table>
        </div>

        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
    <div class="wrapper">
        <span>&copy; Copyright 2011. All rights reserved. It's Brain admin theme by <a href="#" title="">Eugene Kopyov</a></span>
    </div>
</div>

</body>
</html>
