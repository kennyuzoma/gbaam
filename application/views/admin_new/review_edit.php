<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){
            $( "#datepicker" ).datepicker();

            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                    $('#post_'+id).fadeOut();
                }

                return false;
            });

            $('.fake_deny').click(function(){
            	$(this).remove();
            	$('.real_deny_button').show();
            	$('.reasons_box').show();
            });
        });
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>




<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content" id="container">

        
    	<div class="title"><h5><?php echo $title; ?></h5></div>

    	<?php
            if(isset($validation))
            {
            	if($validation == FALSE)
            	{
            		echo '<span class="wrong">'.$message.'</span>';
            	}
            }
        ?>

    	<a style="
		    margin-top: 15px;
		    float: left;
		    width:100%;
		    font-weight: bold;
		    font-size: 18px;
		" href="<?php echo $this->config->item('admin_location') . '/'.$this->uri->segment(2).'/list'; ?>">&laquo; Go Back</a>
        
        <!-- Form begins -->
        <form action="<?php echo base_url() . $this->uri->uri_string(); ?>" class="mainForm" method="post"  enctype="multipart/form-data">
        
        	<!-- Input text fields -->
            <fieldset>
                <div class="widget first">
                    <div class="head"><h5 class="iList">Pending Video Information</h5></div>
                        <div class="rowElem kenny">

							<div class="rowElem">
                                <label class="formLeft" style="margin-top:10px;">Name:</label>
                                <div class="formRight">
                                    <span style="font-size:18px;">
                                    	 <b><?php echo $g->firstname.' '.$g->lastname; ?></b>
                                   	</span>
                                </div>
                            </div>

                        	<div class="rowElem">
                                <label class="formLeft" style="margin-top:10px;">Email:</label>
                                <div class="formRight">
                                    <span style="font-size:18px;">
                                    	 <b><?php echo $g->email; ?></b>
                                   	</span>
                                </div>
                            </div>

                            

                            <div class="rowElem">
                                <label class="formLeft" style="margin-top:10px;">Submitted:</label>
                                <div class="formRight">
                                    <span style="font-size:18px;">
                                    	 <b><?php echo conv_date($g->date_submitted); ?></b>
                                   	</span>
                                </div>
                            </div>

                            <?php if($g->chall == '1'): ?>
                            <?php /*<div class="rowElem">
                                <label class="formLeft">Challenge: </label>
                                <div class="formRight">
                                    <select id="m" name="cat" class="styledselect-month">
                                        <?php foreach($this->Video_model->get_g_ch_cat() as $c): ?>
                                            <option value="<?php echo $c->id; ?>"><?php echo $c->sing_name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="fix"></div>
                            </div>*/ ?>

                            <div class="rowElem">
                                <label class="formLeft" style="margin-top:10px;">Challenge:</label>
                                <div class="formRight">
                                    <span style="font-size:18px;">
                                    	 <b><?php 
                                    	 $chall = $this->Challenge_model->sing_challenge_info($g->chall_cat);
                                    	 echo $chall->display_name; 
                                    	 ?></b>
                                    </span>
                                    <input type="hidden" name="cat" value="<?php echo $chall->id; ?>" />
                                </div>
                            </div>

                            <?php endif; ?>

                             <?php if($g->gtv == '1'): ?>
                            <div class="rowElem">
                                <label class="formLeft">GbaamTV Category: </label>
                                <div class="formRight noSearch">
                                    <select name="tv_cat" class="chzn-select" style="width:250px;">
                                        <?php foreach($this->Video_model->get_gtv_cat() as $c): ?>
                                            <option value="<?php echo $c->name; ?>"
                                                <?php if($c->name == $g->gtv_cat) echo "selected"; ?>><?php echo $c->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="fix"></div>
                            </div>
                            <?php endif; ?>

                            <div class="rowElem">
                            	<?php 
                            		$originalDate = $g->date_release;
									$newDate = date("m/d/Y", strtotime($originalDate));

									if($g->date_release == '1969-12-31')
										$thadate = date('m/d/Y');
									else
										$thadate = $newDate;
								?>
                                <label class="formLeft">Release Date:</label>
                                <div class="formRight"><input type="text" class="maskDate" id="datepicker" name="date" value="<?php echo set_value('date', $thadate); ?>" /></div>
                                <div class="fix"></div>
                            </div>
                            <?php if($g->gtv == '0' && $g->chall == '0'): ?>
                            <div class="rowElem">
                                <label class="formLeft">Artist Name:</label>
                                <div class="formRight">
                                    <input type="text" name="artistname" value="<?php echo set_value('artistname', $g->artist_name); ?>" />
                                </div>
                            </div>
                            <?php endif; ?>
                   
                            

                            <div class="rowElem">
                                <label class="formLeft">Title:</label>
                                <div class="formRight">
                                    <input type="text" name="videotitle" value="<?php echo set_value('videotitle', $g->title); ?>"/>
                                </div>
                            </div>
                           
                            <div class="rowElem">
                                <label class="formLeft">Description:</label>
                                <div class="formRight">
                                    <textarea rows="8" cols="" name="description" class="auto" style="overflow: hidden;" placeholder="Video Description"/><?php echo set_value('descriptiom', $g->description); ?></textarea>
                                </div>
                            </div>

                            <div class="rowElem">
                            	<label class="formLeft">
                            		<label for="tags">Tags:</label>
                            	</label>
                            	<div class="formRight">
                            		<input type="text" id="tags" name="tags" class="tags" value="<?php echo set_value('tags', ''); ?>" />
                            	</div>
                            	<div class="fix"></div>
                    		</div>
							
							<?php 
								if($g->gtv == '1'):
							?>
                            <div class="rowElem">
                                <label class="formLeft">Producer:</label>
                                <div class="formRight">
                                    <input type="text" name="prod" placeholder="Producer" value="<?php echo set_value('prod', $g->prod); ?>" />
                                </div>
                            </div>
                            
                            <div class="rowElem">
                                <label class="formLeft">Director:</label>
                                <div class="formRight">
                                    <input type="text" name="dir" placeholder="Director" value="<?php echo set_value('dir', $g->dir); ?>" />
                                </div>
                            </div>
                            
                            <div class="rowElem">
                                <label class="formLeft">Editor:</label>
                                <div class="formRight">
                                    <input type="text" name="editor" placeholder="Editor" value="<?php echo set_value('editor', $g->editor); ?>" />
                                </div>
                            </div>
                            
                            <div class="rowElem">
                                <label class="formLeft">Company:</label>
                                <div class="formRight">
                                    <input type="text" name="company" placeholder="Company" value="<?php echo set_value('company', $g->company); ?>" />
                                </div>
                            </div>
                        	<?php endif; ?>
                            
                            <?php if($g->video_src != ''): ?>
                            <div class="rowElem">
                                <label class="formLeft">Video Source:</label>
                                <div class="formRight">
                                    <input type="text" name="video_src" placeholder="Video Source" value="<?php echo set_value('video_src', $g->video_src); ?>" />
                                </div>
                                <span style="float:left;width:100%;text-align:center;font-size:20px;margin:20px 0px;">
                                	<a href="<?php echo $g->video_src; ?>" target="_blank">Review Video -> <?php echo $g->video_src; ?></a>
                                </span>
                            </div>
                        	<?php endif; ?>

							<?php if($g->video_src_other != ''): ?>
                            <div class="rowElem">
                                <label class="formLeft">Video Source Other:</label>
                                <div class="formRight">
                                    <input type="text" name="video_src_other" placeholder="Video Source Other" value="<?php echo set_value('video_src_other', $g->video_src_other); ?>" />
                                </div>
                            </div>
                        	<?php endif; ?>
                            
                            
                            <div class="rowElem">
                                <label class="formLeft">Current Thumbnail:</label>
                                <input type="hidden" name="curr_thumb" value="<?php echo $g->thumb; ?>" />
                                <div class="formRight">
                                    <?php 
                                    if($g->yt == 1){
                                        echo '<b style="float:left;width:100%;font-size:20px;text-align:Center;margin-bottom:5px;color:#3a9d23;">*Thumbnail imported from YOUTUBE*</b><br>';
                                    }
                                    ?>
                                    <img class="thumb" src="<?php 

                                        if($g->thumb == '')
                                        {
                                            if($g->yt == 1){

                                                echo 'http://img.youtube.com/vi/'.youtube_id_from_url($g->video_src).'/hqdefault.jpg';
                                            }
                                            else
                                              {
                                                echo base_url()."thumbs/Gbaam3.jpg";
                                              }
                                        }
                                            else
                                                {

                                                    echo base_url() . 'thumbs/' . $g->thumb; 
                                                }
                                        

                                        ?>" />

                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Change Thumbnail?:</label>
                                <div class="formRight">
                                    <input type="file" name="lg_img">
                                </div>
                            </div>


                            <div class="fix"></div>

                        </div>
                        <div class="fix"></div>

                        </div>

              <div class="rowElem kenny reasons_box" style="display:none;float:left;border:1px solid red;">
	                   <div class="rowElem">
	                        <label class="formLeft" style="">Please choose a reason for <b>Denial of this video</b>:</label>
	                        <div class="formRight">
	                            <span style="font-size:18px;">
	                            	<?php foreach($this->Admin_model->get_reasons('deny') as $g): ?>
	                                
	                                <div style="float:left;width:100%;margin-bottom:10px;">
	                                	<input style="float;left;" type="checkbox" name="deny_reasons[]" value="<?php echo $g->id; ?>">
	                                	<b style="float:left;margin-left:10px;margin-top:8px;"><?php echo $g->display_name; ?></b>
	                            	
	                            	</div>

	                            	<?php endforeach; ?>
	                            </span>
	                        </div>
	                   	</div>

	                    <div class="rowElem">
	                        <label class="formLeft" style="">Add a custom reason:</label>
	                        <div class="formRight">
	                            	<textarea rows="8" cols="" name="deny_reasons_other" class="auto" style="overflow: hidden;" placeholder=""/></textarea>
	                        </div>
	                    </div>
	                    <div class="fix"></div>

                        </div>
                  <div class="rowElem kenny">
                   <div style="float:left;margin-top:30px;width:100%;text-align:center;">

					<div class="real_deny_button" style="display:none;">
					<input type="submit" name="submit_deny" value="Deny" class="think red" />
					</div>
					<span  class="think red fake_deny" style="font-size:20px;cursor:pointer;">Deny</span>
                    <input type="submit" name="submit_approve" value="Approve" class="think green" />

                    
                   </div>
					


	                    
	                <div class="fix"></div>

                        </div>


                    
                    <div class="fix"></div>
                </div>
            </fieldset> 
        </form>
        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<?php include('inc/footer.php'); ?>

</body>
</html>
