<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>
	<link href="<?php echo base_url() ?>assets/packs/jp14/jplayer-black-and-yellowJack.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo base_url() ?>assets/packs/jp14/jquery.jplayer.min.js"></script>
    <script type="text/javascript">
	//<![CDATA[
	$(document).ready(function(){

		$("#jquery_jplayer_1").jPlayer({
			ready: function () {
				$(this).jPlayer("setMedia", {
					mp3: "<?php echo base_url().$this->config->item('songs_dir').$song->song_new_filename; ?>"
				});
			},
			volume: 1.0,
			swfPath: "js",
			supplied: "mp3"
		});
	});
	//]]>
	</script>


    <script type="text/javascript">
        $(function(){

        	
            $( "#datepicker" ).datepicker();

              //$('input[type="checkbox"]').on('click',function() {
              $('#posted_by_gbaam').on('click',function(){
			   	$('.selectorwrap').toggle();

            	if($('.author_select').val() == '0')
            	{
            		$('.authorbox').val(saved_other_author).toggle();
            	}
			});

            var saved_other_author = $('.saved_other_author').val();
            
            $('.author_select').change(function(){

			  if($(this).val() == '0'){ // or this.value == 'volvo'
			    if($('.authorbox').css('display') === 'none')
            	{

            		$('.authorbox').val(saved_other_author).show();

            	}
			  }
			  else
			  {
			  	$('.authorbox').val('');
			  	$('.authorbox').hide();
			  }
			});


			$('form').on('submit',function(){

				$('.js_mp3file').hide();
				$('.mp3file').append('<img src="http://gbaam.com/assets/packs/gbaamadmin/images/loaders/loader.gif" /><b>Uploading...</b>');


			});

        });
    </script>
	<style type="text/css">
		#jquery_jplayer_1{
			-webkit-touch-callout: none;
-webkit-user-select: none;
-khtml-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
		}
	</style>
</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>




<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content" id="container">

    	<div class="title"><h5><?php echo $title; ?></h5></div>

    	<?php
            if(isset($validation))
            {
            	if($validation == FALSE)
            	{
            		echo '<span class="wrong">'.$message.'</span>';
            	}
            }
        ?>
			
		<a style="
		    margin-top: 15px;
		    margin-bottom: 15px;
		    float: left;
		    font-weight: bold;
		    font-size: 18px;
		" href="<?php echo $this->config->item('admin_location') . '/'.$this->uri->segment(2).'/list'; ?>">&laquo; Go Back</a>
		<br>
		<div style="float:left;clear:both;"></div>

    	 <a href="<?php echo base_url().'songs/'.$song->id.'/'.$song->permalink; ?>" target="_blank" style="float:left;width:100%;text-align:Center;font-weight:bold;font-size:20px;">View Song</a>
        
        <!-- Form begins -->
        <form action="<?php echo base_url() . $this->uri->uri_string(); ?>" class="mainForm" method="post" enctype='multipart/form-data'>

            <input type="hidden" name="song_id" value="<?php echo $song->id; ?>" />
        	<!-- Input text fields -->
            <fieldset>
                <div class="widget first">
                    <div class="head"><h5 class="iList">Edit song</h5></div>
                        <div class="rowElem kenny">

							
							<?php if($song->author_id != '0' && $this->session->userdata('admin_type') == '1'): ?>
                        	<div class="rowElem">
                                <label class="formLeft" style="margin-top:10px;">Submitted By:</label>
                                <div class="formRight">
                                <?php
								    // get the admin information from the session
								    $me = $this->Admin_model->getAdmin($song->author_id);
								?>
                                	<span style="font-size:18px;">
                                    	 <b><?php echo $me->display_name; ?></b>
                                   	</span>
                                </div>
                            </div>
                        	<?php endif; ?>

                        	<div class="rowElem">
                                <label class="formLeft">Author:</label>
                                <div class="formRight searchDrop">

                               <?php
                                	$my_admin_type = $this->session->userdata('admin_type');
								?>
                                <?php if($my_admin_type > '4'): ?>
                                	<span style="font-size:18px;">
                                    	 <b><?php echo $this->Site_model->getAuthor($song->id,'song'); ?></b>
                                   	</span>
                                <?php else: ?>
									<div class="selectorwrap">
									<select name="author_id" class="author_select chzn-select" style="width:250px;">
                                    	<?php 
                                    	foreach($this->Admin_model->get_users('','byalpha') as $p) {
                                    		echo '<option value="'.$p->aid.'"';
												if($p->aid == $song->author_id)
													echo ' selected="selected"';
                                    		echo '>'.$p->firstname.' '.$p->lastname;
                                    		if($p->admin_type == '6')
                                    			echo ' (Guest Author)';
                                    		echo '</option>';
                                    	} ?>
                                    	<option value="0"
											<?php 
												if('0' == $song->author_id)
													echo ' selected="selected"';
											?>
                                    	>Other Author</option>
                                    </select>
                                    </div>


                                    <input type="text" name="author" placeholder="Other Author" value="<?php echo $song->author; ?>" style="<?php 
												if('0' != $song->author_id)
													echo 'display:none;';
												if(($song->author_id == '0') && ($song->author == ''))
													echo 'display:none;';

											?> margin-top:10px;" class="pbgbaam authorbox" />
                                    
                                    

                                
	                                <div style="">
	                                	<input type="checkbox" id="posted_by_gbaam" name="posted_by_gbaam" <?php
	                                						if(($song->author_id == '0') && ($song->author == ''))
	                                							echo 'checked';
	                                						?>> 
	                                	<label style="margin-top:7px;margin-left:10px;font-size:18px;" for="posted_by_gbaam" class="">Post as <b>Gbaam</b></label>
	                                </div>
	                            <?php endif; ?>
                                </div>
                            </div>

                        	<div class="rowElem">
                                <label class="formLeft">Visibility: </label>
                                <div class="formRight noSearch">
                                    <select name="status" class="chzn-select" style="width:250px;">
                                    	
                                    	<option value="1" <?php if($song->songstatus == 1) echo "selected"; ?>>Public</option>
                                        <option value="2" <?php if($song->songstatus == 2) echo "selected"; ?>>Unlisted</option>
                                        <option value="3" <?php if($song->songstatus == 3) echo "selected"; ?>>Private</option>
                                    </select>
                                </div>
                                <div class="fix"></div>
                            </div>

                            <div class="rowElem">
                            	<?php 
                                    $originalDate = $song->sdate_created;
                                    $newDate = date("m/d/Y", strtotime($originalDate));
                                ?>
                                <label class="formLeft">Date:</label>
                                <div class="formRight"><input name="date" type="text" class="maskDate" id="datepicker" value="<?php echo set_value('date', $newDate); ?>" /></div>
                                <div class="fix"></div>
                            </div>

   

                            <div class="rowElem">
                                <label class="formLeft">Song Title:</label>
                                <div class="formRight">
                                    <input type="text" name="song_title" class="song_title" value="<?php echo set_value('song_title', $song->title); ?>" placeholder="Song Title"/>
                                </div>
                            </div>
                            
                            <div class="rowElem">
                                <label class="formLeft">Artist(s):</label>
                                <div class="formRight">
                                    <input type="text" name="artist_name" value="<?php echo set_value('artist_name', $song->artist); ?>" placeholder="Davido -or- Davido Ft. Ice Prince"/>
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Song Description:</label>
                                <div class="formRight">
                                    <textarea rows="8" cols="" name="description" class="auto" style="overflow: hidden;" placeholder="Song Description"/><?php echo set_value('description', $song->desc); ?></textarea>
                                </div>
                            </div>

                            <div class="rowElem">
                            	<label class="formLeft">
                            		<label for="tags">Tags:</label>
                            	</label>
                            	<div class="formRight">
                            		<input type="text" id="tags" name="tags" class="tags" value="<?php echo set_value('tags', $song->tags); ?>" />
                            	</div>
                            	<div class="fix"></div>
                    		</div>

                            <div class="fix"></div>

                            <div class="rowElem">
                                <label class="formLeft">Artwork:</label>
                                <img src="<?php echo get_image('song',$song->front_cover,$song->img_hash,'200'); ?>" />
                                <div class="formRight">
                                    <input type="file" name="ft_cover">
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Mp3/Wav:</label>

                               	
                               	<div class="formRight mp3file">
                                	<span class="js_mp3file">

                                	<span style="font-weight:bold;font-size:15px;"><?php echo $song->song_original_filename; ?></span>
		                            	<div id="jquery_jplayer_1" class="jp-jplayer"></div>
										<div class="jp-audio-container">
											<div class="jp-audio">
												<div class="jp-type-single">
													<div id="jp_interface_1" class="jp-interface">
														<ul class="jp-controls">
															<li><a href="#" class="jp-play" tabindex="1">play</a></li>
															<li><a href="#" class="jp-pause" tabindex="1">pause</a></li>
															<li><a href="#" class="jp-mute" tabindex="1">mute</a></li>
															<li><a href="#" class="jp-unmute" tabindex="1">unmute</a></li>
														</ul>
														<div class="jp-progress-container">
															<div class="jp-progress">
																<div class="jp-seek-bar">
																	<div class="jp-play-bar"></div>
																</div>
															</div>
														</div>
														<div class="jp-volume-bar-container">
															<div class="jp-volume-bar">
																<div class="jp-volume-bar-value"></div>
															</div>
														</div>
													</div>
												</div>
											</div>
											
											
											</ul>
										</div>
										<br>
                                
                                    	<input type="file"  name="mp3_file">
                                    </span>
                                </div>
                            </div>


						</div>
                    <div class="fix"></div>
          
                    <div class="floatright twoOne">
                   
					<input type="submit" name="submit" value="Update song" class="greyishBtn submitForm" />
                    
                    
                    </div>
                    <div class="fix"></div>
                </div>
            </fieldset> 

            <a href="<?php echo base_url().'songs/'.$song->id.'/'.$song->permalink; ?>" target="_blank" style="float:left;width:100%;text-align:Center;margin:10px 0 30px 0;font-weight:bold;font-size:20px;">View song</a>

        </form>
        
    </div>

    
    
<div class="fix"></div>
</div>

<!-- Footer -->
<?php include('inc/footer.php'); ?>

</body>
</html>
