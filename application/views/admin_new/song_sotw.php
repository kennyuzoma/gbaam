<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title>
 
    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){
            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                    $.post('<?php echo base_url().$this->config->item('admin_location'); ?>/songs/remove_sotw/'+id, function() {
                        $('#post_'+id).fadeOut();
                    });
                    
                }

                return false;
            });
        });
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>


<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content">
        <div class="title"><h5>SOTW</h5></div>
        
        
        <!-- Static table with resizable columns -->
        <div class="widget">
            <div class="head"><h5 class="iFrames">SOTW list</h5></div>
            <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic resize">
                <thead>
                    <tr>
                      <td width="100">Upload Date</td>
                      <td width="450">Title</td>
                      <td width="150">Action</td>
                    </tr>
                </thead>
                <tbody>

                    <?php 
                    if($this->Songs_model->isSOTWavail() == FALSE):
                    ?>
                    <tr>
                        <td colspan="100%">
                            <b><a href="<?php echo $this->config->item('admin_location'); ?>/songs/select_sotw">Add a song for this week</a></b>
                        </td>
                    </tr>

                    <?php
                    else:
                    ?>

                    <?php foreach($get_sotw as $g): ?>

                    <tr class="videopost" id="post_<?php echo $g->id; ?>">
                        <td><?php echo conv_date($g->added); ?></td>
                        <td>
                            <a href="<?php echo $this->config->item('admin_location'); ?>/songs/edit/<?php echo $g->id; ?>" class="info">
                                <img class="thumb" src="<?php echo get_image('song',$g->front_cover,$g->img_hash,'125'); ?>" />
                                <span class="name"><?php echo $g->title.' - '.$g->artist; ?></span>
                            </a>
                        </td>
                        <td align="center">
                        	<?php /*<a href="#" class="deletepost" id="<?php echo $g->id; ?>">Remove MOTW?</a> &middot;<Br> */ ?>
                        	<a href="<?php echo $this->config->item('admin_location'); ?>/songs/list">Replace</a> &middot;<br> 
                        	<a href="chunes/<?php echo $g->id.'/'.$this->Songs_model->getSongUrl($g->id); ?>" target="_blank">View Song</a><br> 
                        </td>
                    </tr>

                    <?php endforeach; ?>
                <?php endif; ?>

                </tbody>
            </table>
        </div>

        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&copy; Copyright 2011. All rights reserved. It's Brain admin theme by <a href="#" title="">Eugene Kopyov</a></span>
    </div>
</div>

</body>
</html>
