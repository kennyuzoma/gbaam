<!DOCTYPE html>
<html> 
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){
            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                     $.post('<?php echo base_url().$this->config->item('admin_location'); ?>/site_users/delete/'+id, function() {
                        $('#post_'+id).fadeOut();
                    });
                }

                return false;
            });
        });
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>


<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content">
        
        <?php 
			$users = $this->User_model->get_users('desc');
		?>
        <!-- Static table with resizable columns -->
        <div class="table" style="margin-top:0px;">
            <div class="head"><h5 class="iFrames">Site Users (<?php echo $users['count']; ?>)</h5></div>
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                <thead>
                    <tr>
                    	<th>ID</th>
                        <th>Username</th>
                        <th>Display Name</th>
                        <th>User Status</th>
                        <th>SignUp Date</th>
                    </tr>
                </thead>
                <tbody>
					<?php
						foreach($users['result'] as $u):
					?>
                    <tr class="gradeA">
                    	<td><?php echo $u->id; ?></td>
                        <td><?php echo $u->username; ?></td>
                        <td><?php echo $u->display_name; ?></td>
                        <td class="center"><?php echo convert_u_status($u->u_status); ?></td>
                        <td class="center"><?php echo conv_date($u->date); ?></td>
                    </tr>
                    <?php
                    	endforeach; 
                    ?>
                    
                </tbody>
            </table>
        </div>

        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&copy; Copyright 2011. All rights reserved. It's Brain admin theme by <a href="#" title="">Eugene Kopyov</a></span>
    </div>
</div>

</body>
</html>
