<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>

    <script type="text/javascript">
        $(function(){

            $( "#datepicker" ).datepicker();

            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                    $('#post_'+id).fadeOut();
                }

                return false;
            });

            $('.wysiwyg').wysiwyg('removeFormat');
            
            var timer = null;
            var permalink_typed = 0;

            $('.js_permalink').keyup(function(){
                permalink_typed = 1;
            });

            if(permalink_typed == 0 )
            {
                $('.art_title').keyup(function() {
                    clearTimeout(timer);
                    timer = setTimeout(doneTyping, 1500);
                });
            }
            


        
        
        });

        function doneTyping () {
        $.post( "<?php echo $this->config->item('admin_location'); ?>/pretty_url/", { url: $('.art_title').val() }, function( data ) {
     
            //alert( data);
            $('.js_permalink').val(data);
        });
    }
    </script>

     <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>

    <script>
        tinymce.init({
            selector: "textarea",
    theme: "modern",
    plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor"
    ],
    external_plugins: {
        "jbimages": "/Projects/gbaam/assets/packs/tinymce/plugins/jbimages/plugin.js"
    },
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar2: "jbimages print preview media | forecolor backcolor emoticons",
    image_advtab: true,
    paste_as_text: true,
    relative_urls : false,
remove_script_host : false,
convert_urls : true
        });
    </script>

 

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>




<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content" id="container">
    	<div class="title"><h5>Blog Posts</h5></div>

    	<?php
            if(isset($validation))
            {
            	if($validation == FALSE)
            	{
            		echo '<span class="wrong">'.$message.'</span>';
            	}
            }
        ?>

        <!-- Form begins -->
        <form action="<?php echo base_url() . $this->uri->uri_string(); ?>" class="mainForm" method="post" enctype="multipart/form-data">

        	<!-- Input text fields -->
            <fieldset>
                <div class="widget first">
                    <div class="head"><h5 class="iList">Add Blog Post</h5></div>
                        <div class="rowElem kenny">
           
                            <div class="rowElem">
                                <label class="formLeft">Upload Featured Image:</label>
                                <div class="formRight">
                                    <input type="file" name="lg_img">
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Title:</label>
                                <div class="formRight">
                                    <input type="text" name="title" class="art_title" placeholder="Title" value="<?php echo set_value('title', ''); ?>" />
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Author:</label>
                                <div class="formRight">
                                    <input type="text" name="author" placeholder="Author" value="<?php echo set_value('author', ''); ?>" />
                                </div>
                            </div>

                            <div class="rowElem">
                            	<label class="formLeft">
                            		<label class="formLeft">
                            		<label for="tags">Tags:</label>
                            	</label>
                            	</label>
                            	<div class="formRight">
                            		<input type="text" id="tags" name="tags" class="tags" value="<?php echo set_value('tags', ''); ?>" />
                            	</div>
                            	<div class="fix"></div>
                    		</div>

                            <div class="rowElem">
                                <label class="formLeft">Permalink (Pretty URL for SEO):</label>
                                <div class="formRight">
                                    <input type="text" name="permalink" class="js_permalink" placeholder="Permalink" value="<?php echo set_value('permalink', ''); ?>" />
                                </div>
                            </div>



                            <div class="fix"></div>

                </div>
            </fieldset> 

            <fieldset>
                <div class="widget">    
                    <div class="head"><h5 class="iPencil">Content</h5></div>
                    <textarea rows="20" name="body" cols=""><?php echo set_value('body', ''); ?></textarea>                
                </div>

                <div class="widget">
                    <div class="floatright twoOne">
                   
                    <input type="submit" name="submit" value="Submit form" class="greyishBtn submitForm" />
                    
                    </div>
                    <div class="fix"></div>
                </div>
            </fieldset>

        </form>
        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<?php include('inc/footer.php'); ?>

</body>
</html>
