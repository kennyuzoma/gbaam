<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){
            $( "#datepicker" ).datepicker();

            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                    $('#post_'+id).fadeOut();
                }

                return false;
            });
        });
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>




<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content" id="container">

        
    	<div class="title"><h5><?php echo $title; ?></h5></div>

    	<?php
            if(isset($validation))
            {
            	if($validation == FALSE)
            	{
            		echo '<span class="wrong">'.$message.'</span>';
            	}
            }
         ?>

    	<a style="
		    margin-top: 15px;
		    float: left;
		    width:100%;
		    font-weight: bold;
		    font-size: 18px;
		" href="<?php echo $this->config->item('admin_location') . '/videolist/'.$this->uri->segment(3); ?>">&laquo; Go Back</a>
        
        <?php foreach($get_video as $g): ?>
        <!-- Form begins -->
        <form action="<?php echo base_url() . $this->uri->uri_string(); ?>" class="mainForm" method="post"  enctype="multipart/form-data">
        
        	<!-- Input text fields -->
            <fieldset>
                <div class="widget first">
                    <div class="head"><h5 class="iList">Enter Video Information</h5></div>
                        <div class="rowElem kenny">
							
							<?php if($g->poster_id != 0): ?>
                        	<div class="rowElem">
                                <label class="formLeft" style="margin-top:10px;">Posted By:</label>
                                <div class="formRight">
                                    <?php $me = $this->Admin_model->getAdmin($g->poster_id); ?>
                                	<span style="font-size:18px;">
                                    	 <b><?php echo $me->display_name; ?></b>
                                   	</span>
                                </div>
                            </div>
                        	<?php endif; ?>

                            <div class="rowElem">
                                <label class="formLeft">Visibility:</label>
                                <div class="formRight noSearch">
                                    <select name="status" class="chzn-select" style="width:250px;">  
                                    	<option value="1" <?php if($g->videostatus == 1) echo "selected"; ?>>
                                        	Public
                                        </option>

                                        <option value="2" <?php if($g->videostatus == 2) echo "selected"; ?>>
                                        	Unlisted
                                        </option>

                                        <option value="3" <?php if($g->videostatus == 3) echo "selected"; ?>>
                                        	Private
                                        </option>
                                        
                                    </select>
                                </div>
                                <div class="fix"></div>
                            </div>


                            <?php if($this->uri->segment(3) == 'challenge'): ?>
                            <div class="rowElem">
                                <label class="formLeft">Challenge: </label>
                                <div class="formRight noSearch">
                                    <select id="m" name="cat" class="styledselect-month chzn-select" style="width:250px;">

                                        <?php foreach($this->Video_model->get_g_ch_cat() as $c): ?>
                                            <option value="<?php echo $c->id; ?>"><?php echo $c->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="fix"></div>
                            </div>
                            <?php endif; ?>

                             <?php if($video_type == 'gbaamtv'): ?>
                            <div class="rowElem">
                                <label class="formLeft">Category: </label>
                                <div class="formRight noSearch">
                                    <select name="tv_cat" class="chzn-select" style="width:250px;">
                                        <option value="0">Select a Category</option>
                                        <?php foreach($this->Video_model->get_gtv_cat() as $c): ?>
                                            <option value="<?php echo $c->id; ?>"
                                                <?php if($c->id == $g->gtv_cat) echo "selected"; ?>><?php echo ucfirst($c->name); ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="fix"></div>
                            </div>
                            <?php endif; ?>

                            <div class="rowElem">
                                <?php 
                                    $originalDate = $g->vdate_created;
                                    $newDate = date("m/d/Y", strtotime($originalDate));
                                ?>
                                <label class="formLeft">Date:</label>
                                <div class="formRight"><input type="text" class="maskDate" name="date" id="datepicker" value="<?php echo set_value('date', $newDate); ?>" /></div>
                                <div class="fix"></div>
                            </div>
                            <?php if($video_type == 'mv'): ?>
                            <div class="rowElem">
                                <label class="formLeft">Artist Name:</label>
                                <div class="formRight">
                                    <input type="text" name="artistname" value="<?php echo set_value('artistname', $g->artist_name); ?>" />
                                </div>
                            </div>
                            <?php endif; ?>
                   
                            
                            <div class="rowElem">
                                <label class="formLeft">Title:</label>
                                <div class="formRight">
                                    <input type="text" name="videotitle" value="<?php echo set_value('videotitle', $g->title); ?>"/>
                                </div>
                            </div>
                           
                            <div class="rowElem">
                                <label class="formLeft">Description:</label>
                                <div class="formRight">
                                    <textarea rows="8" cols="" name="description"  placeholder="Video Description"/><?php echo set_value('description', $g->description); ?></textarea>
                                </div>
                            </div>
                            
                            <div class="rowElem">
                                <label class="formLeft">Video Source:</label>
                                <div class="formRight">
                                    <input type="text" name="video_src" placeholder="Video Source" value="<?php echo set_value('video_src', $g->video_src); ?>"  />
                                </div>
                            </div>

                            <div class="rowElem">
                            	<label class="formLeft">
                            		<label for="tags">Tags:</label>
                            	</label>
                            	<div class="formRight">
                            		<input type="text" id="tags" name="tags" class="tags" value="<?php echo set_value('tags', $g->tags); ?>" />
                            	</div>
                            	<div class="fix"></div>
                    		</div>
                            
                            
                            <div class="rowElem">
                                <label class="formLeft">Current Thumbnail:</label>
                                <input type="hidden" name="curr_thumb" value="<?php echo $g->thumb; ?>" />
                                <div class="formRight">
                                    <?php 
                                    if($g->yt == 1){
                                        echo '<b style="float:left;width:100%;font-size:20px;text-align:Center;margin-bottom:5px;color:#3a9d23;">*Thumbnail imported from YOUTUBE*</b><br>';
                                    }
                                    ?>
                                    <img class="thumb" src="<?php 

                                        if($g->thumb == '' && $g->yt == 1)
											echo 'http://img.youtube.com/vi/'.youtube_id_from_url($g->video_src).'/hqdefault.jpg';
            
                                        else
                                        	echo get_image('video',$g->thumb,$g->img_hash,'200');

                                        ?>" />

                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Change Thumbnail?:</label>
                                <div class="formRight">
                                    <input type="file" name="lg_img">
                                </div>
                            </div>


                            <div class="fix"></div>

                        </div>

              
                  
                    <div class="floatright twoOne">
                   
                    <input type="submit" name="submit" value="Submit form" class="greyishBtn submitForm" />
                    
                    </div>
                    <div class="fix"></div>
                </div>
            </fieldset> 
        </form>
    <?php endforeach; ?>
        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<?php include('inc/footer.php'); ?>

</body>
</html>
