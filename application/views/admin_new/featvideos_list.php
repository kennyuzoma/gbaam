<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title> 

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){
            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                     $.post('<?php echo base_url().$this->config->item('admin_location'); ?>/hotmixtapes/remove/'+id, function() {
                        $('#post_'+id).fadeOut();
                    });
                }

                return false;
            });
        });
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>


<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

    <!-- Content -->
    <div class="content">
        <div class="title"><h5>Featured Videos</h5></div>
        
        
        <!-- Static table with resizable columns -->
        <div class="widget">
            <div class="head"><h5 class="iFrames">Feat Videos</h5></div>
            <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic resize">
                <thead>
                    <tr>
                        <td width="50">Slot</td>
                        <td width="100">Added date</td>
                        <td width="450">Title</td>
                        <td width="100">Action</td>
                    </tr>
                </thead>
                <tbody>
                
                    <?php 
                    for ($i=1; $i <= $limit; $i++):

                        if($this->Video_model->getFeatVideo_slot_count($type,$i) == 0):
                    ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td></td>
                            <td>Empty... <a href="<?php echo $this->config->item('admin_location'); ?>/<?php echo $this->uri->segment(2); ?>/add/<?php echo $type; ?>/<?php echo $i; ?>">Add Video to Slot <?php echo $i; ?></a></td>
                            <td></td>
                        </tr>
                    <?php
                        else:
                            $video = $this->Video_model->getFeatVideo_slot($type,$i);
                    ?>
                        <tr class="videopost" id="post_<?php echo $video->id; ?>">
                            <td align="center"><b><?php echo $video->slot; ?></b></td>
                            <td align="center"><?php echo conv_date($video->date); ?></td>
                            <td>
                                 <a  class="info">
                                     <img class="thumb" src="<?php 

                                        if($video->thumb == '')
                                        {
                                            if($video->yt == 1){

                                                echo 'http://img.youtube.com/vi/'.youtube_id_from_url($video->video_src).'/3.jpg';
                                            }
                                        }
                                            else
                                                {

                                                    echo base_url() . 'thumbs/' . $video->thumb; 
                                                }
                                        

                                        ?>" />
                                    <span class="name">
                                        <?php 
                                        if($video->artist_name == ''){ 
                                            echo $video->title;
                                        } else {
                                           echo $video->artist_name.' - '. $video->title;
                                        }
                                        ?>
                                    </span>
                                </a>
                                
                            </td>
                            <td align="center"><?php /*<a href="#" class="deletepost" id="<?php echo $video->id; ?>">Remove From Slot</a> &middot;<br> */ ?><a href="<?php echo $this->config->item('admin_location'); ?>/<?php echo $this->uri->segment(2); ?>/add/<?php echo $type; ?>/<?php echo $i; ?>">Replace</a> </td>
                        </tr>

                    <?php
                        endif;

                    endfor;
                    ?>
                    
                
                    

                </tbody>
            </table>
        </div>

        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
    <div class="wrapper">
        <span>&copy; Copyright 2011. All rights reserved. It's Brain admin theme by <a href="#" title="">Eugene Kopyov</a></span>
    </div>
</div>

</body>
</html>
