<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title> 

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){

        	var fixHelper = function(e, ui) {
				ui.children().each(function() {
					$(this).width($(this).width());
				});
				return ui;
			};


        	$( "#sortable" ).sortable({ 
			    update: function(event, ui) {  
			        $('#sortable tr').each(function() {
			        	val = $(this).index()+1;
			            $(this).children('td:first-child').html('<b style="font-size:16px;">'+val+'</b>')
			        });

			        var data2 = $(this).sortable('serialize');

			        $.ajax({
			            data: data2,
			            type: 'POST',
			            url: '<?php echo base_url().$this->config->item('admin_location'); ?>/slides/updateorder/'
			      
			        });

			    },
			    helper: fixHelper
			}).disableSelection();

            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                     $.post('<?php echo base_url().$this->config->item('admin_location'); ?>/slides/delete/'+id, function() {
                        $('#post_'+id).fadeOut();
                    });
                }

                return false;
            });

        });
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>


<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content">
        <div class="title"><h5>Edit Slides (Drag to change order)</h5></div>
        
        
        <!-- Static table with resizable columns -->
        <div class="widget">
            <div class="head"><h5 class="iFrames">Slide List (Drag to change order)</h5></div>
            <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic resize">
                <thead>
                    <tr>
                     <td width="50">Order</td>
                      <td width="100">Added date</td>
                      <td width="450">Title</td>
                      <td width="100">Action</td>
                    </tr>
                </thead>
                <tbody id="sortable">

                    <?php foreach($get_slides as $gv): ?>

                    <tr class="videopost" id="post_<?php echo $gv->id; ?>">
                    	<td align="center">
                    		<b style="font-size:16px;">
                    			<?php echo $gv->position; ?>
							</b>
                    	</td>
                        <td align="center"><?php echo conv_date($gv->date_created); ?></td>
                        <td>
                            <a href="<?php echo $this->config->item('admin_location'); ?>/slides/edit/<?php echo $gv->id; ?>" class="info">
                                <img class="thumb_slide"  src="<?php echo base_url().$gv->image; ?>" />
                                
                            </a>
                            <br>
                            <span><b>Slider text: </b><?php echo $gv->alt_text; ?></span>
                            <br>
                            <span><b>Url: </b><?php echo $gv->href; ?></span>
                        </td>
                        <td><a href="#" class="deletepost" id="<?php echo $gv->id; ?>">Delete</a> &middot;<br> <a href="<?php echo $this->config->item('admin_location'); ?>/slides/edit/<?php echo $gv->id; ?>">Edit</a> </td>
                    </tr>

                    <?php endforeach; ?>

                    

                </tbody>
            </table>
        </div>

        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&copy; Copyright 2011. All rights reserved. It's Brain admin theme by <a href="#" title="">Eugene Kopyov</a></span>
    </div>
</div>

</body>
</html>
