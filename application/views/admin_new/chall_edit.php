<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){

            $( "#datepicker" ).datepicker();

            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                    $('#post_'+id).fadeOut();
                }

                return false;
            });
        });
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>




<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content" id="container">
    	<div class="title"><h5>Challenge</h5></div>

    	<a style="
		    margin-top: 15px;
		    float: left;
		    width:100%;
		    font-weight: bold;
		    font-size: 18px;
		" href="<?php echo $this->config->item('admin_location') . '/'.$this->uri->segment(2).'/list'; ?>">&laquo; Go Back</a>
        

        <?php foreach($get_challenge as $g): ?>
        <!-- Form begins -->
        <form action="<?php echo base_url() . $this->uri->uri_string(); ?>" class="mainForm" method="post">

            
        	<!-- Input text fields -->
            <fieldset>
                <div class="widget first">
                    <div class="head"><h5 class="iList">Edit Challenge: <b><?php echo $g->display_name; ?></b></h5></div>
                        <div class="rowElem kenny">

                            <div class="rowElem">
                                <label>Radio :</label> 
                                <div class="formRight">
                                    <input type="radio" name="question1" id="radioOne" value="1"
                                    <?php if($g->active == 1){ echo 'checked="checked"'; } ?> /><label for="radioOne">Active</label>
                                    <input type="radio" name="question1" id="radioTwo" value="0" <?php if($g->active == 0){ echo 'checked="checked"'; } ?> /><label for="radioTwo" >Hidden</label>
                                </div>
                                <div class="fix"></div>
                            </div>
           

                            <div class="rowElem">
                                <label class="formLeft">Challenege Title:</label>
                                <div class="formRight">
                                    <input type="text" name="display_name" class="js_challengetitle" value="<?php echo $g->display_name; ?>"/>
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Url Name:</label>
                                <div class="formRight">
                                    <input type="text" name="name" class="js_permalink" value="<?php echo $g->name; ?>"/>
                                </div>
                            </div>

                    <div class="floatright twoOne">
                   
                    <input type="submit" name="submit" value="Submit form" class="greyishBtn submitForm" />
                    
                    </div>
                    <div class="fix"></div>
                </div>
            </fieldset> 

        </form>

        <?php endforeach; ?>
        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<?php include('inc/footer.php'); ?>

</body>
</html>
