<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>




    <script type="text/javascript">
        $(function(){

            $( "#datepicker" ).datepicker();

            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                    $('#post_'+id).fadeOut();
                }

                return false;
            });

             $('.wysiwyg').wysiwyg('removeFormat');
        });
    </script>

    <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>

    <script type="text/javascript">
tinymce.init({
    selector: "textarea",
    theme: "modern",
    plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor"
    ],
    external_plugins: {
        "jbimages": "/Projects/gbaam/assets/packs/tinymce/plugins/jbimages/plugin.js"
    },
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar2: "jbimages print preview media | forecolor backcolor emoticons",
    image_advtab: true,
    paste_as_text: true,
    relative_urls : false,
remove_script_host : false,
convert_urls : true
    
});
</script>


</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>




<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content" id="container">
    	<div class="title"><h5>Blog </h5></div>

    	<?php
            if(isset($validation))
            {
            	if($validation == FALSE)
            	{
            		echo '<span class="wrong">'.$message.'</span>';
            	}
            }
        ?>
        
    	<a style="
		    margin-top: 15px;
		    float: left;
		    width:100%;
		    font-weight: bold;
		    font-size: 18px;
		" href="<?php echo $this->config->item('admin_location') . '/'.$this->uri->segment(2).'/list'; ?>">&laquo; Go Back</a>

        <?php foreach($get_article as $g): ?>
        <!-- Form begins -->
        <form action="<?php echo base_url() . $this->uri->uri_string(); ?>" class="mainForm" method="post" enctype="multipart/form-data">

        	<!-- Input text fields -->
            <fieldset>
                <div class="widget first">
                    <div class="head"><h5 class="iList">Edit Blog Post</h5></div>
                        <div class="rowElem kenny">

                        	<div class="rowElem">
                                <label class="formLeft">Visibility: </label>
                                <div class="formRight noSearch">
                                    <select name="status" class="chzn-select" style="width:250px;">  
                                    	<option value="1" <?php if($g->status == 1) echo "selected"; ?>>
                                        	Public
                                        </option>

                                        <option value="2" <?php if($g->status == 2) echo "selected"; ?>>
                                        	Unlisted
                                        </option>

                                        <option value="3" <?php if($g->status == 3) echo "selected"; ?>>
                                        	Private
                                        </option>
                                        
                                    </select>
                                </div>
                                <div class="fix"></div>
                            </div>
           
                            <div class="rowElem">
                                <label class="formLeft">Current Image:</label>
                                <div class="formRight">
                                    <img style="max-width:80%;" src="<?php echo get_image('article',$g->image,'',''); ?>" />
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Change Image? <br>(640 x 360):</label>
                                <div class="formRight">
                                    <input type="file" name="lg_img">
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Title:</label>
                                <div class="formRight">
                                    <input type="text" name="title" placeholder="Title" value="<?php echo set_value('title', $g->title); ?>" />
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Author:</label>
                                <div class="formRight">
                                    <input type="text" name="author" placeholder="Author" value="<?php echo set_value('author', $g->author); ?>" />
                                </div>
                            </div>

                            <div class="rowElem">
                            	<label class="formLeft">
                            		<label for="tags">Tags:</label>
                            	</label>
                            	<div class="formRight">
                            		<input type="text" id="tags" name="tags" class="tags" value="<?php echo set_value('tags', $g->tags); ?>" />
                            	</div>
                            	<div class="fix"></div>
                    		</div>

                            <div class="rowElem">
                                <label class="formLeft">Permalink (Pretty URL for SEO):</label>
                                <div class="formRight">
                                    <input type="text" name="permalink" placeholder="Permalink" value="<?php echo set_value('permalink', $g->permalink); ?>" />
                                </div>
                            </div>

                            

                    <div class="fix"></div>
                </div>
            </fieldset> 
             <fieldset>
                <div class="widget">    
                    <div class="head"><h5 class="iPencil">Content</h5></div>
                    <textarea rows="20" name="body" cols=""><?php echo set_value('body', $g->body); ?></textarea>                
                </div>

                <div class="widget">
                    <div class="floatright twoOne">
                   
                    <input type="submit" name="submit" value="Submit form" class="greyishBtn submitForm" />
                    
                    </div>
                    <div class="fix"></div>
                </div>
            </fieldset>
        </form>
        <?php endforeach; ?>
        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<?php include('inc/footer.php'); ?>

</body>
</html>
