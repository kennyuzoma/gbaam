<?php 
	$new = '';

	if($this->session->flashdata("new_mixtape") == '1')
    	$new = 'new';

?>

<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/packs/uploadify/uploadify.css" />
  <script type="text/javascript" src="<?php echo base_url() ?>assets/packs/uploadify/jquery.uploadify.min.js"></script>

    <script type="text/javascript">
        $(function(){

          

            $('#upload_btn').uploadify({ 
      'debug'   : true,

      'swf'   : '<?php echo base_url() ?>assets/packs/uploadify/uploadify.swf',
      'uploader'  : "<?php echo base_url() . $this->config->item('item_location') .'uploadify/'. $mixtape_id."/".$new; ?>",
      'cancelImage' : '<?php echo site_url() ?>res/uploadify-cancel.png',
      'queueID'  : 'file-queue',
      'buttonClass'  : 'button',
      'buttonText' : "Upload Files",
      'multi'   : true,
      'auto'   : true,
      //'removeCompleted': false,

      'fileTypeExts' : '*.mp3;',
      'fileTypeDesc' : 'Image Files',

      'formData'  : {'sessdata' : '<?php echo $this->session->get_encrypted_sessdata() ?>'},
      'method'  : 'post',
      'fileObjName' : 'userfile',

      'queueSizeLimit': 40,
      'simUploadLimit': 3,
      'sizeLimit'  : 10240000
        });
        });
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>




<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

    <!-- Content -->
    <div class="content" id="container">
        <div class="title"><h5><?php echo $title; ?></h5></div>
        
        <div class="uploadify-queue" id="file-queue"></div> 
    	<input type="file" name="userfile" id="upload_btn" />

    	

         <a href="<?php echo base_url().$this->config->item('admin_location'); ?>/mixtape/edit/<?php echo $mixtape_id; ?>/<?php echo $new; ?>" class="oblink">Done? Edit Track Names >></a>
    </div>
   
    
<div class="fix"></div>
</div>

<!-- Footer -->
<?php include('inc/footer.php'); ?>

</body>
</html>
