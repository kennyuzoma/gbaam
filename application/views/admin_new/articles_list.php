<!DOCTYPE html>
<html>
<head> 
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){
            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                     $.post('<?php echo base_url().$this->config->item('admin_location'); ?>/articles/delete/'+id, function() {
                        $('#post_'+id).fadeOut();
                    });
                }

                return false;
            });
        });
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>


<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content">
        <div class="title"><h5><?php if($this->uri->segment(3) == 'review')
                        		echo 'Review';
                        		else
                        			echo 'Edit';
                        	?> Articles</h5></div>
        
        
        <!-- Static table with resizable columns -->
        <div class="widget">
            <div class="head"><h5 class="iFrames">Article List</h5></div>
            <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic resize">
                <thead>
                    <tr>
                      <td width="100">Added date</td>
                      <td width="100">Image</td>
                      <td width="400">Title</td>
                      <td width="100">Action</td>
                    </tr>
                </thead>
                <tbody>
					
					<?php if($get_articles == NULL): ?>
                	<tr class="videopost">
						<td></td>
                            <td></td>
                        <td align="center" width="100%" style="text-align:center;">
                        	<?php if($this->uri->segment(3) == 'review'): ?>
                        	<span style="font-size:18px;font-weight:bold;">There are no articles to review</span>
							<?php else: ?>
                        	<a href="<?php echo $this->config->item('admin_location'); ?>/articles/new" style="font-size:18px;font-weight:bold;">Create your first article!</a>
                        	<?php endif; ?>
                        </td>
                        <td></td>
                        
                    </tr>
                	<?php endif; ?>

                    <?php foreach($get_articles as $gv): ?>

                    <tr class="videopost" id="post_<?php echo $gv->id; ?>">
                        <td align="center"><?php echo conv_date($gv->date_created); ?></td>
                        <td align="center">
                            <a href="<?php echo $this->config->item('admin_location'); ?>/articles/edit/<?php echo $gv->id; ?>" class="info">
                                <img width="60" height="60" src="<?php echo get_image('article',$gv->image,'','60'); ?>" />
                                
                            </a>
                        </td>
                        <td>
                             <a href="<?php echo $this->config->item('admin_location'); ?>/articles/edit/<?php echo $gv->id; ?>" class="info">
                                <b><?php echo $gv->title; ?></b>
                            </a>

                        </td>
                        <td>
							<?php 
								$my_type = $this->session->userdata('admin_type');
								if(($my_type > '4') && ($gv->status == '1') && ($this->session->userdata('admin_user_id') != $gv->poster_id)): 

							?>
								<a href="<?php echo base_url().'editorial/'.$gv->permalink; ?>" target="_blank" >View Article</a>

							<?php else: ?>
								<a href="<?php echo $this->config->item('admin_location'); ?>/articles/edit/<?php echo $gv->id; ?>">
									<b>
		                        	<?php if($this->uri->segment(3) == 'review')
		                        		echo 'Review';
		                        		else
		                        			echo 'Edit';
		                        	?>
		                        	</b>
	                        	</a>&middot;
	                        	<a href="#" class="deletepost" id="<?php echo $gv->id; ?>">Delete</a>

	                        	
	                        <?php endif; ?>

                        </td>
                    </tr>

                    <?php endforeach; ?>

                    

                </tbody>
            </table>
        </div>

        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&copy; Copyright 2011. All rights reserved. It's Brain admin theme by <a href="#" title="">Eugene Kopyov</a></span>
    </div>
</div>

</body>
</html>
