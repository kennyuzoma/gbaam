<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title> 

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){
            $('.deletepost').on('click',function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                     $.post('<?php echo base_url().$this->config->item('admin_location'); ?>/videos/mv/delete/'+id, function() {
                        $('#post_'+id).fadeOut();
                    });
                }

                return false;
            });
        });
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>


<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content">
        <div class="title"><h5>View Artlces</h5></div>
        
        
        <!-- Static table with resizable columns -->
        <div class="widget">
            <div class="head"><h5 class="iFrames">Article list</h5></div>
            <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic resize">
                <thead>
                    <tr>
                      <td width="100">Added date</td>
                      <td width="100">Image</td>
                      <td width="400">Title</td>
                      <td width="100">Action</td>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach($get_articles as $gv): ?>

                    <tr class="videopost" id="post_<?php echo $gv->id; ?>">
                        <td><?php echo conv_date($gv->date_created); ?></td>
                        <td>
                            <a href="<?php echo $this->config->item('admin_location'); ?>/articles/edit/<?php echo $gv->id; ?>" class="info">
                                <img width="50" height="50" src="<?php echo get_image('article',$gv->image,'','60'); ?>" />
                                
                            </a>
                        </td>
                        <td>
                             <a href="<?php echo $this->config->item('admin_location'); ?>/articles/edit/<?php echo $gv->id; ?>" class="info">
                                <b><?php echo $gv->title; ?></b>
                            </a>

                        </td>
                        <td>
                            <a href="<?php echo base_url().$this->config->item('admin_location'); ?>/<?php echo $this->uri->segment(2); ?>/addconf/<?php echo $type; ?>/<?php echo $slot; ?>/<?php echo $gv->id; ?>" class="" id="<?php echo $gv->id; ?>"><b>Add to Slot <?php echo $slot; ?></b></a> 
                        </td>
                    </tr>

                    <?php endforeach; ?>

                    

                </tbody>
            </table>
        </div>

        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&copy; Copyright 2011. All rights reserved. It's Brain admin theme by <a href="#" title="">Eugene Kopyov</a></span>
    </div>
</div>

</body>
</html>
