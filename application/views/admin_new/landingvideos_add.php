<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){
            $('.deletepost').on('click',function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                     $.post('<?php echo base_url().$this->config->item('admin_location'); ?>/videos/mv/delete/'+id, function() {
                        $('#post_'+id).fadeOut();
                    });
                }

                return false;
            });
        });
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>


<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content">
        <div class="title"><h5>Edit Videos</h5></div>
        
        
        <!-- Static table with resizable columns -->
        <div class="widget">
            <div class="head"><h5 class="iFrames">Video list</h5></div>
            <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic resize">
                <thead>
                    <tr>
                        
                         <td width="100">Upload Date</td>

                          

                        <td width="500">Title</td>
                        <td width="100">Action</td>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach($get_videos as $gv): ?>

                    <tr class="videopost" id="post_<?php echo $gv->vid; ?>">
                        <td><?php echo conv_date($gv->date); ?></td>

                        <td>
                            <a href="" class="info">
                                <img class="thumb" src="<?php 

                                        if($gv->thumb == '')
                                        {
                                            if($gv->yt == 1){

                                                echo 'http://img.youtube.com/vi/'.youtube_id_from_url($gv->video_src).'/3.jpg';
                                            }
                                        }
                                            else
                                                {

                                                    echo base_url() . 'thumbs/' . $gv->thumb; 
                                                }
                                        

                                        ?>" />
                                <span class="name">
                                    <?php 
                                        if($gv->artist_name == ''){ 
                                            echo $gv->title;
                                        } else {
                                           echo $gv->artist_name.' - '. $gv->title;
                                        }
                                    ?>
                                </span>
                            </a>
                        </td>
                        <td>
                            <a href="<?php echo base_url().$this->config->item('admin_location'); ?>/<?php echo $this->uri->segment(2); ?>/addconf/<?php echo $type; ?>/<?php echo $gv->vid; ?>" class="" id="<?php echo $gv->vid; ?>"><b>Make <?php echo $type; ?> Landing Video</b></a> 
                        </td>
                    </tr>

                    <?php endforeach; ?>

                    

                </tbody>
            </table>
        </div>

        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&copy; Copyright 2011. All rights reserved. It's Brain admin theme by <a href="#" title="">Eugene Kopyov</a></span>
    </div>
</div>

</body>
</html>
