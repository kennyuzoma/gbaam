<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title> 

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){

            

            $('.makemotw').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure you want to make this mixtape of the week?")) {
                    $.post('<?php echo base_url().$this->config->item('admin_location'); ?>/mixtape/make_motw/'+id, function() {
                        $('#post_'+id).css('background','gold');
                    });
                    
                }

                return false;
            });

        });
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>


<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content">
        <div class="title"><h5>Add to slot <?php echo $slot; ?></h5></div>
        
        
        <!-- Static table with resizable columns -->
        <div class="widget">
            <div class="head"><h5 class="iFrames">Add to slot <?php echo $slot; ?></h5></div>
            <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic resize">
                <thead>
                    <tr>
                      <td width="100">Upload Date</td>
                      <td width="500">Title</td>
                      <td width="100">Action</td>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach($get_mixtapes as $g): ?>

                    <tr class="videopost" id="post_<?php echo $g->id; ?>">
                        <td><?php echo conv_date($g->added); ?></td>
                        <td>
                            <a href="<?php echo $this->config->item('admin_location'); ?>/mixtape/edit/<?php echo $g->id; ?>" class="info">
                                <img class="thumb" src="<?php echo get_image('mixtape',$g->front_cover,$g->img_hash,'125'); ?>" />
                                <span class="name"><?php echo $g->title.' - '.$g->artist; ?></span>
                            </a>
                        </td>
                        <td>
                            <a href="<?php echo base_url().$this->config->item('admin_location'); ?>/hotmixtapes/add_conf/<?php echo $slot; ?>/<?php echo $g->id; ?>" class="" id="<?php echo $g->id; ?>"><b>Add to Slot <?php echo $slot; ?></b></a> 
                        </td>
                    </tr>

                    <?php endforeach; ?>

                </tbody>
            </table>
        </div>

        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&copy; Copyright 2011. All rights reserved. It's Brain admin theme by <a href="#" title="">Eugene Kopyov</a></span>
    </div>
</div>

</body>
</html>
