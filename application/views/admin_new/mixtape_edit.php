<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){

        	$('.rebuildzipfile').on('click', function(){

        		id = $(this).attr('id');

        		$.post('<?php echo base_url().$this->config->item('admin_location'); ?>/create_zip_shell/'+id+'/rebuild', function() {
        			$('.rebuildzipfile').parent('b').html('<span>Rebuilding....</span>');
        		});

        		return false;

        	});

            $( "#datepicker" ).datepicker();

            var fixHelper = function(e, ui) {
				ui.children().each(function() {
					$(this).width($(this).width());
				});
				return ui;
			};

          	$( "#sortable" ).sortable({ 
			    update: function(event, ui) {  
			        $('#sortable div').each(function() {
			        	val = $(this).index()+1;
			            $(this).children('div:first-child').html('<b style="font-size:16px;">'+val+'</b>')
			        });

			        var data2 = $(this).sortable('serialize');

			        $.ajax({
			            data: data2,
			            type: 'POST',
			            url: '<?php echo base_url().$this->config->item('admin_location'); ?>/mixtape/updatetracksorder/'
			      
			        });

			    },
			    helper: fixHelper
			}).disableSelection();

           $('#sortable div').hover(function(){
           		$(this).find('.deletex').toggle();
           });

           $('.deletex').on('click',function(){

                id = $(this).attr('id');

                if (confirm("Are you sure you want to delete this track?")) {

                    $.post('<?php echo base_url().$this->config->item('admin_location'); ?>/deleteMixtapeTrack/'+id, function() {
                    
                        $('#post_'+id).remove();

                        $('#sortable > div').each(function(i,v) {
				           // console.log($(v).find('.tnum').text(i));
				            $(v).find('.tnum').text(i+1);
				        });

                  		var data2 = $("#sortable").sortable('serialize');

				        $.ajax({
				            data: data2,
				            type: 'POST',
				            url: '<?php echo base_url().$this->config->item('admin_location'); ?>/mixtape/updatetracksorder/'
				      
				        });

                    });
                    
                }

                return false;
            });
        });
    </script>

    <style>
	.rowElem {
clear: both;
border-top: 1px solid #e7e7e7;
padding: 2px 16px;
position: relative;
background:#FAFAFA;
}

.sortable div:hover{
	cursor:pointer !important;
}

#sortable .formRight {
    float: left;
    width: 562px;
    margin: 12px 0px;
}

.deletex{
	font-size: 20px;
	font-weight: bold;
	float: left;
	margin: 20px 0 20px 10px;
	cursor: pointer;
	/* width: 50px; */
	color: red;
	display:none;
}
    </style>

	
</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>




<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content" id="container">

    	<div class="title"><h5><?php echo $title; ?></h5></div>

    	<?php
            if(isset($validation))
            {
            	if($validation == FALSE)
            	{
            		echo '<span class="wrong">'.$message.'</span>';
            	}
            }
        ?>
			
		<a style="
		    margin-top: 15px;
		    margin-bottom: 15px;
		    float: left;
		    font-weight: bold;
		    font-size: 18px;
		" href="<?php echo $this->config->item('admin_location') . '/'.$this->uri->segment(2).'/list'; ?>">&laquo; Go Back</a>
		<br>
		<div style="float:left;clear:both;"></div>

    	 <a href="<?php echo base_url().'mixtapes/'.$mixtape->id.'/'.$mixtape->permalink; ?>" target="_blank" style="float:left;width:100%;text-align:Center;font-weight:bold;font-size:20px;">View Mixtape</a>
        
        <!-- Form begins -->
        <form action="<?php echo base_url() . $this->uri->uri_string(); ?>" class="mainForm" method="post" enctype='multipart/form-data'>

            <input type="hidden" name="mixtape_id" value="<?php echo $mixtape->id; ?>" />
        	<!-- Input text fields -->
            <fieldset>
                <div class="widget first">
                    <div class="head"><h5 class="iList">Edit Mixtape</h5></div>
                        <div class="rowElem kenny">

                        	<div class="rowElem">
                                <label class="formLeft">Visibility: </label>
                                <div class="formRight noSearch">
                                    <select name="status" class="chzn-select" style="width:250px;">
                                    	<?php if($this->uri->segment(5) == 'new'): ?>
                                    	<option value="1" selected >Public</option>
                                        <option value="2" >Unlisted</option>
                                        <option value="3" >Private</option>
                                    	<?php else: ?>
                                    	<option value="1" <?php if($mixtape->mixstatus == 1) echo "selected"; ?>>Public</option>
                                        <option value="2" <?php if($mixtape->mixstatus == 2) echo "selected"; ?>>Unlisted</option>
                                        <option value="3" <?php if($mixtape->mixstatus == 3) echo "selected"; ?>>Private</option>
                                    	<?php endif; ?>
                                    </select>
                                </div>
                                <div class="fix"></div>
                            </div>

                            <div class="rowElem">
                            	<?php 
                                    $originalDate = $mixtape->added;
                                    $newDate = date("m/d/Y", strtotime($originalDate));
                                ?>
                                <label class="formLeft">Date:</label>
                                <div class="formRight"><input name="date" type="text" class="maskDate" id="datepicker" value="<?php echo set_value('date', $newDate); ?>" /></div>
                                <div class="fix"></div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Mixtape Title:</label>
                                <div class="formRight">
                                    <input type="text" name="mixtape_title" placeholder="Mixtape Title" value="<?php echo set_value('mixtape_title', $mixtape->title); ?>"/>
                                </div>
                            </div>

                            
                            
                            <div class="rowElem">
                                <label class="formLeft">Artist:</label>
                                <div class="formRight">
                                    <input type="text" name="artist_name" value="<?php echo set_value('artist_name', $mixtape->artist); ?>" placeholder="Artist Name"/>
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Dj:</label>
                                <div class="formRight">
                                    <input type="text" name="dj_name" value="<?php echo set_value('dj_name', $mixtape->dj); ?>" placeholder="djname"/>
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Description:</label>
                                <div class="formRight">
                                    <textarea rows="8" cols="" name="description" class="auto" style="overflow: hidden;" placeholder="Mixtape Description"/><?php echo set_value('description', $mixtape->desc); ?></textarea>
                                </div>
                            </div>

                            <div class="rowElem">
                            	<label class="formLeft">
                            		<label for="tags">Tags:</label>
                            	</label>
                            	<div class="formRight">
                            		<input type="text" id="tags" name="tags" class="tags" value="<?php echo set_value('tags', $mixtape->tags); ?>" />
                            	</div>
                            	<div class="fix"></div>
                    		</div>

                            <div class="fix"></div>

                            <div class="rowElem">
                                <label class="formLeft">Front Cover:</label>
                                <img src="<?php echo get_image('mixtape',$mixtape->front_cover,$mixtape->img_hash,'500'); ?>" />
                                <div class="formRight">
                                    <input type="file" name="ft_cover">
                                </div>
                            </div>

                         	<div class="rowElem">
                                <label class="formLeft">Back Cover (optional):</label>
                                <img src="<?php echo get_image('mixtape',$mixtape->back_cover,'','500'); ?>" />
                                <div class="formRight">
                                    <input type="file" name="bk_cover">
                                </div>
                            </div>
							
							<?php if($this->uri->segment(5) != 'new'): ?>
                            <div class="rowElem">
                                <label class="formLeft">Zip File:</label>
                                <div class="formRight">
                                    <span style="font-size:18px;">
                                    	<?php if($mixtape->zip_file == '...'): ?>
                                    	<b>Rebuilding...</b>
                                    	<?php else: ?>
										<b><a href="#" class="rebuildzipfile" id="<?php echo $mixtape->id; ?>" >Click Here to Rebuild Zip File</a></b>
                                    	<?php endif; ?>
                                   	</span>
                                </div>
                            </div>
                        	<?php endif; ?>

						</div>
                    <div class="fix"></div>
                </div>
            </fieldset> 
            <fieldset>
                <div class="widget first">
                    <div class="head"><h5 class="iList">Edit Tracks (Drag tracks to change order)</h5></div>
                       
                        <div class="rowElem kenny" id="sortable">
                            
                            <?php foreach($mixtape_tracks as $mt): ?>
                            <div class="rowElem" id="post_<?php echo $mt->id; ?>" >
                            	<img src="<?php echo base_url(); ?>assets/packs/gbaamadmin/images/icons/dark/list.png" style="margin:23px 30px 20px -20px;font-weight:bold;font-size:20px;float: left;cursor:move !important;">
                                <span style="margin:20px 0;width:50px;font-weight:bold;font-size:20px;float: left;" class="tnum" name="track_num[<?php echo $mt->id; ?>]">
                                	<?php echo $mt->track_num; ?>
                                </span>

                                <div class="formRight">
                                	<input name="track_name[<?php echo $mt->id; ?>]" type="text" value="<?php echo $mt->title; ?>" />
                                </div>

                                <span class="deletex" id="<?php echo $mt->id; ?>">X</span>

                                <div class="fix"></div>
                            </div>
                            <?php endforeach; ?>
                        </div>

                        <a href="<?php echo base_url().$this->config->item('admin_location'); ?>/mixtape/addtracks/<?php echo $mixtape->id; ?>" style="float:left;width:100%;text-align:Center;margin:10px 0 30px 0;font-weight:bold;font-size:20px;">Add more tracks?</a>
              
                  
                    <div class="floatright twoOne">
                   	<?php
                   		if($this->uri->segment(5) == 'new'):
                   	?>
                    <input type="submit" name="submit" value="Launch mixtape!" class="greenBtn submitForm" />
                    <?php
                   		else:
                   	?>
					<input type="submit" name="submit" value="Update Mixtape" class="greyishBtn submitForm" />
                    <?php
                   		endif;
                   	?>
                    
                    </div>
                    <div class="fix"></div>
                </div>
            </fieldset> 

            <a href="<?php echo base_url().'mixtapes/'.$mixtape->id.'/'.$mixtape->permalink; ?>" target="_blank" style="float:left;width:100%;text-align:Center;margin:10px 0 30px 0;font-weight:bold;font-size:20px;">View Mixtape</a>

        </form>
        
    </div>

    
    
<div class="fix"></div>
</div>

<!-- Footer -->
<?php include('inc/footer.php'); ?>

</body>
</html>
