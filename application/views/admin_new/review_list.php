<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title> 

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){
            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                     $.post('<?php echo base_url().$this->config->item('admin_location'); ?>/challenge/delete/'+id, function() {
                        $('#post_'+id).fadeOut();
                    });
                }

                return false;
            });
        });
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>


<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content">
        <div class="title"><h5>
        	Pending Videos</h5></div>
        
        
        <!-- Static table with resizable columns -->
        <div class="widget">
            <div class="head"><h5 class="iFrames">Video list</h5></div>
            <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic resize">
                <thead>
                    <tr>
                      <td width="100">Submit Date</td>
                      <td width="80">Name</td>
                      <td width="390">Video Title</td>
                      <td width="70">Type</td>
                      <td width="60">Action</td>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach($get_pending as $g): ?>

                    <tr class="videopost" id="post_<?php echo $g->id; ?>">
                        <td align="center">
                        	<?php echo conv_date($g->date_submitted); ?>
                        </td>
                        <td align="center">
                            <?php echo $g->firstname.' '.$g->lastname; ?>
                        </td>
                        <td >
							<a href="<?php echo $this->config->item('admin_location'); ?>/review/edit/<?php echo $g->id; ?>" class="info">
                                <img class="thumb" src="<?php 

                                        if($g->thumb == '')
                                        {
                                            if($g->yt == 1){

                                                echo 'http://img.youtube.com/vi/'.youtube_id_from_url($g->video_src).'/3.jpg';
                                            }
                                            else
                                              {
                                                echo base_url()."thumbs/Gbaam3.jpg";
                                              }

                                        }
                                            else
                                                {

                                                    echo base_url() . 'thumbs/' . $g->thumb; 
                                                }
                                        

                                        ?>" />
                                <span class="name" style="width:200px;">
                                    <?php 
                                        if($g->artist_name == ''){ 
                                            echo $g->title;
                                        } else {
                                           echo $g->artist_name.' - '. $g->title;
                                        }
                                    ?>
                                </span>
                            </a>
                        </td>
                        <td align="center">
                            <b><?php 
                            if($g->gtv == 1) 
                            	echo 'GbaamTV';
                            elseif($g->chall == 1)
                            	echo 'Challenge';
                            else
                            	echo 'Music Video';
                             ?></b>
                            
                        </td>
                       
                        <td><a href="<?php echo $this->config->item('admin_location'); ?>/review/edit/<?php echo $g->id; ?>">Review</a> </td>
                    </tr>

                    <?php endforeach; ?>

                </tbody>
            </table>
        </div>

        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&copy; Copyright 2011. All rights reserved. It's Brain admin theme by <a href="#" title="">Eugene Kopyov</a></span>
    </div>
</div>

</body>
</html>
