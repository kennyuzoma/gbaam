<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){

            $( "#datepicker" ).datepicker();

            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                    $('#post_'+id).fadeOut();
                }

                return false;
            });
        });
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>




<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content" id="container">
    	<div class="title"><h5>Users</h5></div>

		<?php
            if(isset($validation))
            {
              if($validation == FALSE)
              {
                echo '<span class="wrong">'.$message.'</span>';
              }
            }
          ?>

          <a style="
		    margin-top: 15px;
		    float: left;
		    width:100%;
		    font-weight: bold;
		    font-size: 18px;
		" href="<?php echo $this->config->item('admin_location') . '/'.$this->uri->segment(2).'/list'; ?>">&laquo; Go Back</a>

        <!-- Form begins -->
        <form action="<?php echo base_url() . $this->uri->uri_string(); ?>" class="mainForm" method="post" enctype="multipart/form-data">


        	<!-- Input text fields -->
            <fieldset>
                <div class="widget first">

                    <div class="head"><h5 class="iList">View User</h5></div>
                    <?php if($this->session->userdata('admin_type') == '1'): ?>
                    <a href="<?php echo base_url() . $this->config->item('admin_location') . '/users/edit/'.$user_id; ?>" style="float:left;width:100%;text-align:Center;font-weight:bold;font-size:20px;margin-top:10px;background:black;padding:10px 0;color:orange;border-radius:5px;">Edit User</a>
					<?php endif; ?>
                        <div class="rowElem kenny">
							<?php 
                                $admin_type = $this->session->userdata('admin_type');
                            	if(($g->created_by != '0') && ($admin_type == '1')): 
                            ?>
                        	<div class="rowElem">
                                <label class="formLeft" style="margin-top:10px;">Created By:</label>
                                <div class="formRight">
                                    <?php $me = $this->Admin_model->getAdmin($g->created_by); ?>
                                	<span style="font-size:18px;">
                                    	 <b><?php echo $me->display_name; ?></b>
                                   	</span>
                                </div>
                            </div>
                        	<?php endif; ?>

                        	<div class="rowElem">
                                <label class="formLeft" style="margin-top:10px;">
                                	<?php 
                                		if($admin_type == '1')
                                			echo 'Admin';
                                		else
                                			echo 'User';
                                	?>
                                	 Type: 
                                </label>
                                <div class="formRight">
                                	<?php  if($admin_type == '1'): ?>
                                        <?php foreach($this->Admin_model->get_admin_types() as $c): ?>
                                        	<span style="font-size:18px;">
                                            <?php if($c->id == $g->admin_type) echo $c->type_name; ?>
                                            </span>
                                        <?php endforeach; ?>
                                    
                                    <?php 
                                    	else:
                                    ?>
                                	<span style="font-size:18px;">
                                    	 <b><?php echo $g->type_name; ?></b>
                                   	</span>
                                   	<?php 
                                   		endif;
                                   	?>
                                </div>
                                <div class="fix"></div>
                            </div>

                             <div class="rowElem">
                                <label class="formLeft" style="margin-top:10px;">Photo:</label>
                                <img src="<?php echo get_image('admin_user',$g->profile_img,$g->img_hash,'200'); ?>" />
                               
                            </div>
                            

							<div class="rowElem">
                                <label class="formLeft" style="margin-top:10px;">Name:</label>
                                <div class="formRight">
                                    <span style="font-size:18px;">
                                    	 <b><?php echo $g->display_name; ?></b>
                                   	</span>
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft" style="margin-top:10px;">Email:</label>
                                <div class="formRight">
                                    <span style="font-size:18px;">
                                    	 <b><a href="mailto:<?php echo $g->email; ?>" target="_blank"><?php echo $g->email; ?></a></b>
                                   	</span>
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft" style="margin-top:10px;">Bio:</label>
                                <div class="formRight">
                                    <span style="font-size:18px;">
                                    	 <?php echo $g->bio; ?>
                                   	</span>
                                </div>
                            </div>
							
							<?php if($g->website != ''): ?>
                            <div class="rowElem">
                                <label class="formLeft" style="margin-top:10px;">Website:</label>
                                <div class="formRight">
                                   <span style="font-size:18px;">
                                    	 <?php echo $g->website; ?>
                                   	</span>
                                </div>
                            </div>
							<?php endif; ?>
							
							<?php if($g->facebook != ''): ?>
                            <div class="rowElem">
                                <label class="formLeft" style="margin-top:10px;">Facebook:</label>
                                <div class="formRight">
                                    <span style="font-size:18px;">
                                    	 <?php echo $g->facebook; ?>
                                   	</span>
                                </div>
                            </div>
                        	<?php endif; ?>
							
							<?php if($g->twitter != ''): ?>
                            <div class="rowElem">
                                <label class="formLeft" style="margin-top:10px;">Twitter:</label>
                                <div class="formRight">
                                    <span style="font-size:18px;">
                                    	 <?php echo $g->twitter; ?>
                                   	</span>
                                </div>
                            </div>
                        	<?php endif; ?>
							
							<?php if($g->googleplus != ''): ?>
                            <div class="rowElem">
                                <label class="formLeft" style="margin-top:10px;">Google+:</label>
                                <div class="formRight">
                                   <span style="font-size:18px;">
                                    	<?php echo $g->googleplus; ?>
                                   	</span>
                                </div>
                            </div>
                            <?php endif; ?>
							
							<?php if($g->instagram != ''): ?>

                            <div class="rowElem">
                                <label class="formLeft" style="margin-top:10px;">Instagram:</label>
                                <div class="formRight">
                                    <span style="font-size:18px;">
                                    	 <?php echo $g->instagram; ?>
                                   	</span>
                                </div>
                            </div>
                            <?php endif; ?>
							

                         
								
                            


                    <div class="floatright twoOne">
                   
                   
                    
                    </div>
                    <div class="fix"></div>

                </div>
                <?php if($this->session->userdata('admin_type') == '1'): ?>
                    <a href="<?php echo base_url() . $this->config->item('admin_location') . '/users/edit/'.$user_id; ?>" style="float:left;width:100%;text-align:Center;font-weight:bold;font-size:20px;margin-top:10px;background:black;padding:10px 0;color:orange;border-radius:5px;">Edit User</a>
					<?php endif; ?>
            </fieldset> 
        </form>
        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<?php include('inc/footer.php'); ?>

</body>
</html>
