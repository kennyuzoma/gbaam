<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title>
 
    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){
            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                     $.post('<?php echo base_url().$this->config->item('admin_location'); ?>/users/delete/'+id, function() {
                        $('#post_'+id).fadeOut();
                    });
                }

                return false;
            });
        });
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>


<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content">
        <div class="title"><h5>Edit Users</h5></div>
        
        
        <!-- Static table with resizable columns -->
        <div class="widget">
            <div class="head"><h5 class="iFrames">Users List</h5></div>
            <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic resize">
                <thead>
                    <tr>
                      <td width="100">Added date</td>
                      <td width="100">Admin Type</td>
                      <td width="400">Name</td>
                      <td width="100">Action</td>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach($get_users as $gv): ?>

                    <tr class="videopost" id="post_<?php echo $gv->aid; ?>">
                        <td align="center"><?php echo conv_date($gv->date_created); ?></td>
                        <td align="center"><?php echo $gv->type_name; ?></td>
                        <td>
                            <a href="<?php echo $this->config->item('admin_location'); ?>/users/view/<?php echo $gv->aid; ?>" class="info">
                                <b><?php echo $gv->display_name; ?></b>
                            </a>
                        </td>
                        <td>
                        	<?php if($this->session->userdata('admin_type') == '1'): ?>
	                        <a href="#" class="deletepost" id="<?php echo $gv->aid; ?>">Delete</a> &middot;
	                        <a href="<?php echo $this->config->item('admin_location'); ?>/users/edit/<?php echo $gv->aid; ?>">Edit</a> 
	                        <?php else: ?>
								<?php
									if($this->session->userdata('admin_user_id') == $gv->aid){
										echo '<a href="' . $this->config->item('admin_location') . '/users/edit/'.$gv->aid.'"><b>Edit</b></a>';
									} else {
	                        			echo '<a href="' . $this->config->item('admin_location') . '/users/view/'.$gv->aid.'">View</a>';
	                        		}
	                        	?> 
	                    	<?php endif; ?>
                        </td>
                    </tr>

                    <?php endforeach; ?>

                    

                </tbody>
            </table>
        </div>

        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&copy; Copyright 2011. All rights reserved. It's Brain admin theme by <a href="#" title="">Eugene Kopyov</a></span>
    </div>
</div>

</body>
</html>
