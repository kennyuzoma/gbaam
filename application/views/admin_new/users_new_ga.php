<!DOCTYPE html>
<html>
<head>
    <title>Gbaam - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){

            $( "#datepicker" ).datepicker();

            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                    $('#post_'+id).fadeOut();
                }

                return false;
            });
        });
    </script>

</head>

<body>
<!-- Top navigation bar -->
<div id="topNav">
    <div class="fixed">
        <div class="wrapper">
            <?php /*
            <div class="backTo"><a href="#" title=""><img src="http://www.gbaam.com/assets/img/Gbaam_logo270.png" alt="" height="30"/></a></div>
            */ ?>
            <div class="userNav">
                <ul>
 
                    <li><a href="mailto:<?php echo $this->config->item('contact_admin_email'); ?>" title=""><img src="<?php echo base_url(); ?>assets/packs/gbaamadmin/images/icons/topnav/contactAdmin.png" alt="" /><span>Contact admin</span></a></li>
                    
                </ul>
            </div>
            <div class="fix"></div>
        </div>
    </div>
</div>

<!-- Content wrapper -->
<div class="wrapper" style="margin-top:50px;">
    

	<!-- Content -->
    <div class="content" id="container">

    	<div class="title"><h5><?php echo $title; ?></h5></div>

		<?php
            if(isset($validation))
            {
              if($validation == FALSE)
              {
                echo '<span class="wrong">'.$message.'</span>';
              }
            }
          ?>

        <!-- Form begins -->
        <form action="<?php echo base_url() . $this->uri->uri_string(); ?>" class="mainForm" method="post" enctype="multipart/form-data">
			
			<input type="hidden" name="user_id" value="0" />
        	<!-- Input text fields -->
            <fieldset>
                <div class="widget first">
                    <div class="head"><h5 class="iList"><?php echo $title; ?></h5></div>
                        <div class="rowElem kenny">

							<div class="rowElem">
                                <label class="formLeft">First Name:</label>
                                <div class="formRight">
                                    <input type="text" name="firstname" placeholder="First Name" value="<?php echo set_value('firstname', ''); ?>" />
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Last Name:</label>
                                <div class="formRight">
                                    <input type="text" name="lastname" placeholder="Last Name" value="<?php echo set_value('lastname', ''); ?>" />
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Username:</label>
                                <div class="formRight">
                                    <input class="noselect" type="text" name="username" placeholder="Username" value="<?php echo set_value('username', ''); ?>" autocomplete='off' />
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Email:</label>
                                <div class="formRight">
                                    <input type="text" name="email" placeholder="Email Address" value="<?php echo set_value('email', ''); ?>" autocomplete='off' />
                                </div>
                            </div>

                         
								
                            

                            <div class="rowElem">
                                <label class="formLeft">Password:</label>
                                <div class="formRight">
                                    <input type="password" name="password" placeholder="" value="" autocomplete='off'/>
                                </div>
                            </div>


                            <div class="rowElem">
                                <label class="formLeft">Password Again:</label>
                                <div class="formRight">
                                    <input type="password" name="password2" placeholder="" value="" autocomplete='off'/>
                                </div>
                            </div>

                    <div class="floatright twoOne">
                   
                    <input type="submit" name="submit" value="Submit form" class="greyishBtn submitForm" />
                    
                    </div>
                    <div class="fix"></div>
                </div>
            </fieldset> 
        </form>
        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<?php include('inc/footer.php'); ?>

</body>
</html>
