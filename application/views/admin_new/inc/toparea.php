<?php
    // get the admin information from the session
    $uid = $this->session->userdata('admin_user_id'); 
    $me = $this->Admin_model->getAdmin($uid);
?>
<div id="topNav">
    <div class="fixed">
        <div class="wrapper">

            <div class="welcome">
            	<?php if($me->profile_img == ''): ?>
            		<img src="<?php echo base_url(); ?>assets/packs/gbaamadmin/images/userPic.png" alt="" />
				<?php else: ?>
					<img src="<?php echo get_image('admin_user',$me->profile_img,$me->img_hash,'50'); ?>" style="width:22px;" />
				<?php endif; ?>
            	<span>Welcome, <?php echo $me->firstname; ?>!</span>

            </div>

            <div class="userNav">
                <ul>
                    <li><a href="<?php echo $this->config->item('admin_location'); ?>/users/<?php echo $uid; ?>" title=""><img src="<?php echo base_url(); ?>assets/packs/gbaamadmin/images/icons/topnav/profile.png" alt="" /><span>Profile</span></a></li>
                   
                   
                    <li class="dd"><a title=""><img src="<?php echo base_url(); ?>assets/packs/gbaamadmin/images/icons/topnav/messages.png" alt="" /><span>Messages</span><span class="numberTop">8</span></a>
                        <ul class="menu_body">
                            <li><a href="#" title="" class="sAdd">new message</a></li>
                            <li><a href="#" title="" class="sInbox">inbox</a></li>
                            <li><a href="#" title="" class="sOutbox">outbox</a></li>
                            <li><a href="#" title="" class="sTrash">trash</a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo $this->config->item('admin_location'); ?>/settings/general" title=""><img src="<?php echo base_url(); ?>assets/packs/gbaamadmin/images/icons/topnav/settings.png" alt="" /><span>Settings</span></a></li>
                    <li><a href="<?php echo base_url().$this->config->item('admin_location'); ?>/logout" title=""><img src="<?php echo base_url(); ?>assets/packs/gbaamadmin/images/icons/topnav/logout.png" alt="" /><span>Logout</span></a></li>
                </ul>
            </div>
            <div class="fix"></div>
        </div>
    </div>
</div>

<!-- Header -->
<div id="header" class="wrapper">
    <div class="logo">
    	<a href="<?php echo $this->config->item('admin_location'); ?>/home" title="">
    		<img src="<?php echo base_url(); ?>assets/img/Gbaam_logo270.png" alt="" width="189" />
    	</a>

    </div>
    <a href="<?php echo base_url(); ?>" style="float:left;font-size:20px;font-weight:bold;margin-left:200px;margin-top:40px;display:block;padding:5px;background:black;border-radius:5px;color:orange;">&#171;Back to Main Site</a>
    <div class="middleNav">
    	<ul>
            <?php /*
        	<li class="iMes"><a href="#" title=""><span>Support tickets</span></a><span class="numberMiddle">9</span></li>
            */ ?>
            <li class="iStat"><a href="#" title=""><span>Statistics</span></a></li>
            <li class="iUser"><a href="#" title=""><span>User list</span></a></li>
            <?php /*
            <li class="iOrders"><a href="#" title=""><span>Billing panel</span></a></li>  */ ?>
        </ul>
    </div>
    <div class="fix"></div>
    <?php
    	if($this->uri->segment(3) == 'success' || $this->uri->segment(4) == 'success' || $this->uri->segment(5) == 'success' || $this->uri->segment(6) == 'success'):
    ?>
    <div class="js_success" style="float:left;width:100%;background:#e3ffd1;border:1px solid #8cbd75;border-radius:5px;text-align:center;font-size:16px;padding:6px 0;margin-bottom:10px;font-weight:bold;">
		<?php 
			if((($this->session->userdata('admin_type') == '5') || ($this->session->userdata('admin_type') == '6')) && ($this->uri->segment(2) == 'articles')) {
				if($this->uri->segment(3) == 'edit'){
					echo 'Your article was edited successfully!';
				}
				else{
					echo 'Thank you for your article submission! Your article will be reviewed shortly!';
				}
				
			} else {
				echo 'Success!';
			}
		?>

    </div>
    <?php 
    	endif;
    ?>
</div>

