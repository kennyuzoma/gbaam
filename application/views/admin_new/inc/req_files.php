<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">

<base href="<?php echo base_url(); ?>" />

<?php $seg = $this->uri->segment(2); ?>
<?php $seg3 = $this->uri->segment(3); ?>

<link href="<?php echo base_url(); ?>assets/packs/gbaamadmin/css/main.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Oxygen|Open+Sans:700,400,300' rel='stylesheet' type='text/css'>

<script type="text/javascript" src="<?php echo $this->config->item('jquery_file'); ?>"></script>
<script type="text/javascript" src="<?php echo $this->config->item('jqueryui_file'); ?>"></script> 

<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/packs/uploadify/uploadify.css" />
  <script type="text/javascript" src="<?php echo base_url() ?>assets/packs/uploadify/jquery.uploadify-3.1.min.js"></script>

<?php /*
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/spinner/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/spinner/ui.spinner.js"></script>
*/ ?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/wysiwyg/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/wysiwyg/wysiwyg.image.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/wysiwyg/wysiwyg.link.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/wysiwyg/wysiwyg.table.js"></script>



<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/tables/colResizable.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/forms/forms.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/forms/autogrowtextarea.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/forms/autotab.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/forms/jquery.validationEngine-en.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/forms/jquery.validationEngine.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/forms/jquery.dualListBox.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/forms/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/forms/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/forms/jquery.inputlimiter.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/forms/jquery.tagsinput.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/other/calendar.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/other/elfinder.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/uploader/plupload.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/uploader/plupload.html5.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/uploader/plupload.html4.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/uploader/jquery.plupload.queue.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/ui/jquery.progress.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/ui/jquery.jgrowl.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/ui/jquery.tipsy.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/ui/jquery.alerts.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/ui/jquery.colorpicker.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/wizards/jquery.form.wizard.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/wizards/jquery.validate.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/ui/jquery.breadcrumbs.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/ui/jquery.ToTop.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/ui/jquery.listnav.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/ui/jquery.sourcerer.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/ui/jquery.timeentry.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/ui/jquery.prettyPhoto.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/ui/jquery.collapsible.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/custom.js"></script>


<?php if($seg == 'home'): ?> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/flot/jquery.flot.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/flot/jquery.flot.orderBars.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/flot/jquery.flot.pie.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/flot/excanvas.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/plugins/flot/jquery.flot.resize.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/packs/gbaamadmin/js/charts/chart.js"></script>
<?php endif; ?>

<script src="<?php echo base_url(); ?>assets/js/jquery.are-you-sure.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/ays-beforeunload-shim.js"></script>

<script type="text/javascript">

	$(function(){
		<?php if($this->uri->segment(2) != ''): ?>
		$('form').areYouSure();
		<?php endif; ?>
		
		$('.js_success').delay(4000).fadeOut();

	});
</script>

<style>
body{
	font-family: 'Open Sans', sans-serif;
}

	.mainForm{
		float:left;
	}
	div.mce-fullscreen {
		position: fixed;
		top: 40px !important;
		left: 0;
	}

	input[type='checkbox']
	{
		cursor:pointer;
	}
</style>

