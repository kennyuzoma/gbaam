<div class="leftNav">

<?php

/*
New menu
 */
    $seg4 = $this->uri->segment(4);
    $seg3 = $this->uri->segment(3);
    $seg2 = $this->uri->segment(2);

    $admin_location = $this->config->item('admin_location');

?>
        <ul id="menu" style="margin-bottom:20px;">
            <li class="dash">
                <a href="<?php echo $admin_location; ?>/home" title="">
                    <span>Dashboard</span>
                </a>
            </li>

            <?php

            	$admin_type = $this->session->userdata('admin_type'); 
            	$admin_user_id = $this->session->userdata('admin_user_id');

            	if(($admin_type == '1') || ($this->Admin_model->get_user_access($admin_user_id,'videos') == TRUE)): 
															
            ?>
            <li class="gallery">
                <a href="#" title="" <?php if($seg2 == 'review') echo 'id="current"'; ?>class="exp">
                    <span>Pending Videos</span>
                    <?php if($this->Video_model->getPendingVideos_count() > 0): ?>
                    <span class="numberLeft"><?php echo $this->Video_model->getPendingVideos_count(); ?></span>
                	<?php endif; ?>
                </a>
                <ul class="sub">
                    <li><a href="<?php echo $admin_location; ?>/review/list" title="">Review Videos</a></li>
                </ul>
            </li>
            <?php endif; ?>

			<?php if(($admin_type == '1') || ($this->Admin_model->get_user_access($admin_user_id,'videos') == TRUE)):  ?>
            <li class="gallery">
                <a href="#" title="" <?php if(($seg3 == 'mv') || ($seg2 == 'featvideos')) echo 'id="current"'; ?>class="exp">
                    <span>Music Videos</span>
                    <?php /*<span class="numberLeft">2</span>*/ ?>
                </a>
                <ul class="sub">
                    <li><a href="<?php echo $admin_location; ?>/videos/mv/new" title="">New Video</a></li>
                    <li><a href="<?php echo $admin_location; ?>/videolist/mv" title="">Edit Video</a></li>
                    <li><a href="<?php echo $admin_location; ?>/featvideos/list/five" title="">View/Edit <b>FIVE</b> Featured Videos</a></li>
                    <li><a href="<?php echo $admin_location; ?>/featvideos/list/two" title="">View/Edit Top <b>TWO</b> Videos</a></li>
                </ul>
            </li>
			<?php endif; ?>

			<?php if(($admin_type == '1') || ($this->Admin_model->get_user_access($admin_user_id,'gbaamtv') == TRUE)):  ?>
            <li class="gallery">
                <a href="#" title="" <?php if(($seg3 == 'gbaamtv') || ($seg2 == 'gtvfeatvideos') || ($seg4 == 'gbaamtv')) echo 'id="current"'; ?> class="exp">
                    <span>Gbaam TV</span>
                    <?php /*<span class="numberLeft">2</span>*/ ?>
                </a>
                <ul class="sub" style="display:block;">
                    <li><a href="<?php echo $admin_location; ?>/videos/gbaamtv/new" title="">New Video</a></li>
                    <li><a href="<?php echo $admin_location; ?>/videolist/gbaamtv" title="">Edit Video</a></li>
                    <li><a href="<?php echo $admin_location; ?>/gtvfeatvideos/list" title="">View/Edit GbaamTV Featured Videos</a></li>
                    <li><a href="<?php echo $admin_location; ?>/landingvideos/list/gbaamtv" title="">Landing Video</a></li>
                </ul>
            </li>
			<?php endif; ?>

<?php /*
			<?php if(($admin_type == '1') || ($this->Admin_model->get_user_access($admin_user_id,'challenge') == TRUE)):  ?>
            <li class="gallery">
                <a href="#" title="" <?php if(($seg3 == 'challenge') || ($seg4 == 'challenge') || ($seg2 == 'challenge')) echo 'id="current"'; ?> class="exp">
                    <span>Challenge</span>
                </a>
                <ul class="sub">
                    <li><a href="<?php echo $admin_location; ?>/videos/challenge/new" title="">New Video</a></li>
                    <li><a href="<?php echo $admin_location; ?>/videolist/challenge" title="">Edit Video</a></li>
                    <li><a href="<?php echo $admin_location; ?>/challenge/new" title="">New Challenge</a></li>
                    <li><a href="<?php echo $admin_location; ?>/challenge/list" title="">View/Edit Challenges</a></li>
                    <li><a href="<?php echo $admin_location; ?>/landingvideos/list/challenge" title="">Landing Video</a></li>
                </ul>
            </li>
			<?php endif; ?>
*/ ?>
			<?php if(($admin_type == '1') || ($this->Admin_model->get_user_access($admin_user_id,'mixtapes') == TRUE)):  ?>
            <li class="gallery">
                <a href="#" title="" <?php if($seg2 == 'mixtape' || $seg2 == 'hotmixtapes') echo 'id="current"'; ?> class="exp">
                    <span>Mixtapes</span>
                    <?php /*<span class="numberLeft">2</span>*/ ?>
                </a>
                <ul class="sub">
                    <li><a href="<?php echo $admin_location; ?>/mixtape/new" title="">New Mixtape</a></li>
                    <li><a href="<?php echo $admin_location; ?>/mixtape/list" title="">View/Edit Mixtape</a></li>
                    <li><a href="<?php echo $admin_location; ?>/hotmixtapes/list" title="">View All Hot Mixtapes</a></li>
                    <li><a href="<?php echo $admin_location; ?>/mixtape/motw">Edit MOTW</a></li>
                </ul>
            </li>
			<?php endif; ?>

			<?php if(($admin_type == '1') || ($this->Admin_model->get_user_access($admin_user_id,'songs') == TRUE)):  ?>
            <li class="gallery">
                <a href="#" title="" <?php if($seg2 == 'songs') echo 'id="current"'; ?> class="exp">
                    <span>Songs</span>
                    <?php /*<span class="numberLeft">2</span>*/ ?>
                </a>
                <ul class="sub">
                    <li><a href="<?php echo $admin_location; ?>/songs/new" title="">New Song</a></li>
                    <li><a href="<?php echo $admin_location; ?>/songs/list" title="">View/Edit Songs</a></li>
                    <li><a href="<?php echo $admin_location; ?>/songs/sotw">Edit SOTW</a></li>
                </ul>
            </li>
			<?php endif; ?>

			<?php if($this->Admin_model->get_user_access($admin_user_id,'articles') == TRUE):  ?>
            <li class="tables">
                <a href="#" title="" <?php if(($seg2 == 'articles') || ($admin_type == '5') || ($admin_type == '6')) echo 'id="current"'; ?> class="exp">
                    <span>Editorial</span>
                    <?php
                    	if($admin_type <= '4'):
                    		if($this->Site_model->getPendingArticles_count() > 0): ?>
                    <span class="numberLeft"><?php echo $this->Site_model->getPendingArticles_count(); ?></span>
                	<?php 
                			endif; 
                		endif;
                	?>
                </a>
                <ul class="sub">
                    <li><a href="<?php echo $admin_location; ?>/articles/new" title="">New Story</a></li>
                    <li><a href="<?php echo $admin_location; ?>/articles/list" title="">View/Edit Story</a></li>
                    <?php if($admin_type <= '4'): ?>
                    <li><a href="<?php echo $admin_location; ?>/featarticles/list/article" title="">View/Edit Feat Articles</a></li>
                    <li><a href="<?php echo $admin_location; ?>/articles/review" title="">Review Article (<?php echo $this->Site_model->getPendingArticles_count(); ?>)</a></li>
                	<?php endif; ?>
                </ul>
            </li>
			<?php endif; ?>

			<?php if(($admin_type == '1') || ($this->Admin_model->get_user_access($admin_user_id,'blog') == TRUE)):  ?>
            <li class="tables">
                <a href="#" title="" <?php if($seg2 == 'blog') echo 'id="current"'; ?> class="exp">
                    <span>The Blog</span>
                    <?php /*<span class="numberLeft">2</span>*/ ?>
                </a>
                <ul class="sub">
                    <li><a href="<?php echo $admin_location; ?>/blog/new" title="">New Blog Post</a></li>
                    <li><a href="<?php echo $admin_location; ?>/blog/list" title="">View/Edit Blog Posts</a></li>
                </ul>
            </li>
			<?php endif; ?>

			<?php if(($admin_type == '1') || ($this->Admin_model->get_user_access($admin_user_id,'slides') == TRUE)):  ?>
            <li class="tables">
                <a href="#" title="" <?php if($seg2 == 'slides') echo 'id="current"'; ?> class="exp">
                    <span>Front Slides</span>
                    <?php /*<span class="numberLeft">2</span>*/ ?>
                </a>
                <ul class="sub">
                    <li><a href="<?php echo $admin_location; ?>/slides/new" title="">New Slide</a></li>
                    <li><a href="<?php echo $admin_location; ?>/slides/list" title="">View/Edit Slides</a></li>
                </ul>
            </li>
			<?php endif; ?>

			
            <li class="tables">
                <a href="#" title="" <?php if(($seg2 == 'site_users')) echo 'id="current"'; ?> class="exp">
                    <span>Site Users</span>
                    <?php /*<span class="numberLeft">2</span>*/ ?>
                </a>
                <ul class="sub">
                	<?php if($admin_type == '1'): ?>
                    <li><a href="<?php echo $admin_location; ?>/users/new" title="">New User</a></li>
                	<?php endif; ?>
                    <li>
                    	<a href="<?php echo $admin_location; ?>/site_users/list" title="">
                    	<?php 
                    		if($admin_type == '1')
                    			echo 'View/Edit';
                    		else
                    			echo 'View';
                    	?>
                    	 Site Users
                    	</a>
                    </li>
                </ul>
            </li>

            <li class="tables">
                <a href="#" title="" <?php if(($seg2 == 'users') || ($admin_type == '5')) echo 'id="current"'; ?> class="exp">
                    <span>Admins/Authors</span>
                    <?php /*<span class="numberLeft">2</span>*/ ?>
                </a>
                <ul class="sub">
                	<?php if($admin_type == '1'): ?>
                    <li><a href="<?php echo $admin_location; ?>/users/new" title="">New User</a></li>
                	<?php endif; ?>
                    <li>
                    	<a href="<?php echo $admin_location; ?>/users/list" title="">
                    	<?php 
                    		if($admin_type == '1')
                    			echo 'View/Edit';
                    		else
                    			echo 'View';
                    	?>
                    	 Admins/Authors
                    	</a>
                    </li>
                    <li>
                    	<a href="<?php echo $admin_location; ?>/users/listguests" title="">
                    	<?php 
                    		if($admin_type == '1')
                    			echo 'View/Edit';
                    		else
                    			echo 'View';
                    	?>
                    	 Guest Authors
                    	</a>
                    </li>
                    <li><a href="<?php echo $admin_location; ?>/users/edit/<?php echo $this->session->userdata('admin_user_id'); ?>" title="">Edit My Account</a></li>
                </ul>
            </li>

			<?php if($admin_type == '1'): ?>
            <li class="tables">
                <a href="#" title="" class="exp">
                    <span>Site Settings</span>
                    <?php /*<span class="numberLeft">2</span>*/ ?>
                </a>
                <ul class="sub">
                    <li><a href="<?php echo $admin_location; ?>/slides/general" title="">General</a></li>
                </ul>
            </li>
            <?php endif; ?>
<?php /*
			<?php if($admin_type == '1'): ?>
             <li class="contacts">
                <a href="<?php echo $admin_location; ?>/messages" title="">
                    <span>Messages</span>
                </a>
            </li>
            <?php endif; ?>
*/ ?>
        </ul>


<?php

############################
#
#      Old stuff
#
############################

/*
?>
    	<ul id="menu">
        	<li class="dash">
                <a href="index.html" title="" class="active">
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="graphs">
                <a href="charts.html" title="">
                    <span>Graphs and charts</span>
                </a>
            </li>
            <li class="forms">
                <a href="form_elements.html" title="">
                    <span>Form elements</span>
                </a>
            </li>
            <li class="login"><a href="ui_elements.html" title=""><span>Interface elements</span></a></li>
            <li class="typo"><a href="typo.html" title=""><span>Typography</span></a></li>
            <li class="tables"><a href="tables.html" title=""><span>Tables</span></a></li>
            <li class="cal"><a href="calendar.html" title=""><span>Calendar</span></a></li>
            <li class="gallery"><a href="gallery.html" title=""><span>Gallery</span></a></li>
            <li class="widgets"><a href="widgets.html" title=""><span>Widgets</span></a></li>
            <li class="files"><a href="file_manager.html" title=""><span>File manager</span></a></li>
            <li class="errors">
                <a href="#" title="" class="exp">
                    <span>Error pages</span>
                    <span class="numberLeft">6</span>
                </a>
            	<ul class="sub">
                    <li><a href="403.html" title="">403 page</a></li>
                    <li><a href="404.html" title="">404 page</a></li>
                    <li><a href="405.html" title="">405 page</a></li>
                    <li><a href="500.html" title="">500 page</a></li>
                    <li><a href="503.html" title="">503 page</a></li>
                    <li class="last"><a href="offline.html" title="">Website is offline</a></li>
                </ul>
            </li>
            <li class="pic"><a href="icons.html" title=""><span>Buttons and icons</span></a></li>
            <li class="contacts"><a href="contacts.html" title=""><span>Organized contact list</span></a></li>
        </ul>
<?php 
*/
 ?>
    </div>