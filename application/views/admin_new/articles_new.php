<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>

    <script type="text/javascript">
        $(function(){

            $( "#datepicker" ).datepicker();

            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                    $('#post_'+id).fadeOut();
                }

                return false;
            });

            $('.wysiwyg').wysiwyg('removeFormat');
            
            var timer = null;
            var permalink_typed = 0;

            $('.js_permalink').keyup(function(){
            	//console.log('permalink typed:'+permalink_typed);
                permalink_typed = 1;
            });

            

            $('.art_title').keyup(function() {

            	if(permalink_typed == 0 )
            	{
	            	//console.log('permalink typed:'+permalink_typed);
	                clearTimeout(timer);
	                timer = setTimeout(doneTyping, 1000);
                }

            });
            



            //$('input[type="checkbox"]').on('click',function() {
            $('#posted_by_gbaam').on('click',function(){
			   	$('.selectorwrap').toggle();

            	if($('.author_select').val() == '0')
            	{
            		$('.authorbox').val(saved_other_author).toggle();
            	}
			});

            var saved_other_author = $('.saved_other_author').val();
            
            $('.author_select').change(function(){

			  if($(this).val() == '0'){ // or this.value == 'volvo'
			    if($('.authorbox').css('display') === 'none')
            	{
            		$('.authorbox').val(saved_other_author).show();
            	}
			  }
			  else
			  {
			  	$('.authorbox').val('');
			  	$('.authorbox').hide();
			  }
			});


        	$('.editpermalinkbutton').on('click',function(){

        		$(this).hide();
        		$('.okayclick').show();

        		ptextval = $('.permtxt').text();

        		$('.permtxt').html('<input type="text" value="'+ptextval+'"  name="js_ptextinput"  style="font-size: 14px !important;" class="js_ptextinput" />');

        	});

        	$('.okayclick').on('click',function(){

        		ptextval = $('.js_ptextinput').val();

        		$.post( "<?php echo $this->config->item('admin_location'); ?>/pretty_url/", { url: ptextval }, function( data ) {

	        		$('.okayclick').hide();
	        		$('.editpermalinkbutton').show();
	        		$('.js_ptextinput').hide();
	        		$('.js_permalink').val(data);
	        		$('.permtxt').html(data);

	        	});

        	});
        
        });

        function doneTyping () {
	        $.post( "<?php echo $this->config->item('admin_location'); ?>/pretty_url/", { url: $('.art_title').val() }, function( data ) {
	     
	            //alert( data);
	            $('.js_permalink').val(data);
	            $('.permtxt').text(data);
	            $('.ptext').show();
	        });
	    }

    </script>

     <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>

    <script>
        tinymce.init({
            selector: "textarea",
    theme: "modern",
    plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor"
    ],
    <?php if($_SERVER['HTTP_HOST'] == 'www.gbaam.com' || $_SERVER['HTTP_HOST'] == 'gbaam.com'): ?>
    external_plugins: {
        "jbimages": "/assets/packs/tinymce/plugins/jbimages/plugin.js"
    },
    <?php else: ?>
    external_plugins: {
        "jbimages": "/Projects/gbaam/assets/packs/tinymce/plugins/jbimages/plugin.js"
    },
    <?php endif; ?>
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar2: "jbimages print preview media | forecolor backcolor emoticons",
    image_advtab: true,
    paste_as_text: true,
    relative_urls : false,
remove_script_host : false,
convert_urls : true
        });
    </script>

 
	<style>
		.permtxt{
			background:#fffbcc;
			font-weight:Bold;
		}

		.js_ptextinput{
			width: 200px !important;
			
		}

		.jsbutton
		{
padding: 0 8px 1px;
font-size: 11px;
			color: #555;
border-color: #ccc;
background: #f7f7f7;
-webkit-box-shadow: inset 0 1px 0 #fff,0 1px 0 rgba(0,0,0,.08);
box-shadow: inset 0 1px 0 #fff,0 1px 0 rgba(0,0,0,.08);
/*vertical-align: top;*/
display: inline-block;
text-decoration: none;
font-size: 13px;

/*line-height: 26px;
height: 28px;*/
margin: 0;
padding: 0 10px 1px;
cursor: pointer;
border-width: 1px;
border-style: solid;
-webkit-appearance: none;
-webkit-border-radius: 3px;
border-radius: 3px;
white-space: nowrap;
-webkit-box-sizing: border-box;
-moz-box-sizing: border-box;
box-sizing: border-box;
		}
	</style>
</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>




<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content" id="container">
    	<div class="title"><h5>Articles</h5></div>

    	<?php
            if(isset($validation))
            {
            	if($validation == FALSE)
            	{
            		echo '<span class="wrong">'.$message.'</span>';
            	}
            }
        ?>

        <!-- Form begins -->
        <form action="<?php echo base_url() . $this->uri->uri_string(); ?>" class="mainForm" method="post" enctype="multipart/form-data">

        	<!-- Input text fields -->
            <fieldset>
                <div class="widget first">
                    <div class="head"><h5 class="iList">Add Article</h5></div>
                        <div class="rowElem kenny">

                        	<div class="rowElem">
                                <label class="formLeft">Author:</label>
                                <div class="formRight searchDrop">

                                <?php
								    // get the admin information from the session
								    $uid = $this->session->userdata('admin_user_id');
								    $me = $this->Admin_model->getAdmin($uid);
								    $admin_type = $this->session->userdata('admin_type');
								?>
                                <?php if(($admin_type == '5') || ($admin_type == '6')): ?>
                                	<span style="font-size:18px;">
                                    	 <b><?php echo $me->display_name; ?></b>
                                   	</span>
                                   	<input type="hidden" name="author_id" value="<?php echo $uid; ?>" />
                                <?php elseif($this->session->userdata('admin_type') == '1'): ?>
                                	<div class="selectorwrap">
									<select name="author_id" class="author_select chzn-select" style="width:250px;">
                                    	<?php 
                                    	foreach($this->Admin_model->get_users('','byalpha') as $g){
                                    		echo '<option value="'.$g->aid.'"';
												if($g->aid == $this->session->userdata('admin_user_id'))
													echo ' selected="selected"';
                                    		echo '>'.$g->firstname.' '.$g->lastname;
                                    		if($g->admin_type == '6')
                                    			echo ' (Guest Author)';
                                    		echo '</option>';
                                    	} ?>
                                    	<option value="0">Other Author</option>
                                    </select>
                                    </div>


                                    <input type="text" name="author" placeholder="Other Author" value="" style="display:none;margin-top:10px;" class="pbgbaam authorbox" />
                                    
                                   
	                                <div style="">
	                                	<input type="checkbox" id="posted_by_gbaam" name="posted_by_gbaam"> 
	                                	<label style="margin-top:7px;margin-left:10px;font-size:18px;" for="posted_by_gbaam" class="">Post as <b>Gbaam</b></label>
	                                </div>
	                                 <?php endif; ?>
                                </div>

                            </div>
           
                            <div class="rowElem">
                                <label class="formLeft" style="margin-top:10px;">Featured Image:<br>(640x480)</label>
                                <div class="formRight">
                                    <input type="file" name="lg_img">
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Title:</label>
                                <div class="formRight">
                                    <input type="text" name="title" class="art_title" placeholder="Title" value="<?php echo set_value('art_title', ''); ?>" />
                                </div>
                                <input type="hidden" name="permalink" class="js_permalink" placeholder="Permalink" value="<?php echo set_value('permalink', ''); ?>" />
                                 <span class="ptext" style="display:none;margin-bottom:10px;font-size:14px;float:left;width:100%">
                                    <b>Permalink:</b> <?php echo base_url(); ?>editorial/<b><span class="permtxt"></span>
										<span class="editpermalinkbutton jsbutton">Edit</span>
										<span class="okayclick jsbutton" style="display:none;">OK</span>
                                    </b>
                                </span>
                            </div>

                            <div class="rowElem">
                            	<label class="formLeft">
                            		<label for="tags" >Tags:</label>
                            	</label>
                            	</label>
                            	<div class="formRight">
                            		<input type="text" id="tags" name="tags" class="tags" value="<?php echo set_value('tags', ''); ?>" />
                            	</div>
                            	<div class="fix"></div>
                    		</div>

                            



                            <div class="fix"></div>

                </div>
            </fieldset> 

            <fieldset>
                <div class="widget">    
                    <div class="head"><h5 class="iPencil">Content</h5></div>
                    <textarea rows="20" name="body" cols=""><?php echo set_value('body', ''); ?></textarea>                
                </div>

                <div class="widget">
                    <div class="floatright twoOne">
                   
                    <input type="submit" name="submit" value="Submit form" class="greyishBtn submitForm" />
                    
                    </div>
                    <div class="fix"></div>
                </div>
            </fieldset>

        </form>
        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<?php include('inc/footer.php'); ?>

</body>
</html>
