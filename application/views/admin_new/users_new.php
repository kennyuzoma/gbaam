<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>

    <script type="text/javascript">
        $(function(){

            $( "#datepicker" ).datepicker();

            $('.deletepost').click(function(){

                id = $(this).attr('id');

                if (confirm("Are you sure?")) {
                    $('#post_'+id).fadeOut();
                }

                return false;
            });

            $('.js_admin_type').on('change', function() {

            	var admin_type = $(this).val();

            	if(admin_type == 1 || admin_type == 2)
            	{
            		$('.js_access_div').hide();
            	}
            	else
            	{
            		$('.js_access_div').show();
            	}

            	if(admin_type > 4)
            	{
            		// hide the songs area
            		$('.js_chk_slides').hide();

            	}
            	else
            		$('.js_chk_slides').show();
				//alert( this.value ); // or $(this).val()
				
			});
        });
    </script>

</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>




<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content" id="container">
    	<div class="title"><h5>Users</h5></div>

		<?php
            if(isset($validation))
            {
            	if($validation == FALSE)
            	{
            		echo '<span class="wrong">'.$message.'</span>';
            	}
            }
         ?>

        <!-- Form begins -->
        <form action="<?php echo base_url() . $this->uri->uri_string(); ?>" class="mainForm" method="post" enctype="multipart/form-data">
			
			<input type="hidden" name="user_id" value="0" />

        	<!-- Input text fields -->
            <fieldset>
                <div class="widget first">
                    <div class="head"><h5 class="iList">New User</h5></div>
                        <div class="rowElem kenny">

                        	<div class="rowElem">
                                <label class="formLeft">Admin Type: </label>
                                <div class="formRight noSearch">
                                    <select name="admin_type" class="js_admin_type chzn-select" style="width:250px;">
                                        <?php 
                                        	foreach($this->Admin_model->get_admin_types() as $c)
                                        	{
                                        		echo '<option value="'.$c->id.'" '.set_select('admin_type', $c->id);
                                        		//if($this->input->post('admin_type') == $c->id)
                                        		//	echo 'selected ';
                                        		
                                        		echo ' >'.$c->type_name.'</option>';
                                        	} 

                                        ?>
                                    </select>
                                </div>
                                <div class="fix"></div>
                            </div>

                            <div class="rowElem js_access_div">
                                <label class="formLeft">
                                	Access to:
                                </label>
                                <div class="formRight searchDrop">

                                	<?php if($admin_type == '1'): ?>
                                        <?php foreach($this->Admin_model->get_admin_types('access') as $c): ?>
											<div style="float:left;width:100%;margin-bottom:5px;" class="js_chk_<?php echo underscore($c->type); ?>">

	                                        	<input type="checkbox" id="<?php echo $c->type; ?>" name="access[<?php echo $c->id; ?>]" style="float;left;" value="<?php echo $c->id; ?>"
	                                        	<?php echo set_checkbox('access['.$c->id.']', '1'); ?>
	                                        	 />
	                                        	<label for="<?php echo $c->type; ?>" style="float:left;margin-left:10px;margin-top:8px;">
	                                        		<?php echo ucfirst($c->type); ?>
	                                        	</label>
	                                        </div>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                	<span style="font-size:18px;">
                                    	 <b><?php echo $g->type_name; ?></b>
                                   	</span>
                                   	<?php  endif; 	?>
                                </div>
                                <div class="fix"></div>
                            </div>

							<div class="rowElem">
                                <label class="formLeft">First Name:</label>
                                <div class="formRight">
                                    <input type="text" name="firstname" placeholder="First Name" value="<?php echo set_value('firstname', ''); ?>" />
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Last Name:</label>
                                <div class="formRight">
                                    <input type="text" name="lastname" placeholder="Last Name" value="<?php echo set_value('lastname', ''); ?>" />
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Username:</label>
                                <div class="formRight">
                                    <input class="noselect" type="text" name="username" placeholder="Username" value="<?php echo set_value('username', ''); ?>" autocomplete='off' />
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Email:</label>
                                <div class="formRight">
                                    <input type="text" name="email" placeholder="Email Address" value="<?php echo set_value('email', ''); ?>" autocomplete='off' />
                                </div>
                            </div>

                         
								
                            

                            <div class="rowElem">
                                <label class="formLeft">Password:</label>
                                <div class="formRight">
                                    <input type="password" name="password" placeholder="" value="" autocomplete='off'/>
                                </div>
                            </div>


                            <div class="rowElem">
                                <label class="formLeft">Password Again:</label>
                                <div class="formRight">
                                    <input type="password" name="password2" placeholder="" value="" autocomplete='off'/>
                                </div>
                            </div>

                    <div class="floatright twoOne">
                   
                    <input type="submit" name="submit" value="Submit form" class="greyishBtn submitForm" />
                    
                    </div>
                    <div class="fix"></div>
                </div>
            </fieldset> 
        </form>
        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<?php include('inc/footer.php'); ?>

</body>
</html>
