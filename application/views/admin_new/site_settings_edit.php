<!DOCTYPE html>
<html>
<head>
    <title>Gbaam Admin - <?php echo $title; ?></title>

    <!-- Required files -->
    <?php include('inc/req_files.php'); ?>




    <script type="text/javascript">
        $(function(){


        });
    </script>



</head>

<body>

<!-- Top navigation bar -->
<?php include('inc/toparea.php'); ?>




<!-- Content wrapper -->
<div class="wrapper">
    
    <!-- Left navigation -->
    <?php include('inc/leftnav.php'); ?>

	<!-- Content -->
    <div class="content" id="container">
    	<div class="title"><h5>Site Settings</h5></div>

        <?php foreach($get_article as $g): ?>
        <!-- Form begins -->
        <form action="<?php echo base_url() . $this->uri->uri_string(); ?>" class="mainForm" method="post" enctype="multipart/form-data">

        	<!-- Input text fields -->
            <fieldset>
                <div class="widget first">
                    <div class="head"><h5 class="iList">Edit Site Settings</h5></div>
                        <div class="rowElem kenny">
           
                            <div class="rowElem">
                                <label class="formLeft">Current Logo:</label>
                                <div class="formRight">
                                    <img style="max-width:80%;" src="<?php echo base_url().$g->image; ?>" />
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Change Logo? <br></label>
                                <div class="formRight">
                                    <input type="file" name="lg_img">
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Site Description:</label>
                                <div class="formRight">
                                    <textarea rows="8" cols="" name="description" class="auto" style="overflow: hidden;" placeholder="Site Description"/><?php //echo $mixtape->desc; ?></textarea>
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">How many featured videos on <b>homepage</b>?:</label>
                                <div class="formRight">
                                    <input type="text" name="author" placeholder="Author" value="<?php echo $g->author; ?>" />
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">How many featured videos on <b>Music Page</b>?:</label>
                                <div class="formRight">
                                    <input type="text" name="author" placeholder="Author" value="<?php echo $g->author; ?>" />
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">How many <b>Top videos</b>?:</label>
                                <div class="formRight">
                                    <input type="text" name="author" placeholder="Author" value="<?php echo $g->author; ?>" />
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">How many <b>GbaamTV featured videos</b>?:</label>
                                <div class="formRight">
                                    <input type="text" name="author" placeholder="Author" value="<?php echo $g->author; ?>" />
                                </div>
                            </div>

                            <div class="rowElem">
                                <label class="formLeft">Permalink (Pretty URL for SEO):</label>
                                <div class="formRight">
                                    <input type="text" name="permalink" placeholder="Permalink" value="<?php echo $g->permalink; ?>" />
                                </div>
                            </div>

                    <div class="fix"></div>
                </div>

                <div class="widget">
                    <div class="floatright twoOne">
                   
                    <input type="submit" name="submit" value="Submit form" class="greyishBtn submitForm" />
                    
                    </div>
                    <div class="fix"></div>
                </div>
            </fieldset>
        </form>
        <?php endforeach; ?>
        
    </div>
    
<div class="fix"></div>
</div>

<!-- Footer -->
<?php include('inc/footer.php'); ?>

</body>
</html>
