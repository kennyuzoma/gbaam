<!DOCTYPE html>
<html>
<head>

  <?php 
    //Load the reqiured Files View
    echo $this->load->view('inc/req_files'); 
  ?>
  <script>

  $().ready(function() {
    // validate the comment form when it is submitted
    $(".contactform").validate({

        rules: {
          person_name: "required",
          message: "required",
          email: {
            required: true,
            email: true
          }
        },
        messages: {
          person_name: "Please enter your name.",
          email: "Please enter a valid email address.",
          message: "Your message can not be empty."
        }

   });

  });
  </script>



    
</head>

<body class="singleVideo">
  <?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
<div id="wrapper">

  <?php 
    //Load the reqiured header view
    echo $this->load->view('inc/header'); 
  ?>
    
    <div class="vidmain">

      <div style="float:left;width:100%;text-align:center;padding:20px 0;">
          
      	<h1 style="font-size:40px;color:#f90;margin-bottom:20px; font-size:bold;"><?php echo $title; ?></h1>
          
    	<span style="">
			For technical issues please contact: <a style="color:#f90;" href="mailto:support@gbaam.com" target="_blank">Support@gbaam.com</a>
			<br><br>
			To get in touch with our editorial team please contact: <a style="color:#f90;" href="mailto:staff@gbaam.com" target="_blank">Staff@gbaam.com</a>
			<Br><br>
			For Inquiries about Advertising, please <a style="color:#f90;" href="<?php echo base_url(); ?>advertise">Click Here</a>
			<Br><br>
			For any other inquiries, please use the contact form below.

       	</span>
          

        <form action="<?php echo base_url(); ?>contact" method="POST" id="login_form"  class="subsub contactform" style="text-align:left;padding:10px 20px 20px 20px;margin-top:20px;">
          <?php
            if(isset($validation))
            {
              if($validation == FALSE)
              {
                echo '<span class="wrong">'.$message.'</span>';
              }
            }
          ?>

          <input name="s_holder" type="text" class="theholder" />


          <div class="row">
              <label for="name">Name:</label>
              <input name="person_name" type="text" class=" inptxt" id="" placeholder="Your Name" value="<?php echo $this->input->post('name'); ?>" minlength="2" required  />
          </div>

          <div class="row">
              <label for="email">Email:</label>
              <input name="email" type="text" class=" inptxt" id="" placeholder="Email Address" value="<?php echo $this->input->post('email'); ?>"  required />
          </div>

          <div class="row">
              <label for="subject">Subject:</label>
              <input name="subject" type="text" class=" inptxt" id="" placeholder="Subject" value="<?php echo $this->input->post('subject'); ?>"   />
          </div>

          <div class="row">
              <label for="message">Message:</label> 
              <textarea name="message" rows="" class="inptxt" cols="" class="form-textarea"  required></textarea>
          </div>
          
          <div class="row" style="text-align:center;">

            <input type="submit" class="textshad submit" name="submit" value="Submit"   />

          </div>

                  <div style="clear:both;"></div>

              </form>
			     
      </div>
        
  </div>
    
  <?php echo $this->load->view('inc/footer'); ?>
  <script src="<?php echo base_url(); ?>assets/js/jquery.validate.js" type="text/javascript"></script>
  <script>
  $(function(){
    

    $("#login_form").validate();
  });
  </script>

</body>
</html>