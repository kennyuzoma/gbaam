<!DOCTYPE html>
<html>
	<head>

	<?php 
		//Load the reqiured Files View
		echo $this->load->view('inc/req_files'); 
	?>
	    
	<style type="text/css">
	#legal p, #legal ul{
			float: left;
			width: 100%;
			word-break: break-word;
			line-height: 24px;
			color: #eee;
			letter-spacing: normal;
			font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif!important;
			font-size: 14px;
			font-style: normal;
			font-variant: normal;
			font-weight: 400;
		}

		#legal ul {
			list-style:normal;
		}

		#legal ul li{
			display:list-item;
			list-style-type: disc;
			margin-left:20px;
		}
	</style>
	
	</head>

	<body class="singleVideo">

		<?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>

		<div id="wrapper">
		<?php 
			//Load the reqiured header view
			echo $this->load->view('inc/header'); 
		?>
	    
		<div class="vidmain maintext">
			
			<div id="legal" class="reg_content">

				<h1>
					<span class="text">About Us</span>
				</h1>

				<section class="block" id="who_may_use">
	                <p>
	                	Gbaam is a digital media company that promotes the broadest and deepest array of content of interest to the African community by the African community at home and in diaspora.
	                </p>
	                <p>
						It is a web space that had the characteristics of a music video network and a general entertainment network coupled with an online editorial section showcasing African creativity with regards to the arts and entertainment in general.
					</p>
	            </section>



			</div>

			<?php $this->load->view('inc/mainright'); ?>

	        
		</div>
	    
		<?php echo $this->load->view('inc/footer'); ?>

	</body>
</html>