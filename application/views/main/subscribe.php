<!DOCTYPE html>
<html>
<head>

  <?php 
    //Load the reqiured Files View
    echo $this->load->view('inc/req_files'); 
  ?>
    <style>
		#login_form .submit{
			margin-right: 0 !important;
			font-size: 30px;
			padding: 5px 50px;
			margin: 10px;
			display: inline-block;
		}
    </style>
</head>

<body class="singleVideo">
  <?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
<div id="wrapper">

  <?php 
    //Load the reqiured header view
    echo $this->load->view('inc/header'); 
  ?>
    
    <div class="vidmain">

      <div style="float:left;width:100%;text-align:center;padding:20px 0;">
          
          <h1 style="font-size:40px;color:#f90;margin-bottom:20px; font-size:bold;"><?php echo $title; ?></h1>

     <span style="font-size:18px;">Subscribe today to get the latest updates on Gbaam!</span>
          
		<?php 
			if(!is_loggedin()):
		?>
        <form action="<?php echo base_url(); ?>main/subscribe" method="POST" id="login_form"  style="padding:0px 20px 20px 20px;margin-top:20px;">
          <?php
            if(isset($validation))
            {
              if($validation == FALSE)
              { 
                echo '<span class="wrong">'.$message.'</span>';
              }
            }
          ?>
			
			<div class="row">
              <label for="email">Name</label><br>
              <input name="name" type="text" class="ml_email inptxt" id="ml_email" placeholder="Name" value="<?php echo $this->input->post('name'); ?>" autocomplete="off" />
          	</div>

          <div class="row">
              <label for="email">Email</label><br>
              <input name="email" type="text" class="ml_email inptxt" id="ml_email" placeholder="Email" value="<?php echo $this->input->post('email'); ?>" autocomplete="off" />
          </div>


          <div class="row">

            <input type="submit" class="textshad submit" name="submit"  value="Subscribe!" style="margin-left:0;" />

          </div>
          <div style="clear:both;"></div>

        </form>
        <?php 
        	else:
        ?>

    	<div id="login_form" style="padding:20px 20px 20px 20px;margin-top:20px;text-align:center;">
			<a href="<?php echo base_url(); ?>main/subscribe/button" class="textshad submit">Subscribe</a>
    	</div>

    	<?php 
    		endif;
    	?>
			     
      </div>
        
  </div>
    
  <?php echo $this->load->view('inc/footer'); ?>

</body>
</html>