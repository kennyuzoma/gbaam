<?php

$next = '';

if(isset($_GET['next']))
	$next = '?next=' . $_GET['next']; 

?>

<!DOCTYPE html>
<html>
<head>

  <?php 
    //Load the reqiured Files View
    echo $this->load->view('inc/req_files'); 
  ?>
    
</head>

<body class="singleVideo">
  <?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
<div id="wrapper">

  <?php 
    //Load the reqiured header view
    echo $this->load->view('inc/header'); 
  ?>
    
    <div class="vidmain">

      <div style="float:left;width:100%;text-align:center;padding:20px 0;">
          
        <h1 style="font-size:40px;color:#f90;margin-bottom:20px; font-size:bold;">Login to Gbaam</h1>
		
		<?php if(isset($_GET['next'])): ?>
        <p style="margin-bottom:20px;">You must be logged in to access this area</p>
        <?php endif; ?>
          

        <form action="<?php echo base_url() . 'main/login' . $next; ?>" method="POST" id="login_form">
          
		
			<div style="float:left;text-align:center;width:100%;margin-top:20px;margin-bottom:20px;">

				<span class="js_fb_cont" style="">
					<a href="<?php echo base_url() . 'oauth/login/facebook' . $next; ?>" class="js_fb_click">
						<img alt="Connect with Facebook" style="" src="<?php echo base_url(); ?>assets/img/Small_and_Long_200x24.png" />
					</a>
				</span>
				
				&nbsp;&nbsp;&nbsp;&nbsp;

				<span class="js_twitter_cont" style="">
					<a href="<?php echo base_url().'oauth/login/twitter' . $next; ?>" class="js_twitter_click">
						<img alt="Connect with Twitter" style="" src="<?php echo base_url(); ?>assets/img/sign-in-with-twitter-gray.png">
					</a>
				</span>

			</div>

			<hr />

			<?php
            if(isset($validation))
            {
              if($validation == FALSE)
              {
                echo '<span class="wrong">'.$message.'</span>';
              }
            }
          ?>

          <div class="row">
              <label for="email">Username or Email</label><br>
              <input name="username" type="text" class="ml_email inptxt" id="ml_email" placeholder="Username or Email" value="<?php echo $this->input->post('username'); ?>" />
          </div>
          
          <div class="row">
                  <label for="password">Password</label><br>
                    <input name="password" type="password" class="ml_password inptxt" placeholder="Password" />
                  </div>

                  <div class="row">

                    <input type="submit" class="textshad submit" name="login" value="Login" />

                  </div>

                  <div style="clear:both;"></div>
                  <br><bR>
                  <div style="width:100%;text-align:center;">
                  	<a href="<?php echo base_url(); ?>main/forgotpassword" class="forget center">Forgot password?</a>
                  	<div style="clear:both;"></div>
                  </div>

              </form>
			     
      </div>
        
  </div>
    
  <?php echo $this->load->view('inc/footer'); ?>

</body>
</html>