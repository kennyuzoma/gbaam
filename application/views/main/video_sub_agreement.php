    <!DOCTYPE html>
<html>
	<head>

	<?php 
		//Load the reqiured Files View
		echo $this->load->view('inc/req_files'); 
	?>
	    
	</head>

	<body class="singleVideo">

		<?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>

		<div id="wrapper">
		<?php 
			//Load the reqiured header view
			echo $this->load->view('inc/header'); 
		?>
	    
		<div class="vidmain maintext">
			
			<div id="legal">

				<h1>
                	<span class="text">Video Submission Agreement</span>
                	
                	<?php
                		$date = get_setting_updated_date('video_sub_agreement');
                		if($date != FALSE):
                	?>
					<span class="updated">Last Modified: <?php echo $date; ?></span>
					<?php 
						endif;
					?>
                </h1>

				<section class="block">
					<p>
			    By submitting your video, you hereby grant GBAAM LLC,
			    ("Gbaam") the irrevocable right and license to duplicate, broadcast, exhibit, transmit and
			    exploit the video, or to allow Gbaam to grant sublicenses to third parties to duplicate,
			    broadcast, exhibit, transmit and exploit the video, on the internet, television, mobile platforms
			    or other media throughout the world.
			</p>
			    </section>
			    <section class="block">
					<p>

			    You agree that Gbaam shall not be required to make any payment to you or anyone else in
			    exercise of the rights granted in this Agreement, and acknowledge that you shall receive valuable
			    consideration in the form of placement of and promotion of the video.
			</p>
			  </section>
			    <section class="block">
			    	<p>
			    You represent that you either created the video yourself or have received the legal right and
			    authority from the person who created the video to grant to Gbaam full and unrestricted rights
			    to the video, and that our exercise of such rights shall not constitute an infringement of any
			    copyright, civil, personal or property right of any third party. If you are submitting a music
			    video, you represent that you have full authority, permission and license from all copyright
			    holders in the visual and musical works you submit to Gbaam.
			</p>
			  </section>
			    <section class="block">
			    	<p>
			    Unless otherwise agreed to in writing, Gbaam shall have no obligation to publish or post your
			    video. For all videos published or posted upon the site, we reserve the right to remove the video
			    at our discretion and we will give no refunds or compensation.
			</p>
			  </section>
			    <section class="block">
			    	<p>
			    You hereby agree that you will indemnify Gbaam, its officers, employees and agents, from and
			    against any and all claims, demands, actions, losses, liabilities, costs and expenses arising from
			    any breach or alleged breach of any term, warranty or condition of this agreement, or from any
			    claims of infringement or alleged infringement arising from your audio and/or video submission.
			</p>
			    </section>


			</div>

			<?php $this->load->view('inc/mainright'); ?>

	        
		</div>
	    
		<?php echo $this->load->view('inc/footer'); ?>

	</body>
</html>