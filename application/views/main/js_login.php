<?php

$next = '';

if(isset($_GET['next'])) 
	$next = '?next=' . $_GET['next'];

?>

<!DOCTYPE>
<html>
<head>
    <title>Sign in to Disqus</title>
    <link href="<?php echo base_url(); ?>assets/css/login.css" type="text/css" rel="stylesheet" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    

    <!--[if lte IE 8]>
    <script src="<?php echo base_url(); ?>assets/js/ie8.js"></script>
    <![endif]-->
    <!--[if IE 9]>
    <script src="<?php echo base_url(); ?>assets/js/ie9.js"></script>
    <![endif]-->

</head>
<body>
    
        



        
    
    <div id="js-content" style="background:none;">
        <div class="header" style="">
            <img alt="Gbaam.com" src="<?php echo $this->config->item('logo_url'); ?>"  style="height:50px;" />
        </div>
        <div class="container">
            <?php
            if(isset($validation))
            {
              if($validation == FALSE)
              {
                echo '<div class="alert error">'.$message.'</div>';
              }
            }
          ?>
    
    
     <form action="<?php echo base_url() . 'main/login' . $next; ?>" method="POST" id="login_form" novalidate>
          
        <div class="form-row">
            <label>Email or Username</label>
            <input name="username" autofocus type="email" placeholder="Email or Username" value="<?php echo $this->input->post('username'); ?>" tabindex="1" />
        </div>
        <div class="form-row">
            
            <a href="<?php echo base_url(); ?>main/forgotpassword" class="pull-right forget-pw" tabindex="5" target="_blank" onclick="setTimeout(function () { window.close() }, 0);">Forgot password?</a>

            <label>Password</label>
            <input name="password" type="password" placeholder="Password" tabindex="2"/>
        </div>
        <div class="actions">
            <button type="submit" name="login" class="btn main" tabindex="3">Sign in</button>
            <a href="#" onclick="window.close()" tabindex="4">Cancel</a>

            <?php /*<a class="pull-right" style="padding: 10px 0;" href="https://disqus.com/next/register/?forum=gbaam">Need an account?</a>*/ ?>
        </div>
    </form>

        </div>
    </div>

    <script>
    
    if (!("autofocus" in document.createElement("input"))) {
        // Need to defer focus or doesn't play nice with < IE9 placeholder polyfill
        // (200ms is completely arbitrary / works fine)
        setTimeout(function () {
            document.forms[0].username.focus();
        }, 200);
    }
    </script>

    <script>
    

    (function () {
        // https://gist.github.com/3108177
        var supportsGCS = "defaultView" in window.document && "getComputedStyle" in window.document.defaultView;

        function getStyle(element, property) {
            // `element.ownerDocument` returns the used style values relative to the
            // element's parent document (which may be another frame). `defaultView`
            // is required for Safari 2 support and when retrieving framed styles in
            // Firefox 3.6 (https://github.com/jquery/jquery/pull/524#issuecomment-2241183).
            var style = supportsGCS ? element.ownerDocument.defaultView.getComputedStyle(element, null) : element.currentStyle;
            return (style || element.style)[property];
        }

        function getWindowPadding() {
            // Based in part on
            // http://stackoverflow.com/questions/1275849/get-height-of-enter-browser-window-in-ie8

            if (window.outerHeight !== undefined) {
                return {
                    x: window.outerWidth - window.innerWidth,
                    y: window.outerHeight - window.innerHeight
                }
            }

            var docElem = document.documentElement;

            // Old browser (IE8 and below). Need to resize window, observe change in
            // clientWidth/Height in order to determine padding.
            var oldX = docElem.clientWidth;
            var oldY = docElem.clientHeight;

            // clientWidth/Height *will* be smaller than the current window size. But
            // not by much. I figure this is the least jarring size to pick.
            window.resizeTo(oldX, oldY);

            var padding = {
                x: oldX - docElem.clientWidth,
                y: oldY - docElem.clientHeight
            };

            // Restore window to original dimensions
            window.resizeTo(oldX + padding.x, oldY + padding.y);

            return padding;
        }

        function getScrollbarWidth() {
            // credits: http://davidwalsh.name/detect-scrollbar-width
            // Create the measurement node
            var scrollDiv = document.createElement("div");
            scrollDiv.className = "scrollbar-measure";
            document.body.appendChild(scrollDiv);

            // Get the scrollbar width
            var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;

            // Delete the DIV
            document.body.removeChild(scrollDiv);
            return scrollbarWidth;
        }

        if (window.resizeTo) {
            window.onload = function () {
                // Should only calculate document dimensions after page has fully loaded.
                // I've also observed some funky results without using setTimeout to wrap
                // the callback. Other applications seem to resize on a delay, so I suspect
                // this is necessary.
                setTimeout(function () {
                    var content = document.getElementById('js-content'),
                        // small empty space at the bottom (20px) plus additional space for expanded summary on
                        // registration page...
                        buffer = 102,
                        width = content.offsetWidth,
                        height = content.offsetHeight,
                        padding = getWindowPadding(),
                        scrollWidth = getScrollbarWidth(),
                        viewportHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
                        viewportWidth = window.innerWidth || ((document.documentElement.clientWidth || document.body.clientWidth) + scrollWidth),
                        browser = window.outerHeight || viewportHeight;

                    // resize only when content is cropped
                    if(viewportHeight < height + buffer || viewportWidth < width) {
                        window.resizeTo(Math.max(width, viewportWidth), height + (browser - viewportHeight) + buffer);
                    }
                }, 250);
            }
        }
    }());
    </script>
 </body>
</html>
