<!DOCTYPE html>
<html>
<head>

  <?php 
    //Load the reqiured Files View
    echo $this->load->view('inc/req_files'); 
  ?>
    
</head>

<body class="singleVideo">
  <?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
<div id="wrapper">

  <?php 
    //Load the reqiured header view
    echo $this->load->view('inc/header'); 
  ?>
    
    <div class="vidmain">

      <div style="float:left;width:100%;text-align:center;padding:20px 0;">
          
          <h1 style="font-size:40px;color:#f90;margin-bottom:20px; font-size:bold;">Sign Up for Gbaam</h1>

          <span style="margin-bottom:10px;">Create a Gbaam Account!</span>

        
          

        <form action="<?php echo base_url(); ?>main/signup" method="POST" id="login_form" style="margin-top:20px;">
          

          <div style="float:left;text-align:center;width:100%;margin-top:20px;margin-bottom:20px;">

				<span class="js_fb_cont" style="">
					<a href="<?php echo base_url() . 'oauth/login/facebook?next='.uri_string(); ?>" class="js_fb_click">
						<img alt="Connect with Facebook" style="" src="<?php echo base_url(); ?>assets/img/Small_and_Long_200x24.png" />
					</a>
				</span>
				
				&nbsp;&nbsp;&nbsp;&nbsp;

				<span class="js_twitter_cont" style="">
					<a href="<?php echo base_url().'oauth/login/twitter?next='.uri_string(); ?>" class="js_twitter_click">
						<img alt="Connect with Twitter" style="" src="<?php echo base_url(); ?>assets/img/sign-in-with-twitter-gray.png">
					</a>
				</span>

			</div>

			<hr />

			<?php
            if(isset($validation))
            {
              if($validation == FALSE)
              {
                echo '<span class="wrong">'.$message.'</span>';
              }
            }
          ?>

          <input name="s_holder" type="text" class="theholder" />
          <div class="row">
              <label for="email">Name</label><br>
              <input name="s_name" type="text" class="ml_email inptxt" id="ml_email" placeholder="Name" value="<?php echo $this->input->post('s_name'); ?>" autocomplete="off" />
          </div>
          <div class="row">
              <label for="email">Username</label><br>
              <input name="s_username" type="text" class="ml_email inptxt" id="ml_email" placeholder="Username" value="<?php echo $this->input->post('s_username'); ?>" autocomplete="off" />
          </div>

          <div class="row">
              <label for="email">Email</label><br>
              <input name="s_email" type="text" class="ml_email inptxt" id="ml_email" placeholder="Email" value="<?php echo $this->input->post('s_email'); ?>" autocomplete="off" />
          </div>
          
          <div class="row">
            <label for="password">Password</label><br>
            <input name="s_password" type="password" class="ml_password inptxt" placeholder="Password" autocomplete="off" />
          </div>

          <div class="desctxt" style="width:100%;float:left;margin-top:20px;">
					<span class="text">By creating an account, I accept Gbaam's 
					
					<a href="<?php echo base_url(); ?>main/terms"><u>Terms of Service</u></a> and <a href="<?php echo base_url(); ?>main/privacy"><u>Privacy Policy</u></a>.</span>
				</div>

          <div class="row">

            <input type="submit" class="textshad submit" name="signup" value="Signup" />

          </div>
          <div style="clear:both;"></div>

        </form>
			     
      </div>
        
  </div>
    
  <?php echo $this->load->view('inc/footer'); ?>

</body>
</html>