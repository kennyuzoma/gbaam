<!DOCTYPE html>
<html>
	<head>

	<?php 
		//Load the reqiured Files View
		echo $this->load->view('inc/req_files'); 
	?>
	    
	</head>

	<body class="singleVideo">

		<?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>

		<div id="wrapper">
		<?php 
			//Load the reqiured header view
			echo $this->load->view('inc/header'); 
		?>
	    
		<div class="vidmain maintext">
			
			<div id="legal">
			
				<h1>
                	<span class="text">Privacy Policy</span>
                	
                	<?php
                		$date = get_setting_updated_date('legal_privacy');
                		if($date != FALSE):
                	?>
					<span class="updated">Last Modified: <?php echo $date; ?></span>
					<?php 
						endif;
					?>
                </h1>
				
			</div>

			<?php $this->load->view('inc/mainright'); ?>

	        
		</div>
	    
		<?php echo $this->load->view('inc/footer'); ?>

	</body>
</html>