<!DOCTYPE html>
<html>
<head>

	<?php 
		//Load the reqiured Files View
		echo $this->load->view('inc/req_files'); 
	?>

</head>

<style type="text/css">


</style>

<body class="singleVideo">
	<?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
	<div id="wrapper">

	  <?php 
	    //Load the reqiured header view
	    echo $this->load->view('inc/header'); 
	  ?>

		<div class="vidmain">

			<div id="legal" class="reg_content" style="width:600px;border-radius:10px;margin-top:0px;">

				<h1>
					<span class="text"><?php echo $title; ?></span>
				</h1>
				<p>
					<?php echo $content; ?>
				</p>

			</div>



		<?php $this->load->view('inc/rightsidebar'); ?>
 
		<div style="clear:both;"></div>

	</div>
           


    	<?php echo $this->load->view('inc/footer'); ?>
</body>
</html>

