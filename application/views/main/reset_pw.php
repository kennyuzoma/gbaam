<!DOCTYPE html>
<html>
<head>

  <?php 
    //Load the reqiured Files View
    echo $this->load->view('inc/req_files'); 
  ?>
    
</head>

<body class="singleVideo">
  <?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
<div id="wrapper">

  <?php 
    //Load the reqiured header view
    echo $this->load->view('inc/header'); 
  ?>
    
    <div class="vidmain">

      <div style="float:left;width:100%;text-align:center;padding:20px 0;">
          
          <h1 style="font-size:40px;color:#f90;margin-bottom:20px; font-size:bold;">Reset Password</h1>

        
          

        <form action="<?php echo base_url(); ?>main/resetpw?email=<?php echo $_GET['email']; ?>&key=<?php echo $_GET['key']; ?>" method="POST" id="login_form">
          <?php
            if(isset($validation))
            {
              if($validation == FALSE)
              {
                echo '<span class="wrong">'.$message.'</span>';
              }
            }
          ?>
          <div class="row">
            <span class="desctxt">
              Choose a good password you will remember.
            </span>
          </div>

          <div class="row">
              <label for="email">Password</label><br>
              <input name="password" type="password" class="ml_email inptxt" id="ml_email" style="width:350px;" value="" autocomplete="off" />
          </div>

          <div class="row">
              <label for="email">Confirm Password</label><br>
              <input name="conf_password" type="password" class="ml_email inptxt" id="ml_email" style="width:350px;" value="" autocomplete="off" />
          </div>
          
          <div class="row">

            <input type="submit" class="textshad submit" name="confirm_password_change" value="Reset Password" />

          </div>
          <div style="clear:both;"></div>

        </form>
			     
      </div>
        
  </div>
    
  <?php echo $this->load->view('inc/footer'); ?>

</body>
</html>