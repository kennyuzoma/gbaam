<!DOCTYPE html>
<html>
	<head>

	<?php 
		//Load the reqiured Files View
		echo $this->load->view('inc/req_files'); 
	?>

	<style>
		#legal p, #legal ul{
			float: left;
			width: 100%;
			word-break: break-word;
			line-height: 24px;
			color: #eee;
			letter-spacing: normal;
			font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif!important;
			font-size: 14px;
			font-style: normal;
			font-variant: normal;
			font-weight: 400;
			margin-bottom: 1.1em;
		}

		#legal ul {
			list-style:normal;
		}

		#legal ul li{
			display:list-item;
			list-style-type: disc;
			margin-left:20px;
		}
	</style>
	    
	</head>

	<body class="singleVideo">

		<?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>

		<div id="wrapper">
		<?php 
			//Load the reqiured header view
			echo $this->load->view('inc/header'); 
		?>
	    
		<div class="vidmain maintext">
			
			<div id="legal" class="reg_content">

				<h1>
                	<span class="text"><?php echo $title; ?></span>
                	
                	<?php
                		$date = get_setting_updated_date('video_sub_guidelines');
                		if($date != FALSE):
                	?>
					<span class="updated">Last Modified: <?php echo $date; ?></span>
					<?php 
						endif;
					?>
                </h1>

				<section class="block">
	                <p>
	                	We appreciate all the video submissions we recieve. However, there are submissions that Gbaam does not accept. We put these guidelines in place so that you know the kinds of videos not allowed on Gbaam. 
					</p>
					<p>
	                	We do NOT accept videos containing:
	                	<ul>
							<li>Pornography of any kind.</li>
							<li>Extremely violent or gory content that's primarily intended to be shocking, sensational, or disrespectful.</li>
							<li>Copyright Infringement.</li>
							<li>Spam Videos with misleading titles, descriptions, tags, etc to increase views.</li>
	                	</ul>
					</p>
					<p>
	                	These Guidelines will be edited from time to time, so feel free to check back!
	                </p>

	            </section>



			</div>

			<?php $this->load->view('inc/mainright'); ?>

	        
		</div>
	    
		<?php echo $this->load->view('inc/footer'); ?>

	</body>
</html>