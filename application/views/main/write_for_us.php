<!DOCTYPE html>
<html>
	<head>

	<?php 
		//Load the reqiured Files View
		echo $this->load->view('inc/req_files'); 
	?>

	<style>
		#legal p, #legal ul{
			float: left;
			width: 100%;
			word-break: break-word;
			line-height: 24px;
			color: #eee;
			letter-spacing: normal;
			font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif!important;
			font-size: 14px;
			font-style: normal;
			font-variant: normal;
			font-weight: 400;
			margin-bottom: 1.1em;
		}

		#legal ul {
			list-style:normal;
		}

		#legal ul li{
			display:list-item;
			list-style-type: disc;
			margin-left:20px;
		}
	</style>
	    
	</head>

	<body class="singleVideo">

		<?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>

		<div id="wrapper">
		<?php 
			//Load the reqiured header view
			echo $this->load->view('inc/header'); 
		?>
	    
		<div class="vidmain maintext">
			
			<div id="legal" class="reg_content">

				<h1>
					<span class="text"><?php echo $title; ?></span>
				</h1>

				<section class="block">
	                <p>
	                	Are you a blogger or writer that would like to be featured on our site? Gbaam welcomes guest posting and writers from all over! If you want to submit an article to us, please send us an email at <a href="mailto:writeforus@gbaam.com?subject=Write For Us" target="_blank">writeforus@gbaam.com</a>. In the email, please include a link to sample work OR write us a small sample. (2-3 Paragraphs).

	                	<br><Br>
	                	(If you want to become a team member on the <b>Gbaam Writing Staff</b>, please send us an email to <a href="mailto:staff@gbaam.com?subject=Writing Team" target="_blank">Staff@gbaam.com</a>.)
					</p>

					<p>
						Articles we <b>WANT</b>:
						<ul>
							<li>Original Content</li>
							<li>Music Posts</li>
							<li>Entertainment</li>
							<li>Content preferably relating to things with an African context<br> in the categories of News, Music, Entertainment, Gossip, Arts, etc. </li>
						</ul>
					</p>

					<p>
						Articles we <b>DON'T</b> want:
						<ul>
							<li>Unoriginal content</li>		
							<li>Plagarized content</li>		
							<li>Duplicated content</li>	
							<li>Content violating <a href="<?php echo base_url(); ?>main/terms">our terms</a> (especially copyrighted content!)</li>	
							<li>Completely irrelevant content</li>		
						</ul>
					</p>
					<?php /*<p>
						For more info on what you can post or not post, please view our <a href="<?php echo base_url(); ?>writer-guidelines">Writer Guidelines</a>.
					</p>*/ ?>

					<p>
						To get started, email your sample work to <a href="mailto:writeforus@gbaam.com?subject=Write For Us" target="_blank">writeforus@gbaam.com</a>!<br>

						If you want to become a team member on the <b>Gbaam Writing Staff</b>, please send us an email to <a href="mailto:staff@gbaam.com?subject=Writing Team" target="_blank">Staff@gbaam.com</a>.
					</p>

					<p>
						Thanks! <br><i>- The Gbaam Team.</i>
					</p>

	            </section>



			</div>

			<?php $this->load->view('inc/mainright'); ?>

	        
		</div>
	    
		<?php echo $this->load->view('inc/footer'); ?>

	</body>
</html>