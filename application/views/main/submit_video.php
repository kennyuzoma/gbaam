<!DOCTYPE html>
<html>
<head>

  <?php 
    //Load the reqiured Files View
    echo $this->load->view('inc/req_files'); 
  ?>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

  <script>
  $(function() {
    $( "#datepicker" ).datepicker({ minDate: 0});

    var submit_type = $('.submit_type').val();
    if(submit_type == 'video')
    	var correct_text = 'song name.';
    else
    	var correct_text = 'video title';

    $(".subsub").validate({
  
        rules: {
          videotitle: {
          	required: true,
          	minlength: 1
          },
          artist: {
          	required: true,
          	minlength: 1
          },
          description: {
          	required: true,
          	minlength: 1
          },
          first_name: {
          	required: true,
          	minlength: 1
          },
          last_name: {
          	required: true,
          	minlength: 1
          },
          email: {
            required: true,
            email: true
          },
          video_src:{
            required: function(element) {
              if($('.video_src_other').val()=="")//return value based on textbox1 status
                     return true;
                else
                     return false;
            }
          },
          video_src_other:{
            required: function(element) {
              if($('.video_src').val()=="")//return value based on textbox1 status
                     return true;
                else
                     return false;
            }
          }
        },
        messages: {
          videotitle: {
          	required: "Enter a "+correct_text,
          	minlength: jQuery.format("At least {0} characters required!")
          },
          artist: "Enter the Artist's name.",
          description: "Enter a video description.",
          first_name: "Enter your first name.",
          last_name: "Enter your last name.",
          email: "Please enter a valid email address.",
          video_src: "",
          video_src_other: "At least <b>ONE</b> video source must be entered in one of the fields above."

        }

   });

  $('.video_src, .video_src_other').keyup(function (){
   $('.video_src, .video_src_other').next('.error').hide('');
  });

  });
  </script>



    
</head>

<body class="singleVideo">
  <?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
<div id="wrapper">

  <?php 
    //Load the reqiured header view
    echo $this->load->view('inc/header'); 
  ?>
    <?php
    	$seg = $this->uri->segment(3);
    	$disabled = '';
    ?>
    <div class="vidmain">
		

		<ul class="submenu">
      		
      		<li><a <?php if($seg == 'video') echo 'class="sel"'; ?> href="<?php echo base_url(); ?>main/submit/video">Submit Music Video</a></li>
      		<li><a <?php if($seg == 'gbaamtv') echo 'class="sel"'; ?> href="<?php echo base_url(); ?>main/submit/gbaamtv">Submit GbaamTV Video</a></li>
      		<li><a <?php if($seg == 'challenge') echo 'class="sel"'; ?> href="<?php echo base_url(); ?>main/submit/challenge">Submit Challenge Video</a></li>
      		
      	</ul>
          
      <div style="float:left;width:100%;text-align:center;padding:20px 0;">

      		
          <h1 style="font-size:40px;color:#f90;margin-bottom:20px; font-size:bold;">
          	<?php echo $title; ?>
          	<?php if($seg == 'challenge'){ ?>
          	<br>(Coming Soon!)
          	<?php 

          		$disabled = 'disabled';
          	} else {
          		$disabled = '';
          	}?>
          	</h1>
          
          <span style="">
          <?php 
              if($seg == 'video')
                echo $this->config->item('submit_desc_text_mv');
              elseif($seg == 'gbaamtv')
                echo $this->config->item('submit_desc_text_gbaamtv'); 
              elseif($seg == 'challenge')
                echo $this->config->item('submit_desc_text_challenge'); 
          ?>
        </span>
          

        <form action="<?php echo base_url(); ?>main/submit/<?php echo $page; ?>/" method="POST" id="login_form"  class="subsub submitform" style="text-align:left;padding:0px 20px 20px 20px;margin-top:20px;">

        	<input type="hidden" name="submit_type" class="submit_type" value="<?php echo $seg; ?>" />

        	<input name="s_holder" type="text" class="theholder" />
        	
          <?php
            if(isset($validation))
            {
              if($validation == FALSE)
              {
                echo '<span class="wrong">'.$message.'</span>';
              }
            }
          ?>

          
          
          <?php if($seg == 'challenge'): ?>
          <div class="row">
              <label for="email">Challenge:</label> 
              <?php  $chal = $this->Video_model->get_latest_challenge_info(); ?>
              <b style="float:left;margin-top:9px;font-size:24px;color:#f90;"><?php echo $chal->display_name; ?></b>
              <input type="hidden" name="chall_cat" value="<?php echo $chal->id; ?>"  />
          </div>
          <?php endif; ?>
			
			<?php if($seg == 'video'): ?>
			<div class="row">
	              <label for="email">Song Title:</label>
	              <input name="videotitle" type="text" class=" inptxt" id="" placeholder="Song Title" value="<?php echo $this->input->post('videotitle'); ?>" minlength="2" required <?php echo $disabled; ?> />
          	</div>

          	<div class="row">
              <label for="email">Artist:</label>
              <input name="artist" type="text" class=" inptxt" id="" placeholder="Artist Name" value="<?php echo $this->input->post('artist'); ?>" minlength="2" required  <?php echo $disabled; ?>/>
          	</div>

      		<?php else: ?>

          	<div class="row">
              <label for="email">Video Title:</label>
              <input name="videotitle" type="text" class=" inptxt" id="" placeholder="Video Title" value="<?php echo $this->input->post('videotitle'); ?>" minlength="2" required <?php echo $disabled; ?> />
          	</div>
          	<?php endif; ?>

          <div class="row">
              <label for="email">Video Description:</label> 
              <textarea name="description" rows="" class="inptxt" cols="" class="form-textarea"  required <?php echo $disabled; ?>></textarea>
          </div>

          <hr>
          <div class="row">
            <span class="desctxt">
              Please enter a video source below. If your video is hosted on YouTube, enter it on the <b style="color:#f90;">1st box</b> below.<br>
              <b style="color:#f90;">NOTE: Please make sure your video is NOT set to Private or we cant post it on Gbaam!</b><br> 
              Any <b style="color:#f90;">other</b> direct video file, zip file, dropbox link, sendspace link, etc, please enter it on the <b style="color:#f90;">2nd box</b> below! 
            </span>
          </div>

          <div class="row">
              <label for="email">Youtube Video URL<br>(not private):</label>
              <input style="margin-top:10px;" name="video_src" type="text" class=" inptxt video_src" id="" placeholder="Youtube Video URL" value="<?php echo $this->input->post('video_src'); ?>"  <?php echo $disabled; ?> />
          </div>

          <div class="row">
              <label for="email">Other video source file:</label>
              <input name="video_src_other" type="text" class=" inptxt video_src_other" id="" placeholder="Zip file, Dropbox, Sendspace, Etc" value="<?php echo $this->input->post('video_src_other'); ?>" <?php echo $disabled; ?>  />
          </div>

          <hr>

          <div class="row">
              <label for="email">Email:</label>
              <input name="email" type="text" class=" inptxt" id="" placeholder="Email" value="<?php echo $this->input->post('email'); ?>"  required <?php echo $disabled; ?> />
          </div>

          <div class="row">
              <label for="email">First Name:</label>
              <input name="first_name" type="text" class=" inptxt" id="" placeholder="First Name" value="<?php echo $this->input->post('first_name'); ?>"  required <?php echo $disabled; ?> />
          </div>

          <div class="row">
              <label for="email">Last name:</label>
              <input name="last_name" type="text" class=" inptxt" id="" placeholder="Last name" value="<?php echo $this->input->post('last_name'); ?>"  required <?php echo $disabled; ?> />
          </div>
          
      
        <?php if($seg == 'gbaamtv'): ?>

          <div class="row">
              <label for="email">Video Producer:</label>
              <input name="prod" type="text" class=" inptxt" id="" placeholder="Video Producer" value="<?php echo $this->input->post('prod'); ?>"  <?php echo $disabled; ?> />
          </div>

          <div class="row">
              <label for="email">Video Director:</label>
              <input name="dir" type="text" class=" inptxt" id="" placeholder="Video Director" value="<?php echo $this->input->post('dir'); ?>"  <?php echo $disabled; ?> />
          </div>

          <div class="row">
              <label for="email">Copyright/Record Company:</label>
              <input name="company" type="text" class=" inptxt" id="" placeholder="Copyright/Record Company" value="<?php echo $this->input->post('company'); ?>" <?php echo $disabled; ?>  />
          </div>

        <?php endif; ?>

        <?php /* if($seg == 'gbaamtv' || $seg == 'video'): ?>
          <div class="row">
            <label style="margin-top:30px;">Requested Release Date:</label>
            <span style="font-size:13px;color:#f90;">Leave blank if you want us to release the video when its approved</span>
            <input type="text" id="datepicker" class="inptxt" name="date_release" <?php echo $disabled; ?> />
          </div>
          <?php endif;*/ ?>


          
        

          <div class="row" style="text-align:center;">
          	<span class="desctxt" style="margin-bottom:15px;">By clicking submit, you acknowledge that you agree to <br> Gbaam's <a href="<?php echo base_url(); ?>main/video-guidelines"><u>Video Guidelines</u></a> and <a href="<?php echo base_url(); ?>main/video-agreement"><u>Video Submission Agreement.</u></a></span>

            <input type="submit" class="textshad submit" name="submit_newvideo" value="<?php echo $title; ?>" <?php echo $disabled; ?> />

          </div>

                  <div style="clear:both;"></div>

              </form>
			     
      </div>
        
  </div>
    
  <?php echo $this->load->view('inc/footer'); ?>
  

</body>
</html>