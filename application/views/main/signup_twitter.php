<!DOCTYPE html>
<html>
<head>

  <?php 
    //Load the reqiured Files View
    echo $this->load->view('inc/req_files'); 
  ?>
    
</head>

<body class="singleVideo">
  <?php echo $this->load->view('inc/admin_nav'); ?>
    <?php echo $this->load->view('inc/analytics'); ?>
<div id="wrapper">

  <?php 
    //Load the reqiured header view
    echo $this->load->view('inc/header'); 
  ?>
    
    <div class="vidmain">

      <div style="float:left;width:100%;text-align:center;padding:20px 0;">
          
          <h1 style="font-size:40px;color:#f90;margin-bottom:20px; font-size:bold;">Almost Done!</h1>

        <span style="font-size:18px;">Enter your email address to finish registering with Twitter!</span>
          

        <form action="<?php echo base_url(); ?>main/twitter_finish" method="POST" id="login_form"  style="padding:0px 20px 20px 20px;margin-top:20px;text-align:center;">
          <?php
            if(isset($validation))
            {
              if($validation == FALSE)
              { 
                echo '<span class="wrong">'.$message.'</span>';
              }
            }
          ?>

          <div class="row">
              <label for="email">Email</label><br>
              <input name="s_email" type="text" class="ml_email inptxt" id="ml_email" placeholder="Email" value="<?php echo $this->input->post('s_email'); ?>" autocomplete="off" />
          </div>
          
         <?php /* <div class="row">
            <label for="password">Password</label><br>
            <input name="s_password" type="password" class="ml_password inptxt" placeholder="Password" autocomplete="off" />
          </div> */ ?>

          <div class="row">

            <input type="submit" class="textshad submit" name="submit" value="Finish Twitter Signup" style="margin-left:0;" />

          </div>
          <div style="clear:both;"></div>

        </form>
			     
      </div>
        
  </div>
    
  <?php echo $this->load->view('inc/footer'); ?>

</body>
</html>