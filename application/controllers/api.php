<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Api extends REST_Controller  {
	const OK_CODE 			= 200;
	const BADREQUEST_CODE	= 400;
	const UNAUTHORIZED_CODE = 401;
	const NOTFOUND_CODE 	= 404;

##########################################################################################	
#######################################   PRIVATE   ######################################
##########################################################################################	

	private function getVideoURLFromYoutubeId($theId) {
    	$URL_MAP = array(
    			"AXWUEOw-HG0" => "http://r12---sn-nx57yney.googlevideo.com/videoplayback?signature=672561295072DEA12AF599CEB91D9362DA1D5957.D9C10798350593DF8FE32BBCEF207325B0E327A3&pfa=20s&source=youtube&initcwndbps=1703750&ipbits=0&key=yt5&pbr=yes&id=o-ABA05PgFtBsH1-lOgDqjHhkpp3wAF348UJb1KlYH2PTP&fexp=900242%2C902033%2C904844%2C908200%2C914060%2C924640%2C927622%2C930666%2C931983%2C932404%2C934030%2C946012%2C947209%2C948205%2C952302%2C953731%2C953801&pm_type=static&ms=au&mt=1411198732&expire=1411220420&mv=m&upn=I39t3YM0_Oc&itag=43&ip=2601%3A8%3A8400%3A827%3A109%3A953e%3A63b4%3A632f&mm=31&ratebypass=yes&sver=3&sparams=id%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Cmm%2Cms%2Cmv%2Cpbr%2Cpfa%2Cpm_type%2Cratebypass%2Csource%2Cupn%2Cexpire"
    		);
    	return $URL_MAP[$theId];
    	return $theId;
    }

	private function urlExists($url) {
		return true;
	}

	private function isYoutubeURL($url) {

		if (strpos($url, 'youtube') !== false) {
			return true;
		}

		if (strpos($url, 'youtu') !== false) {
			return true;
		}

		return false;
	}

	private function getYoutubeIdFromUrl($url) {
    	$parts = parse_url($url);
    	if (isset($parts['query'])) {
        	parse_str($parts['query'], $qs);
        	if (isset($qs['v'])) {
          		return $qs['v'];
        	} else if ($qs['vi']) {
            	return $qs['vi'];
        	}
    	}

    	if (isset($parts['path'])) {
        	$path = explode('/', trim($parts['path'], '/'));
        	return $path[count($path)-1];
    	}

    	return None;
	}

	private function getType() {
		return strtolower($this->get('type'));
	}
	
	private function getId() {
		return $this->get('id');
	}
	
	private function getCat() {
		return strtolower($this->get('cat'));
	}
		
	private function getKey() {
		return strtolower($this->get('key'));
	}
	
	private function postId() {
		return $this->post('id');
	}
	
	private function getVideoArrayFromObject($video) {
		// sanitize the score output first
		$score = $this->Video_model->api_getScore($video->id);
		$score = $score == 'None' ? 0 : $score;
		
		// we only support youtube URLs for now
		
		if ($this->isYoutubeURL($video->video_src)) {

			$thumbnailURL = '';
			$videoSrcURL = '';
			$youtubeId = $this->getYoutubeIdFromUrl($video->video_src);		
			if ($youtubeId != None) {
				$thumbnailURL = 'http://img.youtube.com/vi/'.$youtubeId.'/maxresdefault.jpg';

				// $jpgList = array('/maxresdefault.jpg', '/hqdefault.jpg', '/0.jpg');
				// foreach ($jpg as $jpgList) {
				// 	$thumbnailURL = 'http://img.youtube.com/vi/'.$youtubeId.$jpg;
				// 	if (urlExists($thumbnailURL)) {
				// 		break;
				// 	}	
				// }
				$URL_MAP = array(
    				"AXWUEOw-HG0" => "http://r12---sn-nx57yney.googlevideo.com/videoplayback?signature=672561295072DEA12AF599CEB91D9362DA1D5957.D9C10798350593DF8FE32BBCEF207325B0E327A3&pfa=20s&source=youtube&initcwndbps=1703750&ipbits=0&key=yt5&pbr=yes&id=o-ABA05PgFtBsH1-lOgDqjHhkpp3wAF348UJb1KlYH2PTP&fexp=900242%2C902033%2C904844%2C908200%2C914060%2C924640%2C927622%2C930666%2C931983%2C932404%2C934030%2C946012%2C947209%2C948205%2C952302%2C953731%2C953801&pm_type=static&ms=au&mt=1411198732&expire=1411220420&mv=m&upn=I39t3YM0_Oc&itag=43&ip=2601%3A8%3A8400%3A827%3A109%3A953e%3A63b4%3A632f&mm=31&ratebypass=yes&sver=3&sparams=id%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Cmm%2Cms%2Cmv%2Cpbr%2Cpfa%2Cpm_type%2Cratebypass%2Csource%2Cupn%2Cexpire",
    				"4oa5KVZtAnU" => "http://r4---sn-nx57yn76.googlevideo.com/videoplayback?id=o-AJJ8yy3lfj1AwutasuPrPOXFpUcyCA5THQrY-Wph3I3e&source=youtube&mv=m&sver=3&ratebypass=yes&sparams=id%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Cmm%2Cms%2Cmv%2Cratebypass%2Csource%2Cupn%2Cexpire&signature=AD1D27D4224EC4649CA5B77F50F44E8D5899872E.416F1B94204E0DC91F33C264F55F82265A1BAFC0&ip=2601%3A8%3A8400%3A827%3A109%3A953e%3A63b4%3A632f&key=yt5&upn=rhotFzOpqlM&fexp=907258%2C912105%2C927622%2C930666%2C931330%2C931983%2C932404%2C934030%2C942462%2C946013%2C947209%2C951809%2C952302%2C952701%2C953801&ms=au&itag=43&mt=1411200487&expire=1411222229&mm=31&ipbits=0&initcwndbps=1762500",
    				"17vC8qZILJE" => "http://r9---sn-nx57yn7k.googlevideo.com/videoplayback?ipbits=0&sver=3&ip=73.35.203.234&sparams=gcr%2Cid%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Cmm%2Cms%2Cmv%2Cratebypass%2Csource%2Cupn%2Cexpire&fexp=927622%2C930666%2C931983%2C932404%2C934030%2C935027%2C946023%2C947209%2C948703%2C952302%2C953801&itag=43&id=o-AD1wbQ9rPcJHhQzeUI22ji7IUBm9pfbYVnyU5zknqlNW&mt=1411200616&expire=1411222281&key=yt5&mm=31&mv=m&upn=XiNoVOmvgfI&initcwndbps=1760000&ratebypass=yes&source=youtube&gcr=us&ms=au",
    			);
				$videoSrcURL = $URL_MAP[$youtubeId];
			}


			return array(
					'id' => $video->id,
					'title' => $video->title,
					'artist_name' => $video->artist_name,
					'video_src' => $video->video_src,
					'thumb' => $thumbnailURL,
					'score' => $score,
					'prod' => $video->prod,
					'dir' => $video->dir,
					'editor' => $video->editor,
					'description' => $video->description,
					'duration' => $video->duration,
					'v_count' => $video->v_count,
					'date' => $video->date
					);

		} else {
			return array();
		}		
	}
	
	private function getTVArrayFromObject($tv) {
		return array(
					'id' => $tv->id,
					'title' => $tv->title,
					'artist_name' => $tv->artist_name,
					'video_src' => $tv->video_src,
					'thumb' => base_url().'thumbs/'.$tv->thumb,
					'score' =>$this->Video_model->api_getScore($tv->id),
					'prod' => $tv->prod,
					'dir' => $tv->dir,
					'editor' => $tv->editor,
					'description' => $tv->description,
					'duration' => $tv->duration,
					'v_count' => $tv->v_count,
					'date' => $tv->date
					);
	}
	
	private function getChallengeArrayFromObject($chal) {
		return array(
					'id' => $chal->id,
					'cat' => $chal->cat,
					'title' => $chal->title,
					'description' => $chal->description,
					'video_src' => $chal->video_src,
					'thumb' => base_url().'thumbs/'.$chal->thumb,
					'active' => $chal->active,
					'v_count' => $chal->v_count,
					'duration' => $chal->duration,
					'date' => $chal->date
					);
	}
	
	private function getMixtapeArrayFromObject($mixtape) {
		return array(
					'id' => $mixtape->id,
					'title' => $mixtape->title,
					'desc' => $mixtape->desc,
					'artist' => $mixtape->artist,
					'front_cover' => $mixtape->front_cover,
					'front_cover_thumb' => base_url().$mixtape->front_cover_thumb,
					'v_count' => $mixtape->v_count,
					'added' => $mixtape->added
					);
	}

##########################################################################################	
#######################################   PUBLIC   #######################################
##########################################################################################	
	
	public function __construct() {
		parent::__construct();
		$this->load->model('Video_model');
		$this->load->model('Site_model');
		$this->load->model('Challenge_model');
		$this->load->model('Mixtape_model');
		$this->load->model('Search_model');
		// $this->load->model('Comments_model');
		// $this->load->model('Settings_model');
	}
		
	############## VIDEOS ##############
	
	public function videos_get() {  
        // inputs:  type => all, feat
        // outputs:	video array [on success]
        //			NOTFOUND_CODE [on failure]	
        
        $videos = array();

        switch ($this->getType()) {
        	case 'all':
        		{
        			foreach ($this->Video_model->getAllVideos() as $video)
					{
						$videoArray = $this->getVideoArrayFromObject($video);
						if (count($videoArray)) {
							$videos[] = $videoArray;
						} 
					}
        		}
        		break;
        		
        	default: // always do the default... show the featured videos
        	case 'feat':
        		{
        			$count = $this->Video_model->getFeatVideoTypeCount("two");
        			for ($slot = 1; $slot <= $count; ++$slot)
        			{
        				$videos[] = $this->Video_model->getFeatVideo_slot("two", $slot);
        			}
        			
        			$count = $this->Video_model->getFeatVideoTypeCount("five");
        			for ($slot = 1; $slot <= $count; ++$slot)
        			{
        				$videos[] = $this->Video_model->getFeatVideo_slot("five", $slot);
        			}
        		}
        		break;
        }
        
        if (count($videos))
			$this->response($videos, self::OK_CODE);
		else 
			$this->response(array('error' => 'Videos not found'), self::NOTFOUND_CODE);
    }
    
    public function video_get() {
    	// inputs: id => val, 
    	// outputs:
    	
    	if (! $this->getId() ) {
    		$this->response(array('error' => 'Video id not provided'), self::BADREQUEST_CODE);
    	} else {
    		$video = $this->Video_model->getVideo($this->getId());

    		if ($video)
    			$this->response($this->getVideoArrayFromObject($video[0]), self::OK_CODE);
    		else
    			$this->response(array('error' => 'Video not found!'), self::NOTFOUND_CODE);
    	}
    }
    
    public function video_post() {
    	// inputs: id => val, 
    	// outputs:
    	
    	// consume the input as json
    	$json = stripslashes(file_get_contents('php://input'));
  		$json = json_decode($json);
   		
    	if (! $json ) {
    		$this->response(array('error' => 'Video id not provided'), self::BADREQUEST_CODE);
    	} else {
    		$affectedRows = $this->Video_model->addview($json->id);
    		if ($affectedRows)
    			$this->response(array('status' => 'Success'), self::OK_CODE);
    		else
    			$this->response(array('status' => 'Failed'), self::BADREQUEST_CODE);
    	}
    }
    
	############## TVS ##############
    
    public function tvs_get() {
    	// inputs: 	type => feat, all 
    	//			cat => variety, interviews, shortfilms, reality, documentary
    	// outputs:

		// type and category cannot both be available. return error if so.
		if ($this->getType() && $this->getCat()) {
			$this->response(array('error' => 'TV type and category are mutually exclusive.'), self::BADREQUEST_CODE);
		}
			
		$tvs = array();
		if ($this->getCat()) {        
			foreach ($this->Video_model->getGbaamTVByCat($this->getCat()) as $tv) {
				$tvs[] = $this->getTVArrayFromObject($tv);
			}
		} else {
			switch ($this->getType()) {
				case 'all':
					{
						foreach ($this->Video_model->getGbaamTVByCat() as $tv) {
							$tvs[] = $this->getTVArrayFromObject($tv);
						}
					}
					break;
					
				default: // always do the default.. show featured tvs
				case 'feat':
					{
						foreach ($this->Video_model->getFeatGbaamVids() as $tv) {
							$tvs[] = $this->getTVArrayFromObject($tv);
						}
					}
					break;
			}
		}
		        
        if (count($tvs)) {
			$this->response($tvs, self::OK_CODE);
        } else { 
			$this->response(array('error' => 'TVs not found'), self::NOTFOUND_CODE);
		}
    }
    
    public function tv_get() {
    	// inputs: id => val, 
    	// outputs:
    	
    	if (! $this->getId() ) {
    		$this->response(array('error' => 'TV id not provided'), self::BADREQUEST_CODE);
    	} else {
    		$tv = $this->Video_model->getTVVideo($this->getId());

    		if ($tv) {
    			$this->response($this->getTVArrayFromObject($tv[0]), self::OK_CODE);
    		} else {
    			$this->response(array('error' => 'TV not found!'), self::NOTFOUND_CODE);
    		}
    	}
    }
    
    public function tv_post() {
    	$this->response(array('error' => 'TV update not supported yet.'), self::UNAUTHORIZED_CODE);
    }
    
    ############## CHALLENGES ##############
    public function challenges_get() {
    	// type and category cannot both be available. return error if so.
		if ($this->getType() && $this->getCat()) {
			$this->response(array('error' => 'Challenge type and category are mutually exclusive.'), self::BADREQUEST_CODE);
		}


    	$challenges = array();
    	$response = array();
    	$returnCode =  self::NOTFOUND_CODE;
    	
    	if ($this->getCat()) {        
			foreach ($this->Challenge_model->getChallengeVideos($this->getCat()) as $chal) {
				$challenges[] = $this->getChallengeArrayFromObject($chal);
			}
		} else {
			switch ($this->getType()) {
				case 'cat':
					{
						$challenges = $this->Challenge_model->getChallengeCategories();
					}
					break;
					
				case 'leaders':
					{
						$response = array('error' => 'Challenge leaders not supported yet.');
						$returnCode = self::UNAUTHORIZED_CODE;
					}
					break;
					
				default:
				case 'all':
					{
						foreach ($this->Challenge_model->getChallengeVideos() as $chal) {
							$challenges[] = $this->getChallengeArrayFromObject($chal);
						}
					}
					break;
			}
        }
    	
    	if (count($challenges)) {
    		$this->response($challenges, self::OK_CODE);
    	} else if (count($response)) {
    		$this->response($response, $returnCode);
    	} else {
    		$this->response(array('error' => 'Challenges not found'), self::NOTFOUND_CODE);
    	}
    }
    
    public function challenge_get() {
	    // inputs: id => val, 
    	// outputs:

		if (! $this->getId() ) {
			$this->response(array('error' => 'Challenge id not provided'), self::BADREQUEST_CODE);
		} else {
			$chal = $this->Challenge_model->getChallengeVideo($this->getId());
			
			if ($chal) {
				$this->response($this->getChallengeArrayFromObject($chal[0]), self::OK_CODE);
			} else {
				$this->response(array('error' => 'Challenge not found!'), self::NOTFOUND_CODE);
			}
		}
	}
	
	public function challenge_post() {
		$this->response(array('error' => 'Challenge update not supported yet.'), self::UNAUTHORIZED_CODE);
	}

	############## MIXTAPES ##############
	public function mixtapes_get() {
		$mixtapes = array();
		switch ($this->getType()) {
        	case 'motw':
        		{
        			foreach ($this->Mixtape_model->getMotw() as $mixtape) {
        				$mixtapes[] = $this->getMixtapeArrayFromObject($mixtape);
        			}
        		}
        		break;
        		
        	case 'hot':
        		{
        			foreach ($this->Mixtape_model->getHotMixtapes() as $mixtape) {
        				$mixtapes[] = $this->getMixtapeArrayFromObject($mixtape);
        			}
        		}
        		break;
        	
        	case 'latest':
        		{
        			foreach ($this->Mixtape_model->getLatestMix() as $mixtape) {
        				$mixtapes[] = $this->getMixtapeArrayFromObject($mixtape);
        			}
        		}
        		break;
        		
        	default:
        	case 'all':
        		{
        			foreach ($this->Mixtape_model->getAllMixtapes() as $mixtape) {
        				$mixtapes[] = $this->getMixtapeArrayFromObject($mixtape);
        			}
        		}
        		break;
        }
        
        if (count($mixtapes)) {
        	$this->response($mixtapes, self::OK_CODE);
        } else {
        	$this->response(array('error' => 'Mixtapes not found'), self::NOTFOUND_CODE);
        }
	}
	
	public function mixtape_get($id) {
		if (! $this->getId() ) {
			$this->response(array('error' => 'Mixtape id not provided'), self::BADREQUEST_CODE);
		} else {
			$mixtape = $this->Mixtape_model->getMixtape($this->getId());
			
			if ($mixtape) {
				$this->response($this->getMixtapeArrayFromObject($mixtape[0]), self::OK_CODE);
			} else {
				$this->response(array('error' => 'Mixtape not found!'), self::NOTFOUND_CODE);
			}
		}

	}

	public function mixtape_post() {
		$this->response(array('error' => 'Mixtape update not supported yet.'), self::UNAUTHORIZED_CODE);
	}

	############## SEARCH ##############
	
	public function search_get() {
		$searches = array();
		
		if (! $this->getType() ) {
			$this->response(array('error' => 'Search type not provided'), self::BADREQUEST_CODE);
		}
		
		if ($this->getKey()) {
			switch ($this->getType()) {
				case 'video':
					{
						foreach ($this->Search_model->getSearch($this->getKey(), 'video') as $video) {
							$searches[] = $this->getVideoArrayFromObject($video);
						}
					}
					break;
				
			}
		}

		if (count($searches)) {
			$this->response($searches, self::OK_CODE);
		} else {
			$this->response(array('error' => 'Nothing found!'), self::NOTFOUND_CODE);
		}
	}
	
	############## COMMENTS ##############

	/*public function comments_get() {
		$this->response($this->Comments_model->get_comments(79), self::OK_CODE);
	}*/

	############## SETTINGS ##############
	public function setting_get() {
		$settingType = $this->getType();
		
		if ($settingType == "placeholder") {
			$placeholderURL = "http://gbaam.com/assets/img/gvid1280x960.gif";
			$this->response($placeholderURL, self::OK_CODE);
		}
	}
}
