<?php
/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */
class Ajax extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		
		// Cant access this straight from the browser! Only Ajax Requests!
		if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
            $this->isAjax = true;
        else 
            $this->isAjax = false;
        
        // if this is not an ajax request
        if(!$this->isAjax)
        {
            show_404();
            exit;
        }
	}

	public function load_gtv_videos($last_id, $page = '')
	{
		// only if last_id is available
		if($last_id != '')
		{
			// load the gtv videos
			$d['load_gtv_videos'] = $this->Video_model->ajax_load_gtv_videos($last_id,$page);

			// show the page/view
			$this->load->view('ajax/load_gtv_vids',$d);
		}
	}

	public function video_preference($type)
	{
		// set the domain depending on the environment.
		if($_SERVER['HTTP_HOST'] == 'localhost')
			$domain = '.localhost';
		else
			$domain = '.gbaam.com';
		
		$cookie = array(
		    'name'   => 'video_preference',
		    'value'  => $type,
		    'expire' => '86500',
		    'domain' => $domain,
		    'path'   => '/',
		    'prefix' => '',
		    'secure' => FALSE
		);

		$this->input->set_cookie($cookie);
	}

	/**
	 * THis is from a different application
	 * @return [type] [description]
	 */
	public function js_check_username()
	{
		
		$username = $_GET['username'];

		//get my id
		$session_id = $this->session->userdata('user_id');

		$sql = "SELECT * FROM users where username = '{$username}'";

		$query = $this->db->query($sql);
		$row = $query->row();

		//blank
		if($username == '')
		{
			echo 'empty';
		}
		else
		{
			// username is taken
			if($query->num_rows() == 1)
			{
				// if this is me then pass
				if($row->id == $session_id)
				{
					// 
					echo 'free';
				}
				// this is not mine
				else
				{
		            echo 'taken';
				}
			}
			// username is free
			else
			{
	            echo 'free';
			}
		}

	}

	public function js_remove_profilepic()
	{
		$user_id = loggedin_user_id();

		$data['profile_img'] = '';
		
		$this->db->where('id', $user_id);
		$this->db->update('users', $data);
	}

}