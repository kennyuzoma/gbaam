
<?php

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class Sb extends CI_Controller
{
	
	function __construct()
    {
		parent::__construct();
		$this->output->enable_profiler(TRUE);
	}

	function delete_user($user_id)
	{
		$this->db->query('DELETE FROM users where id = '.$user_id);
		$this->db->query('DELETE FROM users_settings where user_id = '.$user_id);
	}

	function move_dates()
	{
		$sql = "SELECT * FROM video";
		$query = $this->db->query($sql);

		foreach($query->result() as $q)
		{
			if($q->date_created == '')
			{
				$upd['date_created'] = $q->date. ' 00:00:00';
				$this->db->where('id',$q->id);
				$this->db->update('video',$upd);
			}
		}
	}
}