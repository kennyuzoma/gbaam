<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */
class Admin2 extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->model('Challenge_model');
		$this->load->model('Blog_model');

		$this->load->helper('inflector');
		
		if($this->session->userdata('admin_user_id') == '1')
		{
			$this->output->enable_profiler(TRUE);
			error_reporting(E_ALL);
			ini_set('display_errors', 1);
		}
	}

	public function index()
	{
		$session = $this->session->userdata('admin_logged_in');

		$d['title'] = 'Admin Login';

		if($session)
		{
			redirect('admin2/home');
		}
		else
		{
			if($this->input->post('submit_login'))
			{

				$this->form_validation->set_error_delimiters('<span style="z-index:100;float:left;width:100%;background:#FFD1D3;border:1px solid #BD7575;border-radius:5px;text-align:center;font-size:16px;padding:6px 0;margin-bottom:10px;font-weight:bold;">', '</span>');

				if($this->form_validation->run('admin_login') == FALSE )
				{
					$d['message'] = validation_errors();
					$this->load->view('admin_new/login',$d);
				}
				else
					$this->Admin_model->validate();
			}
			else
			{
				$this->load->view('admin_new/login',$d);
			}
		}
	}

	function _login_check()
	{
		if ($this->Admin_model->checklogin($this->input->post('username'),$this->input->post('password')) == FALSE)
		{
			$this->form_validation->set_message('_login_check', 'Incorrect Username or Password');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function _doesUserExist_username()
	{
		//Store in Site_model
		$username = $this->input->post('username');
		$user_id = $this->input->post('user_id');

		//if the provided email and password match a user then it will return false
		if ($this->Admin_model->doesUserExist_username($username,$user_id) === 'reserved')
		{
			$this->form_validation->set_message('_doesUserExist_username', 'This username is RESERVED. Please remove it from the reserved list and try again.');
			
			// return FALSE to trigger error message for validation.
			return FALSE; 
		}
		elseif ($this->Admin_model->doesUserExist_username($username,$user_id) == TRUE)
		{
			$this->form_validation->set_message('_doesUserExist_username', 'Username is not available.');
			
			// return FALSE to trigger error message for validation.
			return FALSE; 
		}
		else
		{
			return TRUE;
		}
	}

	public function _doesUserExist_email()
	{
		//Store in Site_model
		$email = $this->input->post('email');
		$user_id = $this->input->post('user_id');

		//if the provided email and password match a user then it will return false
		if ($this->Admin_model->doesUserExist_email($email,$user_id) == TRUE)
		{
			$this->form_validation->set_message('_doesUserExist_email', 'Email Address is not available.');
			
			// return false to trigger error message.
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}


	public function home()
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect('admin2/');

		$this->load->view('admin_new/index_home');
	}

	public function videolist($type)
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect('admin2/');

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'videos') == FALSE)
			redirect('admin2/');

		$d['video_type'] = $type;
		switch($type){
			case 'gbaamtv':
			$d['title'] = 'GbaamTV';
			$d['viewlink'] = 'gbaamtv/v/';
			break;
			case 'mv':
			$d['title'] = 'Music';
			$d['viewlink'] = 'video/';
			break;
			case 'challenge':
			$d['title'] = 'Challenge';
			$d['viewlink'] = 'challenge/view/';
			break;
		}

		$d['get_videos'] = $this->Admin_model->getAllVideos_Admin($type);

		$this->load->view('admin_new/video_list', $d);
	}

	public function videos($type, $slug, $video_id = '')
	{
		/*if(!isset($type) || !isset($slug))
			echo 'yeah';
			//redirect('admin2/');
*/
			
		if(!$this->session->userdata('admin_logged_in'))
			redirect('admin2/');

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'videos') == FALSE)
			redirect('admin2/');

		$d['video_type'] = $type;

		//Set titles based on type
		switch($type){
			case 'gbaamtv':
				$d['title'] = 'GbaamTV';
				$d['viewlink'] = 'gbaamtv/v/';
				$validation_rule = 'admin_gbaamtv';
				break;

			case 'mv':
				$d['title'] = 'Music';
				$d['viewlink'] = 'video/';
				$validation_rule = 'admin_video';
				break;

			case 'challenge':
				$d['title'] = 'Challenge';
				$d['viewlink'] = 'challenge/view/';
				$validation_rule = 'admin_challenge';
				break;
		}

		if($slug == 'new')
		{
			if($this->input->post('submit'))
			{
				
				$d['validation'] = $this->form_validation->run('admin_video');

				if($d['validation'] == TRUE )
				{
					// Gather all the information
					$title = $this->input->post('videotitle');
					$artistname = $this->input->post('artistname');
					$prod = $this->input->post('prod');
					$dir = $this->input->post('dir');
					$desc = $this->input->post('description');
					$tags = $this->input->post('tags');
					$videosrc = $this->input->post('video_src');
					$curr_thumb = $this->input->post('curr_thumb');
					$originalDate = $this->input->post('date');
					

					if($originalDate == '')
						$originalDate = date('Y-m-d');

					$date = date("Y-m-d", strtotime($originalDate));

					// Other default values
					$gbaamtv = 0;
					$gtv_cat = 0;
					$chall = 0;
					$chall_cat = 0;

					// If GbaamTV
					if($type == 'gbaamtv')
					{
						$gbaamtv = 1;
						$gtv_cat = $this->input->post('tv_cat');
					}
					

					if($type == 'challenge')
					{
						$chall = 1;
						$chall_cat = $this->input->post('chall_cat');
					}

					// Insert the video
					$ins_id = $this->Admin_model->insertVideo($title,$artistname,$prod,$dir,$desc,$videosrc,$date,$gbaamtv,$gtv_cat,$chall,$chall_cat,$tags);

					// Add a thumbnail if uploaded
					$this->Add_video_thumbnail($ins_id,$curr_thumb);

					redirect('admin2/videolist/'.$type.'/success');
				}
				else
				{
					$d['message'] = validation_errors();
				}
			}

			$this->load->view('admin_new/video_new_vid', $d);
		}

		if($slug == 'edit')
		{
			if($video_id == '')
				redirect('admin2/videos/mv/all');

			$d['get_video'] = $this->Video_model->getVideo( $video_id );

			if($this->input->post('submit'))
			{
				$d['s_message'] = 'Video Saved Successfully';
				
				$d['validation'] = $this->form_validation->run('admin_video');

				if($d['validation'] == TRUE )
				{

					// Gather all the information
					$title = $this->input->post('videotitle');
					$artistname = $this->input->post('artistname');
					$prod = $this->input->post('prod');
					$dir = $this->input->post('dir');
					$desc = $this->input->post('description');
					$tags = $this->input->post('tags');
					$videosrc = $this->input->post('video_src');
					$curr_thumb = $this->input->post('curr_thumb');
					$originalDate = $this->input->post('date');
					$status = $this->input->post('status');

					if($originalDate == '')
						$originalDate = date('Y-m-d');

					$date = date("Y-m-d", strtotime($originalDate));
					//echo $originalDate;

					// Other default values
					$gbaamtv = 0;
					$gtv_cat = 0;
					$chall = 0;
					$chall_cat = 0;

					// If GbaamTV
					if($type == 'gbaamtv')
					{
						$gbaamtv = 1;
						$gtv_cat = $this->input->post('tv_cat');
					}
					

					if($type == 'challenge')
					{
						$chall = 1;
						$chall_cat = $this->input->post('chall_cat');
					}

					// Insert the video
					
					$this->Admin_model->updateVideo($video_id, $title,$artistname,$prod,$dir,$desc,$videosrc,$curr_thumb,$date,$gbaamtv,$gtv_cat,$chall,$chall_cat,$tags,$status);

					// Add a thumbnail if uploaded
					$this->Add_video_thumbnail($video_id,$curr_thumb);

					// redirect to same page to refresh form
					redirect('admin2/videos/'.$type.'/edit/'. $video_id.'/success');
				}
				else
				{
					$d['message'] = validation_errors();
				}
			}
			$this->load->view('admin_new/video_edit_vid', $d);

			
		}

		if($slug == 'delete')
		{
			if($this->input->is_ajax_request())
				$this->Admin_model->deleteVideo($video_id);
		}


		if($slug == 'five_feat')
		{
			$d['get_challenges'] = $this->Challenge_model->challenge_list();
			$this->load->view('admin_new/chall_list', $d);
		}

	}

	public function challenge($slug, $challenge_id = '')
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect('admin2/');

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'challenge') == FALSE)
			redirect('admin2/');
		

		if($slug == 'new')
		{
			$d['title'] = 'New Challenge';
			$this->load->view('admin_new/chall_new', $d);

			if($this->input->post('submit'))
			{
				$status = $this->input->post('question1');
				$name = $this->input->post('name');
				$display_name = $this->input->post('display_name');

				$this->Challenge_model->newChallenge($status,$name, $display_name);

				redirect('admin2/challenge/list/success');
			}
		}

		if($slug == 'edit')
		{
			$d['title'] = 'Edit Challenge';

			$d['get_challenge'] = $this->Challenge_model->challenge_info($challenge_id);
			$this->load->view('admin_new/chall_edit', $d);
			
			if($this->input->post('submit'))
			{
				$status = $this->input->post('question1');
				$name = $this->input->post('name');
				$display_name = $this->input->post('display_name');

				$this->Challenge_model->editChallenge($challenge_id, $status,$name,$display_name);
				redirect('admin2/challenge/edit/'.$challenge_id.'/success');
			}
		}

		if($slug == 'delete')
		{
			if($this->input->is_ajax_request())
				$this->Challenge_model->deleteChallenge($challenge_id);
		}

		if($slug == 'list')
		{
			$d['title'] = 'Challenge List';
			$d['get_challenges'] = $this->Challenge_model->challenge_list();
			$this->load->view('admin_new/chall_list', $d);
		}


	}

	public function review($slug, $video_id = '')
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect('admin2/');
		
		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'videos') == FALSE)
			redirect('admin2/');

		if($slug == 'edit')
		{
			$d['g'] = $this->Admin_model->getPendingVideo($video_id);
			$d['title'] = 'Review Video';
			

			if($this->input->post('submit_approve'))
			{
				$d['validation'] = $this->form_validation->run('admin_video');

				if($d['validation'] == FALSE )
				{
					$d['message'] = validation_errors();
				}
				else
				{
					// Gather all the information
					$title = $this->input->post('videotitle');
					$artistname = $this->input->post('artistname');
					$prod = $this->input->post('prod');
					$dir = $this->input->post('dir');
					$desc = $this->input->post('description');
					$tags = $this->input->post('tags');
					$videosrc = $this->input->post('video_src');
					$curr_thumb = $this->input->post('curr_thumb');
					$originalDate = $this->input->post('date');

					if($originalDate == '')
						$originalDate = date('Y-m-d');

					$date = date("Y-m-d", strtotime($originalDate));

					// Other default values
					$gbaamtv = 0;
					$gtv_cat = 0;
					$chall = 0;
					$chall_cat = 0;

					// If GbaamTV
					if($d['g']->gtv == '1')
					{
						$gbaamtv = 1;
						$gtv_cat = $this->input->post('tv_cat');
					}
					

					if($d['g']->chall == '1')
					{
						$chall = 1;
						$chall_cat = $this->input->post('cat');
					}

					// Insert the video
					$ins_id = $this->Admin_model->insertVideo($title,$artistname,$prod,$dir,$desc,$videosrc,$date,$gbaamtv,$gtv_cat,$chall,$chall_cat,$tags);

					// Add a thumbnail if uploaded
					$this->Add_video_thumbnail($ins_id,$curr_thumb);

					$this->Admin_model->processpending('approved',$video_id, $ins_id);

					redirect('admin2/review/edit/'.$video_id.'/success');
				}
			}
			if($this->input->post('submit_deny'))
			{
				

				$this->Admin_model->processpending('deny',$video_id);

				redirect('admin2/review/list/');
			}

			$this->load->view('admin_new/review_edit', $d);
		}

		if($slug == 'delete')
		{
			if($this->input->is_ajax_request())
				$this->Admin_model->deletePending($video_id);
		}

		if($slug == 'list')
		{
			$d['title'] = 'Review List';
			$d['get_pending'] = $this->Admin_model->getPendingVideos();
			$this->load->view('admin_new/review_list', $d);
		}


	}


	public function mixtape($slug, $mixtape_id = '')
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect('admin2/');

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'mixtapes') == FALSE)
			redirect('admin2/');
		
		// Set the mixtape id from the 2nd segment
		$d['mixtape_id'] = $mixtape_id;

		// Get all mixtape info if a mixtape_id is avail
		if($mixtape_id != '')
		{
			$mixtape = $this->Mixtape_model->getMixtape_new_admin($mixtape_id);
			$d['mixtape'] = $mixtape;

			$mixtape_tracks = $this->Mixtape_model->getMixtapeTracks($mixtape_id);
			$d['tracks_count'] = $mixtape_tracks['count'];
			$d['mixtape_tracks'] = $mixtape_tracks['result'];
		}

		// New Mixtape
		if($slug == 'new')
		{
			$d['title'] = 'New Mixtape';

			$date = date("Y-m-d H:i:s", strtotime($this->input->post('date')));

			if($this->input->post('submit'))
			{
				$d['validation'] = $this->form_validation->run('admin_mixtape');

				if($d['validation'] == FALSE )
				{
					$d['message'] = validation_errors();
				}
				else
				{
					$mixtape_id = $this->Admin_model->insertMixtape();

					// this is after because i believe it clears the inserted id.... idk
					$this->session->set_flashdata('new_mixtape','1');

					$this->upload_covers($mixtape_id);

					redirect('admin2/mixtape/addtracks/'.$mixtape_id);
				}
			}

			$this->load->view('admin_new/mixtape_new', $d);
		}

		//Adding tracks to mixtape
		if($slug == 'addtracks')
		{
			// pass the variable the new flash data variable if available
			if($this->session->flashdata('new_mixtape') == '1')
				$this->session->set_flashdata('new_mixtape','1');

			$d['title'] = 'Add Tracks to mixtape - "<b>'.$mixtape->title.'</b>"';
			$this->load->view('admin_new/mixtape_add_tracks', $d);
		}

		//Editing the mixtape
		if($slug == 'edit')
		{
			if($this->Admin_model->is_this_my_post('mixtapes',$mixtape_id) == FALSE)
				redirect('admin2');

			//if($this->uri->segment(5) == 'success')
			//	unset($_SESSION['flash:new:new_mixtape']);

			// pass the variable the new flash data variable if available
			if($this->session->flashdata('new_mixtape') == '1')
				$this->session->set_flashdata('new_mixtape','1');


			if($mixtape_id == '')
			{
				redirect('admin2/mixtape/list');
			}


			$d['title'] = 'Edit mixtape - "<b>'.$mixtape->title.'</b>"';

			if($this->input->post('submit'))
			{		
				$d['validation'] = $this->form_validation->run('admin_mixtape');

				if($d['validation'] == FALSE )
				{
					$d['message'] = validation_errors();
				}
				else
				{		
					if($this->uri->segment(5) == 'new')
						$this->Admin_model->updateMixtape($mixtape_id,'new');
					else
						$this->Admin_model->updateMixtape($mixtape_id);

					$this->upload_covers($mixtape_id);

					$track_num = $this->input->post('track_num');
					$track_name = $this->input->post('track_name');
					$mixtpe_id = $this->input->post('mixtape_id');

					if($track_num != '')
					{
						foreach($track_num as $k=>$v)
						{
							$sql = "UPDATE mixtape_tracks SET track_num='{$v}' WHERE id='{$k}'";
							$this->db->query($sql);
						}
					}

					if($track_name != '')
					{
						foreach($track_name as $k=>$v)
						{
							$sql = "UPDATE mixtape_tracks SET title='{$v}' WHERE id='{$k}'";
							$this->db->query($sql);
						}
					}
					
					//unset($_SESSION['flash:new:new_mixtape']);

					if($this->uri->segment(5) == 'new')
						$this->create_zip_shell($mixtape_id);

					redirect('admin2/mixtape/edit/'.$mixtape_id.'/success');
				}
				
			}


			$this->load->view('admin_new/mixtape_edit', $d);

			
		}


		// List all the mixtapes
		if($slug == 'list')
		{
			$d['get_mixtapes'] = $this->Mixtape_model->getAllMixtapes_admin();
			$d['title'] = 'All Mixtapes';
			$this->load->view('admin_new/mixtape_list', $d);
		}

		if($slug == 'motw')
		{
			$d['get_motw'] = $this->Mixtape_model->getMotw();
			$d['title'] = 'Motw';
			$this->load->view('admin_new/mixtape_motw', $d);
		}

		if($slug == 'select_motw')
		{
			$d['get_mixtapes'] = $this->Mixtape_model->getAllMixtapes();
			$d['title'] = 'All Mixtapes';
			$this->load->view('admin_new/mixtape_list', $d);
		}

		if($slug == 'delete')
		{
			if($this->input->is_ajax_request())
				$this->Mixtape_model->deleteMixtape($mixtape_id);
		}

		if($slug == 'remove_motw')
		{
			$this->Mixtape_model->deleteMOTW($mixtape_id);
		}

		if($slug == 'make_motw')
		{
			$this->Mixtape_model->AddToSlot('1','motw',$mixtape_id);
		}

		if($slug == 'updatetracksorder')
		{
			$this->Admin_model->updateTracksOrder();
		}
	}

	public function deleteMixtapeTrack($track_id)
	{
		$this->Admin_model->deleteMixtapeTrack($track_id);
	}

	public function hotmixtapes($slug, $slot='', $mixtape_id = '')
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect('admin2/');

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'mixtapes') == FALSE)
			redirect('admin2/');
		
		// Set the mixtape id from the 2nd segment
		$d['mixtape_id'] = $mixtape_id;

		// Get all mixtape info if a mixtape_id is avail
		if($mixtape_id != '')
		{
			$mixtape = $this->Mixtape_model->getMixtape_new($mixtape_id);
			$d['mixtape'] = $mixtape;

			$mixtape_tracks = $this->Mixtape_model->getMixtapeTracks($mixtape_id);
			$d['tracks_count'] = $mixtape_tracks['count'];
			$d['mixtape_tracks'] = $mixtape_tracks['result'];
		}

		//Editing the mixtape
		if($slug == 'edit')
		{

			if($mixtape_id == '')
			{
				redirect('admin2/mixtape/list');
			}


			$d['title'] = 'Edit mixtape - "<b>'.$mixtape->title.'</b>"';

			

			if($this->input->post('submit'))
			{

				
				$this->Admin_model->updateMixtape($mixtape_id);

				$this->upload_covers($mixtape_id);

				$track_num = $this->input->post('track_num');
				$track_name = $this->input->post('track_name');
				$mixtpe_id = $this->input->post('mixtape_id');

				foreach($track_num as $k=>$v)
				{
					$sql = "UPDATE mixtape_tracks SET track_num='{$v}' WHERE id='{$k}'";
					$this->db->query($sql);
				}

				foreach($track_name as $k=>$v)
				{
					$sql = "UPDATE mixtape_tracks SET title='{$v}' WHERE id='{$k}'";
					$this->db->query($sql);
				}

				redirect('admin2/mixtape/edit/'.$mixtape_id.'/success');
				
			}


			$this->load->view('admin_new/hotmixtape_edit', $d);

		}


		// List all the mixtapes
		if($slug == 'list')
		{
			$d['hmixtape1'] = $this->Mixtape_model->getHotMixtape(1);
			$d['hmixtape2'] = $this->Mixtape_model->getHotMixtape(2);
			$d['hmixtape3'] = $this->Mixtape_model->getHotMixtape(3);

			$d['title'] = 'All Mixtapes';
			$this->load->view('admin_new/hotmixtape_list', $d);
		}

		if($slug == 'add')
		{
			$d['title'] = 'Add to slot $slot';
			$d['slot'] = $slot;
			$d['get_mixtapes'] = $this->Mixtape_model->getAllMixtapes();
			$this->load->view('admin_new/hotmixtape_add', $d);
		}

		if($slug == 'add_conf')
		{
			
			$this->Mixtape_model->AddToSlot($slot,'hot_mixtape',$mixtape_id );
			redirect('admin2/hotmixtapes/list/success');
		}

		if($slug == 'delete')
		{
			if($this->input->is_ajax_request())
				$this->Mixtape_model->deleteMixtape($mixtape_id);
		}

		if($slug == 'remove_motw')
		{
			$this->Mixtape_model->remove_motw($mixtape_id);
		}
	}

	public function songs($slug, $song_id = '')
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect('admin2/');

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'songs') == FALSE)
			redirect('admin2/');
		
		// Set the song id from the 2nd segment
		$d['song_id'] = $song_id;

		// Get all song info if a $song_id is avail
		if($song_id != '')
		{
			$song = $this->Songs_model->getSong_new_admin($song_id);
			$d['song'] = $song;
		}

		// New song
		if($slug == 'new')
		{
			$d['title'] = 'New Song';
			
			$date = date("Y-m-d H:i:s", strtotime($this->input->post('date')));

			if($this->input->post('submit'))
			{
				$d['validation'] = $this->form_validation->run('admin_song');

				if($d['validation'] == FALSE )
				{
					$d['message'] = validation_errors();
				}
				else
				{	
					$song_id = $this->Admin_model->insertSong();

					$this->upload_covers_songs($song_id);
					$this->upload_song($song_id);

					redirect('admin2/songs/list');
				}
			}

			$this->load->view('admin_new/song_new', $d);
		}
		

		//Editing the song
		if($slug == 'edit')
		{
			if($this->Admin_model->is_this_my_post('songs',$song_id) == FALSE)
				redirect('admin2');

			if($song_id == '')
			{
				redirect('admin2/songs/list');
			}

			$d['title'] = 'Edit Song - "<b>'.$song->title.'</b>"';


			if($this->input->post('submit'))
			{	
				$d['validation'] = $this->form_validation->run('admin_song');

				if($d['validation'] == FALSE )
				{
					$d['message'] = validation_errors();
				}
				else
				{				
					$this->Admin_model->updateSong($song_id);

					$this->upload_covers_songs($song_id);
					$this->upload_song($song_id,'edit');

					redirect('admin2/songs/edit/'.$song_id.'/success');
				}
				
			}

			$this->load->view('admin_new/song_edit', $d);

		}

		// List all the mixtapes
		if($slug == 'list')
		{
			$d['get_songs'] = $this->Songs_model->getAllSongs_admin();
			$d['title'] = 'All Songs';
			$this->load->view('admin_new/song_list', $d);
		}

		// delete songs
		if($slug == 'delete')
		{
			if($this->input->is_ajax_request())
				$this->Songs_model->deleteSong($song_id);
		}

		if($slug == 'sotw')
		{
			$d['get_sotw'] = $this->Songs_model->getSotw();
			$d['title'] = 'Sotw';
			$this->load->view('admin_new/song_sotw', $d);
		}

		if($slug == 'select_sotw')
		{
			$d['get_songs'] = $this->Songs_model->getAllSongs();
			$d['title'] = 'All Songs';
			$this->load->view('admin_new/song_list', $d);
		}

		if($slug == 'make_sotw')
		{
			$this->Songs_model->AddToSlot('1','sotw',$song_id);
		}

		if($slug == 'remove_sotw')
		{
			$this->Songs_model->remove_sotw($song_id);
		}


	}

	public function upload_covers_songs($song_id)
	{

		$type = 'song';
		$song = $this->Songs_model->getSong_new_admin($song_id);

		
		$config3['upload_path'] = "./".$this->config->item('uploaded_images_folder').$type;
		$config3['allowed_types'] = 'jpeg|jpg|png|gif';
		$config3['max_size']	= '10000';
		$config3['max_width']  = '10240';
		$config3['max_height']  = '7680';

		//$this->load->library('upload', $config);
		$this->upload->initialize($config3);

		if ( ! $this->upload->do_upload('ft_cover'))
		{
			// Only Uncomment for error checking
			//$error = $this->upload->display_errors();
			//var_dump($this->upload->data());
			//var_dump($error); 
		}
		else
		{		
			// Get photo data
			$photodata = array('upload_data' => $this->upload->data());

			//delete the old song cover
			$this->Photo_model->delete_image('songs',$song->front_cover);
			

			//send to get the image manipulated
			$photo_id = $this->Photo_model->actual_upload($type,$song_id,$photodata);

			$data['front_cover'] = $photo_id;

			$this->db->where('id' , $song_id);
			$this->db->update('songs',$data);


		}

	}

	public function upload_song($song_id,$action = '')
	{
		if($action == 'edit')
		{
			// rename song just incase
			$sql = "SELECT * FROM songs where id = ".$song_id;
			$query = $this->db->query($sql);
			$row = $query->row();

			$song_file = "./".$this->config->item('songs_dir').$row->song_new_filename;
			
			$newname = $row->artist.' - '.$row->title.$row->song_ext;
			rename($song_file,"./".$this->config->item('songs_dir').$newname);

			$data['song_new_filename'] = $newname;
			$this->db->where('id', $song_id);
			$this->db->update('songs', $data); 

		}

		$config2['upload_path'] = "./".$this->config->item('songs_dir'); 
		$config2['allowed_types'] = 'mp3|wav|ogg';
		$config2['max_size'] = '0';
		//$this->load->library('upload', $config2);
		
		$this->upload->initialize($config2);


		if ( ! $this->upload->do_upload('mp3_file'))
		{
			// Only Uncomment for error checking
			$error = $this->upload->display_errors();
			//var_dump($this->upload->data());
			
			$d['error'] = $error; 
			$d['status'] = 'fail';

			//print_r($d['error']);
		}
		else
		{
			// search for old song and delete it
			$sql = "SELECT * FROM songs where id = ".$song_id;
			$query = $this->db->query($sql);
			$row = $query->row();

			if($row->song_new_filename != '')
			{
				$song_file = "./".$this->config->item('songs_dir').$row->song_new_filename;
				if(file_exists($song_file))
					unlink($song_file);
			}
			

			// Get upload data
			$data = $this->upload->data('mp3_file');
			//print_r($data);

			$song_hash = md5($data['file_name'].date('Y-m-d H:i:s'));
			//$newname = $config['upload_path'].'/'.$img_hash.'.mp3';
			$newname = $row->artist.' - '.$row->title.$data['file_ext'];
			rename($data['full_path'],$config2['upload_path'].'/'.$newname);

			$data = array(
				'song_original_filename' => $data['file_name'],
				'song_new_filename' => $newname,
				'song_hash' => $song_hash,
				'song_ext' => $data['file_ext']
			);

			$this->db->where('id',$song_id);
			$this->db->update('songs',$data);

			$d['status'] = 'success';
		} 

		//return $d;
	} 

	public function featvideos($slug, $type, $slot='',$video_id = '')
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect('admin2/');

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'videos') == FALSE)
			redirect('admin2/');
		
		$d['type'] = $type;

		if($type == 'five')
		{
			$d['title'] = '5 Featured Videos';
			$d['limit'] = 5;
		}
		
		elseif($type == 'two')
		{
			$d['title'] = 'Top Two';
			$d['limit'] = 2;
		}


		// List all the mixtapes
		if($slug == 'list')
		{
			$this->load->view('admin_new/featvideos_list', $d);
		}

		if($slug == 'add')
		{
			$d['title'] = '5 Featured Videos Add';

			$d['slot'] = $slot;
			$d['get_videos'] = $this->Admin_model->getAllVideos_Admin('mv');
			$this->load->view('admin_new/featvideos_add', $d);
		}

		if($slug == 'addconf')
		{
			$this->Video_model->AddToSlot($type, $slot, $video_id);
			redirect('admin2/featvideos/list/'.$type.'/success');
		}

	}

	public function gtvfeatvideos($slug, $type = '', $slot='',$video_id = '')
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect('admin2/');

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'videos') == FALSE)
			redirect('admin2/');
		
		$d['type'] = $type;
		$d['limit'] = 4;

		$d['title'] = 'Featured Videos';

		if($type == '')
		{
			$d['title'] = 'Select GbaamTV Category';
			$d['gtv_cats'] = $this->Video_model->get_gtv_cat();
			$this->load->view('admin_new/gtv_catselect', $d);
		}
		else
		{
			// List all the mixtapes
			if($slug == 'list')
			{
				$this->load->view('admin_new/featvideos_list', $d);
			}

			if($slug == 'add')
			{
				$d['title'] = 'Featured Videos';

				$d['slot'] = $slot;
				//$d['get_videos_count'] = $this->Video_model->getGbaamTVByCat($type);
				
				if($type == 'gtv_main')
					$d['get_videos'] = $this->Admin_model->getAllVideos_Admin('gbaamtv');
				else
					$d['get_videos'] = $this->Video_model->getGbaamTVByCat($type);

				$this->load->view('admin_new/featvideos_add', $d);
			}

			if($slug == 'addconf')
			{
				$this->Video_model->AddToSlot($type, $slot, $video_id);
				redirect('admin2/gtvfeatvideos/list/'.$type.'/success');
			}

		}

		
	}

	public function landingvideos($slug, $type, $video_id = '')
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect('admin2/');

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'videos') == FALSE)
			redirect('admin2/');
		
		$d['type'] = $type;
		$d['get_videos'] = $this->Admin_model->getAllVideos_Admin('mv');

		if($type == 'gbaamtv')
		{
			$d['title'] = 'GbaamTV Landing Video';
			$d['feat_type'] = 'landing_gtv';
			$d['get_videos'] = $this->Admin_model->getAllVideos_Admin('gbaamtv');
		}
		
		elseif($type == 'challenge')
		{
			$d['title'] = 'challenge Landing Video';
			$d['feat_type'] = 'landing_chall';
			$d['get_videos'] = $this->Admin_model->getAllVideos_Admin('challenge');

		}

		$slot = $d['feat_type'];

		// List all the mixtapes
		if($slug == 'list')
		{
			$this->load->view('admin_new/landingvideos_list', $d);
		}

		if($slug == 'add')
		{
			$d['title'] = $d['type'] . ' Videos Add';
			
			$this->load->view('admin_new/landingvideos_add', $d);
		}

		if($slug == 'addconf')
		{
			$this->Video_model->AddToSlot($slot,'1',$video_id );
			redirect('admin2/landingvideos/list/'.$type.'/success');
		}

	}


	public function settings($slug)
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect('admin2/');

		// if admin type == 1 or 2
		
		$d['title'] = 'Settings';
		$this->load->view('admin_new/settings_home', $d);

	}

	public function slides($slug,$id = '')
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect('admin2/');

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'slides') == FALSE)
			redirect('admin2/');
		
		if($slug == 'new')
		{
			$d['title'] = 'New Slide';
			
			if($this->input->post('submit'))
			{
				$d['validation'] = $this->form_validation->run('admin_slide');

				if($d['validation'] == FALSE )
				{
					$d['message'] = validation_errors();
				}
				else
				{
					// insert the data
					$link = $this->input->post('link');
					$alt_text = $this->input->post('alt_text');

					$this->Admin_model->insertSlide($link,$alt_text);

					//get the inserted id
					$ins_id = $this->db->insert_id();

					// upload the photo
					$config['upload_path'] = "./data/homeimg";
					$config['allowed_types'] = '*';
					$config['max_size'] = 0;
					
					$this->upload->initialize($config);

					if ( ! $this->upload->do_upload('lg_img'))
					{
						/*$error = array('error' => $this->upload->display_errors());
						echo print_r($error);*/
					}
					else
					{
						
						$data = $this->upload->data();

						$img_hash = md5($data['file_name'].date('Y-m-d H:i:s'));
						$newname = $config['upload_path'].'/'.$img_hash.'.jpg';
						rename($data['full_path'],$newname);

						$ins_data = array(
							'image' => 'data/homeimg/'.$img_hash.'.jpg',
							'date_created' => date('Y-m-d H:i:s')
							);

						$this->db->where('id' , $ins_id);
						$this->db->update('slides',$ins_data);

						
					}

					redirect('admin2/slides/list/success');
				}

			}

			$this->load->view('admin_new/slides_new', $d);

		}

		if($slug == 'edit')
		{
			$d['title'] = 'Edit Slide';

			$d['get_slide'] = $this->Site_model->getSlide($id);
			

			if($this->input->post('submit'))
			{
				$d['validation'] = $this->form_validation->run('admin_slide');

				if($d['validation'] == FALSE )
				{
					$d['message'] = validation_errors();
				}
				else
				{
					// insert the data
					$link = $this->input->post('link');
					$alt_text = $this->input->post('alt_text');
					$status = $this->input->post('status');

					$this->Admin_model->updateSlide($id,$link,$alt_text,$status);


					// upload the photo
					$config['upload_path'] = "./data/homeimg";
					$config['allowed_types'] = '*';
					$config['max_size'] = 0;
					
					$this->upload->initialize($config);

					if ( ! $this->upload->do_upload('lg_img'))
					{
						/*$error = array('error' => $this->upload->display_errors());
						echo print_r($error);*/
					}
					else
					{
						
						$data = $this->upload->data();

						$img_hash = md5($data['file_name'].date('Y-m-d H:i:s'));
						$newname = $config['upload_path'].'/'.$img_hash.'.jpg';
						rename($data['full_path'],$newname);

						$ins_data = array(
							'image' => 'data/homeimg/'.$img_hash.'.jpg'
							);

						$this->db->where('id' , $id);
						$this->db->update('slides',$ins_data);

						
					}
					redirect('admin2/slides/list/success');
				}
			}

			$this->load->view('admin_new/slides_edit', $d);
		}

		if($slug == 'delete')
		{
			if($this->input->is_ajax_request())
				$this->Admin_model->deleteSlide($id);
		}

		if($slug == 'updateorder')
		{
			$this->Admin_model->updateSlideOrder();
		}

		if($slug == 'list')
		{
			$d['title'] = 'All Slides';
			$d['get_slides'] = $this->Site_model->getSlides_admin();
			$this->load->view('admin_new/slides_list', $d);
		}
	}



	public function users($slug,$id = '')
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect('admin2/');

		$admin_type = $this->session->userdata('admin_type');

		if($slug == 'new')
		{
			if(($admin_type != '1') && ($admin_type != '2'))
				redirect('admin2/');

			$d['title'] = 'New User';
			
			if($this->input->post('submit'))
			{
				$post_admin_type = $this->input->post('admin_type');
				$email = $this->input->post('email');
				$username = $this->input->post('username');
				$password1 = $this->input->post('password');
				$password2 = $this->input->post('password2');

				$this->form_validation->set_rules('firstname','First Name','required');
				$this->form_validation->set_rules('admin_type', '', '');

				if(($post_admin_type == '1' || $post_admin_type == '2'))
				{
					$this->form_validation->set_rules('username','Username','required|callback__doesUserExist_username');
					$this->form_validation->set_rules('email','Email','required|callback__doesUserExist_email');
					$this->form_validation->set_rules('password', 'Password', 'required|matches[password2]');
					$this->form_validation->set_rules('password2', 'Password Confirmation', 'required');

				}
				else
				{
					// if these things were typed
					if($password1 != '' || $email != '' || $username != '')
					{
						$this->form_validation->set_rules('username','Username','required|callback__doesUserExist_username');
						$this->form_validation->set_rules('email','Email','required|callback__doesUserExist_email');
						$this->form_validation->set_rules('password', 'Password', 'required|matches[password2]');
						$this->form_validation->set_rules('password2', 'Password Confirmation', 'required');
					}
				}

				$d['validation'] = $this->form_validation->run();

				if($d['validation'] == TRUE )
				{
					// insert the data
					$this->Admin_model->insert_user();

					$this->upload_profilepic($this->db->insert_id());

					redirect('admin2/users/list/success');
				}
				else
				{
					$d['message'] = validation_errors();
				}


			}

			$this->load->view('admin_new/users_new', $d);

		}

		if($slug == 'edit')
		{
			if(($admin_type != '1') && ($admin_type != '2'))
				if($id != $this->session->userdata('admin_user_id'))
					redirect('admin2/');

			$d['title'] = 'Edit User';

			$d['g'] = $this->Admin_model->get_user($id);
			$d['user_id'] = $id;
			$user_id = $id;
			

			if($this->input->post('submit'))
			{
				$email = $this->input->post('email');
				$username = $this->input->post('username');
				$password1 = $this->input->post('password');
				$password2 = $this->input->post('password2');

				$sql = "SELECT * FROM admin where id = ".$user_id;
				$query = $this->db->query($sql);

				$row = $query->row();

				$this->form_validation->set_rules('firstname','First Name','required');
				
				// if an email address wasnt entered 
				if($email == '')
				{
					if($row->email != '' && $row->admin_type != '6')
					{
						$this->form_validation->set_rules('email','Email','required|valid_email|callback__doesUserExist_email');
					}
				}

				// if an email address was entered
				if($email != '')
				{
					// if this user had none of these
					if($row->email == '' && $row->password == '')
					{
						$this->form_validation->set_rules('email','Email','required|valid_email|callback__doesUserExist_email');
						$this->form_validation->set_rules('password', 'Password', 'required|matches[password2]');
						$this->form_validation->set_rules('password2', 'Password Confirmation', 'required');
					}
					
					// if this user wants to just change their email address
					if($row->email != '' && $row->password != '')
					{
						$this->form_validation->set_rules('email','Email','required|valid_email|callback__doesUserExist_email');
					}

					else
					{
						$this->form_validation->set_rules('email','Email','required|valid_email|callback__doesUserExist_email');
					}



				}


				// if password was entered
				if($password1 != '')
				{
					$this->form_validation->set_rules('email','Email','required|valid_email|callback__doesUserExist_email');
					$this->form_validation->set_rules('password', 'Password', 'required|matches[password2]');
					$this->form_validation->set_rules('password2', 'Password Confirmation', 'required');
				}

				// if username was entered
				if($username != '')
					$this->form_validation->set_rules('username','Username','required|callback__doesUserExist_username');
				

				$d['validation'] = $this->form_validation->run();

				if($d['validation'] == TRUE )
				{
					// insert the data
					$this->Admin_model->update_user($id);
					$this->upload_profilepic($id);

					redirect('admin2/users/edit/'.$id.'/success');
				}
				else
				{
					$d['message'] = validation_errors();
				}


			}

			$this->load->view('admin_new/users_edit', $d);
		}

		if($slug == 'view')
		{
			$d['title'] = 'View User';
			$d['user_id'] = $id;

			$d['g'] = $this->Admin_model->get_user($id);

			$this->load->view('admin_new/users_view', $d);
		}



		if($slug == 'delete')
		{
			if($this->input->is_ajax_request())
				$this->Admin_model->deleteAdmin($id);
		}

		if($slug == 'list')
		{
			$d['title'] = 'All Users';
			$d['get_users'] = $this->Admin_model->get_users('noguest');
			$this->load->view('admin_new/users_list', $d);
		}

		if($slug == 'listguests')
		{
			$d['title'] = 'All Guest Authors';
			$d['get_users'] = $this->Admin_model->get_users('guest');
			$this->load->view('admin_new/users_list', $d);
		}
	}

	public function site_users($slug,$id = '')
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect('admin2/');

		// users access
		
		if($slug == 'new')
		{
			$d['title'] = 'New User';
			
			if($this->input->post('submit'))
			{
				$post_admin_type = $this->input->post('admin_type');
				$email = $this->input->post('email');
				$username = $this->input->post('username');
				$password1 = $this->input->post('password');
				$password2 = $this->input->post('password2');

				$this->form_validation->set_rules('firstname','First Name','required');
				$this->form_validation->set_rules('admin_type', '', '');

				if(($post_admin_type == '1' || $post_admin_type == '2'))
				{
					$this->form_validation->set_rules('username','Username','required|callback__doesUserExist_username');
					$this->form_validation->set_rules('email','Email','required|callback__doesUserExist_email');
					$this->form_validation->set_rules('password', 'Password', 'required|matches[password2]');
					$this->form_validation->set_rules('password2', 'Password Confirmation', 'required');

				}
				else
				{
					// if these things were typed
					if($password1 != '' || $email != '' || $username != '')
					{
						$this->form_validation->set_rules('username','Username','required|callback__doesUserExist_username');
						$this->form_validation->set_rules('email','Email','required|callback__doesUserExist_email');
						$this->form_validation->set_rules('password', 'Password', 'required|matches[password2]');
						$this->form_validation->set_rules('password2', 'Password Confirmation', 'required');
					}
				}

				$d['validation'] = $this->form_validation->run();

				if($d['validation'] == TRUE )
				{
					// insert the data
					$this->Admin_model->insert_user();

					$this->upload_profilepic($this->db->insert_id());

					redirect('admin2/site_users/list/success');
				}
				else
				{
					$d['message'] = validation_errors();
				}


			}

			$this->load->view('admin_new/users_new', $d);

		}

		if($slug == 'edit')
		{
			$d['title'] = 'Edit User';

			$d['g'] = $this->Admin_model->get_user($id);
			$d['user_id'] = $id;
			$user_id = $id;
			

			if($this->input->post('submit'))
			{
				$email = $this->input->post('email');
				$username = $this->input->post('username');
				$password1 = $this->input->post('password');
				$password2 = $this->input->post('password2');

				$sql = "SELECT * FROM admin where id = ".$user_id;
				$query = $this->db->query($sql);

				$row = $query->row();

				$this->form_validation->set_rules('firstname','First Name','required');
				
				// if an email address wasnt entered 
				if($email == '')
				{
					if($row->email != '' && $row->admin_type != '6')
					{
						$this->form_validation->set_rules('email','Email','required|valid_email|callback__doesUserExist_email');
					}
				}

				// if an email address was entered
				if($email != '')
				{
					// if this user had none of these
					if($row->email == '' && $row->password == '')
					{
						$this->form_validation->set_rules('email','Email','required|valid_email|callback__doesUserExist_email');
						$this->form_validation->set_rules('password', 'Password', 'required|matches[password2]');
						$this->form_validation->set_rules('password2', 'Password Confirmation', 'required');
					}
					
					// if this user wants to just change their email address
					if($row->email != '' && $row->password != '')
					{
						$this->form_validation->set_rules('email','Email','required|valid_email|callback__doesUserExist_email');
					}

					else
					{
						$this->form_validation->set_rules('email','Email','required|valid_email|callback__doesUserExist_email');
					}



				}


				// if password was entered
				if($password1 != '')
				{
					$this->form_validation->set_rules('email','Email','required|valid_email|callback__doesUserExist_email');
					$this->form_validation->set_rules('password', 'Password', 'required|matches[password2]');
					$this->form_validation->set_rules('password2', 'Password Confirmation', 'required');
				}

				// if username was entered
				if($username != '')
					$this->form_validation->set_rules('username','Username','required|callback__doesUserExist_username');
				

				$d['validation'] = $this->form_validation->run();

				if($d['validation'] == TRUE )
				{
					// insert the data
					$this->Admin_model->update_user($id);
					$this->upload_profilepic($id);

					redirect('admin2/users/edit/'.$id.'/success');
				}
				else
				{
					$d['message'] = validation_errors();
				}


			}

			$this->load->view('admin_new/users_edit', $d);
		}

		if($slug == 'view')
		{
			$d['title'] = 'View User';
			$d['user_id'] = $id;

			$d['g'] = $this->Admin_model->get_user($id);

			$this->load->view('admin_new/users_view', $d);
		}



		if($slug == 'delete')
		{
			if($this->input->is_ajax_request())
				$this->Admin_model->deleteAdmin($id);
		}

		if($slug == 'list')
		{
			$d['title'] = 'All Users';
			//$d['get_users'] = $this->Admin_model->get_users('noguest');
			$this->load->view('admin_new/site_users_list', $d);
		}

		if($slug == 'listguests')
		{
			$d['title'] = 'All Guest Authors';
			$d['get_users'] = $this->Admin_model->get_users('guest');
			$this->load->view('admin_new/users_list', $d);
		}
	}

	public function ga_register()
	{
		$d['title'] = 'Guest Author Registration';
		

		if($this->input->post('submit'))
		{
			$this->form_validation->set_rules('firstname', 'First Name', 'required');
			$this->form_validation->set_rules('lastname', 'Last Name', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback__doesUserExist_email');
			$this->form_validation->set_rules('username','Username','required|callback__doesUserExist_username');
			$this->form_validation->set_rules('password', 'Password', 'required|matches[password2]');
			$this->form_validation->set_rules('password2', 'Password Confirmation', 'required');

			$d['validation'] = $this->form_validation->run();

			if($d['validation'] == TRUE )
			{
				$_POST['admin_type'] = '6';

				// insert the data
				$this->Admin_model->insert_user();

				redirect('admin2?success=true');
			}
			else
			{
				$d['message'] = validation_errors();
			}


		}

		$this->load->view('admin_new/users_new_ga', $d);

	}



	public function articles($slug,$id = '')
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect('admin2/');

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'articles') == FALSE)
			redirect('admin2/');
		
		if($slug == 'new')
		{
			$d['title'] = 'New Article';
			

			if($this->input->post('submit'))
			{
				$d['validation'] = $this->form_validation->run('admin_article');

				if($d['validation'] == FALSE )
				{
					$d['message'] = validation_errors();
				}
				else
				{
					// insert the data
					$title = $this->input->post('title');
					$body = $this->input->post('body');
					$author = $this->input->post('author');
					$author_id = $this->input->post('author_id');
					$permalink = $this->input->post('permalink');
					$tags = $this->input->post('tags');
					$posted_by_gbaam = $this->input->post('posted_by_gbaam');


					$this->Admin_model->insertArticle($title,$body, $author_id, $author, $permalink, $tags, $posted_by_gbaam);

					//get the inserted id
					$ins_id = $this->db->insert_id();

					// upload the photo
					$config['upload_path'] = "./data/homeimg";
					$config['allowed_types'] = '*';
					$config['max_size'] = 0;
					$this->load->library('upload', $config);
					/*
					if ( ! $this->upload->do_upload('lg_img'))
					{
						//$error = array('error' => $this->upload->display_errors());
						//echo print_r($error);
					}
					else
					{
						
						$data = $this->upload->data();

						$img_hash = md5($data['file_name'].date('Y-m-d H:i:s'));
						$newname = $config['upload_path'].'/'.$img_hash.'.jpg';
						rename($data['full_path'],$newname);

						$ins_data = array(
							'image' => 'data/homeimg/'.$img_hash.'.jpg'
						);

						$this->db->where('id' , $ins_id);
						$this->db->update('articles',$ins_data);

						
					}*/

					$type = 'article';

					if ( !$this->upload->do_upload('lg_img'))	
					{
						//$error = array('error' => $this->upload->display_errors());
						//echo print_r($error);
					}
					else	
					{

						// Get photo data
						$photodata = array('upload_data' => $this->upload->data());

						//send to get the image manipulated
						$photo_id = $this->Photo_model->actual_upload($type,$ins_id,$photodata);

						$ins_data['image'] = $photo_id;

						$this->db->where('id' , $ins_id);
						$this->db->update('articles',$ins_data);


					}

					redirect('admin2/articles/list/success');
				}

				

			}

			$this->load->view('admin_new/articles_new', $d);

			

		}

		if($slug == 'edit')
		{
			if($this->Admin_model->is_this_my_post('articles',$id) == FALSE)
				redirect('admin2');

			$d['title'] = 'Edit Article';

			$d['get_article'] = $this->Site_model->getArticle_admin($id);
			

			if($this->input->post('submit') || $this->input->post('submit_deny'))
			{
				$d['validation'] = $this->form_validation->run('admin_article');

				if($d['validation'] == FALSE )
				{
					$d['message'] = validation_errors();
				}
				else
				{
					// insert the data
					$title = $this->input->post('title');
					$body = $this->input->post('body');
					$author = $this->input->post('author');
					$author_id = $this->input->post('author_id');
					$permalink = $this->input->post('permalink');
					$tags = $this->input->post('tags');
					$status = $this->input->post('status');
					$posted_by_gbaam = $this->input->post('posted_by_gbaam');

					$this->Admin_model->updateArticle($id,$title,$body,$author,$author_id,$permalink, $tags, $status, $posted_by_gbaam);

					// upload the photo
					$config['upload_path'] = "./data/homeimg";
					$config['allowed_types'] = '*';
					$config['max_size'] = 0;
					$this->load->library('upload', $config);
	/*
					if ( ! $this->upload->do_upload('lg_img'))
					{
						//$error = array('error' => $this->upload->display_errors());
						//echo print_r($error);
					}
					else
					{
						
						$data = $this->upload->data();

						$img_hash = md5($data['file_name'].date('Y-m-d H:i:s'));
						$newname = $config['upload_path'].'/'.$img_hash.'.jpg';
						rename($data['full_path'],$newname);

						$ins_data = array(
							'image' => 'data/homeimg/'.$img_hash.'.jpg'
						);

						$this->db->where('id' , $id);
						$this->db->update('articles',$ins_data);

						
					}
	*/
					$type = 'article';

					if ( !$this->upload->do_upload('lg_img'))	
					{
						//$error = array('error' => $this->upload->display_errors());
						//echo print_r($error);
					}
					else	
					{

						// Get photo data
						$photodata = array('upload_data' => $this->upload->data());

						//send to get the image manipulated
						$photo_id = $this->Photo_model->actual_upload($type,$id,$photodata);

						$ins_data['image'] = $photo_id;

						$this->db->where('id' , $id);
						$this->db->update('articles',$ins_data);


					}

					if($this->input->post('submit_deny'))
						redirect('admin2/articles/review/success');
					else
						redirect('admin2/articles/edit/'.$id.'/success');
				}
				
			}

			$this->load->view('admin_new/articles_edit', $d);
		}

		if($slug == 'delete')
		{
			if($this->input->is_ajax_request())
				$this->Admin_model->deleteArticle($id);
		}

		if($slug == 'review')
		{
			if($this->session->userdata('admin_type') > '4')
				redirect('admin2');
			
			$d['title'] = 'Review Articles';
			$d['get_articles'] = $this->Site_model->getArticlesToReview_admin();
			$this->load->view('admin_new/articles_list', $d);
		}


		if($slug == 'list')
		{
			$d['title'] = 'All Articles';
			$d['get_articles'] = $this->Site_model->getArticles_admin();
			$this->load->view('admin_new/articles_list', $d);
		}
	}

	public function featarticles($slug, $type, $slot='',$article_id = '')
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect('admin2/');

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'articles') == FALSE)
			redirect('admin2/');
		
		$d['type'] = $type;

		if($type == 'article')
		{
			$d['title'] = '3 Featured Articles';
			$d['limit'] = 3;
		}

		// List all the articles
		if($slug == 'list')
		{
			$this->load->view('admin_new/featarticles_list', $d);
		}

		if($slug == 'add')
		{
			$d['title'] = $d['title']." Add";

			$d['slot'] = $slot;
			$d['get_articles'] = $this->Site_model->getArticles();
			$this->load->view('admin_new/featarticles_add', $d);
		}

		if($slug == 'addconf')
		{
			$this->Site_model->AddToSlot($type, $slot, $article_id);
			redirect('admin2/featarticles/list/'.$type.'/success');
		}

	}

	public function blog($slug,$id = '')
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect('admin2/');

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'blog') == FALSE)
			redirect('admin2/');
		
		if($slug == 'new')
		{
			$d['title'] = 'New Blog Post';
			

			if($this->input->post('submit'))
			{
				$d['validation'] = $this->form_validation->run('admin_article');

				if($d['validation'] == FALSE )
				{
					$d['message'] = validation_errors();
				}
				else
				{
					// insert the data
					$title = $this->input->post('title');
					$body = $this->input->post('body');
					$author = $this->input->post('author');
					$permalink = $this->input->post('permalink');
					$tags = $this->input->post('tags');


					$this->Admin_model->insertBlogPost($title,$body, $author, $permalink, $tags);

					//get the inserted id
					$ins_id = $this->db->insert_id();

					// upload the photo
					$config['upload_path'] = "./data/homeimg";
					$config['allowed_types'] = '*';
					$config['max_size'] = 0;
					$this->load->library('upload', $config);

					$type = 'blog';

					if ( !$this->upload->do_upload('lg_img'))	
					{
						//$error = array('error' => $this->upload->display_errors());
						//echo print_r($error);
					}
					else	
					{

						// Get photo data
						$photodata = array('upload_data' => $this->upload->data());

						//send to get the image manipulated
						$photo_id = $this->Photo_model->actual_upload($type,$ins_id,$photodata);

						$ins_data['image'] = $photo_id;

						$this->db->where('id' , $ins_id);
						$this->db->update('blog_posts',$ins_data);


					}

					redirect('admin2/blog/list/success');
				}

			}

			$this->load->view('admin_new/blogpost_new', $d);

			

		}

		if($slug == 'edit')
		{
			if($this->Admin_model->is_this_my_post('blog',$id) == FALSE)
				redirect('admin2');

			$d['title'] = 'Edit Blog Post';

			$d['get_article'] = $this->Blog_model->getArticle_admin($id);
			

			if($this->input->post('submit'))
			{
				$d['validation'] = $this->form_validation->run('admin_article');

				if($d['validation'] == FALSE )
				{
					$d['message'] = validation_errors();
				}
				else
				{
					// insert the data
					$title = $this->input->post('title');
					$body = $this->input->post('body');
					$author = $this->input->post('author');
					$permalink = $this->input->post('permalink');
					$tags = $this->input->post('tags');
					$status = $this->input->post('status');

					$this->Admin_model->updateBlogPost($id,$title,$body,$author, $permalink, $tags, $status);


					// upload the photo
					$config['upload_path'] = "./data/homeimg";
					$config['allowed_types'] = '*';
					$config['max_size'] = 0;
					$this->load->library('upload', $config);
	/*
					if ( ! $this->upload->do_upload('lg_img'))
					{
						//$error = array('error' => $this->upload->display_errors());
						//echo print_r($error);
					}
					else
					{
						
						$data = $this->upload->data();

						$img_hash = md5($data['file_name'].date('Y-m-d H:i:s'));
						$newname = $config['upload_path'].'/'.$img_hash.'.jpg';
						rename($data['full_path'],$newname);

						$ins_data = array(
							'image' => 'data/homeimg/'.$img_hash.'.jpg'
						);

						$this->db->where('id' , $id);
						$this->db->update('articles',$ins_data);

						
					}
	*/
					$type = 'blog';

					if ( !$this->upload->do_upload('lg_img'))	
					{
						//$error = array('error' => $this->upload->display_errors());
						//echo print_r($error);
					}
					else	
					{

						// Get photo data
						$photodata = array('upload_data' => $this->upload->data());

						//send to get the image manipulated
						$photo_id = $this->Photo_model->actual_upload($type,$id,$photodata);

						$ins_data['image'] = $photo_id;

						$this->db->where('id' , $id);
						$this->db->update('blog_posts',$ins_data);


					}

					redirect('admin2/blog/edit/'.$id.'/success');

				}
				
			}
			$this->load->view('admin_new/blogpost_edit', $d);
		}

		if($slug == 'delete')
		{
			if($this->input->is_ajax_request())
				$this->Admin_model->deleteBlogPosts($id);
		}

		if($slug == 'list')
		{
			$d['title'] = 'All Blog Posts';
			$d['get_articles'] = $this->Blog_model->getArticles_admin();
			$this->load->view('admin_new/blogpost_list', $d);
		}
	}

	public function messages($slug,$message_id)
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect('admin2/');
		
		if($slug == 'home')
		{
			$d['title'] = 'Messages';
			$this->load->view('admin_new/messages_home', $d);
		}

		if($slug == 'new')
		{
			$d['title'] = 'New Message';
			$this->load->view('admin_new/messages_new', $d);
		}

		if($slug == 'view')
		{
			$d['title'] = 'View Message';
			$this->load->view('admin_new/message_view',$d);
		}
	}

	public function stats()
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect('admin2/');
		

	}

	public function upload()
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect('admin2/');
		
		$this->load->view('admin_new/upload_form');
	}

	public function uploadify($mixtape_id, $new = '')
	{
		//if(!$this->session->userdata('admin_logged_in'))
		//	redirect('admin2/');
		
		$config['upload_path'] = "./data/mixtape/audio"; 
		$config['allowed_types'] = 'mp3|wav|ogg';
		$config['max_size'] = 0;
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload("userfile"))
		{
			//$error = $this->upload->display_errors();
			//var_dump($this->upload->data());
			//var_dump($error); 
		}
		else
		{
			// Get upload data
			$data = $this->upload->data();

			//print_r(var_dump($data));
			
			$img_hash = md5($data['file_name'].date('Y-m-d H:i:s'));
			$newname = $config['upload_path'].'/'.$img_hash.'.mp3';
			rename($data['full_path'],$newname);

			//check if there is previeous tracks and return the previous
			//track number
			$sql = "SELECT * FROM mixtape_tracks where mixtape_id = " . $mixtape_id;
			$q = $this->db->query($sql);
			$count = $q->num_rows();


			// set the trackname variable
			$track_name = $data['raw_name'];

			if($new == 'new')
			{
				// check if the first 2 intergers are track numbers
				$first_two_chars = substr($track_name, 0, 2);
				if(is_numeric($first_two_chars))
				{
					//set the track num 
					$track_num = intval($first_two_chars);
				}
				else
				{
					//if there is no previous tracks then the first track is 1
					if($count == 0)
					{
						$track_num = '1';
					}
					elseif($count > 0)
					{
						$track_num = $count+1;
					}
				}
			}
			else
			{
				$sql = "SELECT * FROM mixtape_tracks where mixtape_id = ".$mixtape_id." order by track_num DESC LIMIT 1";
				$query = $this->db->query($sql);

				$row = $query->row();

				$track_num = $row->track_num+1;
			}
			


			// remove first 2 characters if its a number
			if(is_numeric($first_two_chars))
			{
				$track_name = substr($data['raw_name'], 2);
			}

			// remove all underscores from name
			$track_name = str_replace('_', ' ', $track_name);

			// change datpif to gbaam
			//$track_name = str_replace('(DatPiff Exclusive)', '(Gbaam.com)', $track_name);
			$track_name = str_replace('(DatPiff Exclusive)', '', $track_name);

			// capitalize produced
			$track_name = str_replace('produced', 'Produced', $track_name);
			$track_name = str_replace('prod', 'Prod', $track_name);

			// remove all white space after
			$track_name = trim($track_name);

			//remove any begining dashes
			if(substr($track_name, 0, 1) == '-')
			{
				$track_name = substr($track_name, 1);
			} 

			// remove white spaces again after removing dashes
			$track_name = trim($track_name);

			$data = array(
				'mixtape_id' => $mixtape_id,
				'track_num' => $track_num,
				'title' => $track_name,
				'orginal_filename' => $data['file_name'],
				'hash' => $img_hash,
				'ext' => $data['file_ext']//,
				//'size' => $this->formatSizeUnits($data['file_size'], 1)
				);

			$this->db->insert('mixtape_tracks',$data);

		} 
	}  

	public function upload_covers($mixtape_id)
	{


		$type = 'mixtape';
		$mixtape = $this->Mixtape_model->getMixtape_new_admin($mixtape_id);

		
		$config['upload_path'] = "./".$this->config->item('uploaded_images_folder').'/'.$type;
		$config['allowed_types'] = 'jpeg|jpg|png|gif';
		$config['max_size']	= '10000';
		$config['max_width']  = '10240';
		$config['max_height']  = '7680';

		$this->upload->initialize($config);

		if ( $this->upload->do_upload('ft_cover'))
		{		
			// Get photo data
			$photodata = array('upload_data' => $this->upload->data());

			//delete the old mixtape cover
			$this->Photo_model->delete_image('mixtape',$mixtape->front_cover);
			

			//send to get the image manipulated
			$photo_id = $this->Photo_model->actual_upload($type,$mixtape_id,$photodata);

			$data['front_cover'] = $photo_id;

			$this->db->where('id' , $mixtape_id);
			$this->db->update('mixtapes',$data);


		}/*
		else{
			$d['error'] = array('error' => $this->upload->display_errors());
			print_r($d['error']);
		}*/


		if( $this->upload->do_upload("bk_cover"))
		{
			// Get photo data
			$photodata = array('upload_data' => $this->upload->data());

			//delete the old mixtape cover
			$this->Photo_model->delete_image('mixtape',$mixtape->back_cover);

			//send to get the image manipulated
			$photo_id = $this->Photo_model->actual_upload($type,$mixtape_id,$photodata);

			$data['back_cover'] = $photo_id;

			$this->db->where('id' , $mixtape_id);
			$this->db->update('mixtapes',$data);


		}
	}

	 

	public function upload_profilepic($user_id)
	{


		$type = 'admin_user';

		$config['upload_path'] = "./".$this->config->item('uploaded_images_folder').$type;
		$config['allowed_types'] = 'jpeg|jpg|png|gif';
		$config['max_size']	= '10000';
		$config['max_width']  = '10240';
		$config['max_height']  = '7680';

		$this->upload->initialize($config); 

		if ( $this->upload->do_upload('lg_img'))
		{		
			// Get photo data
			$photodata = array('upload_data' => $this->upload->data());

			//send to get the image manipulated
			$photo_id = $this->Photo_model->actual_upload($type,$user_id,$photodata);

			$data['profile_img'] = $photo_id;

			$this->db->where('id' , $user_id);
			$this->db->update('admin',$data);


		}
		else{
			//$d['error'] = array('error' => $this->upload->display_errors());
			//print_r($d['error']);
		}
	}

	public function formatSizeUnits($bytes)
	{
		if ($bytes >= 1073741824)
		{
			$bytes = number_format($bytes / 1073741824, 2) . ' GB';
		}
		elseif ($bytes >= 1048576)
		{
			$bytes = number_format($bytes / 1048576, 2) . ' MB';
		}
		elseif ($bytes >= 1024)
		{
			$bytes = number_format($bytes / 1024, 2) . ' KB';
		}
		elseif ($bytes > 1)
		{
			$bytes = $bytes . ' bytes';
		}
		elseif ($bytes == 1)
		{
			$bytes = $bytes . ' byte';
		}
		else
		{
			$bytes = '0 bytes';
		}

		return $bytes;
	}

	public function Add_video_thumbnail($video_id)
	{
		$type = 'video';
		$video = $this->Video_model->getVideo_row($video_id);

		
		$config3['upload_path'] = "./".$this->config->item('uploaded_images_folder').$type;
		$config3['allowed_types'] = 'jpeg|jpg|png|gif';
		$config3['max_size']	= '10000';
		$config3['max_width']  = '10240';
		$config3['max_height']  = '7680';

		//$this->load->library('upload', $config);
		$this->upload->initialize($config3);

		if ( ! $this->upload->do_upload('lg_img'))
		{
			// Only Uncomment for error checking
			//$error = $this->upload->display_errors();
			//var_dump($this->upload->data());
			//var_dump($error); 
		}
		else
		{		
			// Get photo data
			$photodata = array('upload_data' => $this->upload->data());

			//delete the old song cover
			$this->Photo_model->delete_image('video',$video->thumb);
			
			// send to get the image manipulated
			$photo_id = $this->Photo_model->actual_upload($type,$video_id,$photodata);

			// update the video with the new thumbnail
			$data['thumb'] = $photo_id;
			$this->db->where('id' , $video_id);
			$this->db->update('video',$data);
		}

	}

	public function logout()
	{
		$this->Admin_model->log_activity('logout','0','logged out');
		$this->session->sess_destroy();
		redirect('admin2/');
	}

	public function pretty_url()
	{
		$this->output->enable_profiler(FALSE);
		$text = $this->input->post('url');
		echo clean_url_slug($text);
	}

	public function c_url($str)
	{
		echo clean_url_slug(urldecode($str));
	}

	public function newpermalinks()
	{
		$sql = "SELECT * FROM mixtapes";
		$query = $this->db->query($sql);

		foreach($query->result() as $q)
		{
			$url = clean_url_slug($q->artist.'-'.$q->title);
			$sql = "UPDATE mixtapes SET  permalink =  '{$url}' WHERE  id =$q->id";
			$this->db->query($sql);
		}
	}

	public function newpermalinks_art()
	{
		$sql = "SELECT * FROM articles";
		$query = $this->db->query($sql);

		foreach($query->result() as $q)
		{
			$url = clean_url_slug($q->title);
			$sql = "UPDATE articles SET  permalink =  '{$url}' WHERE  id =$q->id";
			$this->db->query($sql);
		}
	}

	function create_zip_shell($mixtape_id,$rebuild = '')
	{
		if($rebuild == 'rebuild')
		{
			$sql = "Update mixtapes SET zip_file = '...' where id = ".$mixtape_id;
			$this->db->query($sql);
		}

		if($_SERVER['HTTP_HOST'] == 'localhost')
		{
			$command = '/Applications/MAMP/bin/php/php5.5.10/bin/php /Users/TOCHI/Dropbox/Web\ Development/Projects/gbaam/cron.php admin2 create_zip '.$mixtape_id;
		}
		else
		{
			$command = '/usr/bin/php /var/www/html/cron.php admin2 create_zip '.$mixtape_id;
		}

		shell_exec(sprintf('%s > /dev/null 2>&1 &', $command));
	}

	public function create_zip($mixtape_id)
	{
		ini_set('memory_limit','500M');
		ini_set('max_execution_time','6000');

		$this->load->library('zip');

		$mix_audio_dir = $this->config->item('mixtape_audio_dir');
		$mix_covers_dir = $this->config->item('mixtape_covers_dir');
		$mix_zip_dir = $this->config->item('mixtape_zip_dir');

		// Get the mixtape data
		$getTracks = $this->Mixtape_model->getMixtapeTracks($mixtape_id);
		$get_tape = $this->Mixtape_model->getMixtape_new_admin($mixtape_id);

		// Clean out the title
		//$mixtape_title = preg_replace("/[^a-zA-Z 0-9]+/", " ", $get_tape->title);
		$mixtape_title = preg_replace('/[^a-zA-Z0-9]/i',' ', $get_tape->title);
		$mixtape_title = str_replace('.',' ',$mixtape_title);
		$mixtape_title = str_replace(' ', '_', $mixtape_title);
		$mixtape_title = str_replace('__', '_', $mixtape_title);
		
		// increment number
		$number = 01;
		foreach($getTracks['result'] as $g)
		{
			$track = $mix_audio_dir.$g->hash.$g->ext;
			$new_name = str_pad($number++, 2, "0", STR_PAD_LEFT).' - '.$g->title.$g->ext;
			$this->zip->rename_and_read_file($track,$new_name);
		}

		if($get_tape->front_cover != '')
		{
			$images_folder = $this->config->item('uploaded_images_folder').'mixtape';

			$image_id = $get_tape->front_cover;

			$photo = $this->Photo_model->get_photo($get_tape->front_cover);
			$img_hash = $photo->img_hash;
			

			$front_cover = $images_folder.'/'.$image_id.'_'.$img_hash.'.jpg';

			$front_cover_name = $mixtape_title.'-front.jpg';
			$this->zip->rename_and_read_file($front_cover,'00 - '.$front_cover_name);
		}

		if($get_tape->back_cover != '')
		{
			$images_folder = $this->config->item('uploaded_images_folder').'mixtape';

			$image_id = $get_tape->back_cover;

			$photo = $this->Photo_model->get_photo($get_tape->back_cover);
			$img_hash = $photo->img_hash;
			

			$back_cover = $images_folder.'/'.$image_id.'_'.$img_hash.'.jpg';

			//echo $back_cover;

			$back_cover_name = $mixtape_title.'-back.jpg';
			$this->zip->rename_and_read_file($back_cover,'00 - '.$back_cover_name);
		}

		$zip_name = $mixtape_title.'-(Gbaam.com).zip';

		// store in the folder for now
		$this->zip->archive($mix_zip_dir.$zip_name);

		// Update the mixtape
		$upd = array(
			'zip_file' => $zip_name
			);

		$this->db->where('id',$mixtape_id);
		$this->db->update('mixtapes',$upd);

		if($this->input->is_cli_request())
		{
			echo "Mixtape zip successfully created!".PHP_EOL;
			
		}

		return;
	}

	function update_dn()
	{
		$this->Admin_model->update_dn();
	}

	function create_mixtape_hash()
	{
		$sql = "SELECT * FROM mixtapes";
		$q = $this->db->query($sql);

		foreach($q->result() as $d)
		{
			$upd['hash'] = md5($d->id);
			$this->db->where('id',$d->id);
			$this->db->update('mixtapes',$upd);
		}
	}


}