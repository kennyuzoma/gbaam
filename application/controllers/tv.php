<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class Tv extends CI_Controller {

	function __construct()
    {
        parent::__construct();

        // redirect any "gbaamtv" url coming in.
        if($this->uri->segment(1) == 'gbaamtv')
        {
        	// find and replace all gbaamtv urls
        	$uri_string = $this->uri->uri_string();
			$redirect_string = str_replace('gbaamtv', "tv", $uri_string);

			// finally redirect.
			redirect($redirect_string);
        }

        // clean this up later... 
		// If user has not entered their email address for twitter registration....
		$user_status = $this->session->userdata('u_status');
		if(isset($user_status) && $user_status == '4')
			redirect('main/twitter_finish');

		//$this->output->enable_profiler(TRUE);
    }

	public function index($page = '')
	{
		// get the home page video description
		$getVideo = $this->Video_model->getVideo(3);
		foreach($getVideo as $gv)
			$video_desc = $gv->description;

		// Page Elements
		$d['title'] = 'Gbaam TV';
		$d['meta_desc'] = 'Watch documentaries, interviews, comedy shows, and much more!';
		$d['video_desc'] = $video_desc;
		$d['date'] = date('F d Y');
		$d['getLatestVideos'] = $this->Video_model->getLatestGbaamVids($page,8);
		$d['gbaamtv'] = 1;

		// get the landing video
		$landing = $this->Video_model->getFeatVideo_slot('landing_gtv','1');
		$l = $landing;
		$d['videoid'] = $l->id;

		// set page type
		if($page != '')
			$d['type'] = $page;
		
		else
			$d['type'] = 'gtv_main';
		
		// Set the view
		$this->load->view('gtv/gbaamtv_home',$d);
	}

	public function view($id)
	{
		redirect('tv/v/'.$id);
	}

	public function v($id)
	{
		// if empty redirect home
		if($id == '')
			redirect('tv');

		// this is if the url is already hashed
		$my_identifier = $id; 

		if(!is_numeric($id)) 
		{
			$id = hashids_decrypt($id); // this is for video id
			$my_identifier = hashids_encrypt($id); // this is for disqus
		}
		else
		{
			redirect('/tv/v/'.hashids_encrypt($id));
		}

		$d['my_identifier'] = $my_identifier; // this is for disqus
		$d['disqus_url'] = base_url().'video/'.$my_identifier;

		$d['page_type'] = 'video';

		
		
		$getVideo = $this->Video_model->getVideo($id);

		// if this video doesnt exist
		if($getVideo == FALSE)
		{
			show_404();
		}
		else
		{
			$this->Video_model->addview($id);
			
			foreach($getVideo as $gv)
			{
				$d['video_title'] = $gv->title;
				$d['video_desc'] = $gv->description;
				$d['video_src'] = $gv->video_src;
				$d['title'] = $gv->title;
				$currdate = new DateTime($gv->date_created);
				$d['date'] =  $currdate->format('F j, Y');
				$d['gtv_cat'] = $gv->gtv_cat;
				$d['status'] = $gv->videostatus;
				$d['v_count'] = $gv->v_count;
				$d['thumb'] = $gv->thumb;
				$d['yt'] = $gv->yt;
				$gtv = $gv->gtv;
				
			}

			if($gtv == 0)
				redirect('video/'.$my_identifier);
			

			if($d['thumb'] == '')
			{

				if($d['yt'] == 1)
					$d['thumb'] = 'http://img.youtube.com/vi/'.youtube_id_from_url($d['video_src']).'/mqdefault.jpg';
				
				else
					$d['thumb'] = base_url().'thumbs/Gbaam3.jpg';  

			}
			else
				$d['thumb'] = base_url().'thumbs/'.$d['thumb'];
			
			$d['title'] = $d['video_title'];
			$d['videoid'] = $id;

			$d['getLatestVideos'] = $this->Video_model->getLatestGbaamVids($d['gtv_cat'],8);


			$this->load->view('gtv/gbaamtv_view',$d);
		}
	}

	

	public function all($page = '')
	{
		// default segment to fetch from
		$seg_number = 4; 

		if(is_numeric($page))
		{
			$page = '';
			$seg_number = 3;
			
		}
		// the uri segment from the url
		$seg = $this->uri->segment($seg_number);

		// page elements
		$d['title'] = 'All GbaamTV Videos';
		$d['meta_desc'] = 'Browse all the GbaamTV videos.';

		// create pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/tv/all/'.$page;
		$config['total_rows'] = $this->Video_model->getGbaamTVByCat_count($page);
		$config['per_page'] = 10;
		$config['num_links'] = 10;
		$config['uri_segment'] = $seg_number;
		$config['full_tag_open'] = '<div class="pagination2" style="margin-top:10px;">';
		$config['full_tag_close'] = '</div>';
		$this->pagination->initialize($config);
		$d['pagi_links'] = $this->pagination->create_links();

		// get the videos
		$d['getVideos'] = $this->Video_model->getGbaamTVByCat($page, $config['per_page'], $seg);

		// Load the view
		$this->load->view('gtv/gbaam_tv_all',$d);
	}

	

}