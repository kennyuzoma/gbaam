<?php 

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */
class Challenge extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		//$this->output->enable_profiler(TRUE);
		$this->load->model('Challenge_model');

		// clean this up later... 
		// If user has not entered their email address for twitter registration....
		$user_status = $this->session->userdata('u_status');
		if(isset($user_status) && $user_status == '4')
			redirect('main/twitter_finish');
	}
	
	public function index()
	{
		$d['get_cats'] = $this->Video_model->get_g_ch_cat();
		$d['cat'] = $this->Video_model->get_latest_challenge();
		$d['cat_info'] = $this->Video_model->get_latest_challenge_info($d['cat']);

		$landing = $this->Video_model->getFeatVideo_slot('landing_chall','1');
		$l = $landing;
		$d['videoid'] = $l->id;

		$d['title'] = 'Challenge';
		$d['meta_desc'] = 'Check out our gbaam challenge video submitted by our users';
	

		$d['sidebarIndexModels'] = TRUE;
		$this->load->view('chall/gexp',$d);	
	}
	
	public function cat($cat)
	{
		// if there is no Category
		if($cat == '')
			redirect('challenge');

		$d['get_cats'] = $this->Video_model->get_g_ch_cat();
		$d['get_side_videos'] = $this->Challenge_model->get_videos_by_cat($cat);

		$d['cat'] = $cat;
		$d['cat_info'] = $this->Video_model->get_latest_challenge_info($d['cat']);
		
		if($this->Challenge_model->first_vid_in_cat_count($cat) == FALSE)
		{
			$this->load->view('gexp',$d);	
		}
			
		else{
			
			$get_video = $this->Challenge_model->first_vid_in_cat($cat);
			foreach($get_video as $v)
			{
				$d['title'] = $v->title;
				$d['videoid'] = $v->id;
				$d['page'] = $v->chall_cat;
				$d['v_count'] = $v->v_count;
				//$cat = $v->chall_cat;
			}


				$getmix = $this->Challenge_model->get_vote($d['videoid']);
				$my_score = $getmix['my_score'];
				$total_score = $getmix['total_score'];
				//echo $total_score;
				//echo $my_score.'/'.$total_score;
				if($total_score == 0 ) 
				{
					$d['isScore'] = FALSE;
				}
				else
				{
					$d['isScore'] = TRUE;
					$percent = ($my_score / $total_score) * 100;
					$percent = round($percent);
					$d['final_score'] = $percent;


					$css_p = round(($my_score / $total_score) * 10);
					$d['css_score'] = $css_p;
				}
				$this->load->view('chall/gexp',$d);	
		}
		
	}
	
	public function view($id)
	{
		// if there is no id
		if($id == '')
			redirect('challenge');

		$d['page_type'] = 'video';
		
		//$this->load->model('Comments_model');
		//$comments = $this->Comments_model->get_comments($id);

		$get_video = $this->Challenge_model->get_video($id);

		// if this video doesnt exist
		if($get_video == FALSE)
		{
			show_404();
		}
		else
		{
			$this->Video_model->addview($id);
			$d['get_cats'] = $this->Video_model->get_g_ch_cat();
			
			
			foreach($get_video as $v)
			{
				$d['title'] = $v->title;
				$d['videoid'] = $id;
				$d['page'] = $v->chall_cat;
				$d['v_count'] = $v->v_count;
				$d['cat'] = $v->chall_cat;
				$cat = $v->chall_cat;
			}

			// Get the active challenge ID
			$current_cat_id = $this->Video_model->get_latest_challenge();
			$d['cat_info'] = $this->Video_model->get_latest_challenge_info($d['cat']);


			if( $current_cat_id == $cat)
			{
				$d['active_chal'] = TRUE;
			}
			else
			{
				$d['active_chal'] = FALSE;
			}
			
			$d['get_side_videos'] = $this->Challenge_model->get_videos_by_cat($cat);
			
			
				$getmix = $this->Challenge_model->get_vote($id);
				$my_score = $getmix['my_score'];
				$total_score = $getmix['total_score'];
				//echo $total_score;
				//echo $my_score.'/'.$total_score;
				if($total_score == 0 ) 
				{
					$d['isScore'] = FALSE;
				}
				else
				{
					$d['isScore'] = TRUE;
					$percent = ($my_score / $total_score) * 100;
					$percent = round($percent);
					$d['final_score'] = $percent;
					
					
					$css_p = round(($my_score / $total_score) * 10);
					$d['css_score'] = $css_p;
				}
			
			//Load View
			$this->load->view('chall/challenge_view',$d);
		}
	}
	
	public function all($cat = '')
	{
		$d['title'] = 'All videos';
		$d['meta_desc'] = 'Browse all of our challenge videos';

		$d['get_cats'] = $this->Video_model->get_g_ch_cat();
		$d['get_video_list'] = $this->Challenge_model->get_vids_by_cat($cat);
		
		//Load View
		$this->load->view('chall/challenge_all',$d);
	}
	
	public function add_vote($videoid,$choice)
	{
		$this->Challenge_model->add_vote($videoid,$choice);
	}

	public function myname($name)
	{
		echo 'My name is '.$name;
	}
	
	
}
