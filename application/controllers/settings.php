<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class Settings extends CI_Controller {

	function __construct()
    {
        parent::__construct();

		$this->output->enable_profiler(TRUE);
 
		// redirect the user to login page
		// if not logged in already.
       	login_redirect();

       	// clean this up later... 
		// If user has not entered their email address for twitter registration....
		$user_status = $this->session->userdata('u_status');
		if(isset($user_status) && $user_status == '4')
			redirect('main/twitter_finish');
    }

    public function index()
    {
    	$d['title'] = 'General Settings';
    	$d['meta_desc'] = 'Your Settings.';

    	if($this->input->post('submit'))
    	{
    		//
    		
    		
    		$user = $this->User_model->get_user();

    		$username_field = $this->input->post('username');

    		// default username rules
    		$rules = 'trim|required|xss_clean|callback__doesUserExist_username';

    		$fb_setting = $this->User_model->get_user_setting('facebook_id');
    		
    		if($fb_setting != '')
    		{
	    		if(($fb_setting != '') && ($user->username == ''))
	    		{
	    			if($username_field == '')
	    				$rules = '';
	    		}
	    	}

    		$this->form_validation->set_rules('username','Username',$rules);

    		$d['validation'] = $this->form_validation->run('settings');

			if($d['validation'] == TRUE )
			{
				// update the user
				$this->User_model->update_user('','home');

				// upload the profile pic
				$this->upload_profilepic();

				// set the message
				$d['message'] = 'Profile Updated Successfully';
			}
			else
			{
				$d['message'] = validation_errors();

				//echo $rules;
			}
    	}
    	

        $this->load->view('settings/settings_home',$d);
    }


    public function password()
    {
        $d['title'] = 'Password';
        $d['meta_desc'] = 'Password Settings';

        // when the form is submitted
        if($this->input->post('submit'))
    	{
	        $d['validation'] = $this->form_validation->run('settings_password');

			if($d['validation'] == TRUE )
			{
				// update the user
				$this->User_model->update_user('','password');

				// set the message
				$d['message'] = 'Password Updated Successfully';
			}
			else
			{
				$d['message'] = validation_errors();
			}
		}
        
        $this->load->view('settings/settings_home',$d);
    }

    public function notifications()
    {
        $d['title'] = 'Notifications';
        $d['meta_desc'] = 'Notification Settings';

        // when the form is submitted
        if($this->input->post('submit'))
    	{
	        //$d['validation'] = $this->form_validation->run('settings_notifications');
    		$d['validation'] = TRUE;
			if($d['validation'] == TRUE )
			{
				// update the user
				$this->User_model->update_user('','notifications');

				// set the message
				$d['message'] = 'Email Settings saved!';
			}
			else
			{
				$d['message'] = validation_errors();
			}
		}
        
        $this->load->view('settings/settings_home',$d);
    }

    public function social()
    {
        $d['title'] = 'Connected Social Networks';
        $d['meta_desc'] = 'Connected Social Networks';

        // when the form is submitted
        if($this->input->post('submit'))
    	{
	        $d['validation'] = $this->form_validation->run('settings_social');

			if($d['validation'] == TRUE )
			{
				// update the user
				$this->User_model->update_user('','social');

				// set the message
				$d['message'] = 'Social Networks saved!';
			}
			else
			{
				$d['message'] = validation_errors();
			}
		}
        
        $this->load->view('settings/settings_home',$d);
    }

    public function delete()
    {
        $d['title'] = 'Deactivate';
        $this->load->view('forum_commingsoon',$d);
    }

    public function confirm_account($hash)
    {
    	$user_id = $this->session->userdata('user_id');

    	$this->User_model->confirm_account($hash);
    }

    public function close_conf()
    {
    	$this->session->set_userdata("close_conf", 'TRUE');
    }

    public function _doesEmailExist()
	{
		//Store in Site_model
		$post_email = $this->input->post('email');
		
		if(isset($post_email))
			$email = $this->input->post('email');

		//if the provided email and password match a user then it will return false
		if ($this->User_model->existing_email($email) == TRUE)
		{
			$sql = "SELECT * FROM users WHERE email='{$email}' LIMIT 1;";
			$query = $this->db->query($sql);
			$row = $query->row();

			if($row->facebook_id != '')
			{
				$this->form_validation->set_message('_doesEmailExist', 'There is a user with that email address already!');
				return FALSE;
			}
			else
			{
				$this->form_validation->set_message('_doesEmailExist', 'There is a user with that email address already!');
				return FALSE;
			}

		}
		else
		{
			return TRUE;
		}
	}

	public function _doesUserExist_username()
	{
		//Store in Site_model
		$username = $this->input->post('username');

		//if the provided email and password match a user then it will return false
		if ($this->User_model->doesUserExist_username($username) == TRUE)
		{
			$this->form_validation->set_message('_doesUserExist_username', 'Username is not available.');

			// return false to trigger the error message
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function upload_profilepic()
	{

		$user_id = loggedin_user_id();

		$type = 'user';

		$config['upload_path'] = "./".$this->config->item('uploaded_images_folder').$type;
		$config['allowed_types'] = 'jpeg|jpg|png|gif';
		$config['max_size']	= '10000';
		$config['max_width']  = '10240';
		$config['max_height']  = '7680';

		$this->upload->initialize($config); 

		if ( $this->upload->do_upload('pro_pic'))
		{		
			// Get photo data
			$photodata = array('upload_data' => $this->upload->data());

			//send to get the image manipulated
			$photo_id = $this->Photo_model->actual_upload($type,$user_id,$photodata);

			$data['profile_img'] = $photo_id;

			$this->db->where('id' , $user_id);
			$this->db->update('users',$data);


		}
		else{
			//$d['error'] = array('error' => $this->upload->display_errors());
			//print_r($d['error']);
		}
	}




}
