<?php

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class Search extends CI_Controller {
	function __construct()
    {
        parent::__construct();
		//$this->output->enable_profiler(TRUE);
        
        // clean this up later... 
		// If user has not entered their email address for twitter registration....
		$user_status = $this->session->userdata('u_status');
		if(isset($user_status) && $user_status == '4')
			redirect('main/twitter_finish');
    }

    public function index()
    {
    	redirect('/');

    	// finish later
    	//$this->load->view('search_home');
    }

    
	public function pre_search()
	{
		$term = $this->input->post('term');
		$term = str_replace(' ','+',$term);
		redirect('/search/srch/'.$term);
	}
	
	public function srch($termm = '',$type='')
	{
		//$this->output->enable_profiler(TRUE);
		$this->load->model('Search_model');

		$d['term'] = str_replace('+',' ',$termm);
		
		$d['title'] = 'Search Results for - "'.$d['term'].'"';
		$d['meta_desc'] = $d['title'];
		$d['pg_title'] = 'Search Results for - "<span style="color:#F90;">'.$d['term'].'</span>"';
		
		if($termm == '')
		{
			$d['getSearch_video'] = '';
			$d['getSearch_gbaamtv'] = '';
			//$d['getSearch_chal'] = '';
			$d['getSearch_artist'] = '';
			$d['getSearch_store'] = '';
			$d['getSearch_mixtape'] = '';
			$d['getSearch_song'] = '';
			$d['getSearch_articles'] = '';

		}
		else
		{
			if($type == 'video')
			{
				$d['getSearch_video_count'] = $this->Search_model->getSearch_count($d['term'],'video');
				$d['getSearch_video'] = $this->Search_model->getSearch($d['term'],'video');
				$d['single'] = TRUE;
			}
			else
			{
				$d['getSearch_video_count'] = $this->Search_model->getSearch_count($d['term'],'video');
				$d['getSearch_video'] = $this->Search_model->getSearch($d['term'],'video',3);
				$d['single'] = FALSE;
			}

			if($type == 'gbaamtv')
			{
				$d['getSearch_gbaamtv_count'] = $this->Search_model->getSearch_count($d['term'],'tv');
				$d['getSearch_gbaamtv'] = $this->Search_model->getSearch($d['term'],'tv');
				$d['single'] = TRUE;
			}
			else
			{
				$d['getSearch_gbaamtv_count'] = $this->Search_model->getSearch_count($d['term'],'tv');
				$d['getSearch_gbaamtv'] = $this->Search_model->getSearch($d['term'],'tv',3);
				$d['single'] = FALSE;
			}
		/*	if($type == 'chal')
			{
				$d['getSearch_chal_count'] = $this->Search_model->getSearch_count($d['term'],'chal');
				$d['getSearch_chal'] = $this->Search_model->getSearch($d['term'],'chal');
				$d['single'] = TRUE;
			}
			else
			{
				$d['getSearch_chal_count'] = $this->Search_model->getSearch_count($d['term'],'chal');
				$d['getSearch_chal'] = $this->Search_model->getSearch($d['term'],'chal',3);
				$d['single'] = FALSE;
			}*/

			if($type == 'mixtape')
			{
				$d['getSearch_mixtape_count'] = $this->Search_model->getSearch_count($d['term'],'mixtape');
				$d['getSearch_mixtape'] = $this->Search_model->getSearch($d['term'],'mixtape');
				$d['single'] = TRUE;
			}
			else
			{
				$d['getSearch_mixtape_count'] = $this->Search_model->getSearch_count($d['term'],'mixtape');
				$d['getSearch_mixtape'] = $this->Search_model->getSearch($d['term'],'mixtape',3);
				$d['single'] = FALSE;
			}
			if($type == 'song')
			{
				$d['getSearch_song_count'] = $this->Search_model->getSearch_count($d['term'],'song');
				$d['getSearch_song'] = $this->Search_model->getSearch($d['term'],'song');
				$d['single'] = TRUE;
			}
			else
			{
				$d['getSearch_song_count'] = $this->Search_model->getSearch_count($d['term'],'song');
				$d['getSearch_song'] = $this->Search_model->getSearch($d['term'],'song',3);
				$d['single'] = FALSE;
			}
			if($type == 'articles')
			{
				$d['getSearch_articles_count'] = $this->Search_model->getSearch_count($d['term'],'articles');
				$d['getSearch_articles'] = $this->Search_model->getSearch($d['term'],'articles');
				$d['single'] = TRUE;
			}
			else
			{
				$d['getSearch_articles_count'] = $this->Search_model->getSearch_count($d['term'],'articles');
				$d['getSearch_articles'] = $this->Search_model->getSearch($d['term'],'articles',3);
				$d['single'] = FALSE;
			}

			/*	
			if($type == 'artist')	
				$d['getSearch_artist'] = $this->Search_model->getSearch($d['term'],'artist');
			else
				$d['getSearch_artist'] = $this->Search_model->getSearch($d['term'],'artist',3);
				
			if($type == 'store')	
				$d['getSearch_store'] = $this->Search_model->getSearch($d['term'],'store');
			else 
				$d['getSearch_store'] = $this->Search_model->getSearch($d['term'],'store',3);
			 * 
			 */
		}
			//$d['getSearch_video'] = $this->Search_model->getSearch($d['term'],'video');
		$this->load->view('search/search', $d); 
	}


	public function srchsing($termm = '',$type='')
	{
		//$this->output->enable_profiler(TRUE);
		$this->load->model('Search_model');
		if(strstr($termm, '%20')){
			$d['term'] = str_replace('%20',' ',$termm);
		} else {
			$d['term'] = $termm;
		}
		$d['title'] = 'Search Results for - "'.$d['term'].'"';
		$d['meta_desc'] = $d['title'];
		$d['pg_title'] = 'Search Results for - "<span style="color:#F90;">'.$d['term'].'</span>"';

		$seg_number = 5; 
		$seg = $this->uri->segment($seg_number);
		$this->load->library('pagination');

		$config['base_url'] = base_url().'/search/srchsing/'.$termm.'/'.$type;
		if($type == 'articles')
			$config['per_page'] = 5;
		else
			$config['per_page'] = 10;
		$config['num_links'] = 10;
		$config['full_tag_open'] = '<div class="pagination2" style="margin-top:-10px;margin-bottom:20px;">';
		$config['full_tag_close'] = '</div>';
		
		if($termm == '')
		{
			$d['getSearch_video'] = '';
			$d['getSearch_gbaamtv'] = '';
			//$d['getSearch_chal'] = '';
			$d['getSearch_artist'] = '';
			$d['getSearch_store'] = '';
			$d['getSearch_mixtape'] = '';
			$d['getSearch_articles'] = '';

		}
		else
		{
			if($type == 'video')
			{
				$d['getSearch_video_count'] = $this->Search_model->getSearch_count($d['term'],'video');
				$d['getSearch_video'] = $this->Search_model->getSearch($d['term'],'video', $config['per_page'], $seg);
				$count = $d['getSearch_video_count'];

			}


			if($type == 'gbaamtv')
			{
				$d['getSearch_gbaamtv_count'] = $this->Search_model->getSearch_count($d['term'],'tv');
				$d['getSearch_gbaamtv'] = $this->Search_model->getSearch($d['term'],'tv', $config['per_page'], $seg);
				$count = $d['getSearch_gbaamtv_count'];

			}

		/*	if($type == 'chal')
			{
				$d['getSearch_chal_count'] = $this->Search_model->getSearch_count($d['term'],'chal');
				$d['getSearch_chal'] = $this->Search_model->getSearch($d['term'],'chal', $config['per_page'], $seg);
				$count = $d['getSearch_chal_count'];
			}
*/

			if($type == 'mixtape')
			{
				$d['getSearch_mixtape_count'] = $this->Search_model->getSearch_count($d['term'],'mixtape');
				$d['getSearch_mixtape'] = $this->Search_model->getSearch($d['term'],'mixtape', $config['per_page'], $seg);
				$count = $d['getSearch_mixtape_count'];

			}

			if($type == 'song')
			{
				$d['getSearch_song_count'] = $this->Search_model->getSearch_count($d['term'],'song');
				$d['getSearch_song'] = $this->Search_model->getSearch($d['term'],'song', $config['per_page'], $seg);
				$count = $d['getSearch_song_count'];

			}

			if($type == 'articles')
			{
				$d['getSearch_articles_count'] = $this->Search_model->getSearch_count($d['term'],'articles');
				$d['getSearch_articles'] = $this->Search_model->getSearch($d['term'],'articles', $config['per_page'], $seg);
				$count = $d['getSearch_articles_count'];

			}

		}


		
		$config['total_rows'] = $count;
		
		$config['uri_segment'] = $seg_number;
		
		
		$this->pagination->initialize($config);
		
		$d['pagi_links'] = $this->pagination->create_links();
		$this->load->view('search/search_sing', $d); 
	}
	
}

?>