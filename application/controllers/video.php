<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class Video extends CI_Controller {

	function __construct()
    {
        parent::__construct(); 

		if($_SERVER['HTTP_HOST'] == 'localhost')
			$this->output->enable_profiler(TRUE);

		// clean this up later... 
		// If user has not entered their email address for twitter registration....
		$user_status = $this->session->userdata('u_status');
		if(isset($user_status) && $user_status == '4')
			redirect('main/twitter_finish');
    }

	public function index()
	{
		// get the targetted uri segment
		$seg = $this->uri->segment(3);

		// Page Elements
		$d['title'] = 'Music Videos';
		$d['meta_desc'] = 'Gbaam Music Videos is the number one source for African music videos from all over the web in one place!';

		// Load Pagination and Set Config vars
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/video/all/';
		$config['total_rows'] = $this->Video_model->get_dates_new_count();
		$config['per_page'] = 5;
		$config['num_links'] = 5;
		$config['uri_segment'] = 3;
		$config['full_tag_open'] = '<div class="pagination2" style="margin-top:10px;margin-bottom:10px;">';
		$config['full_tag_close'] = '</div>';
		$this->pagination->initialize($config);
		$d['pagi_links'] = $this->pagination->create_links();
		
		// get the videos
		$d['getVideos'] = $this->Video_model->get_dates_new($config['per_page'],$seg);

		// load view
		$this->load->view('video/musicvideos_home',$d);
	}

	public function all()
	{
		// get the targetted uri segment
		$seg = $this->uri->segment(3);

		// Page Elements
		$d['title'] = 'All Music Videos';
		$d['meta_desc'] = 'Browse all the Gbaam Music videos.';

		// Load Pagination and Set Config vars
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/video/all/';
		$config['total_rows'] = $this->Video_model->get_dates_new_count();
		$config['per_page'] = 5;
		$config['num_links'] = 5;
		$config['uri_segment'] = 3;
		$config['full_tag_open'] = '<div class="pagination2" style="margin-top:10px;margin-bottom:10px;">';
		$config['full_tag_close'] = '</div>';
		$this->pagination->initialize($config);
		$d['pagi_links'] = $this->pagination->create_links();

		// get the videos
		$d['getVideos'] = $this->Video_model->get_dates_new($config['per_page'],$seg);

		// load the view
		$this->load->view('video/musicvideos_home',$d);
	}

	public function view($videoid)
	{
		
		$d['video_pref'] = $this->input->cookie('video_preference',TRUE); 
		//echo $video_pref;
		// this is if the url is already hashed
		$my_identifier = $videoid; 

		// redirect if this method was reached by video id... eg... gbaam.com/video/v/23423
		if(!is_numeric($videoid)) 
		{
			$videoid = hashids_decrypt($videoid); // this is for video id
			$my_identifier = hashids_encrypt($videoid); // this is for disqus
		}
		else
		{
			redirect('/video/'.hashids_encrypt($videoid));
		}

		// Set page vars
		$d['my_identifier'] = $my_identifier; // this is for disqus
		$d['disqus_url'] = base_url().'video/'.$my_identifier; 
		$d['page_type'] = 'video';

		//get video
		$getVideo = $this->Video_model->getVideo($videoid);

		// if this video doesnt exist
		if($getVideo == FALSE)
		{
			show_404();
		}
		else
		{

			// add a view when someone views this video
			$this->Video_model->addview($videoid);

			foreach($getVideo as $gv)
			{
				$d['video_title'] = $gv->title;
				$d['video_desc'] = $gv->description;
				$d['video_src'] = $gv->video_src;
				$d['v_count'] = $gv->v_count;
				$d['title'] = $gv->title;
				$d['artist_name'] = $gv->artist_name;
				$d['status'] = $gv->videostatus;
				$currdate = new DateTime($gv->date_created);
				$d['date'] =  $currdate->format('F j, Y');
				$thumb = $gv->thumb;
				$yt = $gv->yt;
				$gtv = $gv->gtv;
			}
			if($gtv == 1)
				redirect('gbaamtv/v/'.$my_identifier);
			

			$d['videoid'] = $videoid;
			$d['title'] = $d['artist_name'].' - '.$d['video_title'];

			if($thumb == '')
			{

				if($yt == 1)
					$d['thumb'] = 'http://img.youtube.com/vi/'.youtube_id_from_url($d['video_src']).'/3.jpg';
				
				else
					$d['thumb'] = base_url().'thumbs/Gbaam3.jpg';  

			}
			else
				$d['thumb'] = base_url().'thumbs/'.$thumb;
			

			$getmix = $this->Video_model->get_vote($videoid);
			$my_score = $getmix['my_score'];
			$total_score = $getmix['total_score'];
			//echo $total_score;
			//echo $my_score.'/'.$total_score;
			if($total_score == 0 )
			{
				$d['isScore'] = FALSE;
			}
			else
			{
				$d['isScore'] = TRUE;
				$percent = ($my_score / $total_score) * 100;
				$percent = round($percent);
				$d['final_score'] = $percent;


				$css_p = round(($my_score / $total_score) * 10);
				$d['css_score'] = $css_p;
			}

			$this->load->view('video/musicvideos_view',$d);
		}

	}

	public function embed($videoid,$auto='',$gbaamtv = '')
	{
		// if there is no video id... redirect
		if($videoid == '')
			redirect('video');

		// no auto play on mobile deviced
		if($this->agent->is_mobile())
			$auto = '';
		
		// convert numeric characters to video hash
		if(!is_numeric($videoid))
			 $videoid = hashids_decrypt($videoid);
		
		// get the video and set vars	
		$getVideo = $this->Video_model->getVideo($videoid);
		foreach($getVideo as $gv)
		{
			$d['video_title'] = $gv->title;
			$d['video_desc'] = $gv->description;
			$d['video_src'] = $gv->video_src;
			$d['thumb'] = base_url() . 'thumbs/' . $gv->thumb; 
			$d['title'] = $gv->title;
			$d['artist_name'] = $gv->artist_name;
			$d['gtv'] = $gv->gtv;
		}
		$d['videoid'] = $videoid;
		$d['auto'] = $auto;

		// load view
		$this->load->view('video/video_embed',$d);
	}
	
	// combine to one controller and set for ajax request only...
	public function add_vote($videoid,$choice)
	{
		// those variables are required
		if($videoid != '' || $choice != '')
		{
			// set the vote
			$vote = $this->Video_model->add_vote($videoid,$choice);

			// if returned true or false... return a status message
			if($vote == TRUE)
				$status['status'] = 'good';
			
			if($vote == FALSE)
				$status['status'] = 'bad';
			
			// encode the message in json
			echo json_encode($status);
		}

	}



}
