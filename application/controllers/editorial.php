<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */
class Editorial extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        //if($_SERVER['HTTP_HOST'] == 'localhost')
		//	$this->output->enable_profiler(TRUE); 
        	
        // clean this up later... 
		// If user has not entered their email address for twitter registration....
		$user_status = $this->session->userdata('u_status');
		if(isset($user_status) && $user_status == '4')
			redirect('main/twitter_finish');

    }
    
    public function index()
    {
    	// set the page elements for the view
    	$d['title'] = 'Editorials';
        $d['meta_desc'] = 'Check out and Stay up to date on our Latest Articles, News, Stories and much more!';

    	// Load the pagination class and set the vars
        $this->load->library('pagination');
        $config['base_url'] = base_url().'/editorial/home';
        $config['total_rows'] = $this->db->where('status','1')->get('articles')->num_rows();
        $config['per_page'] = 5;
        $config['num_links'] = 10;
        $config['full_tag_open'] = '<div class="pagination2" style="margin-bottom:10px;">';
        $config['full_tag_close'] = '</div>';
        $this->pagination->initialize($config);
        
        $d['pagi_links'] = $this->pagination->create_links();

        // get the articles
        $d['get_articles'] = $this->Site_model->getArticles($config['per_page'], $this->uri->segment(3));

		// load views
        $this->load->view('article/article_home',$d);
    } 

    public function home($page = '')
    {
        // if page one, just redirect to home
        if($page == '')
            redirect('editorial');

        // set the page elements for the view
        $d['title'] = 'Editorials';
        $d['meta_desc'] = 'Check out and Stay up to date on our Latest Articles, News, Stories and much more!';

        $this->load->library('pagination');
        $config['base_url'] = base_url().'/editorial/home';
        $config['total_rows'] = $this->db->where('status','1')->get('articles')->num_rows();
        $config['per_page'] = 5;
        $config['num_links'] = 10;
        $config['full_tag_open'] = '<div class="pagination2" style="margin-bottom:10px;">';
        $config['full_tag_close'] = '</div>';

        $this->pagination->initialize($config);
        $d['pagi_links'] = $this->pagination->create_links();

        // get the articles
        $d['get_articles'] = $this->Site_model->getArticles($config['per_page'], $this->uri->segment(3));
        
        // Load the View
        $this->load->view('article/article_home',$d);
    } 

    public function view($slug)
    {
        // if empty redirect home
        if($slug == '')
            redirect('editorial');
        
        $d['social_buttons'] = TRUE;
        $d['page_type'] = 'article';

        $article = $this->Site_model->getArticle($slug);


        if($article == FALSE)
        {
            show_404();
        }
        else
        {
            foreach($article as $g)
            {
                $d['id'] = $g->id;
                $art_id = $g->id;
                $d['title'] = $g->title;
                $d['body'] = $g->body;
                $d['image'] = $g->image;
               // $d['author'] = $g->author;
               	$d['author_id'] = $g->author_id;
               	$d['guest'] = $g->guest;
                $d['permalink'] = $g->permalink;
                $d['date'] = $g->date_created;
                $d['status'] = $g->status;
            	
            	$d['author'] = $this->Site_model->getAuthor($g->id);

                // add a view when someone views this video
                $this->Site_model->addview($art_id);

                
                // disqus stuff
                $d['my_identifier'] = 'editorial-'.$art_id; // this is for disqus
                $d['disqus_url'] = base_url().'editorial/'.$d['permalink'];

                $this->load->view('article/article_view',$d);
            }
        }
    }

    public function author($username)
    {
    	// if no username redirect to editorial
    	if($username == '')
    		redirect('editorial');

    	// set the page elements for the view
    	$d['title'] = 'Posts by '.$username;
        $d['meta_desc'] = 'View all posts by '.$username;
    	
    	// set the author information
    	if($username == 'gbaam')
    	{
    		$author_id = 0;
    	}
    	else
    	{
    		$author = $this->Admin_model->get_user_by_username($username);
    		$author_id = $author->aid;
    	}
    	
    	// create the pagination
    	$this->load->library('pagination');
        $config['base_url'] = base_url().'/editorial/author/'.$username;
        $config['total_rows'] = $this->db->where('author_id',$author_id)->where('status','1')->get('articles')->num_rows();
        $config['per_page'] = 5;
        $config['num_links'] = 10;
        $config['full_tag_open'] = '<div class="pagination2" style="margin-bottom:10px;">';
        $config['full_tag_close'] = '</div>';
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $d['pagi_links'] = $this->pagination->create_links();

        // get the articls
        $d['get_articles'] = $this->Site_model->getArticlesByAuthorId($author_id, $config['per_page'], $this->uri->segment(4));

        // load view
        $this->load->view('article/article_home',$d);
    }
    
}

?>
