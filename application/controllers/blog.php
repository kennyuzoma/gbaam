<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */
class Blog extends CI_Controller {

    function __construct()
    {
        parent::__construct();
		//$this->output->enable_profiler(TRUE); 
		$this->load->model('Blog_model');

		// clean this up later... 
		// If user has not entered their email address for twitter registration....
		$user_status = $this->session->userdata('u_status');
		if(isset($user_status) && $user_status == '4')
			redirect('main/twitter_finish');
    }
    
    public function index()
    {
        $this->load->library('pagination');

        $config['base_url'] = base_url().'/blog/home';
        $config['total_rows'] = $this->db->get('blog_posts')->num_rows();
        $config['per_page'] = 5;
        $config['num_links'] = 10;
        $config['full_tag_open'] = '<div class="pagination2" style="margin-bottom:10px;">';
        $config['full_tag_close'] = '</div>';
        
        $this->pagination->initialize($config);
        
        //$d['getMixtapes'] = $this->db->get('mixtapes', $config['per_page'], $this->uri->segment(3));
        $d['pagi_links'] = $this->pagination->create_links();

        $d['get_articles'] = $this->Blog_model->getArticles($config['per_page'], $this->uri->segment(3));

        $d['title'] = 'The Gbaam Blog';
        $d['meta_desc'] = 'Check out our Blog to find out whats going on at Gbaam';
       // $d['get_articles'] = $this->Blog_model->getArticles();
		
        $this->load->view('blog/blog_home',$d);
    } 

    public function home($page = '')
    {
        // if page one, just redirect to home
        if($page == '')
            redirect('editorial');

        $this->load->library('pagination');

        $config['base_url'] = base_url().'/blog/home';
        $config['total_rows'] = $this->db->get('blog_posts')->num_rows();
        $config['per_page'] = 5;
        $config['num_links'] = 10;
        $config['full_tag_open'] = '<div class="pagination2" style="margin-bottom:10px;">';
        $config['full_tag_close'] = '</div>';
        
        $this->pagination->initialize($config);
        
        //$d['getMixtapes'] = $this->db->get('mixtapes', $config['per_page'], $this->uri->segment(3));
        $d['pagi_links'] = $this->pagination->create_links();

        $d['get_articles'] = $this->Blog_model->getArticles($config['per_page'], $this->uri->segment(3));

        $d['title'] = 'The Gbaam Blog';
       // $d['get_articles'] = $this->Blog_model->getArticles();
        
        $this->load->view('blog/blog_home',$d);
    } 

    public function view($slug)
    {
        // if empty redirect home
        if($slug == '')
            redirect('blog');
        
        $d['social_buttons'] = TRUE;
        $d['page_type'] = 'article';

        $article = $this->Blog_model->getArticle($slug);

        if($article == FALSE)
        {
            show_404();
        }
        else
        {
            foreach($article as $g)
            {
                $d['id'] = $g->id;
                $art_id = $g->id;
                $d['title'] = $g->title;
                $d['body'] = $g->body;
                $d['image'] = $g->image;
                $d['author'] = $g->author;
                $d['permalink'] = $g->permalink;
                $d['date'] = $g->date_created;
            

                // add a view when someone views this video
                $this->Site_model->addview($art_id);

                
                // disqus stuff
                $d['my_identifier'] = 'blog-'.$art_id; // this is for disqus
                $d['disqus_url'] = base_url().'blog/'.$d['permalink'];

                $this->load->view('blog/blog_view',$d);
            }
        }
    }
    
}

?>
