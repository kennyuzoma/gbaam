<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */
class Chunes extends CI_Controller {

    function __construct()
    {
        parent::__construct();
		//$this->output->enable_profiler(TRUE);
		
		// go thru all these and redirect to the correct URL
		switch($this->uri->segment(1)){
			case 'song':
			case 'songs':
			case 'chune':
				redirect('chunes/'); // include uri string later?
				break;
		}

		// clean this up later... 
		// If user has not entered their email address for twitter registration....
		$user_status = $this->session->userdata('u_status');
		if(isset($user_status) && $user_status == '4')
			redirect('main/twitter_finish');

    }
    
    public function index()
    {
        $d['title'] = 'Chunes';
        $d['meta_desc'] = "Listen and Download the latest tracks from your favorite artists. Updated Daily.";

        if($this->Songs_model->isSOTWavail() == TRUE)
        {
	        $d['getSotw'] = $this->Songs_model->getSotw();
			foreach($this->Songs_model->getSotw() as $g)
				$sotwid =  $g->sid;

			
			$getsng = $this->Songs_model->get_vote_final($sotwid);
			$d['isScore'] = $getsng['isScore'];
			$d['final_score'] = $getsng['final_score'];
        }

        // get the songs
		$thesongs = $this->Songs_model->getAllSongs_2013(8);
		$d['getSongs'] = $thesongs['result'];
		$d['getSongsCount'] = $thesongs['count'];

        
		//$getLatestMix = $this->Songs_model->getLatestMix($latest);
        $this->load->view('songs/songs_home',$d);
    }
    
	public function song($sname = '',$permalink = '')
	{
		if($sname == '' || $permalink == '')
			redirect('songs');

		// this is if the url is already hashed
		$d['page_type'] = 'song';

		$getSong = $this->Songs_model->getSong($sname,$permalink);
		if($getSong == FALSE)
		{
			show_404();
		}
		else
		{
			foreach($getSong as $gm)
			{
				$d['songid'] = $gm->id;
				$d['title'] = $gm->title .' - '. $gm->artist;
				$d['song_ext'] = str_replace('.', '', $gm->song_ext);	
				$d['desc'] = $gm->desc;
				$d['a_id'] = $gm->artist;
				$d['front_cover'] = $gm->front_cover;
				$d['status'] = $gm->songstatus;
				$d['v_count'] = $gm->v_count; 
				$d['d_count'] = $gm->d_count;
				$d['img_hash'] = $gm->img_hash;
				$d['date_created'] = $gm->date_created;
				$d['song_file'] = base_url().$this->config->item('songs_dir').$gm->song_new_filename;
				$d['song_hash'] = $gm->song_hash;
				$d['permalink'] = $gm->permalink;
			}

			$this->Songs_model->addview('',$d['songid']);
			$getmix = $this->Songs_model->get_vote($d['songid']);
			$my_score = $getmix['my_score'];
			$total_score = $getmix['total_score'];

			if($total_score == 0)
			{
				$d['isScore'] = FALSE;
			}
			else
			{
				$d['isScore'] = TRUE;
				$percent = ($my_score / $total_score) * 10;
				$percent = round($percent);
				$d['final_score'] = $percent;
			}

			// disqus stuff
			$d['my_identifier'] = 'chune-'.$d['songid']; // this is for disqus
			$d['disqus_url'] = base_url().'chunes/'.$d['songid'].'/'.$d['permalink'];

	        $this->load->view('songs/song_view',$d);	
	    }
	}
	
	public function all()
	{
		$d['title'] = 'All Chunes';
		$d['meta_desc'] = "Browse all of the songs on Gbaam!";

		$this->load->library('pagination');

		$config['base_url'] = base_url().'/chunes/all';
		$config['total_rows'] = $this->db->get('songs')->num_rows();
		$config['per_page'] = 16;
		$config['num_links'] = 10;
		$config['full_tag_open'] = '<div class="pagination2">';
		$config['full_tag_close'] = '</div>';
		
		$this->pagination->initialize($config);
		
		//$d['getSongs'] = $this->db->get('mixtapes', $config['per_page'], $this->uri->segment(3));
		$d['pagi_links'] = $this->pagination->create_links();

		$d['getSongs'] = $this->Songs_model->getAllSongs($config['per_page'], $this->uri->segment(3));
	
		$this->load->view('songs/songs_all',$d);	
	}

	public function d($mobile_hash = '')
	{
		if($mobile_hash != '')
		{
			$hash = $mobile_hash;
		}
		else
		{
			$hash = $this->input->post('song');
		}

		if($hash == '')
		{
			redirect('chunes');
		}
		else
		{
			$s = $this->Songs_model->getSong_by_hash($hash);

			$this->Songs_model->addview('d',$s->id);	
			
			$file = './'.$this->config->item('songs_dir').$s->song_new_filename;

			$this->load->helper('download');
			$data = file_get_contents($file); // Read the file's contents
			$name = $s->song_new_filename;

			force_download($name, $data); 
		}
	}

	public function download($id)
	{
		if($id != '')
		{

			foreach($this->Songs_model->getSong($id) as $g)
			{
				$title = $g->title;
				$artist = $g->artist;
				$loc = $g->folder;
				$zip = $g->zip_file;	
			}


			$file = base_url().$this->config->item('mixtape_zip_dir').$zip;

			//echo $file;
	 
			 if(!$file)
			 {  
			     // File doesn't exist, output error
			     die('file not found');
			 }
			 else
			 {

			     header('Location: '.$file);
			 }
		}
		else
		{
			redirect('mixtapes');
		}
		
	}
	
	public function add_vote($songid,$choice)
	{
		if($songid != '' || $choice != '')
		{
			$vote = $this->Songs_model->add_vote($songid,$choice);

			if($vote == TRUE)
			{
				$status['status'] = 'good';
			}
			if($vote == FALSE)
			{
				$status['status'] = 'bad';
			}

			echo json_encode($status);
		}

	}


	
}

?>
