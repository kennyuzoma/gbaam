<?php

/**
* 
*/
class Articles extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();

		$this->load->model('Blog_model');

		$this->load->helper('inflector');
		
		admin_show_errors();

		if(!$this->session->userdata('admin_logged_in'))
			redirect($this->config->item('admin_location'));

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'articles') == FALSE)
			redirect($this->config->item('admin_location'));
	}

	public function index_new()
	{
		$d['title'] = 'New Article';
		

		if($this->input->post('submit'))
		{
			$d['validation'] = $this->form_validation->run('admin_article');

			if($d['validation'] == FALSE )
			{
				$d['message'] = validation_errors();
			}
			else
			{
				// insert the data
				$title = $this->input->post('title');
				$body = $this->input->post('body');
				$author = $this->input->post('author');
				$author_id = $this->input->post('author_id');
				$permalink = $this->input->post('permalink');
				$tags = $this->input->post('tags');
				$posted_by_gbaam = $this->input->post('posted_by_gbaam');


				$this->Admin_model->insertArticle($title,$body, $author_id, $author, $permalink, $tags, $posted_by_gbaam);

				//get the inserted id
				$ins_id = $this->db->insert_id();

				// upload the photo
				$config['upload_path'] = "./data/homeimg";
				$config['allowed_types'] = '*';
				$config['max_size'] = 0;
				$this->load->library('upload', $config);
				
				$type = 'article';

				if ( !$this->upload->do_upload('lg_img'))	
				{
					//$error = array('error' => $this->upload->display_errors());
					//echo print_r($error);
				}
				else	
				{

					// Get photo data
					$photodata = array('upload_data' => $this->upload->data());

					//send to get the image manipulated
					$photo_id = $this->Photo_model->actual_upload($type,$ins_id,$photodata);

					$ins_data['image'] = $photo_id;

					$this->db->where('id' , $ins_id);
					$this->db->update('articles',$ins_data);


				}

				redirect($this->admin_location . '/articles/list/success');
			}

			

		}

		$this->load->view('admin_new/articles_new', $d);

		

	}

	public function edit($id)
	{
		if($this->Admin_model->is_this_my_post('articles',$id) == FALSE)
			redirect($this->admin_location);

		$d['title'] = 'Edit Article';

		$d['get_article'] = $this->Site_model->getArticle_admin($id);
		

		if($this->input->post('submit') || $this->input->post('submit_deny'))
		{
			$d['validation'] = $this->form_validation->run('admin_article');

			if($d['validation'] == FALSE )
			{
				$d['message'] = validation_errors();
			}
			else
			{
				// insert the data
				$title = $this->input->post('title');
				$body = $this->input->post('body');
				$author = $this->input->post('author');
				$author_id = $this->input->post('author_id');
				$permalink = $this->input->post('permalink');
				$tags = $this->input->post('tags');
				$status = $this->input->post('status');
				$posted_by_gbaam = $this->input->post('posted_by_gbaam');

				$this->Admin_model->updateArticle($id,$title,$body,$author,$author_id,$permalink, $tags, $status, $posted_by_gbaam);

				// upload the photo
				$config['upload_path'] = "./data/homeimg";
				$config['allowed_types'] = '*';
				$config['max_size'] = 0;
				$this->load->library('upload', $config);

				$type = 'article';

				if ( !$this->upload->do_upload('lg_img'))	
				{
					//$error = array('error' => $this->upload->display_errors());
					//echo print_r($error);
				}
				else	
				{

					// Get photo data
					$photodata = array('upload_data' => $this->upload->data());

					//send to get the image manipulated
					$photo_id = $this->Photo_model->actual_upload($type,$id,$photodata);

					$ins_data['image'] = $photo_id;

					$this->db->where('id' , $id);
					$this->db->update('articles',$ins_data);


				}

				if($this->input->post('submit_deny'))
					redirect($this->admin_location . '/articles/review/success');
				else
					redirect($this->admin_location . '/articles/edit/'.$id.'/success');
			}
			
		}

		$this->load->view('admin_new/articles_edit', $d);
	}

	public function delete()
	{
		if($this->input->is_ajax_request())
			$this->Admin_model->deleteArticle($id);
	}

	public function review()
	{
		if($this->session->userdata('admin_type') > '4')
			redirect($this->admin_location);
		
		$d['title'] = 'Review Articles';
		$d['get_articles'] = $this->Site_model->getArticlesToReview_admin();
		$this->load->view('admin_new/articles_list', $d);
	}


	public function articles_list()
	{
		$d['title'] = 'All Articles';
		$d['get_articles'] = $this->Site_model->getArticles_admin();
		$this->load->view('admin_new/articles_list', $d);
	}
	

	public function featarticles($slug, $type, $slot='',$article_id = '')
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect($this->config->item('admin_location'));

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'articles') == FALSE)
			redirect($this->config->item('admin_location'));
		
		$d['type'] = $type;

		if($type == 'article')
		{
			$d['title'] = '3 Featured Articles';
			$d['limit'] = 3;
		}

		// List all the articles
		if($slug == 'list')
		{
			$this->load->view('admin_new/featarticles_list', $d);
		}

		if($slug == 'add')
		{
			$d['title'] = $d['title']." Add";

			$d['slot'] = $slot;
			$d['get_articles'] = $this->Site_model->getArticles();
			$this->load->view('admin_new/featarticles_add', $d);
		}

		if($slug == 'addconf')
		{
			$this->Site_model->AddToSlot($type, $slot, $article_id);
			redirect($this->admin_location . '/featarticles/list/'.$type.'/success');
		}

	}

	public function blog($slug,$id = '')
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect($this->config->item('admin_location'));

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'blog') == FALSE)
			redirect($this->config->item('admin_location'));
		
		if($slug == 'new')
		{
			$d['title'] = 'New Blog Post';
			

			if($this->input->post('submit'))
			{
				$d['validation'] = $this->form_validation->run('admin_article');

				if($d['validation'] == FALSE )
				{
					$d['message'] = validation_errors();
				}
				else
				{
					// insert the data
					$title = $this->input->post('title');
					$body = $this->input->post('body');
					$author = $this->input->post('author');
					$permalink = $this->input->post('permalink');
					$tags = $this->input->post('tags');


					$this->Admin_model->insertBlogPost($title,$body, $author, $permalink, $tags);

					//get the inserted id
					$ins_id = $this->db->insert_id();

					// upload the photo
					$config['upload_path'] = "./data/homeimg";
					$config['allowed_types'] = '*';
					$config['max_size'] = 0;
					$this->load->library('upload', $config);

					$type = 'blog';

					if ( !$this->upload->do_upload('lg_img'))	
					{
						//$error = array('error' => $this->upload->display_errors());
						//echo print_r($error);
					}
					else	
					{

						// Get photo data
						$photodata = array('upload_data' => $this->upload->data());

						//send to get the image manipulated
						$photo_id = $this->Photo_model->actual_upload($type,$ins_id,$photodata);

						$ins_data['image'] = $photo_id;

						$this->db->where('id' , $ins_id);
						$this->db->update('blog_posts',$ins_data);


					}

					redirect($this->admin_location . '/blog/list/success');
				}

			}

			$this->load->view('admin_new/blogpost_new', $d);

			

		}

		if($slug == 'edit')
		{
			if($this->Admin_model->is_this_my_post('blog',$id) == FALSE)
				redirect($this->admin_location);

			$d['title'] = 'Edit Blog Post';

			$d['get_article'] = $this->Blog_model->getArticle_admin($id);
			

			if($this->input->post('submit'))
			{
				$d['validation'] = $this->form_validation->run('admin_article');

				if($d['validation'] == FALSE )
				{
					$d['message'] = validation_errors();
				}
				else
				{
					// insert the data
					$title = $this->input->post('title');
					$body = $this->input->post('body');
					$author = $this->input->post('author');
					$permalink = $this->input->post('permalink');
					$tags = $this->input->post('tags');
					$status = $this->input->post('status');

					$this->Admin_model->updateBlogPost($id,$title,$body,$author, $permalink, $tags, $status);


					// upload the photo
					$config['upload_path'] = "./data/homeimg";
					$config['allowed_types'] = '*';
					$config['max_size'] = 0;
					$this->load->library('upload', $config);
	
					$type = 'blog';

					if ( !$this->upload->do_upload('lg_img'))	
					{
						//$error = array('error' => $this->upload->display_errors());
						//echo print_r($error);
					}
					else	
					{

						// Get photo data
						$photodata = array('upload_data' => $this->upload->data());

						//send to get the image manipulated
						$photo_id = $this->Photo_model->actual_upload($type,$id,$photodata);

						$ins_data['image'] = $photo_id;

						$this->db->where('id' , $id);
						$this->db->update('blog_posts',$ins_data);


					}

					redirect($this->admin_location . '/blog/edit/'.$id.'/success');

				}
				
			}
			$this->load->view('admin_new/blogpost_edit', $d);
		}

		if($slug == 'delete')
		{
			if($this->input->is_ajax_request())
				$this->Admin_model->deleteBlogPosts($id);
		}

		if($slug == 'list')
		{
			$d['title'] = 'All Blog Posts';
			$d['get_articles'] = $this->Blog_model->getArticles_admin();
			$this->load->view('admin_new/blogpost_list', $d);
		}
	}

	public function newpermalinks_art()
	{
		$sql = "SELECT * FROM articles";
		$query = $this->db->query($sql);

		foreach($query->result() as $q)
		{
			$url = clean_url_slug($q->title);
			$sql = "UPDATE articles SET  permalink =  '{$url}' WHERE  id =$q->id";
			$this->db->query($sql);
		}
	}

}