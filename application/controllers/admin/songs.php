<?php

/**
* 
*/
class Songs extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();

		$this->load->helper('inflector');
		
		admin_show_errors();

		if(!$this->session->userdata('admin_logged_in'))
			redirect($this->config->item('admin_location'));

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'songs') == FALSE)
			redirect($this->config->item('admin_location'));
	}
/*
		// Set the song id from the 2nd segment
		$d['song_id'] = $song_id;

		// Get all song info if a $song_id is avail
		if($song_id != '')
		{
			$song = $this->Songs_model->getSong_new_admin($song_id);
			$d['song'] = $song;
		}
*/
	// New song
	public function index_new()
	{
		$d['title'] = 'New Song';
		
		$date = date("Y-m-d H:i:s", strtotime($this->input->post('date')));

		if($this->input->post('submit'))
		{
			$d['validation'] = $this->form_validation->run('admin_song');

			if($d['validation'] == FALSE )
			{
				$d['message'] = validation_errors();
			}
			else
			{	
				$song_id = $this->Admin_model->insertSong();

				$this->upload_covers_songs($song_id);
				$this->upload_song($song_id);

				redirect($this->admin_location . '/songs/list');
			}
		}

		$this->load->view('admin_new/song_new', $d);
	}
	

	//Editing the song
	public function edit()
	{
		if($this->Admin_model->is_this_my_post('songs',$song_id) == FALSE)
			redirect($this->admin_location);

		if($song_id == '')
		{
			redirect($this->admin_location . '/songs/list');
		}

		$d['title'] = 'Edit Song - "<b>'.$song->title.'</b>"';


		if($this->input->post('submit'))
		{	
			$d['validation'] = $this->form_validation->run('admin_song');

			if($d['validation'] == FALSE )
			{
				$d['message'] = validation_errors();
			}
			else
			{				
				$this->Admin_model->updateSong($song_id);

				$this->upload_covers_songs($song_id);
				$this->upload_song($song_id,'edit');

				redirect($this->admin_location . '/songs/edit/'.$song_id.'/success');
			}
			
		}

		$this->load->view('admin_new/song_edit', $d);

	}

	// List all the mixtapes
	public function songs_list()
	{
		$d['get_songs'] = $this->Songs_model->getAllSongs_admin();
		$d['title'] = 'All Songs';
		$this->load->view('admin_new/song_list', $d);
	}

	// delete songs
	public function delete($song_id)
	{
		if($this->input->is_ajax_request())
			$this->Songs_model->deleteSong($song_id);
	}

	public function sotw()
	{
		$d['get_sotw'] = $this->Songs_model->getSotw();
		$d['title'] = 'Sotw';
		$this->load->view('admin_new/song_sotw', $d);
	}

	public function select_sotw()
	{
		$d['get_songs'] = $this->Songs_model->getAllSongs();
		$d['title'] = 'All Songs';
		$this->load->view('admin_new/song_list', $d);
	}

	public function make_sotw($song_id)
	{
		$this->Songs_model->AddToSlot('1','sotw',$song_id);
	}

	public function remove_sotw($song_id)
	{
		$this->Songs_model->remove_sotw($song_id);
	}


	public function upload_covers_songs($song_id)
	{

		$type = 'song';
		$song = $this->Songs_model->getSong_new_admin($song_id);

		
		$config3['upload_path'] = "./".$this->config->item('uploaded_images_folder').$type;
		$config3['allowed_types'] = 'jpeg|jpg|png|gif';
		$config3['max_size']	= '10000';
		$config3['max_width']  = '10240';
		$config3['max_height']  = '7680';

		//$this->load->library('upload', $config);
		$this->upload->initialize($config3);

		if ( ! $this->upload->do_upload('ft_cover'))
		{
			// Only Uncomment for error checking
			//$error = $this->upload->display_errors();
			//var_dump($this->upload->data());
			//var_dump($error); 
		}
		else
		{		
			// Get photo data
			$photodata = array('upload_data' => $this->upload->data());

			//delete the old song cover
			$this->Photo_model->delete_image('songs',$song->front_cover);
			

			//send to get the image manipulated
			$photo_id = $this->Photo_model->actual_upload($type,$song_id,$photodata);

			$data['front_cover'] = $photo_id;

			$this->db->where('id' , $song_id);
			$this->db->update('songs',$data);


		}

	}

	public function upload_song($song_id,$action = '')
	{
		if($action == 'edit')
		{
			// rename song just incase
			$sql = "SELECT * FROM songs where id = ".$song_id;
			$query = $this->db->query($sql);
			$row = $query->row();

			$song_file = "./".$this->config->item('songs_dir').$row->song_new_filename;
			
			$newname = $row->artist.' - '.$row->title.$row->song_ext;
			rename($song_file,"./".$this->config->item('songs_dir').$newname);

			$data['song_new_filename'] = $newname;
			$this->db->where('id', $song_id);
			$this->db->update('songs', $data); 

		}

		$config2['upload_path'] = "./".$this->config->item('songs_dir'); 
		$config2['allowed_types'] = 'mp3|wav|ogg';
		$config2['max_size'] = '0';
		//$this->load->library('upload', $config2);
		
		$this->upload->initialize($config2);


		if ( ! $this->upload->do_upload('mp3_file'))
		{
			// Only Uncomment for error checking
			$error = $this->upload->display_errors();
			//var_dump($this->upload->data());
			
			$d['error'] = $error; 
			$d['status'] = 'fail';

			//print_r($d['error']);
		}
		else
		{
			// search for old song and delete it
			$sql = "SELECT * FROM songs where id = ".$song_id;
			$query = $this->db->query($sql);
			$row = $query->row();

			if($row->song_new_filename != '')
			{
				$song_file = "./".$this->config->item('songs_dir').$row->song_new_filename;
				if(file_exists($song_file))
					unlink($song_file);
			}
			

			// Get upload data
			$data = $this->upload->data('mp3_file');
			//print_r($data);

			$song_hash = md5($data['file_name'].date('Y-m-d H:i:s'));
			//$newname = $config['upload_path'].'/'.$img_hash.'.mp3';
			$newname = $row->artist.' - '.$row->title.$data['file_ext'];
			rename($data['full_path'],$config2['upload_path'].'/'.$newname);

			$data = array(
				'song_original_filename' => $data['file_name'],
				'song_new_filename' => $newname,
				'song_hash' => $song_hash,
				'song_ext' => $data['file_ext']
			);

			$this->db->where('id',$song_id);
			$this->db->update('songs',$data);

			$d['status'] = 'success';
		} 

		//return $d;
	}
}