<?php

/**
* 
*/
class Slides extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();

		$this->load->helper('inflector');
		
		admin_show_errors();

		if(!$this->session->userdata('admin_logged_in'))
			redirect($this->config->item('admin_location'));

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'slides') == FALSE)
			redirect($this->config->item('admin_location'));
	}

	public function index_new()
	{
		$d['title'] = 'New Slide';
		
		if($this->input->post('submit'))
		{
			$d['validation'] = $this->form_validation->run('admin_slide');

			if($d['validation'] == FALSE )
			{
				$d['message'] = validation_errors();
			}
			else
			{
				// insert the data
				$link = $this->input->post('link');
				$alt_text = $this->input->post('alt_text');

				$this->Admin_model->insertSlide($link,$alt_text);

				//get the inserted id
				$ins_id = $this->db->insert_id();

				// upload the photo
				$config['upload_path'] = "./data/homeimg";
				$config['allowed_types'] = '*';
				$config['max_size'] = 0;
				
				$this->upload->initialize($config);

				if ( ! $this->upload->do_upload('lg_img'))
				{
					/*$error = array('error' => $this->upload->display_errors());
					echo print_r($error);*/
				}
				else
				{
					
					$data = $this->upload->data();

					$img_hash = md5($data['file_name'].date('Y-m-d H:i:s'));
					$newname = $config['upload_path'].'/'.$img_hash.'.jpg';
					rename($data['full_path'],$newname);

					$ins_data = array(
						'image' => 'data/homeimg/'.$img_hash.'.jpg',
						'date_created' => date('Y-m-d H:i:s')
						);

					$this->db->where('id' , $ins_id);
					$this->db->update('slides',$ins_data);

					
				}

				redirect($this->admin_location . '/slides/list/success');
			}

		}

		$this->load->view('admin_new/slides_new', $d);

	}

	public function edit($id)
	{
		$d['title'] = 'Edit Slide';

		$d['get_slide'] = $this->Site_model->getSlide($id);
		

		if($this->input->post('submit'))
		{
			$d['validation'] = $this->form_validation->run('admin_slide');

			if($d['validation'] == FALSE )
			{
				$d['message'] = validation_errors();
			}
			else
			{
				// insert the data
				$link = $this->input->post('link');
				$alt_text = $this->input->post('alt_text');
				$status = $this->input->post('status');

				$this->Admin_model->updateSlide($id,$link,$alt_text,$status);


				// upload the photo
				$config['upload_path'] = "./data/homeimg";
				$config['allowed_types'] = '*';
				$config['max_size'] = 0;
				
				$this->upload->initialize($config);

				if ( ! $this->upload->do_upload('lg_img'))
				{
					/*$error = array('error' => $this->upload->display_errors());
					echo print_r($error);*/
				}
				else
				{
					
					$data = $this->upload->data();

					$img_hash = md5($data['file_name'].date('Y-m-d H:i:s'));
					$newname = $config['upload_path'].'/'.$img_hash.'.jpg';
					rename($data['full_path'],$newname);

					$ins_data = array(
						'image' => 'data/homeimg/'.$img_hash.'.jpg'
						);

					$this->db->where('id' , $id);
					$this->db->update('slides',$ins_data);

					
				}
				redirect($this->admin_location . '/slides/list/success');
			}
		}

		$this->load->view('admin_new/slides_edit', $d);
	}

	public function delete($id)
	{
		if($this->input->is_ajax_request())
			$this->Admin_model->deleteSlide($id);
	}

	public function updateorder()
	{
		$this->Admin_model->updateSlideOrder();
	}

	public function slidelist()
	{
		$d['title'] = 'All Slides';
		$d['get_slides'] = $this->Site_model->getSlides_admin();
		$this->load->view('admin_new/slides_list', $d);
	}

}