<?php

/**
* 								
*/
class Users extends MY_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->helper('inflector');
		
		admin_show_errors();

		if(!$this->session->userdata('admin_logged_in'))
			redirect($this->config->item('admin_location'));

		$admin_type = $this->session->userdata('admin_type');
	}		

	public function index_new()
	{
		if(($this->admin_type != '1') && ($this->admin_type != '2'))
			redirect($this->config->item('admin_location'));

		$d['title'] = 'New User';
		
		if($this->input->post('submit'))
		{
			$post_admin_type = $this->input->post('admin_type');
			$email = $this->input->post('email');
			$username = $this->input->post('username');
			$password1 = $this->input->post('password');
			$password2 = $this->input->post('password2');

			$this->form_validation->set_rules('firstname','First Name','required');
			$this->form_validation->set_rules('admin_type', '', '');

			if(($post_admin_type == '1' || $post_admin_type == '2'))
			{
				$this->form_validation->set_rules('username','Username','required|callback__doesUserExist_username');
				$this->form_validation->set_rules('email','Email','required|callback__doesUserExist_email');
				$this->form_validation->set_rules('password', 'Password', 'required|matches[password2]');
				$this->form_validation->set_rules('password2', 'Password Confirmation', 'required');

			}
			else
			{
				// if these things were typed
				if($password1 != '' || $email != '' || $username != '')
				{
					$this->form_validation->set_rules('username','Username','required|callback__doesUserExist_username');
					$this->form_validation->set_rules('email','Email','required|callback__doesUserExist_email');
					$this->form_validation->set_rules('password', 'Password', 'required|matches[password2]');
					$this->form_validation->set_rules('password2', 'Password Confirmation', 'required');
				}
			}

			$d['validation'] = $this->form_validation->run();

			if($d['validation'] == TRUE )
			{
				// insert the data
				$this->Admin_model->insert_user();

				$this->upload_profilepic($this->db->insert_id());

				redirect($this->admin_location . '/users/list/success');
			}
			else
			{
				$d['message'] = validation_errors();
			}


		}

		$this->load->view('admin_new/users_new', $d);

	}

	public function edit($id)
	{
		if(($this->admin_type != '1') && ($this->admin_type != '2'))
			if($id != $this->session->userdata('admin_user_id'))
				redirect($this->config->item('admin_location'));

		$d['title'] = 'Edit User';

		$d['g'] = $this->Admin_model->get_user($id);
		$d['user_id'] = $id;
		$user_id = $id;
		

		if($this->input->post('submit'))
		{
			$email = $this->input->post('email');
			$username = $this->input->post('username');
			$password1 = $this->input->post('password');
			$password2 = $this->input->post('password2');

			$sql = "SELECT * FROM admin where id = ".$user_id;
			$query = $this->db->query($sql);

			$row = $query->row();

			$this->form_validation->set_rules('firstname','First Name','required');
			
			// if an email address wasnt entered 
			if($email == '')
			{
				if($row->email != '' && $row->admin_type != '6')
				{
					$this->form_validation->set_rules('email','Email','required|valid_email|callback__doesUserExist_email');
				}
			}

			// if an email address was entered
			if($email != '')
			{
				// if this user had none of these
				if($row->email == '' && $row->password == '')
				{
					$this->form_validation->set_rules('email','Email','required|valid_email|callback__doesUserExist_email');
					$this->form_validation->set_rules('password', 'Password', 'required|matches[password2]');
					$this->form_validation->set_rules('password2', 'Password Confirmation', 'required');
				}
				
				// if this user wants to just change their email address
				if($row->email != '' && $row->password != '')
				{
					$this->form_validation->set_rules('email','Email','required|valid_email|callback__doesUserExist_email');
				}

				else
				{
					$this->form_validation->set_rules('email','Email','required|valid_email|callback__doesUserExist_email');
				}



			}


			// if password was entered
			if($password1 != '')
			{
				$this->form_validation->set_rules('email','Email','required|valid_email|callback__doesUserExist_email');
				$this->form_validation->set_rules('password', 'Password', 'required|matches[password2]');
				$this->form_validation->set_rules('password2', 'Password Confirmation', 'required');
			}

			// if username was entered
			if($username != '')
				$this->form_validation->set_rules('username','Username','required|callback__doesUserExist_username');
			

			$d['validation'] = $this->form_validation->run();

			if($d['validation'] == TRUE )
			{
				// insert the data
				$this->Admin_model->update_user($id);
				$this->upload_profilepic($id);

				redirect($this->admin_location . '/users/edit/'.$id.'/success');
			}
			else
			{
				$d['message'] = validation_errors();
			}


		}

		$this->load->view('admin_new/users_edit', $d);
	}

	public function view($id)
	{
		$d['title'] = 'View User';
		$d['user_id'] = $id;

		$d['g'] = $this->Admin_model->get_user($id);

		$this->load->view('admin_new/users_view', $d);
	}



	public function delete($id)
	{
		if($this->input->is_ajax_request())
			$this->Admin_model->deleteAdmin($id);
	}

	public function mixtape_list()
	{
		$d['title'] = 'All Users';
		$d['get_users'] = $this->Admin_model->get_users('noguest');
		$this->load->view('admin_new/users_list', $d);
	}

	public function listguests()
	{
		$d['title'] = 'All Guest Authors';
		$d['get_users'] = $this->Admin_model->get_users('guest');
		$this->load->view('admin_new/users_list', $d);
	}
	

	public function site_users($slug,$id = '')
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect($this->config->item('admin_location'));

		// users access
		
		if($slug == 'new')
		{
			$d['title'] = 'New User';
			
			if($this->input->post('submit'))
			{
				$post_admin_type = $this->input->post('admin_type');
				$email = $this->input->post('email');
				$username = $this->input->post('username');
				$password1 = $this->input->post('password');
				$password2 = $this->input->post('password2');

				$this->form_validation->set_rules('firstname','First Name','required');
				$this->form_validation->set_rules('admin_type', '', '');

				if(($post_admin_type == '1' || $post_admin_type == '2'))
				{
					$this->form_validation->set_rules('username','Username','required|callback__doesUserExist_username');
					$this->form_validation->set_rules('email','Email','required|callback__doesUserExist_email');
					$this->form_validation->set_rules('password', 'Password', 'required|matches[password2]');
					$this->form_validation->set_rules('password2', 'Password Confirmation', 'required');

				}
				else
				{
					// if these things were typed
					if($password1 != '' || $email != '' || $username != '')
					{
						$this->form_validation->set_rules('username','Username','required|callback__doesUserExist_username');
						$this->form_validation->set_rules('email','Email','required|callback__doesUserExist_email');
						$this->form_validation->set_rules('password', 'Password', 'required|matches[password2]');
						$this->form_validation->set_rules('password2', 'Password Confirmation', 'required');
					}
				}

				$d['validation'] = $this->form_validation->run();

				if($d['validation'] == TRUE )
				{
					// insert the data
					$this->Admin_model->insert_user();

					$this->upload_profilepic($this->db->insert_id());

					redirect($this->admin_location . '/site_users/list/success');
				}
				else
				{
					$d['message'] = validation_errors();
				}


			}

			$this->load->view('admin_new/users_new', $d);

		}

		if($slug == 'edit')
		{
			$d['title'] = 'Edit User';

			$d['g'] = $this->Admin_model->get_user($id);
			$d['user_id'] = $id;
			$user_id = $id;
			

			if($this->input->post('submit'))
			{
				$email = $this->input->post('email');
				$username = $this->input->post('username');
				$password1 = $this->input->post('password');
				$password2 = $this->input->post('password2');

				$sql = "SELECT * FROM admin where id = ".$user_id;
				$query = $this->db->query($sql);

				$row = $query->row();

				$this->form_validation->set_rules('firstname','First Name','required');
				
				// if an email address wasnt entered 
				if($email == '')
				{
					if($row->email != '' && $row->admin_type != '6')
					{
						$this->form_validation->set_rules('email','Email','required|valid_email|callback__doesUserExist_email');
					}
				}

				// if an email address was entered
				if($email != '')
				{
					// if this user had none of these
					if($row->email == '' && $row->password == '')
					{
						$this->form_validation->set_rules('email','Email','required|valid_email|callback__doesUserExist_email');
						$this->form_validation->set_rules('password', 'Password', 'required|matches[password2]');
						$this->form_validation->set_rules('password2', 'Password Confirmation', 'required');
					}
					
					// if this user wants to just change their email address
					if($row->email != '' && $row->password != '')
					{
						$this->form_validation->set_rules('email','Email','required|valid_email|callback__doesUserExist_email');
					}

					else
					{
						$this->form_validation->set_rules('email','Email','required|valid_email|callback__doesUserExist_email');
					}



				}


				// if password was entered
				if($password1 != '')
				{
					$this->form_validation->set_rules('email','Email','required|valid_email|callback__doesUserExist_email');
					$this->form_validation->set_rules('password', 'Password', 'required|matches[password2]');
					$this->form_validation->set_rules('password2', 'Password Confirmation', 'required');
				}

				// if username was entered
				if($username != '')
					$this->form_validation->set_rules('username','Username','required|callback__doesUserExist_username');
				

				$d['validation'] = $this->form_validation->run();

				if($d['validation'] == TRUE )
				{
					// insert the data
					$this->Admin_model->update_user($id);
					$this->upload_profilepic($id);

					redirect($this->admin_location . '/users/edit/'.$id.'/success');
				}
				else
				{
					$d['message'] = validation_errors();
				}


			}

			$this->load->view('admin_new/users_edit', $d);
		}

		if($slug == 'view')
		{
			$d['title'] = 'View User';
			$d['user_id'] = $id;

			$d['g'] = $this->Admin_model->get_user($id);

			$this->load->view('admin_new/users_view', $d);
		}



		if($slug == 'delete')
		{
			if($this->input->is_ajax_request())
				$this->Admin_model->deleteAdmin($id);
		}

		if($slug == 'list')
		{
			$d['title'] = 'All Users';
			//$d['get_users'] = $this->Admin_model->get_users('noguest');
			$this->load->view('admin_new/site_users_list', $d);
		}

		if($slug == 'listguests')
		{
			$d['title'] = 'All Guest Authors';
			$d['get_users'] = $this->Admin_model->get_users('guest');
			$this->load->view('admin_new/users_list', $d);
		}
	}

	public function ga_register()
	{
		$d['title'] = 'Guest Author Registration';
		

		if($this->input->post('submit'))
		{
			$this->form_validation->set_rules('firstname', 'First Name', 'required');
			$this->form_validation->set_rules('lastname', 'Last Name', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback__doesUserExist_email');
			$this->form_validation->set_rules('username','Username','required|callback__doesUserExist_username');
			$this->form_validation->set_rules('password', 'Password', 'required|matches[password2]');
			$this->form_validation->set_rules('password2', 'Password Confirmation', 'required');

			$d['validation'] = $this->form_validation->run();

			if($d['validation'] == TRUE )
			{
				$_POST['admin_type'] = '6';

				// insert the data
				$this->Admin_model->insert_user();

				redirect($this->admin_location . '?success=true');
			}
			else
			{
				$d['message'] = validation_errors();
			}


		}

		$this->load->view('admin_new/users_new_ga', $d);

	}

	public function upload_profilepic($user_id)
	{
		$type = 'admin_user';

		$config['upload_path'] = "./".$this->config->item('uploaded_images_folder').$type;
		$config['allowed_types'] = 'jpeg|jpg|png|gif';
		$config['max_size']	= '10000';
		$config['max_width']  = '10240';
		$config['max_height']  = '7680';

		$this->upload->initialize($config); 

		if ( $this->upload->do_upload('lg_img'))
		{		
			// Get photo data
			$photodata = array('upload_data' => $this->upload->data());

			//send to get the image manipulated
			$photo_id = $this->Photo_model->actual_upload($type,$user_id,$photodata);

			$data['profile_img'] = $photo_id;

			$this->db->where('id' , $user_id);
			$this->db->update('admin',$data);


		}
		else{
			//$d['error'] = array('error' => $this->upload->display_errors());
			//print_r($d['error']);
		}
	}

}