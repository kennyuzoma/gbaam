<?php

class Mixtapes extends MY_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->helper('inflector');
		
		admin_show_errors();

		if(!$this->session->userdata('admin_logged_in'))
			redirect($this->config->item('admin_location'));

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'mixtapes') == FALSE)
			redirect($this->config->item('admin_location'));

	}
	
	// New Mixtape
	public function index_new()
	{
		$d['title'] = 'New Mixtape';

		$date = date("Y-m-d H:i:s", strtotime($this->input->post('date')));

		if($this->input->post('submit'))
		{
			$d['validation'] = $this->form_validation->run('admin_mixtape');

			if($d['validation'] == FALSE )
			{
				$d['message'] = validation_errors();
			}
			else
			{
				$mixtape_id = $this->Admin_model->insertMixtape();

				// this is after because i believe it clears the inserted id.... idk
				$this->session->set_flashdata('new_mixtape','1');

				$this->upload_covers($mixtape_id);

				redirect($this->admin_location . '/mixtape/addtracks/'.$mixtape_id);
			}
		}

		$this->load->view('admin_new/mixtape_new', $d);
	}

	//Adding tracks to mixtape
	public function addtracks($mixtape_id)
	{
		// pass the variable the new flash data variable if available
		if($this->session->flashdata('new_mixtape') == '1')
			$this->session->set_flashdata('new_mixtape','1');

		$d['title'] = 'Add Tracks to mixtape - "<b>'.$mixtape->title.'</b>"';
		$this->load->view('admin_new/mixtape_add_tracks', $d);
	}

	//Editing the mixtape
	public function edit($mixtape_id)
	{
		if($this->Admin_model->is_this_my_post('mixtapes',$mixtape_id) == FALSE)
			redirect($this->admin_location);

		// Set the mixtape id from the 2nd segment
		$d['mixtape_id'] = $mixtape_id;

		// Get all mixtape info if a mixtape_id is avail
		if($mixtape_id != '')
		{
			$mixtape = $this->Mixtape_model->getMixtape_new_admin($mixtape_id);
			$d['mixtape'] = $mixtape;

			$mixtape_tracks = $this->Mixtape_model->getMixtapeTracks($mixtape_id);
			$d['tracks_count'] = $mixtape_tracks['count'];
			$d['mixtape_tracks'] = $mixtape_tracks['result'];
		}


		// pass the variable the new flash data variable if available
		if($this->session->flashdata('new_mixtape') == '1')
			$this->session->set_flashdata('new_mixtape','1');


		if($mixtape_id == '')
		{
			redirect($this->admin_location . '/mixtape/list');
		}


		$d['title'] = 'Edit mixtape - "<b>'.$mixtape->title.'</b>"';

		if($this->input->post('submit'))
		{		
			$d['validation'] = $this->form_validation->run('admin_mixtape');

			if($d['validation'] == FALSE )
			{
				$d['message'] = validation_errors();
			}
			else
			{		
				if($this->uri->segment(5) == 'new')
					$this->Admin_model->updateMixtape($mixtape_id,'new');
				else
					$this->Admin_model->updateMixtape($mixtape_id);

				$this->upload_covers($mixtape_id);

				$track_num = $this->input->post('track_num');
				$track_name = $this->input->post('track_name');
				$mixtpe_id = $this->input->post('mixtape_id');

				if($track_num != '')
				{
					foreach($track_num as $k=>$v)
					{
						$sql = "UPDATE mixtape_tracks SET track_num='{$v}' WHERE id='{$k}'";
						$this->db->query($sql);
					}
				}

				if($track_name != '')
				{
					foreach($track_name as $k=>$v)
					{
						$sql = "UPDATE mixtape_tracks SET title='{$v}' WHERE id='{$k}'";
						$this->db->query($sql);
					}
				}
				
				//unset($_SESSION['flash:new:new_mixtape']);

				if($this->uri->segment(5) == 'new')
					$this->create_zip_shell($mixtape_id);

				redirect($this->admin_location . '/mixtape/edit/'.$mixtape_id.'/success');
			}
			
		}


		$this->load->view('admin_new/mixtape_edit', $d);

	}


	// List all the mixtapes
	public function mixtape_list()
	{
		$d['get_mixtapes'] = $this->Mixtape_model->getAllMixtapes_admin();
		$d['title'] = 'All Mixtapes';
		$this->load->view('admin_new/mixtape_list', $d);
	}

	public function motw()
	{
		$d['get_motw'] = $this->Mixtape_model->getMotw();
		$d['title'] = 'Motw';
		$this->load->view('admin_new/mixtape_motw', $d);
	}

	public function select_motw()
	{
		$d['get_mixtapes'] = $this->Mixtape_model->getAllMixtapes();
		$d['title'] = 'All Mixtapes';
		$this->load->view('admin_new/mixtape_list', $d);
	}

	public function delete($mixtape_id)
	{
		if($this->input->is_ajax_request())
			$this->Mixtape_model->deleteMixtape($mixtape_id);
	}

	public function remove_motw($mixtape_id)
	{
		$this->Mixtape_model->deleteMOTW($mixtape_id);
	}

	public function make_motw($mixtape_id)
	{
		$this->Mixtape_model->AddToSlot('1','motw',$mixtape_id);
	}

	public function updatetracksorder()
	{
		$this->Admin_model->updateTracksOrder();
	}

	public function deleteMixtapeTrack($track_id)
	{
		$this->Admin_model->deleteMixtapeTrack($track_id);
	}

	public function hotmixtapes($slug, $slot='', $mixtape_id = '')
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect($this->config->item('admin_location'));

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'mixtapes') == FALSE)
			redirect($this->config->item('admin_location'));
		
		// Set the mixtape id from the 2nd segment
		$d['mixtape_id'] = $mixtape_id;

		// Get all mixtape info if a mixtape_id is avail
		if($mixtape_id != '')
		{
			$mixtape = $this->Mixtape_model->getMixtape_new($mixtape_id);
			$d['mixtape'] = $mixtape;

			$mixtape_tracks = $this->Mixtape_model->getMixtapeTracks($mixtape_id);
			$d['tracks_count'] = $mixtape_tracks['count'];
			$d['mixtape_tracks'] = $mixtape_tracks['result'];
		}

		//Editing the mixtape
		if($slug == 'edit')
		{

			if($mixtape_id == '')
			{
				redirect($this->admin_location . '/mixtape/list');
			}


			$d['title'] = 'Edit mixtape - "<b>'.$mixtape->title.'</b>"';

			

			if($this->input->post('submit'))
			{

				
				$this->Admin_model->updateMixtape($mixtape_id);

				$this->upload_covers($mixtape_id);

				$track_num = $this->input->post('track_num');
				$track_name = $this->input->post('track_name');
				$mixtpe_id = $this->input->post('mixtape_id');

				foreach($track_num as $k=>$v)
				{
					$sql = "UPDATE mixtape_tracks SET track_num='{$v}' WHERE id='{$k}'";
					$this->db->query($sql);
				}

				foreach($track_name as $k=>$v)
				{
					$sql = "UPDATE mixtape_tracks SET title='{$v}' WHERE id='{$k}'";
					$this->db->query($sql);
				}

				redirect($this->admin_location . '/mixtape/edit/'.$mixtape_id.'/success');
				
			}


			$this->load->view('admin_new/hotmixtape_edit', $d);

		}


		// List all the mixtapes
		if($slug == 'list')
		{
			$d['hmixtape1'] = $this->Mixtape_model->getHotMixtape(1);
			$d['hmixtape2'] = $this->Mixtape_model->getHotMixtape(2);
			$d['hmixtape3'] = $this->Mixtape_model->getHotMixtape(3);

			$d['title'] = 'All Mixtapes';
			$this->load->view('admin_new/hotmixtape_list', $d);
		}

		if($slug == 'add')
		{
			$d['title'] = 'Add to slot $slot';
			$d['slot'] = $slot;
			$d['get_mixtapes'] = $this->Mixtape_model->getAllMixtapes();
			$this->load->view('admin_new/hotmixtape_add', $d);
		}

		if($slug == 'add_conf')
		{
			
			$this->Mixtape_model->AddToSlot($slot,'hot_mixtape',$mixtape_id );
			redirect($this->admin_location . '/hotmixtapes/list/success');
		}

		if($slug == 'delete')
		{
			if($this->input->is_ajax_request())
				$this->Mixtape_model->deleteMixtape($mixtape_id);
		}

		if($slug == 'remove_motw')
		{
			$this->Mixtape_model->remove_motw($mixtape_id);
		}
	}

	function create_mixtape_hash()
	{
		$sql = "SELECT * FROM mixtapes";
		$q = $this->db->query($sql);

		foreach($q->result() as $d)
		{
			$upd['hash'] = md5($d->id);
			$this->db->where('id',$d->id);
			$this->db->update('mixtapes',$upd);
		}
	}

	function create_zip_shell($mixtape_id,$rebuild = '')
	{
		if($rebuild == 'rebuild')
		{
			$sql = "Update mixtapes SET zip_file = '...' where id = ".$mixtape_id;
			$this->db->query($sql);
		}

		if($_SERVER['HTTP_HOST'] == 'localhost')
		{
			$command = '/Applications/MAMP/bin/php/php5.5.10/bin/php /Users/TOCHI/Dropbox/Web\ Development/Projects/gbaam/cron.php admin2 create_zip '.$mixtape_id;
		} 
		else
		{
			$command = '/usr/bin/php /var/www/html/cron.php admin2 create_zip '.$mixtape_id;
		}

		shell_exec(sprintf('%s > /dev/null 2>&1 &', $command));
	}

	public function create_zip($mixtape_id)
	{
		ini_set('memory_limit','500M');
		ini_set('max_execution_time','6000');

		$this->load->library('zip');

		$mix_audio_dir = $this->config->item('mixtape_audio_dir');
		$mix_covers_dir = $this->config->item('mixtape_covers_dir');
		$mix_zip_dir = $this->config->item('mixtape_zip_dir');

		// Get the mixtape data
		$getTracks = $this->Mixtape_model->getMixtapeTracks($mixtape_id);
		$get_tape = $this->Mixtape_model->getMixtape_new_admin($mixtape_id);

		// Clean out the title
		//$mixtape_title = preg_replace("/[^a-zA-Z 0-9]+/", " ", $get_tape->title);
		$mixtape_title = preg_replace('/[^a-zA-Z0-9]/i',' ', $get_tape->title);
		$mixtape_title = str_replace('.',' ',$mixtape_title);
		$mixtape_title = str_replace(' ', '_', $mixtape_title);
		$mixtape_title = str_replace('__', '_', $mixtape_title);
		
		// increment number
		$number = 01;
		foreach($getTracks['result'] as $g)
		{
			$track = $mix_audio_dir.$g->hash.$g->ext;
			$new_name = str_pad($number++, 2, "0", STR_PAD_LEFT).' - '.$g->title.$g->ext;
			$this->zip->rename_and_read_file($track,$new_name);
		}

		if($get_tape->front_cover != '')
		{
			$images_folder = $this->config->item('uploaded_images_folder').'mixtape';

			$image_id = $get_tape->front_cover;

			$photo = $this->Photo_model->get_photo($get_tape->front_cover);
			$img_hash = $photo->img_hash;
			

			$front_cover = $images_folder.'/'.$image_id.'_'.$img_hash.'.jpg';

			$front_cover_name = $mixtape_title.'-front.jpg';
			$this->zip->rename_and_read_file($front_cover,'00 - '.$front_cover_name);
		}

		if($get_tape->back_cover != '')
		{
			$images_folder = $this->config->item('uploaded_images_folder').'mixtape';

			$image_id = $get_tape->back_cover;

			$photo = $this->Photo_model->get_photo($get_tape->back_cover);
			$img_hash = $photo->img_hash;
			

			$back_cover = $images_folder.'/'.$image_id.'_'.$img_hash.'.jpg';

			//echo $back_cover;

			$back_cover_name = $mixtape_title.'-back.jpg';
			$this->zip->rename_and_read_file($back_cover,'00 - '.$back_cover_name);
		}

		$zip_name = $mixtape_title.'-(Gbaam.com).zip';

		// store in the folder for now
		$this->zip->archive($mix_zip_dir.$zip_name);

		// Update the mixtape
		$upd = array(
			'zip_file' => $zip_name
			);

		$this->db->where('id',$mixtape_id);
		$this->db->update('mixtapes',$upd);

		if($this->input->is_cli_request())
		{
			echo "Mixtape zip successfully created!".PHP_EOL;
			
		}

		return;
	}

	public function newpermalinks()
	{
		$sql = "SELECT * FROM mixtapes";
		$query = $this->db->query($sql);

		foreach($query->result() as $q)
		{
			$url = clean_url_slug($q->artist.'-'.$q->title);
			$sql = "UPDATE mixtapes SET  permalink =  '{$url}' WHERE  id =$q->id";
			$this->db->query($sql);
		}
	}

	public function uploadify($mixtape_id, $new = '')
	{
		//if(!$this->session->userdata('admin_logged_in'))
		//	redirect($this->config->item('admin_location'));
		
		$config['upload_path'] = "./data/mixtape/audio"; 
		$config['allowed_types'] = 'mp3|wav|ogg';
		$config['max_size'] = 0;
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload("userfile"))
		{
			//$error = $this->upload->display_errors();
			//var_dump($this->upload->data());
			//var_dump($error); 
		}
		else
		{
			// Get upload data
			$data = $this->upload->data();

			//print_r(var_dump($data));
			
			$img_hash = md5($data['file_name'].date('Y-m-d H:i:s'));
			$newname = $config['upload_path'].'/'.$img_hash.'.mp3';
			rename($data['full_path'],$newname);

			//check if there is previeous tracks and return the previous
			//track number
			$sql = "SELECT * FROM mixtape_tracks where mixtape_id = " . $mixtape_id;
			$q = $this->db->query($sql);
			$count = $q->num_rows();


			// set the trackname variable
			$track_name = $data['raw_name'];

			if($new == 'new')
			{
				// check if the first 2 intergers are track numbers
				$first_two_chars = substr($track_name, 0, 2);
				if(is_numeric($first_two_chars))
				{
					//set the track num 
					$track_num = intval($first_two_chars);
				}
				else
				{
					//if there is no previous tracks then the first track is 1
					if($count == 0)
					{
						$track_num = '1';
					}
					elseif($count > 0)
					{
						$track_num = $count+1;
					}
				}
			}
			else
			{
				$sql = "SELECT * FROM mixtape_tracks where mixtape_id = ".$mixtape_id." order by track_num DESC LIMIT 1";
				$query = $this->db->query($sql);

				$row = $query->row();

				$track_num = $row->track_num+1;
			}
			


			// remove first 2 characters if its a number
			if(is_numeric($first_two_chars))
			{
				$track_name = substr($data['raw_name'], 2);
			}

			// remove all underscores from name
			$track_name = str_replace('_', ' ', $track_name);

			// change datpif to gbaam
			//$track_name = str_replace('(DatPiff Exclusive)', '(Gbaam.com)', $track_name);
			$track_name = str_replace('(DatPiff Exclusive)', '', $track_name);

			// capitalize produced
			$track_name = str_replace('produced', 'Produced', $track_name);
			$track_name = str_replace('prod', 'Prod', $track_name);

			// remove all white space after
			$track_name = trim($track_name);

			//remove any begining dashes
			if(substr($track_name, 0, 1) == '-')
			{
				$track_name = substr($track_name, 1);
			} 

			// remove white spaces again after removing dashes
			$track_name = trim($track_name);

			$data = array(
				'mixtape_id' => $mixtape_id,
				'track_num' => $track_num,
				'title' => $track_name,
				'orginal_filename' => $data['file_name'],
				'hash' => $img_hash,
				'ext' => $data['file_ext']//,
				//'size' => $this->formatSizeUnits($data['file_size'], 1)
				);

			$this->db->insert('mixtape_tracks',$data);

		} 
	}  

	public function upload_covers($mixtape_id)
	{


		$type = 'mixtape';
		$mixtape = $this->Mixtape_model->getMixtape_new_admin($mixtape_id);

		
		$config['upload_path'] = "./".$this->config->item('uploaded_images_folder').'/'.$type;
		$config['allowed_types'] = 'jpeg|jpg|png|gif';
		$config['max_size']	= '10000';
		$config['max_width']  = '10240';
		$config['max_height']  = '7680';

		$this->upload->initialize($config);

		if ( $this->upload->do_upload('ft_cover'))
		{		
			// Get photo data
			$photodata = array('upload_data' => $this->upload->data());

			//delete the old mixtape cover
			$this->Photo_model->delete_image('mixtape',$mixtape->front_cover);
			

			//send to get the image manipulated
			$photo_id = $this->Photo_model->actual_upload($type,$mixtape_id,$photodata);

			$data['front_cover'] = $photo_id;

			$this->db->where('id' , $mixtape_id);
			$this->db->update('mixtapes',$data);


		}/*
		else{
			$d['error'] = array('error' => $this->upload->display_errors());
			print_r($d['error']);
		}*/


		if( $this->upload->do_upload("bk_cover"))
		{
			// Get photo data
			$photodata = array('upload_data' => $this->upload->data());

			//delete the old mixtape cover
			$this->Photo_model->delete_image('mixtape',$mixtape->back_cover);

			//send to get the image manipulated
			$photo_id = $this->Photo_model->actual_upload($type,$mixtape_id,$photodata);

			$data['back_cover'] = $photo_id;

			$this->db->where('id' , $mixtape_id);
			$this->db->update('mixtapes',$data);


		}
	}

	public function formatSizeUnits($bytes)
	{
		if ($bytes >= 1073741824)
		{
			$bytes = number_format($bytes / 1073741824, 2) . ' GB';
		}
		elseif ($bytes >= 1048576)
		{
			$bytes = number_format($bytes / 1048576, 2) . ' MB';
		}
		elseif ($bytes >= 1024)
		{
			$bytes = number_format($bytes / 1024, 2) . ' KB';
		}
		elseif ($bytes > 1)
		{
			$bytes = $bytes . ' bytes';
		}
		elseif ($bytes == 1)
		{
			$bytes = $bytes . ' byte';
		}
		else
		{
			$bytes = '0 bytes';
		}

		return $bytes;
	}




}
