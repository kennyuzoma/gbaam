<?php

/**
* 
*/
class Videos extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();

		$this->load->model('Challenge_model');

		$this->load->helper('inflector');
		
		admin_show_errors();

		if(!$this->session->userdata('admin_logged_in'))
			redirect($this->config->item('admin_location'));

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'videos') == FALSE)
			redirect($this->config->item('admin_location'));

	}

	public function videolist($type)
	{

		$d['video_type'] = $type;
		switch($type){
			case 'gbaamtv':
			$d['title'] = 'GbaamTV';
			$d['viewlink'] = 'gbaamtv/v/';
			break;
			case 'mv':
			$d['title'] = 'Music';
			$d['viewlink'] = 'video/';
			break;
			case 'challenge':
			$d['title'] = 'Challenge';
			$d['viewlink'] = 'challenge/view/';
			break;
		}

		$d['get_videos'] = $this->Admin_model->getAllVideos_Admin($type);

		$this->load->view('admin_new/video_list', $d);
	}

	public function videos($type, $slug, $video_id = '')
	{

		
		$d['video_type'] = $type;

		//Set titles based on type
		switch($type){
			case 'gbaamtv':
				$d['title'] = 'GbaamTV';
				$d['viewlink'] = 'gbaamtv/v/';
				$validation_rule = 'admin_gbaamtv';
				break;

			case 'mv':
				$d['title'] = 'Music';
				$d['viewlink'] = 'video/';
				$validation_rule = 'admin_video';
				break;

			case 'challenge':
				$d['title'] = 'Challenge';
				$d['viewlink'] = 'challenge/view/';
				$validation_rule = 'admin_challenge';
				break;
		}

		if($slug == 'new')
		{
			if($this->input->post('submit'))
			{
				
				$d['validation'] = $this->form_validation->run('admin_video');

				if($d['validation'] == TRUE )
				{
					// Gather all the information
					$title = $this->input->post('videotitle');
					$artistname = $this->input->post('artistname');
					$prod = $this->input->post('prod');
					$dir = $this->input->post('dir');
					$desc = $this->input->post('description');
					$tags = $this->input->post('tags');
					$videosrc = $this->input->post('video_src');
					$curr_thumb = $this->input->post('curr_thumb');
					$originalDate = $this->input->post('date');
					

					if($originalDate == '')
						$originalDate = date('Y-m-d');

					$date = date("Y-m-d", strtotime($originalDate));

					// Other default values
					$gbaamtv = 0;
					$gtv_cat = 0;
					$chall = 0;
					$chall_cat = 0;

					// If GbaamTV
					if($type == 'gbaamtv')
					{
						$gbaamtv = 1;
						$gtv_cat = $this->input->post('tv_cat');
					}
					

					if($type == 'challenge')
					{
						$chall = 1;
						$chall_cat = $this->input->post('chall_cat');
					}

					// Insert the video
					$ins_id = $this->Admin_model->insertVideo($title,$artistname,$prod,$dir,$desc,$videosrc,$date,$gbaamtv,$gtv_cat,$chall,$chall_cat,$tags);

					// Add a thumbnail if uploaded
					$this->Add_video_thumbnail($ins_id,$curr_thumb);

					redirect($this->admin_location . '/videolist/'.$type.'/success');
				}
				else
				{
					$d['message'] = validation_errors();
				}
			}

			$this->load->view('admin_new/video_new_vid', $d);
		}

		if($slug == 'edit')
		{
			if($video_id == '')
				redirect($this->admin_location . '/videos/mv/all');

			$d['get_video'] = $this->Video_model->getVideo( $video_id );

			if($this->input->post('submit'))
			{
				$d['s_message'] = 'Video Saved Successfully';
				
				$d['validation'] = $this->form_validation->run('admin_video');

				if($d['validation'] == TRUE )
				{

					// Gather all the information
					$title = $this->input->post('videotitle');
					$artistname = $this->input->post('artistname');
					$prod = $this->input->post('prod');
					$dir = $this->input->post('dir');
					$desc = $this->input->post('description');
					$tags = $this->input->post('tags');
					$videosrc = $this->input->post('video_src');
					$curr_thumb = $this->input->post('curr_thumb');
					$originalDate = $this->input->post('date');
					$status = $this->input->post('status');

					if($originalDate == '')
						$originalDate = date('Y-m-d');

					$date = date("Y-m-d", strtotime($originalDate));
					//echo $originalDate;

					// Other default values
					$gbaamtv = 0;
					$gtv_cat = 0;
					$chall = 0;
					$chall_cat = 0;

					// If GbaamTV
					if($type == 'gbaamtv')
					{
						$gbaamtv = 1;
						$gtv_cat = $this->input->post('tv_cat');
					}
					

					if($type == 'challenge')
					{
						$chall = 1;
						$chall_cat = $this->input->post('chall_cat');
					}

					// Insert the video
					
					$this->Admin_model->updateVideo($video_id, $title,$artistname,$prod,$dir,$desc,$videosrc,$curr_thumb,$date,$gbaamtv,$gtv_cat,$chall,$chall_cat,$tags,$status);

					// Add a thumbnail if uploaded
					$this->Add_video_thumbnail($video_id,$curr_thumb);

					// redirect to same page to refresh form
					redirect($this->admin_location . '/videos/'.$type.'/edit/'. $video_id.'/success');
				}
				else
				{
					$d['message'] = validation_errors();
				}
			}
			$this->load->view('admin_new/video_edit_vid', $d);

			
		}

		if($slug == 'delete')
		{
			if($this->input->is_ajax_request())
				$this->Admin_model->deleteVideo($video_id);
		}


		if($slug == 'five_feat')
		{
			$d['get_challenges'] = $this->Challenge_model->challenge_list();
			$this->load->view('admin_new/chall_list', $d);
		}

	}

	public function challenge($slug, $challenge_id = '')
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect($this->config->item('admin_location'));

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'challenge') == FALSE)
			redirect($this->config->item('admin_location'));
		

		if($slug == 'new')
		{
			$d['title'] = 'New Challenge';
			$this->load->view('admin_new/chall_new', $d);

			if($this->input->post('submit'))
			{
				$status = $this->input->post('question1');
				$name = $this->input->post('name');
				$display_name = $this->input->post('display_name');

				$this->Challenge_model->newChallenge($status,$name, $display_name);

				redirect($this->admin_location . '/challenge/list/success');
			}
		}

		if($slug == 'edit')
		{
			$d['title'] = 'Edit Challenge';

			$d['get_challenge'] = $this->Challenge_model->challenge_info($challenge_id);
			$this->load->view('admin_new/chall_edit', $d);
			
			if($this->input->post('submit'))
			{
				$status = $this->input->post('question1');
				$name = $this->input->post('name');
				$display_name = $this->input->post('display_name');

				$this->Challenge_model->editChallenge($challenge_id, $status,$name,$display_name);
				redirect($this->admin_location . '/challenge/edit/'.$challenge_id.'/success');
			}
		}

		if($slug == 'delete')
		{
			if($this->input->is_ajax_request())
				$this->Challenge_model->deleteChallenge($challenge_id);
		}

		if($slug == 'list')
		{
			$d['title'] = 'Challenge List';
			$d['get_challenges'] = $this->Challenge_model->challenge_list();
			$this->load->view('admin_new/chall_list', $d);
		}


	}

	public function review($slug, $video_id = '')
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect($this->config->item('admin_location'));
		
		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'videos') == FALSE)
			redirect($this->config->item('admin_location'));

		if($slug == 'edit')
		{
			$d['g'] = $this->Admin_model->getPendingVideo($video_id);
			$d['title'] = 'Review Video';
			

			if($this->input->post('submit_approve'))
			{
				$d['validation'] = $this->form_validation->run('admin_video');

				if($d['validation'] == FALSE )
				{
					$d['message'] = validation_errors();
				}
				else
				{
					// Gather all the information
					$title = $this->input->post('videotitle');
					$artistname = $this->input->post('artistname');
					$prod = $this->input->post('prod');
					$dir = $this->input->post('dir');
					$desc = $this->input->post('description');
					$tags = $this->input->post('tags');
					$videosrc = $this->input->post('video_src');
					$curr_thumb = $this->input->post('curr_thumb');
					$originalDate = $this->input->post('date');

					if($originalDate == '')
						$originalDate = date('Y-m-d');

					$date = date("Y-m-d", strtotime($originalDate));

					// Other default values
					$gbaamtv = 0;
					$gtv_cat = 0;
					$chall = 0;
					$chall_cat = 0;

					// If GbaamTV
					if($d['g']->gtv == '1')
					{
						$gbaamtv = 1;
						$gtv_cat = $this->input->post('tv_cat');
					}
					

					if($d['g']->chall == '1')
					{
						$chall = 1;
						$chall_cat = $this->input->post('cat');
					}

					// Insert the video
					$ins_id = $this->Admin_model->insertVideo($title,$artistname,$prod,$dir,$desc,$videosrc,$date,$gbaamtv,$gtv_cat,$chall,$chall_cat,$tags);

					// Add a thumbnail if uploaded
					$this->Add_video_thumbnail($ins_id,$curr_thumb);

					$this->Admin_model->processpending('approved',$video_id, $ins_id);

					redirect($this->admin_location . '/review/edit/'.$video_id.'/success');
				}
			}
			if($this->input->post('submit_deny'))
			{
				

				$this->Admin_model->processpending('deny',$video_id);

				redirect($this->admin_location . '/review/list/');
			}

			$this->load->view('admin_new/review_edit', $d);
		}

		if($slug == 'delete')
		{
			if($this->input->is_ajax_request())
				$this->Admin_model->deletePending($video_id);
		}

		if($slug == 'list')
		{
			$d['title'] = 'Review List';
			$d['get_pending'] = $this->Admin_model->getPendingVideos();
			$this->load->view('admin_new/review_list', $d);
		}


	}

	public function featvideos($slug, $type, $slot='',$video_id = '')
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect($this->config->item('admin_location'));

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'videos') == FALSE)
			redirect($this->config->item('admin_location'));
		
		$d['type'] = $type;

		if($type == 'five')
		{
			$d['title'] = '5 Featured Videos';
			$d['limit'] = 5;
		}
		
		elseif($type == 'two')
		{
			$d['title'] = 'Top Two';
			$d['limit'] = 2;
		}


		// List all the mixtapes
		if($slug == 'list')
		{
			$this->load->view('admin_new/featvideos_list', $d);
		}

		if($slug == 'add')
		{
			$d['title'] = '5 Featured Videos Add';

			$d['slot'] = $slot;
			$d['get_videos'] = $this->Admin_model->getAllVideos_Admin('mv');
			$this->load->view('admin_new/featvideos_add', $d);
		}

		if($slug == 'addconf')
		{
			$this->Video_model->AddToSlot($type, $slot, $video_id);
			redirect($this->admin_location . '/featvideos/list/'.$type.'/success');
		}

	}

	public function gtvfeatvideos($slug, $type = '', $slot='',$video_id = '')
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect($this->config->item('admin_location'));

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'videos') == FALSE)
			redirect($this->config->item('admin_location'));
		
		$d['type'] = $type;
		$d['limit'] = 4;

		$d['title'] = 'Featured Videos';

		if($type == '')
		{
			$d['title'] = 'Select GbaamTV Category';
			$d['gtv_cats'] = $this->Video_model->get_gtv_cat();
			$this->load->view('admin_new/gtv_catselect', $d);
		}
		else
		{
			// List all the mixtapes
			if($slug == 'list')
			{
				$this->load->view('admin_new/featvideos_list', $d);
			}

			if($slug == 'add')
			{
				$d['title'] = 'Featured Videos';

				$d['slot'] = $slot;
				//$d['get_videos_count'] = $this->Video_model->getGbaamTVByCat($type);
				
				if($type == 'gtv_main')
					$d['get_videos'] = $this->Admin_model->getAllVideos_Admin('gbaamtv');
				else
					$d['get_videos'] = $this->Video_model->getGbaamTVByCat($type);

				$this->load->view('admin_new/featvideos_add', $d);
			}

			if($slug == 'addconf')
			{
				$this->Video_model->AddToSlot($type, $slot, $video_id);
				redirect($this->admin_location . '/gtvfeatvideos/list/'.$type.'/success');
			}

		}

		
	}

	public function landingvideos($slug, $type, $video_id = '')
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect($this->config->item('admin_location'));

		if($this->Admin_model->get_user_access($this->session->userdata('admin_user_id'),'videos') == FALSE)
			redirect($this->config->item('admin_location'));
		
		$d['type'] = $type;
		$d['get_videos'] = $this->Admin_model->getAllVideos_Admin('mv');

		if($type == 'gbaamtv')
		{
			$d['title'] = 'GbaamTV Landing Video';
			$d['feat_type'] = 'landing_gtv';
			$d['get_videos'] = $this->Admin_model->getAllVideos_Admin('gbaamtv');
		}
		
		elseif($type == 'challenge')
		{
			$d['title'] = 'challenge Landing Video';
			$d['feat_type'] = 'landing_chall';
			$d['get_videos'] = $this->Admin_model->getAllVideos_Admin('challenge');

		}

		$slot = $d['feat_type'];

		// List all the mixtapes
		if($slug == 'list')
		{
			$this->load->view('admin_new/landingvideos_list', $d);
		}

		if($slug == 'add')
		{
			$d['title'] = $d['type'] . ' Videos Add';
			
			$this->load->view('admin_new/landingvideos_add', $d);
		}

		if($slug == 'addconf')
		{
			$this->Video_model->AddToSlot($slot,'1',$video_id );
			redirect($this->admin_location . '/landingvideos/list/'.$type.'/success');
		}

	}

	public function Add_video_thumbnail($video_id)
	{
		$type = 'video';
		$video = $this->Video_model->getVideo_row($video_id);

		
		$config3['upload_path'] = "./".$this->config->item('uploaded_images_folder').$type;
		$config3['allowed_types'] = 'jpeg|jpg|png|gif';
		$config3['max_size']	= '10000';
		$config3['max_width']  = '10240';
		$config3['max_height']  = '7680';

		//$this->load->library('upload', $config);
		$this->upload->initialize($config3);

		if ( ! $this->upload->do_upload('lg_img'))
		{
			// Only Uncomment for error checking
			//$error = $this->upload->display_errors();
			//var_dump($this->upload->data());
			//var_dump($error); 
		}
		else
		{		
			// Get photo data
			$photodata = array('upload_data' => $this->upload->data());

			//delete the old song cover
			$this->Photo_model->delete_image('video',$video->thumb);
			
			// send to get the image manipulated
			$photo_id = $this->Photo_model->actual_upload($type,$video_id,$photodata);

			// update the video with the new thumbnail
			$data['thumb'] = $photo_id;
			$this->db->where('id' , $video_id);
			$this->db->update('video',$data);
		}

	}


}