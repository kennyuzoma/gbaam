<?php

/**
* 	Extends MY_Controller for admin stuff
*/
class Home extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->load->helper('inflector');
		
		admin_show_errors();

	}

	public function index()
	{
		$session = $this->session->userdata('admin_logged_in');

		$d['title'] = 'Admin Login';

		if($session)
		{
			redirect($this->admin_location . '/home');
		}
		else
		{
			if($this->input->post('submit_login'))
			{

				$this->form_validation->set_error_delimiters('<span style="z-index:100;float:left;width:100%;background:#FFD1D3;border:1px solid #BD7575;border-radius:5px;text-align:center;font-size:16px;padding:6px 0;margin-bottom:10px;font-weight:bold;">', '</span>');

				if($this->form_validation->run('admin_login') == FALSE )
				{
					$d['message'] = validation_errors();
					$this->load->view('admin_new/login',$d);
				}
				else
					$this->Admin_model->validate();
			}
			else
			{
				$this->load->view('admin_new/login',$d);
			}
		}
	}

	function _login_check()
	{
		if ($this->Admin_model->checklogin($this->input->post('username'),$this->input->post('password')) == FALSE)
		{
			$this->form_validation->set_message('_login_check', 'Incorrect Username or Password');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function _doesUserExist_username()
	{
		//Store in Site_model
		$username = $this->input->post('username');
		$user_id = $this->input->post('user_id');

		//if the provided email and password match a user then it will return false
		if ($this->Admin_model->doesUserExist_username($username,$user_id) === 'reserved')
		{
			$this->form_validation->set_message('_doesUserExist_username', 'This username is RESERVED. Please remove it from the reserved list and try again.');
			
			// return FALSE to trigger error message for validation.
			return FALSE; 
		}
		elseif ($this->Admin_model->doesUserExist_username($username,$user_id) == TRUE)
		{
			$this->form_validation->set_message('_doesUserExist_username', 'Username is not available.');
			
			// return FALSE to trigger error message for validation.
			return FALSE; 
		}
		else
		{
			return TRUE;
		}
	}

	public function _doesUserExist_email()
	{
		//Store in Site_model
		$email = $this->input->post('email');
		$user_id = $this->input->post('user_id');

		//if the provided email and password match a user then it will return false
		if ($this->Admin_model->doesUserExist_email($email,$user_id) == TRUE)
		{
			$this->form_validation->set_message('_doesUserExist_email', 'Email Address is not available.');
			
			// return false to trigger error message.
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}


	public function home2()
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect($this->config->item('admin_location'));

		$this->load->view('admin_new/index_home');
	}

	public function settings($slug)
	{
		if(!$this->session->userdata('admin_logged_in'))
			redirect($this->config->item('admin_location'));

		// if admin type == 1 or 2
		
		$d['title'] = 'Settings';
		$this->load->view('admin_new/settings_home', $d);

	}

	public function pretty_url()
	{
		$this->output->enable_profiler(FALSE);
		$text = $this->input->post('url');
		echo clean_url_slug($text);
	}

	public function logout()
	{
		$this->Admin_model->log_activity('logout','0','logged out');
		$this->session->sess_destroy();
		redirect($this->config->item('admin_location'));
	}





}