<?php

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */
/**
 *  Cron Controller
 *  - Handles All cron jobs 
 */
class Cron extends CI_Controller
{
	function __contruct(){
		parent::__contruct();
	}

	/**
	 * Backs UP the database and Emails to specified Email
	 * - Ran by cron job
	 * @return [type] [description]
	 */
	function backup_database()
	{
		// Load the DB utility class
		$this->load->dbutil();

		// Backup your entire database and assign it to a variable
		$backup =& $this->dbutil->backup(); 

		// which environment is this? Live, Local, etc
		$env = strtoupper($this->config->item('env'));

		// declare the file name
		$file_name = 'Gbaam '.$env.' DB Backup - '.date('n-d-Y H:i:s').'.gz';

		$full_file = './application/backups/'.$file_name;

		//Load the file helper and write the file to your server
		$this->load->helper('file');

		// store the file in the server
		if ( ! write_file($full_file, $backup))
		     echo 'Unable to write the file';
		else
		     echo 'File written!';
		
		$config['mailtype'] = 'html';
		$this->load->library('Email',$config);

		// email message
		$message = 'Your backup is available!';

		//send the message
		$this->email->from('backups@gbaam.com', 'Gbaam');
		$this->email->to('kennyuzoma@gmail.com');
		$this->email->subject('Daily Backup '.date('n-d-Y'));
		$this->email->message($message);
		$this->email->attach($full_file);
		$this->email->send();
	}


	/**
	 * cUrl handler to ping the Sitemap submission URLs for Search Engines... 
	 * @param  [string] $url - which url to ping
	 * @return [string]      - Returns $httpCode
	 */
	function myCurl($url)
	{
		// Curl Init
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		// return the HTTPCODE
		return $httpCode;
	}


	/**
	* Sitemap Submitter
	* Use this script to submit your site maps automatically to Google, Bing.MSN and Ask
	* Trigger this script on a schedule of your choosing or after your site map gets updated.
	*/
	function generate_sitemap()
	{
		// Load the sitemap library
		$this->load->library('sitemap');
	    $this->sitemap->create();

		//Set this to be your site map URL
		$sitemapUrl = "http://gbaam.com/sitemap.xml";

		//Google
		$url = "http://www.google.com/webmasters/sitemaps/ping?sitemap=".$sitemapUrl;
		$returnCode = $this->myCurl($url);
		echo "<p>Google Sitemaps has been pinged (return code: $returnCode).</p>";

		//Bing / MSN
		$url = "http://www.bing.com/webmaster/ping.aspx?siteMap=".$sitemapUrl;
		$returnCode = $this->myCurl($url);
		echo "<p>Bing / MSN Sitemaps has been pinged (return code: $returnCode).</p>";

		//ASK
		$url = "http://submissions.ask.com/ping?sitemap=".$sitemapUrl;
		$returnCode = $this->myCurl($url);
		echo "<p>ASK.com Sitemaps has been pinged (return code: $returnCode).</p>";
	}

	/**
	 * Run everyday to find out which items have been marked 'deleted=0' for a month and remove them
	 * 
	 */
	function monthly_delete()
	{
		// Check the videos
		$this->Admin_model->monthly_unlink('videos');

		// check the mixtapes
		$this->Admin_model->monthly_unlink('mixtapes');

		// check the articles
		$this->Admin_model->monthly_unlink('articles');

		// check the blog posts
		$this->Admin_model->monthly_unlink('blog_posts');

		// check the slides.
		$this->Admin_model->monthly_unlink('slides');
	}

}
