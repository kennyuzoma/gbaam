<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class Main extends CI_Controller { 

	function __construct()
    {
		parent::__construct();
		//error_reporting(E_ALL);
		//ini_set('display_errors', 'on');
		//ini_set('html_errors', false);
		//if($_SERVER['HTTP_HOST'] == 'localhost')
		//$this->output->enable_profiler(TRUE);
		//	
		
		// clean this up later... 
		// If user has not entered their email address for twitter registration....
		$user_status = $this->session->userdata('u_status');
		if(($this->uri->segment(2) != 'twitter_finish') && ($this->uri->segment(2) != 'logout'))
		{
			if(isset($user_status) && $user_status == '4')
				redirect('main/twitter_finish');
		}
    }

    /**
     * This is the home page of the site
     * @return the view is called and displayed
     */
    public function index()
    {
        $d['title'] = '';
        $d['meta_desc'] = 'The latest African Music, Videos, Mixtapes, Albums, and Articles from the most authoritative sources across the web.';//htmlentities(strip_tags($this->config->item('site_description')));

		$d['getSlides'] = $this->Site_model->getSlides();
		$d['getSlidesCount'] = $this->Site_model->getSlides_count();
	
        $this->load->view('homepage',$d);
        
    } 

    /**
     * Submit Page
     * @param  string $page - which page is this refering to...
     * @return view       View is returned
     */
	public function submit($page ='')
	{

		if(!$this->session->userdata('logged_in'))
       		redirect('main/login?next=main/submit/'.$page);


		$d['page'] = $page;

		if($page == '')
		{
			redirect('main/submit/video');
		}
		elseif($page == 'gbaamtv')
		{
			$d['title'] = 'Submit GbaamTV Video';
			$d['meta_desc'] = 'Submit your video to be featured on GbaamTV';
		}
		elseif($page == 'video')
		{
			$d['title'] = 'Submit Music Video';
			$d['meta_desc'] = 'Submit your music video to be featured on Gbaam';
		}
		elseif($page == 'challenge')
		{
			$d['title'] = 'Submit Challenge Video';
			$d['meta_desc'] = 'Submit your challenge to be featured on Gbaam Challenge';

		}
		else { // show 404
			show_404();
		}
		
		if (isset($_POST['submit_newvideo']))
		{

			//$this->output->enable_profiler(TRUE);

			$is_spam_filled_out = $this->input->post('s_holder');

			if($is_spam_filled_out == '')
			{
				// default variables
				$gbaamtv = 0;
				$gtv_cat = 0;
				$chall = 0;
				$chall_cat = 0;
				$artist = '';
				$prod = '';
				$dir = '';
				$editor = '';
				$company = '';


				$email = $this->input->post('email');
				$fname = $this->input->post('first_name');
				$lname = $this->input->post('last_name');
				$title = $this->input->post('videotitle');

				if($page == 'video')
					$artist = $this->input->post('artist');

				if($page == 'gbaamtv')
				{
					$prod = $this->input->post('prod');
					$dir = $this->input->post('dir');
					//$editor = $this->input->post('editor');
					$company = $this->input->post('company');
				}

				$desc = $this->input->post('description');
				$video_src = $this->input->post('video_src');
				$video_src_other = $this->input->post('video_src_other');
				//$date = $this->input->post('year').'-'.$this->input->post('month').'-'.$this->input->post('day');
				$date_release = $this->input->post('date_release');
				$date_release = date('Y-m-d', strtotime($date_release));


				// If GbaamTV
				if($page == 'gbaamtv')
				{
					$gbaamtv = 1;
					$gtv_cat = $this->input->post('tv_cat');
				}
				

				if($page == 'challenge')
				{
					$chall = 1;
					$chall_cat = $this->input->post('chall_cat');
				}


				$this->session->set_flashdata('item', 'Thank you for submiting a video! We will email you soon!');

			

				$this->Admin_model->insertPendingVideo($page,$email,$fname,$lname,$title,$artist,$prod,$dir,$editor,$company,$desc,$video_src,$video_src_other,$date_release,$gbaamtv,$gtv_cat,$chall,$chall_cat);

				$d['title'] = 'Thank You for your submission!';
				$this->load->view('main/submit_success',$d);
			}
		}
		else
		{
			$this->load->view('main/submit_video',$d);
		}

	}

	
	public function login($js = '')
	{
		//$this->output->enable_profiler(TRUE);

		$d['title'] = "Login";
		$d['meta_desc'] = 'Login to Gbaam using your social network or Email address to access the members area!';

		//Check if logged in
		if($this->session->userdata('logged_in') == 1)
		{
			redirect('/');
		}

		if($js == 'js')
		{
			$this->load->view('main/js_login',$d);
		}
		else
		{

	        if(isset($_POST['login']))
			{
				//If validation went perfect
				$d['validation'] = $this->form_validation->run('form_login');

				if($d['validation'] == TRUE )
				{
					//Gather post info
					$username = $this->input->post('username');
					$pass = $this->input->post('password');
					$next = $this->input->get('next');

					//log the user in
					$this->User_model->logUserIn($username,$pass,$next);

					
					redirect($next);
				}
				else
				{
					//Add "Failed login text"
					$d['message'] = validation_errors();
					$this->load->view('main/login',$d);

				}
			}
			else
			{
				$this->load->view('main/login',$d);
			}
		
		}

	}

	public function jslogin($js = '')
	{
		//$this->output->enable_profiler(TRUE);

		$d['title'] = "Login";
		$d['meta_desc'] = 'Login to Gbaam using your social network or Email address to access the members area!';

		//Check if logged in
		if($this->session->userdata('logged_in') == 1)
		{
			redirect('/');
		}

		//keep the previous flash data
		$this->session->keep_flashdata('bounce_uri');

		if($js == 'js')
		{
			$this->load->view('main/js_login',$d);
		}
		else
		{

	        if(isset($_POST['login']))
			{
				//If validation went perfect
				$d['validation'] = $this->form_validation->run('form_login');

				if($d['validation'] == TRUE )
				{
					//Gather post info
					$username = $this->input->post('username');
					$pass = $this->input->post('password');
					$next = $this->input->post('next');

					//log the user in
					$this->User_model->logUserIn($username,$pass);

					echo '<script>window.close()</script>';
	
				}
				else
				{
					//Add "Failed login text"
					$d['message'] = validation_errors();
					$this->load->view('main/js_login',$d);

				}
			}
			else
			{
				$this->load->view('main/js_login',$d);
			}
		
		}

	}

	
	public function signup()
	{
		$d['title'] = 'Sign Up';
		$d['meta_desc'] = 'Sign up for Gbaam using your social network or Email address to become a member!';

		if($this->session->userdata('logged_in') == 1)
		{
			redirect('/');
		}

		if(isset($_POST['signup']))
		{
			$is_spam_filled_out = $this->input->post('s_holder');

			if($is_spam_filled_out == '')
			{

				$d['validation'] = $this->form_validation->run();
				
				//Run validation
				if($d['validation'] == TRUE )
				{
					//Gather Post information
					$name = $this->input->post('s_name');
					$username = $this->input->post('s_username');
					$email = $this->input->post('s_email');
					$pass = $this->input->post('s_password');
					//$next = $this->input->post('next');

					//if signup was good the redirect to first stage
					if($this->User_model->signUp($name,$username,$email,$pass) == TRUE)
					{
						//change name later
						//redirect('home');
						//$this->Main_model->logUserIn($email,$pass);
						$this->User_model->logUserIn($email,$pass);
						//redirect('main/welcome/step1');
						redirect('/');

					}
					else
					{
						//registration went wrong????
						//$d['message'] = validation_errors();
						$this->load->view('main/signup',$d);
					}
				}
				else
				{
					//If user exists, send them to this page
					$d['message'] = validation_errors();
					$this->load->view('main/signup',$d);
				}
			}
		}
		else
		{
			$this->load->view('main/signup',$d);
		}
	}

	public function twitter_finish()
	{
		$d['title'] = 'Twitter Signup';
		$d['meta_desc'] = 'Finish Twitter Setup';

		if($this->session->userdata('u_status') != '4')
			redirect('/');

		if(isset($_POST['submit']))
		{
			$d['validation'] = $this->form_validation->run();
			
			//Run validation
			if($d['validation'] == TRUE )
			{
				//Gather Post information
				$email = $this->input->post('s_email');
				//$next = $this->input->post('next');

				//if signup was good the redirect to first stage
				if($this->User_model->finish_twitter_signup($email,$this->session->userdata('user_id')) == TRUE)
				{
					redirect('/');
				}
				else
				{
					//registration went wrong????
					//$d['message'] = validation_errors();
					$this->load->view('main/signup_twitter',$d);
				}
			}
			else
			{
				//$this->session->set_flashdata('finish_twitter', 'TRUE');
				//If user exists, send them to this page
				$d['message'] = validation_errors();
				$this->load->view('main/signup_twitter',$d);
			}
		}
		else
		{
			$this->load->view('main/signup_twitter',$d);
		}
	}

	public function forgotpassword()
	{
		$d['title'] = 'Forgot Password';
		$d['meta_desc'] = "Forgot Your Password? Fill out the form to request to change your password.";

		if(isset($_POST['submit']))
		{
			//Run validation
			$d['validation'] = $this->form_validation->run();
			if($d['validation'] == TRUE )
			{
				//email variabale
				$email = $this->input->post('f_email');
				

				//success message
				$d['message'] = 'Your Password request was submitted!<br>Check your email, <b>'.$email.'</b> for instructions on resetting your password.';

				// get teh user
				$user = $this->User_model->get_user_by_email_or_username($email);

				//store the request in the database
				$this->User_model->password_reset_req($user->email);
				$d['email'] = $user->email;

				//email the user
				$this->Email_model->forgot_pwd($user->email);

				// load view
				$this->load->view('main/forgotpassword_success',$d);
			}
			else
			{
				$d['message'] = validation_errors();
				$this->load->view('main/forgotpassword',$d);
			}
		}
		else
		{
			$this->load->view('main/forgotpassword',$d);
		}
		
	}

	

	public function contact()
	{
		$d['title'] = 'Contact Us';
		$d['meta_desc'] = 'Send us a message to Contact Us';

		if(isset($_POST['submit']))
		{
			$is_spam_filled_out = $this->input->post('s_holder');

			if($is_spam_filled_out == '')
			{
				//Run validation
				$d['validation'] = $this->form_validation->run();
				if($d['validation'] == TRUE )
				{
					$this->Main_model->submit_contact();
					$this->load->view('main/contact_success',$d);
				}
				else
				{
					$d['message'] = validation_errors();
					$this->load->view('main/contact',$d);
				}
			}
			
		}
		else
		{
			$this->load->view('main/contact',$d);
		}		
	}

	public function about()
	{
		$d['page'] = 'about';
		$d['title'] = 'About Us';
		$d['meta_desc'] = 'African Music Videos, Music, Albums, and Entertainment in one source!';
	  	$this->load->view('main/about',$d);
	}

	public function blog()
	{
		$d['title'] = 'Blog';
		$d['meta_desc'] = 'The Gbaam Blog.';
		$d['content'] = "This section is undergoing mantianence! Check back soon!";
	  	$this->load->view('forum_commingsoon',$d);
	}

	public function advertise()
	{
		$d['page'] = 'advertise';
		$d['title'] = 'Advertise';
		$d['meta_desc'] = 'Advertise with us!';
		$d['content'] = "This section is undergoing mantianence! Check back soon!";
	  	$this->load->view('forum_commingsoon',$d);
	}

	public function writeforus()
	{
		$d['page'] = 'writeforus';
		$d['title'] = 'Write For Us';
		$d['meta_desc'] = 'Submit your articles, guest post, or become a gbaam author!';
	  	$this->load->view('main/write_for_us',$d);
	}

	public function writer_guidelines()
	{
		$d['page'] = 'writer-guidelines';
		$d['title'] = 'Writer Guidelines';
		$d['meta_desc'] = 'View our Guidelines on the type of articles we accept and deny on Gbaam';
	  	$this->load->view('main/writer_guidelines',$d);
	}

	public function privacy()
	{
		$d['page'] = 'privacy';
		$d['title'] = 'Privacy';
		$d['meta_desc'] = 'Our Privacy Policy.';
	  	$this->load->view('main/privacy',$d);
	}

	public function terms()
	{
		$d['page'] = 'terms';
		$d['title'] = 'Terms';
		$d['meta_desc'] = 'Our Terms and Conditions.';
	 	$this->load->view('main/terms',$d);
	}

	public function video_agreement()
	{
		$d['page'] = 'vsa';
		$d['title'] = 'Video Submission Agreement';
		$d['meta_desc'] = 'Our Video Submission Agreement.';
	 	$this->load->view('main/video_sub_agreement',$d);
	}

	public function video_guidelines()
	{
		$d['page'] = 'vsa';
		$d['title'] = 'Video Guidelines';
		$d['meta_desc'] = 'Our Video Guidelines.';
	 	$this->load->view('main/video_sub_guidelines',$d);
	}

	public function rss()
	{
		$d['title'] = 'Rss';
		$d['meta_desc'] = 'Download our RSS feed.';
		$d['content'] = "This section is undergoing mantianence! Check back real soon!";
	 	$this->load->view('forum_commingsoon',$d);
	}

	public function page($page)
	{
		// get the page
		$page = $this->Site_model->get_page($page);

		if($page == NULL)
			show_404();

		if($page == '')
			redirect('main');

		$d['title'] = $page->title;
		$d['content'] = $page->content;
		$d['meta_desc'] = $page->meta_desc;
		

		$this->load->view('main/page',$d);
	}

	public function subscribe($status = '')
	{
		// if this user is already subscribed
		if($status == 'thank-you')
		{
			$page = $this->Site_model->get_page('thank-you-for-subscribing');

			$d['title'] = $page->title;
			$d['content'] = $page->content;
			$d['meta_desc'] = $page->meta_desc;
			
			$this->load->view('main/page',$d);
		}
		elseif($status == 'button')
		{
			// if the user is not logged in... redirect to the subscribe area
			if(!is_loggedin())
				redirect('subscribe');

			$this->Email_model->subscribe_me('button');
			redirect('main/subscribe/thank-you');

		}
		else
		{
			if($this->User_model->get_user_setting('email_gbaam_news') == '1')
				redirect('/');

			$d['title'] = 'Subscribe to the Gbaam Newsletter';
			$d['meta_desc'] = 'Subscribe to the Gbaam Newsletter';

			if(isset($_POST['submit']))
			{
				$d['validation'] =  $this->form_validation->run('subscribe');
				if($d['validation'] == TRUE)
				{
					$this->Email_model->subscribe_me();
					redirect('main/subscribe/thank-you');
				}
				else
				{
					$d['message'] = validation_errors();
					$this->load->view('main/subscribe',$d);
					
				}
			}
			else
			{
				$this->load->view('main/subscribe',$d);
			}
		}

		
	}

	public function logout($type = '',$hash = '')
	{
		$url = $this->input->get('next');
		$this->session->sess_destroy();

		if($type == 'banned')
			$this->session->set_flashdata('user_banned', 'TRUE');

		if($type == 'confirm')
			$this->User_model->confirm_account($hash,'STAY');

		
		redirect($url);
	}

	/*
	*	Check if info is wrong or correct
	*/
	public function _check_login()
	{
		$email = $this->input->post('username');
		$password = $this->input->post('password');

		if ($this->User_model->doesUserExist($email,$password) == 'bad_password')
		{
			$this->form_validation->set_message('_check_login', 'Your username/password are incorrect');

			return FALSE;
		}
		elseif ($this->User_model->doesUserExist($email,$password) == 'doesnt_exist')
		{
			// $this->form_validation->set_message('_check_login', 'This user does not exist');
			// Instead of saying this user does not exist, we will just say that the user and password is
			// incorrect for security reasons.
			
			$this->form_validation->set_message('_check_login', 'Your username/password are incorrect');
			
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	

	function resetpw($email='',$key='')
	{
		
		$email = $_GET['email'];
		$key = $_GET['key'];

		
		// if both of these are empty
		if($key == '' || $email == '')
			redirect('/');

		$d['title'] = 'Reset Your Password';
		$d['meta_desc'] = 'Reset Your Password';

		// check if this is a valid request
		$valid_request = $this->User_model->check_password_request($email,$key);
		
		if($valid_request == FALSE)
		{
			redirect('/');
		}
		else 
		{
			if(isset($_POST['confirm_password_change']))
			{

				//Run validation
				$d['validation'] = $this->form_validation->run();
				if($d['validation'] == TRUE )
				{
					$pass = $this->input->post('conf_password');

					$this->db->where('email', $email);
					$this->db->update('users',array('pass' => $this->bcrypt->hash_password($pass)));

					$this->session->set_flashdata('user_resetpwd', 'TRUE');

					// resolve this
					$this->User_model->resolve_password($email,$key);

					$this->User_model->logUserIn($email,$pass);
					
					redirect('/');
				}
				else
				{
					$d['message'] = validation_errors();
					$this->load->view('main/reset_pw',$d);
				}
			}
			else
			{
				$this->load->view('main/reset_pw',$d);
			}
		}
		
		
	}


	public function _doesUserExist()
	{
		//Store in Site_model
		$email = $this->input->post('username');
		$password = $this->input->post('s_password');

		//if the provided email and password match a user then it will return false
		if ($this->User_model->doesUserExist($email,$password) == TRUE)
		{
			$this->form_validation->set_message('_doesUserExist', 'There is a user with that email address already!');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function _doesUserExist_username()
	{
		//Store in Site_model
		$username = $this->input->post('s_username');

		//if the provided email and password match a user then it will return false
		if ($this->User_model->doesUserExist_username($username) == TRUE)
		{
			$this->form_validation->set_message('_doesUserExist_username', 'Username is not available.');

			// return false to trigger the error message
			return FALSE;
		}/*
		elseif ($this->User_model->doesUserExist_username($username,$user_id) == 'reserved')
		{
			$this->form_validation->set_message('_doesUserExist_username', 'This username is RESERVED. Please remove it from the reserved list and try again.');
			
			// return FALSE to trigger error message for validation.
			return FALSE; 
		}*/
		else
		{
			return TRUE;
		}
	}

	public function _doesUserExist_forgot()
	{
		//Store in Site_model
		$username = $this->input->post('f_email');

		//if the provided email and password match a user then it will return false
		if ($this->User_model->doesUserExist_forgot($username) == FALSE)
		{
			$this->form_validation->set_message('_doesUserExist_forgot', 'User this does not exist.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function _doesEmailExist($arg1 = '', $arg2 = '') // for the codeigniter purpose
	{
		//Store in Site_model
		$post_email = $this->input->post('s_email');
		
		if(isset($post_email))
			$email = $this->input->post('s_email');
		else
			$email = $this->input->post('username');

		// if an email was found.
		if ($this->User_model->existing_email($email) == TRUE)
		{
			$sql = "SELECT * FROM users WHERE email='{$email}' LIMIT 1;";
			$query = $this->db->query($sql);
			$row = $query->row();

			//echo $arg2;

			if($arg2 == 'twitter')
				$message = 'There is a user with that email address. <br>If this is your email, Login to your Gbaam Account with this email address, go to your <b>Settings</b> and merge your twitter acount with that account there.';
			else
				$message = 'There is a user with that email address already.';


			/*if($row->facebook_id != '')
			{
				$message = 'There is a user with that email address already!';
			}
			else
			{
				
				
			}*/

			$this->form_validation->set_message('_doesEmailExist', $message);
				return FALSE;

		}
		else
		{
			return TRUE;
		}
	}

	public function _isEmailSubscribed()
	{
		$email = $this->input->post('email');

		if($this->Email_model->isEmailSubscribed($email) == TRUE)
		{
			$message = 'This email address is already subscribed .';

			$this->form_validation->set_message('_isEmailSubscribed', $message);
				return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	public function change_gtv_videos()
	{
		$this->db->query("UPDATE video SET gtv_cat = 1 WHERE gtv_cat = 'variety'");
		$this->db->query("UPDATE video SET gtv_cat = 2 WHERE gtv_cat = 'interviews'");
		$this->db->query("UPDATE video SET gtv_cat = 3 WHERE gtv_cat = 'shortfilms'");
		$this->db->query("UPDATE video SET gtv_cat = 4 WHERE gtv_cat = 'reality'");
		$this->db->query("UPDATE video SET gtv_cat = 5 WHERE gtv_cat = 'documentary'");

	}

	public function t()
	{
		require_once(APPPATH.'libraries/getid3/getid3.php'); 

	    $getID3 = new getID3;

	    $file = './data/songs/Jeezy - Hot Nigga (Remix).mp3';
	  
	    set_time_limit(30);

	    $ThisFileInfo = $getID3->analyze($file);
	    getid3_lib::CopyTagsToComments($ThisFileInfo);

		echo 'File name: '.$ThisFileInfo['filenamepath'].'<br>';
	   	echo 'Artist: '.(!empty($ThisFileInfo['comments_html']['artist']) ? implode('<BR>', $ThisFileInfo['comments_html']['artist']) : '&nbsp;').'<br>';
	   	echo 'Title: '.(!empty($ThisFileInfo['comments_html']['title']) ? implode('<BR>', $ThisFileInfo['comments_html']['title'])  : '&nbsp;').'<br>';
	   	echo 'Bitrate: '.(!empty($ThisFileInfo['audio']['bitrate']) ? round($ThisFileInfo['audio']['bitrate'] / 1000).' kbps'   : '&nbsp;').'<br>';
	   	echo 'Play time: '.(!empty($ThisFileInfo['playtime_string']) ? $ThisFileInfo['playtime_string']                          : '&nbsp;').'<br>';

	   	$Image='data:'.$ThisFileInfo['comments']['picture'][0]['image_mime'].';charset=utf-8;base64,'.base64_encode($ThisFileInfo['comments']['picture'][0]['data']);

		echo '<img id="FileImage" width="150" src="'.$Image.'" height="150">';
	}

	public function w()
	{
		
	}

	public function y()
	{
		echo youtube_id_from_url('http://youtu.be/NLqAF9hrVbY');
		echo '<br>';
		echo youtube_id_from_url('http://www.youtube.com/watch?v=-wtIMTCHWuI');
		echo '<br>';
		echo youtube_id_from_url('http://www.youtube.com/v/-wtIMTCHWuI?version=3&autohide=1');
		echo '<br>';
		echo youtube_id_from_url('http://www.youtube.com/oembed?url=http%3A//www.youtube.com/watch?v%3D-wtIMTCHWuI&format=json');
		echo '<br>';
		
	}


	public function g()
	{
		$this->output->enable_profiler(TRUE);
		$sql = "SELECT * FROM users";
		$q = $this->db->query($sql);



		foreach($q->result() as $s)
		{
			if($s->facebook_id != '')
			{
				$sql = "UPDATE users SET signup_method = 'facebook' where id = {$s->id}";
				$this->db->query($sql);
			}
			if($s->twitter_id != '')
			{
				$sql = "UPDATE users SET signup_method = 'twitter' where id = {$s->id}";
				$this->db->query($sql);
			}
		}
	}

	public function q()
	{
		$this->load->library('twconnect');
		$a = array(
			'user_id' => '57157330'
		);
		//$this->twconnect->twaccount_verify_credentials();
		/*echo '<pre>';
		print_r($this->twconnect->tw_get('users/show',$a));
		echo '</pre>';*/

		$user = $this->twconnect->tw_get('users/show', $a);
		echo $user->screen_name;
	}

	function username($username)
	{
		echo next_avail_username($username);
	}

	function durl()
	{
		header('Location: https://disqus.com/api/oauth/2.0/authorize/?client_id=s98j6mVdB0zyC4I6l0v1Ms1U2uoGEkBWxM5EAtsq8hax9rX4cBGG3gQjvQUzz72K&scope=read,write&response_type=code&redirect_uri=http://www.gbaam.com/main/dis');
	}

	function dis()
	{
		// load the disqus config
		$this->load->config('disqus');



		//if(!isset($_SESSION['disqus_access_token']))
		//{
			$url = "https://disqus.com/api/oauth/2.0/access_token/";

		    // Get cURL resource
			$curl = curl_init();
			// Set some options - we are passing in a useragent too here
			curl_setopt($curl, CURLOPT_RETURNTRANSFER , 1);
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_POST, 1);
			$fields = array(
		        'grant_type' => 'authorization_code',
		        'client_id' => $this->config->item('disqus_public_key'),
		        'client_secret' => $this->config->item('disqus_secret_key'),
		        'redirect_uri' => 'http://www.gbaam.com/main/dis',
		        'code' => $_GET['code']
		    );
			curl_setopt($curl, CURLOPT_POSTFIELDS, $fields); 

			// Send the request & save response to $resp
			$resp = curl_exec($curl);
			curl_close($curl);
			$r = json_decode($resp,true);
			
			$at = $_SESSION['access_token'] = $r['access_token'];
			echo $at;
	//	}
	//	else
	//	{
			
	//	}

		/*
		echo '<pre>';
		print_r($r);
		echo '</pre>';*/


	}

	function ym()
	{
		$this->load->config('disqus');
			$url = "https://disqus.com/api/3.0/users/updateProfile.json";

		  //  $qs = http_build_query($fields);

		    // Get cURL resource
			$curl = curl_init();

			// Set some options - we are passing in a useragent too here
			curl_setopt($curl, CURLOPT_RETURNTRANSFER , 1);
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_POST, 1);

			$fields = array(
		        'access_token' => $_SESSION['access_token'],
		        'api_key' => $this->config->item('disqus_public_key'),
		        'api_secret' => $this->config->item('disqus_secret_key')
		    );

			curl_setopt($curl, CURLOPT_POSTFIELDS, $fields); 

			// Send the request & save response to $resp
			$resp = curl_exec($curl);
			curl_close($curl);
			$r = json_decode($resp,true);

			echo '<pre>';
			print_r($r);
			echo '</pre>';
	}

	function twit()
	{
		
	}

	function mc()
	{
		$this->load->library('mailchimp_library');
		/*$result = $this->mailchimp_library->call('lists/subscribe', array(
			'id'                => '56de05bbdf',
			'email'             => array('email'=>'Kennyuzoma@gmail.com'),
			'merge_vars'        => array('FNAME'=>'Kenny', 'LNAME'=>'Uzoma'),
			'double_optin'      => false,
			'update_existing'   => true,
			'replace_interests' => false,
			'send_welcome'      => false,
		));*/

		// get the gbaam newsletter list
		$result = $this->mailchimp_library->call('lists/list', array(
                'filters'                => array(
                	'list_name' => 'Gbaam Newsletter'
                	)
            ));

		// 
		echo '<pre>';
		print_r($result);
		echo '</pre>';
	}



}