<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class Users extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        //$this->output->enable_profiler(TRUE);
        
        // clean this up later... 
		// If user has not entered their email address for twitter registration....
		$user_status = $this->session->userdata('u_status');
		if(isset($user_status) && $user_status == '4')
			redirect('main/twitter_finish');
    }

    public function index()
    {
    	$d['title'] = 'Users';
        $this->load->view('forum_commingsoon',$d);
    }

    public function profile($username)
    {
    	if($username == '')
    		$username = loggedin_user_id();
    	// add the user view
    	$this->User_model->add_user_view($username);

    	$get_user = $this->User_model->get_user($username);

    	// if this user does not exist.
    	if($get_user == FALSE)
    		exit(show_404());

    	// get the user
    	$d['user'] = $get_user; //returns the row when true;

    	$settings = $this->User_model->get_all_user_settings_array($d['user']->id);
    	foreach($settings as $g)
    	{
    		$d[$g->name] = $g->value;
    	}

        $d['title'] = $d['display_name'];
        $d['meta_desc'] = 'profile';
        $this->load->view('users/users_profile',$d);
    }

    public function likes($user_id)
    {
        $d['title'] = 'My Likes';
        $this->load->view('forum_commingsoon',$d);
    }

    public function favorites($user_id = '')
    {
        $d['title'] = 'My Favorites';
        $d['meta_desc'] = 'User Favorites!';

        $this->load->view('users/users_favs',$d);
    }

    public function friends($user_id)
    {
        $d['title'] = 'My Friends';
        $this->load->view('forum_commingsoon',$d);
    }

    public function messages()
    {
        $d['title'] = 'My Profile';
        $this->load->view('forum_commingsoon',$d);
    }

    public function deactivate()
    {
        $d['title'] = 'Deactivate';
        $this->load->view('forum_commingsoon',$d);
    }

    public function confirm_account($hash)
    {
    	$user_id = $this->session->userdata('user_id');

    	$this->User_model->confirm_account($hash);
    }

    public function close_conf()
    {
    	$this->session->set_userdata("close_conf", 'TRUE');
    }


}
