<?php

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class Oauth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		parse_str( $_SERVER['QUERY_STRING'], $_REQUEST );

        $this->config->load("facebook",TRUE);
        
        $config = $this->config->item('facebook');

        $this->load->library('Facebook', $config);

        //$this->output->enable_profiler(TRUE);
	}

	public function index($type)
	{
		if($type == 'facebook')
		{
			$next = $this->input->get('next');

	        // get the users information
	        $user_id = $this->facebook->getUser();
	 
	        // if the User is not authenticated then the id will equal 0
	        if($user_id == 0)
	        {
	            redirect('/');
	        } 
	        else 
	        {
	            // Store users information in user variable
	            $user = $this->facebook->api('/me');

	            // register the user if he/she is not in our database
	            if($this->User_model->isMember_facebook($user) == FALSE)
	            {
	                $this->User_model->social_signup('facebook',$user);
	            }

	            //log the user in
	            $this->User_model->login_facebook($user);
	            
	            if($this->session->userdata('logged_in') == TRUE)
	            {
	                header('location: '.base_url().$next);
	            }
	            
	        }

		}

		if($type == 'twitter')
		{
			$next = $this->input->get('next');
			$this->load->library('twconnect');

			$ok = $this->twconnect->twprocess_callback();
			$flash_next = $this->session->flashdata('next');

			if ( $ok ) 
			{ 
				
				if($this->session->flashdata('finish_twitter') == 'TRUE')
					$this->session->set_flashdata('finish_twitter', 'TRUE');
				//Checks user status and redirects
				$this->User_model->checkUserStatus($next);
			}
			else {
				$this->session->sess_destroy();
				redirect ('/');

			}
		}

	}

	public function login($type)
	{
		if($type == 'facebook')
		{
			$next = $this->input->get('next');

	        $url = $this->facebook->getLoginUrl(array('redirect_uri'=>base_url().'oauth/index/facebook?next='.$next, 'scope'=>'email, user_birthday'));

	        header('Location: '.$url);
		}

		if($type == 'twitter')
		{
			$this->load->library('twconnect');

			$next = $this->input->get('next');
			$this->session->set_flashdata($next, 'next');

			$ok = $this->twconnect->twredirect('oauth/index/twitter?next='.$next);

			if (!$ok)
				redirect('oauth/login/twitter');
				//echo 'Could not connect to Twitter. Refresh the page or try again later.';
			
		}
	}


	public function logout()
	{
		$next = $this->input->get('next');
        $this->facebook->destroySession();
        session_destroy();
        $this->session->sess_destroy();
        redirect($next);
	}
	

}