<?php 

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class Error extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
	}

    public function index()
    {
    	// set the status header
        $this->output->set_status_header('404');
        
        // page elements
		$d['title'] = "Oops! 404 page";
		$d['meta_desc'] = 'Page does not exist!';

		// load view
        $this->load->view('error/error_404',$d);
    }
	
}