<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */
class Email extends CI_Controller {

	public function __construct()
	{
		error_reporting(E_ALL);
		ini_set('display_errors', 1);
		parent::__construct();
		$this->load->library('Email');
		//$this->output->enable_profiler(TRUE);

	}

	function forgot_password()
	{
		$this->Email_model->forgot_password();
	}

	function new_message()
	{
		$this->Email_model->approved_email('26','video');
	}

	function deact()
	{
		$this->Email_model->account_deactivated('47');
	}

	function new_comments()
	{

	}

	function new_friend_request()
	{

	}

	function new_wall_post()
	{

	}

	function event_invite()
	{

	}

	function event_changes()
	{

	}

	function monthly_newsletter()
	{

	}

	public function welcome($user_id)
	{
		$this->Email_model->welcome($user_id);
	}

	public function forgot_pwd($email)
	{
		$this->Email_model->forgot_pwd($user_id);
	}

	public function banned($user_id)
	{
		$this->Email_model->banned($user_id);
	}

	function confirm_account($user_id)
	{
		$this->Email_model->confirm_account($user_id);
	}

	public function resend_conf()
	{
		$id = $this->session->userdata('user_id');
		if($id != '')
		{
			$this->Email_model->confirm_account($id);
		}
		
	}

}