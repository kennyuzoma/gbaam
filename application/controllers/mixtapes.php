<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class Mixtapes extends CI_Controller {

    function __construct()
    {
        parent::__construct();

		if($this->uri->segment(1) === 'mixtape')
    		redirect('mixtapes/');//.$this->uri->uri_string());
		
		$this->load->library('user_agent');

		// clean this up later... 
		// If user has not entered their email address for twitter registration....
		$user_status = $this->session->userdata('u_status');
		if(isset($user_status) && $user_status == '4')
			redirect('main/twitter_finish');

		//$this->output->enable_profiler(TRUE);
    }
    
    public function index()
    {
    
        $d['title'] = 'Mixtapes';
        $d['meta_desc'] = 'Listen, Download, and share the latest Mixtapes, Eps, Music Projects, and Albums!';

        // get the mixtapes
		$themixtapes = $this->Mixtape_model->getAllMixtapes_2013(8);
		$d['getMixtapes'] = $themixtapes['result'];
		$d['getMixtapesCount'] = $themixtapes['count'];

        if($this->Mixtape_model->isMOTWavail() == TRUE)
        {
	        	$d['getMotw'] = $this->Mixtape_model->getMotw();
			foreach($this->Mixtape_model->getMotw() as $g)
				$motwid =  $g->mid;

			
			$getmix = $this->Mixtape_model->get_vote_final($motwid);
			$d['isScore'] = $getmix['isScore'];
			$d['final_score'] = $getmix['final_score'];


        }
        
		$hmx = $this->Mixtape_model->getHotMixtapes();
		$d['getHotMixtapes'] = $hmx['result'];
		$d['getHotMixtapes_count'] = $hmx['count'];
		//$getLatestMix = $this->Mixtape_model->getLatestMix($latest);
        $this->load->view('mixtape/mixtape_home',$d);
    }
    
	public function tape($sname = '',$permalink = '')
	{
		

		if($sname == '' || $permalink == '')
			redirect('mixtapes');

		// this is if the url is already hashed
		$d['page_type'] = 'mixtape';

		$getMixtape = $this->Mixtape_model->getMixtape($sname,$permalink);
		if($getMixtape == FALSE)
		{
			show_404();
		}
		else
		{
			foreach($getMixtape as $gm)
			{
				$d['mixtapeid'] = $gm->id;
				$d['title'] = $gm->title .' - '. $gm->artist;	
				
				$d['desc'] = $gm->desc;
				$d['a_id'] = $gm->artist;
				$d['front_cover'] = $gm->front_cover;
				$d['back_cover'] = $gm->back_cover;
				$d['status'] = $gm->mixstatus;
				$d['v_count'] = $gm->v_count; 
				$d['d_count'] = $gm->d_count;
				$d['img_hash'] = $gm->img_hash;
				$d['hash'] = $gm->hash;
				$d['raw_zip_file'] = $gm->zip_file;
				$d['zip_file'] = base_url().$this->config->item('mixtape_zip_dir').$gm->zip_file;
				$d['permalink'] = $gm->permalink;
			}

			$this->Mixtape_model->addview('',$d['mixtapeid']);
			$getmix = $this->Mixtape_model->get_vote($d['mixtapeid']);
			$my_score = $getmix['my_score'];
			$total_score = $getmix['total_score'];
			//echo $my_score.'/'.$total_score;
			if($total_score == 0)
			{
				$d['isScore'] = FALSE;
			}
			else
			{
				$d['isScore'] = TRUE;
				$percent = ($my_score / $total_score) * 10;
				$percent = round($percent);
				$d['final_score'] = $percent;
			}
			$getTracks = $this->Mixtape_model->getMixtapeTracks($d['mixtapeid']);
			$d['get_tracks'] = $getTracks['result'];


			// disqus stuff
			$d['my_identifier'] = 'mixtape-'.$d['mixtapeid']; // this is for disqus
			$d['disqus_url'] = base_url().'mixtapes/'.$d['mixtapeid'].'/'.$d['permalink'];
			
	        $this->load->view('mixtape/mixtape_view',$d);	
	    }
	}
	
	public function all()
	{
		$d['title'] = 'All  Mixtapes';
		$d['meta_desc'] = "Browse all of our Gbaam Mixtapes.";

		$this->load->library('pagination');

		$config['base_url'] = base_url().'/mixtapes/all';
		$config['total_rows'] = $this->db->where('status','1')->get('mixtapes')->num_rows();
		$config['per_page'] = 16;
		$config['num_links'] = 10;
		$config['full_tag_open'] = '<div class="pagination2">';
		$config['full_tag_close'] = '</div>';
		
		$this->pagination->initialize($config);
		
		//$d['getMixtapes'] = $this->db->get('mixtapes', $config['per_page'], $this->uri->segment(3));
		$d['pagi_links'] = $this->pagination->create_links();

		$d['getMixtapes'] = $this->Mixtape_model->getAllMixtapes($config['per_page'], $this->uri->segment(3));

		$this->load->view('mixtape/mixtape_all',$d);	
	}

	public function d($mobile_hash = '')
	{	
		if($mobile_hash != '')
		{
			$hash = $mobile_hash;
		}
		else
		{
			$hash = $this->input->post('mixtape');
		}
		

		if($hash == '')
		{
			redirect('mixtapes');
		}
		else
		{
			$g = $this->Mixtape_model->getMixtape_by_hash($hash);

			$this->Mixtape_model->addview('d',$g->id);	
			
			$title = $g->title;
			$artist = $g->artist;
			$zip = $g->zip_file;	


			$file = base_url().$this->config->item('mixtape_zip_dir').$zip;
	 
			 if(!$file)
			 {  
			     // File doesn't exist, output error
			     die('file not found');
			 }
			 else
			 {
			 	/*if($this->agent->is_mobile())
			 	{
			 		$data = file_get_contents('/var/www/html/'.$this->config->item('mixtape_zip_dir').$zip);
					$filename = $zip;

			 		header('Content-Description: File Transfer');
					header('Content-Type: application/octet-stream');
					header('Content-Disposition: attachment; filename="'.$filename.'"');
					header("Content-Transfer-Encoding: binary");
					header('Expires: 0');
					header('Cache-Control: must-revalidate');
					header('Pragma: public');
					header("Content-Length: ".filesize($data));
					exit($data);
			 	}	
			 	else
			 	{*/
			     header('Location: '.$file);
			 	//}
			 }
		}

		

	}
	

	public function download($id)
	{
		if($id != '')
		{

			//foreach($this->Mixtape_model->getMixtape($id) as $g)
		//	{
		$g = $this->Mixtape_model->getMixtape_new($id);

				$title = $g->title;
				$artist = $g->artist;
				$zip = $g->zip_file;	
//}


			$file = base_url().$this->config->item('mixtape_zip_dir').$zip;

			//echo $file;
	 
			 if(!$file)
			 {  
			     // File doesn't exist, output error
			     die('file not found');
			 }
			 else
			 {

			     header('Location: '.$file);
			 }
		}
		else
		{
			redirect('mixtapes');
		}
		
	}
	
	public function add_vote($mixtapeid,$choice)
	{
		if($mixtapeid != '' || $choice != '')
		{
			$vote = $this->Mixtape_model->add_vote($mixtapeid,$choice);

			if($vote == TRUE)
			{
				$status['status'] = 'good';
			}
			if($vote == FALSE)
			{
				$status['status'] = 'bad';
			}

			echo json_encode($status);
		}

	}

	public function sd()
	{
		$g = $this->Mixtape_model->getMixtape_new('59');

				$title = $g->title;
				$artist = $g->artist;
				$zip = $g->zip_file;	

				//echo $zip;
//}

		$data = file_get_contents('/var/www/html/'.$this->config->item('mixtape_zip_dir').$zip);
					$filename = $zip;



		// Grab the file extension
		$x = explode('.', $filename);
		$extension = end($x);

			$mime = 'application/octet-stream';
		


			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'.$filename.'"');
			header("Content-Transfer-Encoding: binary");
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header("Content-Length: ".filesize($data));
		

		exit($data);


		//$file =  basename($file);
/*
					header('Content-Description: File Transfer');
				    header('Content-Type: application/octet-stream');
				    header('Content-Disposition: attachment; filename='.$file);
				    header('Content-Transfer-Encoding: binary');
				    header('Expires: 0');
				    header('Cache-Control: must-revalidate');
				    header('Pragma: public');
				    header('Content-Length: ' . filesize($file));*/

		

	}
	
}

?>
