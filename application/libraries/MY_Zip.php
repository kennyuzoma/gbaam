<?php


class MY_Zip extends CI_Zip {

	public function __construct()
    {
        parent::__construct();
    }

	function rename_and_read_file($path, $new_name, $preserve_filepath = FALSE)
	{
		if ( ! file_exists($path))
		{
			return FALSE;
		}

		if (FALSE !== ($data = file_get_contents($path)))
		{
			$full_path_with_name = str_replace("\\", "/", $path); 
			$path_only = dirname($full_path_with_name); 
			$new_name = $path_only.'/'.$new_name; 

			if ($preserve_filepath === FALSE)
			{
				$new_name = preg_replace("|.*/(.+)|", "\\1", $new_name);
			}


			$this->add_data($new_name, $data);
			return TRUE;
		}
		return FALSE;
	} 

}