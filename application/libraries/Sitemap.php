<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
*  Sitemap Class
*  Copyright (c) 2008 - 2013 All Rights Reserved.
*
*  Props to Mike's Imagination for the approach
*  http://www.mikesimagination.net/blog/post/29-Aug-12/Codeigniter-auto-XML-sitemap
*
*  Generates sitemap
*/

class Sitemap {

// CI instance property
	protected $ci;

	/**
	*  Constructor
	*/
	public function __construct()
	{
		// Get the CI instance by reference to make the CI superobject available in this library
		$this->ci =& get_instance();
	}

	/**
	*  Generate sitemap
	*/
	public function create()
	{
		// Begin assembling the sitemap starting with the header
		$sitemap = "<\x3Fxml version=\"1.0\" encoding=\"UTF-8\"\x3F>\n<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\" 
  xmlns:video=\"http://www.google.com/schemas/sitemap-video/1.1\">\n";

		// Static Pages not in database
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url() . "</loc>\n\t</url>\n\n";
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('about') . "</loc>\n\t</url>\n\n";
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('contact') . "</loc>\n\t</url>\n\n";
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('search') . "</loc>\n\t</url>\n\n";

		// from the main controller
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('main/login') . "</loc>\n\t</url>\n\n";
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('main/signup') . "</loc>\n\t</url>\n\n";
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('main/forgotpassword') . "</loc>\n\t</url>\n\n";
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('main/submit/video') . "</loc>\n\t</url>\n\n";
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('main/submit/gbaamtv') . "</loc>\n\t</url>\n\n";
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('main/submit/challenge') . "</loc>\n\t</url>\n\n";
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('main/terms') . "</loc>\n\t</url>\n\n";
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('main/privacy') . "</loc>\n\t</url>\n\n";
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('main/video_agreement') . "</loc>\n\t</url>\n\n";



		// Music Videos
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('video') . "</loc>\n\t</url>\n\n";
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('video/all') . "</loc>\n\t</url>\n\n";

		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('tv') . "</loc>\n\t</url>\n\n";
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('tv/variety') . "</loc>\n\t</url>\n\n";
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('tv/interviews') . "</loc>\n\t</url>\n\n";
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('tv/shortfilms') . "</loc>\n\t</url>\n\n";
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('tv/reality') . "</loc>\n\t</url>\n\n";
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('tv/documentary') . "</loc>\n\t</url>\n\n";
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('tv/all') . "</loc>\n\t</url>\n\n";
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('tv/all/variety') . "</loc>\n\t</url>\n\n";
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('tv/all/interviews') . "</loc>\n\t</url>\n\n";
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('tv/all/shortfilms') . "</loc>\n\t</url>\n\n";
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('tv/all/reality') . "</loc>\n\t</url>\n\n";
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('tv/all/documentary') . "</loc>\n\t</url>\n\n";

		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('challenge') . "</loc>\n\t</url>\n\n";

		$videos = $this->ci->Video_model->getAllVideos();
		foreach($videos as $v)
		{
			// image conditional
			if($v->thumb == '')
			{
				if($v->yt == 1)
					$thumb = 'http://img.youtube.com/vi/'.youtube_id_from_url($v->video_src).'/mqdefault.jpg';
				
				else
					$thumb = base_url().'thumbs/Gbaam3.jpg';
            	
			}
			else 
				$thumb =  base_url().'thumbs/'.$v->thumb;

			// site url conditional
			
			// gbaam tv
			if($v->gtv == '1' && $v->chall == '0')
			{
				$siteurl = site_url('tv/v/'.hashids_encrypt($v->id));
				$title = htmlspecialchars($v->title);

			}

			// challenge
			elseif($v->gtv == '0' && $v->chall == '1')
			{
				$siteurl = site_url('challenge/v/'.hashids_encrypt($v->id));
				$title = htmlspecialchars($v->title);
			}

			// music video
			elseif($v->gtv == '0' && $v->chall == '0')
			{
				$siteurl = site_url('video/'.hashids_encrypt($v->id));
				$title = htmlspecialchars($v->artist_name.' - '.$v->title);
			}
			
			// descript strip out fat
			$desc = strip_tags(htmlspecialchars($v->description));
			$desc = str_replace(array("\n","\r\n"), '', $desc);

			$sitemap .= "\t<url>\n";
			$sitemap .= "\t\t<loc>" . $siteurl . "</loc>\n";
			//	$sitemap .= "\t\t<lastmod>" . '2012-21-31' . "</lastmod>\n";
			$sitemap .= "\t\t<image:image>\n";
			$sitemap .= "\t\t\t<image:loc>".$thumb."</image:loc>\n";
			$sitemap .= "\t\t</image:image>\n";
			$sitemap .= "\t\t<video:video>\n";   
			$sitemap .= "\t\t<video:content_loc>".htmlspecialchars($v->video_src)."</video:content_loc>\n";
			//$sitemap .= "\t\t\t<video:player_loc allow_embed=\"no\" autoplay=\"ap=1\">".
							//$this->ci->config->item('player_url').'video/embed/'.hashids_encrypt($v->id).'/auto'
						//	."</video:player_loc>\n";
			$sitemap .= "\t\t\t<video:thumbnail_loc>".$thumb."</video:thumbnail_loc>\n";
			$sitemap .= "\t\t\t<video:title>".htmlspecialchars($title)."</video:title>\n";
			$sitemap .= "\t\t\t<video:description>".htmlspecialchars($desc)."</video:description>\n";
			$sitemap .= "\t\t</video:video>\n";
			$sitemap .= "\t</url>\n\n";
		}



		



		// mixtapes
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('mixtapes') . "</loc>\n\t</url>\n\n";
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('mixtapes/all') . "</loc>\n\t</url>\n\n";

		$this->ci->load->model('Mixtape_model');
		$mixtapes = $this->ci->Mixtape_model->getAllMixtapes();
		foreach($mixtapes as $m)
		{
			$sitemap .= "\t<url>\n\t\t<loc>" . site_url('mixtapes/'. $m->id .'/'.$m->permalink) . "</loc>\n\t</url>\n\n";
		}
		



		// editorial
		$sitemap .= "\t<url>\n\t\t<loc>" . site_url('editorial') . "</loc>\n\t</url>\n\n";

		$articles = $this->ci->Site_model->getArticles();
		foreach($articles as $a)
		{
			$sitemap .= "\t<url>\n\t\t<loc>" . site_url('editorial/'.$a->permalink) . "</loc>\n\t</url>\n\n";
		}



		// add blog stuff here


		// Close with the footer
		$sitemap .= "</urlset>\n";

		// Write the sitemap string to file. Make sure you have permissions to write to this file.
		$file = fopen('/var/www/html/sitemap.xml', 'w');
		fwrite($file, $sitemap);
		fclose($file);

		// If this is the production instance, attempt to update Google with the new sitemap.
		// (The instance is set in the index.php file)
		
		/*if(ENVIRONMENT === 'production')
		{
			// Ping Google via http request with the encoded sitemap URL
			$sitemap_url = site_url('sitemap.xml');
			$google_url = "http://www.google.com/webmasters/tools/ping?sitemap=".urlencode($sitemap_url);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,2);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt ($ch, CURLOPT_URL, $google_url);
			$response = curl_exec($ch);
			$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

			// Log error if update fails
			if (substr($http_status, 0, 1) != 2)
			{
				log_message('error', 'Ping Google with updated sitemap failed. Status: ' . $http_status);
				log_message('error', '    ' . $google_url);
			}
		}*/

		return;
	}
}