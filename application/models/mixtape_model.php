<?php 

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class Mixtape_model extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
        
    }
    
   
    function getMotw()
    {
		//arange by date
        $sql = "SELECT *, mixtapes.id AS mid FROM feat_items 
        		LEFT JOIN mixtapes ON mixtapes.id = feat_items.type_id 
				LEFT JOIN photos ON photos.photo_id = mixtapes.front_cover 
        		WHERE mixtapes.status = 1 and feat_type = 'motw' and active = 1 order by feat_items.type_id DESC LIMIT 1 ";
        $query = $this->db->query($sql); 
        return $query->result();
    }
  
   /*
   function getMotw()
    {
		//arange by date
        $sql = "SELECT * FROM mixtapes WHERE motw = 1 ORDER BY id DESC LIMIT 1 ";
        $query = $this->db->query($sql); 
        return $query->result();
    }
    */


    function makeMotw($mixtape_id)
	{
		$date_created = date('Y-m-d H:i:s');

		$sql = "INSERT INTO  mixtapes_motw ( mixtape_id, date_created ) 
				VALUES ('{$mixtape_id}',  '{$date_created}');";  
		$this->db->query($sql);
	}

	function isMOTWavail()
	{
		$sql = "SELECT * FROM feat_items LEFT JOIN mixtapes ON mixtapes.id = feat_items.type_id WHERE feat_type = 'motw' and mixtapes.status = 1 and feat_items.active = 1 ORDER BY feat_items.id DESC LIMIT 1";
        $query = $this->db->query($sql); 
        $count = $query->num_rows();

        if($count >= 1)
        {
        	return TRUE;
        }
        else
        {
        	return FALSE;
        }
	}
	 
	function getHotMixtapes()
	{
		$sql = "SELECT * 
				FROM feat_items
				LEFT JOIN mixtapes ON mixtapes.id = feat_items.type_id  
				LEFT JOIN photos ON photos.photo_id = mixtapes.front_cover 
				WHERE mixtapes.status = 1 and feat_items.active = 1 and feat_type = 'hot_mixtape'  order by feat_items.id DESC LIMIT 3";
        $query = $this->db->query($sql);
        $d['result'] = $query->result();
        $d['count'] = $query->num_rows();

        return $d;
	}

	function getHotMixtape($slot)
	{
		$sql = "SELECT * FROM feat_items LEFT JOIN mixtapes ON mixtapes.id = feat_items.type_id  LEFT JOIN photos ON photos.photo_id = mixtapes.front_cover WHERE mixtapes.status = 1 and feat_type = 'hot_mixtape' and slot = {$slot} order by feat_items.id DESC LIMIT 1";

		$query = $this->db->query($sql);

		return $query->row();
	}


	function AddToSlot($slot, $feat_type, $mixtapeid)
	{
		$sql = "UPDATE feat_items SET active=0 WHERE slot={$slot} and feat_type = '{$feat_type}' and type='mixtape'";
		$this->db->query($sql);

		$ins['feat_type'] = $feat_type;
		$ins['slot'] = $slot;
		$ins['type_id'] = $mixtapeid;
		$ins['type'] = 'mixtape';
		$ins['active'] = 1;
		$ins['datestart'] = date('Y-m-d H:i:s');

		$this->db->insert('feat_items',$ins);

		;
		$this->Admin_model->log_activity('feat_mixtape',$this->db->insert_id(),'slot','Feat Type: '.$feat_type.' Slot '.$slot);
	}

	function getHotMixtape_by_slot($slot)
	{
		$sql = "SELECT * FROM feat_items 
				LEFT JOIN mixtapes ON mixtapes.id = feat_items.type_id  
				LEFT JOIN photos ON photos.photo_id = mixtapes.front_cover  
				WHERE mixtapes.status = 1 and feat_type = 'hot_mixtape' and slot = {$slot} and mixtapes.status = 1 and feat_items.active = 1 order by feat_items.id DESC LIMIT 1";

		$query = $this->db->query($sql);

		return $query->row();
	}
	
	function getHotMixtape_count($slot)
	{
		$sql = "SELECT * FROM feat_items LEFT JOIN mixtapes ON mixtapes.id = feat_items.type_id  WHERE mixtapes.status = 1 and active = 1 and feat_type = 'hot_mixtape' and slot = {$slot} order by feat_items.id DESC LIMIT 1";

		$query = $this->db->query($sql);

		return $query->num_rows();
	}
	
	function getLatestMix()
	{
		$sql = "SELECT * FROM mixtapes  LEFT JOIN photos ON photos.photo_id = mixtapes.front_cover WHERE mixtapes.status = 1 Order by id DESC";
        $query = $this->db->query($sql);
        return $query->result();
	}
	
	function getMixtape($sname,$permalink)
	{ 
		if($sname != '' || $permalink != '')
		{
			$sql = "SELECT *, mixtapes.status as mixstatus from mixtapes LEFT JOIN photos ON photos.photo_id = mixtapes.front_cover where id = ".$sname." and permalink = '".$permalink."' and mixtapes.status != 0";	
			$query = $this->db->query($sql);
			if($query->num_rows() > 0)
				return $query->result();
			else
				return FALSE;
		}
		else
		{
			return FALSE;
		}
	}

	function getMixtape_new($sname)
	{ 
		$sql = "SELECT * from mixtapes LEFT JOIN photos ON photos.photo_id = mixtapes.front_cover where mixtapes.status != 0 and id = ".$sname;	
		$query = $this->db->query($sql);
		return $query->row();
	}

	function getMixtape_by_hash($hash)
	{ 
		$sql = "SELECT * from mixtapes LEFT JOIN photos ON photos.photo_id = mixtapes.front_cover where mixtapes.status != 0 and hash = '".$hash."'";	
		$query = $this->db->query($sql);
		return $query->row();
	}
	
	function getMixtape_new_admin($sname)
	{ 
		$sql = "SELECT *, mixtapes.status as mixstatus from mixtapes LEFT JOIN photos ON photos.photo_id = mixtapes.front_cover where id = ".$sname;	
		$query = $this->db->query($sql);
		return $query->row();
	}
	
	
	function getMixtapeUrl($sname)
	{
		$sql = "SELECT * from mixtapes where id = ".$sname;	
		$query = $this->db->query($sql); 
		foreach($query->result() as $b)
		{
			$artist = clean_url_slug($b->artist);
			$title = clean_url_slug($b->title); 
			$id = $b->id;
		}
		
		//$url = $artist.'-'.$title.'-_-'.$id;
		$url = $artist.'-'.$title;
		return $url;
	}
	
	function getAllMixtapes($limit = '', $offset = '')
	{
		$sql = "SELECT * from mixtapes  LEFT JOIN photos ON photos.photo_id = mixtapes.front_cover where mixtapes.status = 1 order by id desc";
			
			if($limit != '')
			{
				$sql .= " LIMIT";

				if($offset != '')
					$sql .= " {$offset},";

				$sql .= " {$limit}";
			}
				

			

		$query = $this->db->query($sql);
		return $query->result();	

	}

	function getAllMixtapes_admin($limit = '', $offset = '')
	{
		$sql = "SELECT * from mixtapes  LEFT JOIN photos ON photos.photo_id = mixtapes.front_cover WHERE mixtapes.status != 0  order by id desc";
			
			if($limit != '')
			{
				$sql .= " LIMIT";

				if($offset != '')
					$sql .= " {$offset},";

				$sql .= " {$limit}";
			}
				

			

		$query = $this->db->query($sql);
		return $query->result();	

	}


	function getAllMixtapes_2013($limit = '')
	{
		$sql = "SELECT * from mixtapes  LEFT JOIN photos ON photos.photo_id = mixtapes.front_cover Where mixtapes.status = 1 order by id desc";
			if($limit != '')
				$sql .= " LIMIT {$limit}";
		$query = $this->db->query($sql);

		$d['result'] = $query->result();	
		$d['count'] = $query->num_rows;
		return $d;
	}

	function getMixtapeTracks($mixtape_id)
	{
		$sql = "SELECT * FROM mixtape_tracks WHERE mixtape_id = ".$mixtape_id." Order by track_num ASC";
		$query = $this->db->query($sql);

		$d['result'] = $query->result();
		$d['count'] = $query->num_rows();

		return $d;
	}
	
	function addview($d = '',$id)
	{
		$view = 1;//rand(1, 3);
		if ($d == '')
		{
			$sql = "UPDATE mixtapes SET v_count=v_count+{$view} WHERE id={$id}";
		}
		elseif($d == 'd')
			$sql = "UPDATE mixtapes SET d_count=d_count+{$view} WHERE id={$id}";
			
		$this->db->query($sql);		
	}

	function add_vote($mixtapeid,$choice)
	{
		$user_id = $this->session->userdata('user_id');

		if( $this->session->userdata('logged_in') == FALSE )
		{
			return FALSE;
		}
		else
		{
			$sql1 = "SELECT * From votes WHERE type_id = {$mixtapeid} and voter_id = {$user_id}  and type = 'mixtape' order by id desc limit 1";

			$query = $this->db->query($sql1);

			if($query->num_rows() == 1)
			{
				$sql = "UPDATE votes SET choice='{$choice}' WHERE type_id = {$mixtapeid} and voter_id = {$user_id} and type = 'mixtape' ";
			}
			else
			{
				$sql = "INSERT INTO  votes ( type_id  ,  choice, voter_id, type )
				VALUES ('{$mixtapeid}',  '{$choice}', {$user_id}, 'mixtape');";
			}
			
			$this->db->query($sql);

			return TRUE;
		}
		
	}
	function did_i_vote($mixtape_id)
	{
		$user_id = $this->session->userdata('user_id');

		$sql = "SELECT * From votes WHERE type_id = {$mixtape_id} and voter_id = {$user_id} and type = 'mixtape' order by id desc limit 1";

		$query = $this->db->query($sql);

		$q['num_rows'] = $query->num_rows();
		$q['row'] = $query->row();

		return $q;

	}
	
	function get_vote($mixtapeid)
	{
		$sql = "SELECT choice FROM votes WHERE type_id = {$mixtapeid} and type = 'mixtape' ";
		
		$query = $this->db->query($sql);

		$bl = array();
		foreach($query->result() as $b) 
		{
			$bl[] = $b->choice;	
		}
		
		$rows = $query->num_rows();
		$d['total_score'] = $rows*4;
		$d['my_score'] = array_sum($bl);
		
		return $d; 
	}

	function get_vote_final($mixtapeid)
	{
		$sql = "SELECT choice FROM votes WHERE type_id = {$mixtapeid} and type = 'mixtape' ";
		
		$query = $this->db->query($sql);

		$bl = array();
		foreach($query->result() as $b) 
		{
			$bl[] = $b->choice;	
		}
		
		$rows = $query->num_rows();
		$getmix['total_score'] = $rows*4;
		$getmix['my_score'] = array_sum($bl);
		
		$my_score = $getmix['my_score'];
		$total_score = $getmix['total_score'];
			
		//echo $my_score.'/'.$total_score;
		if($total_score == 0)
		{
			$d['isScore'] = FALSE;
			$d['final_score'] = FALSE;
		}
		else
		{
			$d['isScore'] = TRUE;
			$percent = ($my_score / $total_score) * 10;
			$percent = round($percent);
			$d['final_score'] = $percent; 
		}

		return $d; 
	}

	

	function updateMixtapeTracks($mixtape_id)
	{
		// get all the mixtape tracks and get the LAST track
		$sql = "SELECT * FROM `mixtape_tracks` WHERE mixtape_id = 15 order by id  desc";
		$query = $this->db->query($sql);
		$q = $query->row();

		//last track
		$last_track_id = $q->id;

		//foreach
	}

	function deleteMixtape($mixtape_id)
	{

		/*$this->db->delete('mixtapes', array('id' => $mixtape_id)); 
		$this->db->delete('votes', array('type_id' => $mixtape_id)); 
		$this->db->delete('mixtape_tracks', array('mixtape_id' => $mixtape_id)); 
		$this->db->delete('feat_items', array('type_id' => $mixtape_id,'type' => 'mixtape'));*/

		;
		$this->Admin_model->log_activity('mixtape',$mixtape_id,'delete');

		$upd1['status'] = '0';
		$upd1['date_deleted'] = date('Y-m-d H:i:s');
		$this->db->update('mixtapes', $upd1, array('id' => $mixtape_id)); 

		$upd['status'] = '0';
		$this->db->update('votes', $upd, array('type_id' => $mixtape_id)); 
		$this->db->update('mixtape_tracks', $upd, array('mixtape_id' => $mixtape_id)); 
		$this->db->update('feat_items', $upd, array('type_id' => $mixtape_id,'type' => 'mixtape'));

		//unlink all mixtapes
	}

	function delete_motw($mixtape_id)
	{

		$this->db->delete('feat_items', array('type_id' => $mixtape_id,'feat_type' => 'motw')); 

		//unlink all mixtapes
	}


	function deleteMixtapeTrack($track_id)
	{
		// delete the mixtape tracks
		$sql3 = "DELETE FROM mixtape_tracks where id = {$track_id} ";
		$this->db->query($sql3);
	}

	function insertMixtape()
	{
		$permalink = clean_url_slug($this->input->post('artist_name').'-'.$this->input->post('mixtape_title'));

		$date = date("Y-m-d H:i:s", strtotime($this->input->post('date')));

		$data = array(
			'title' => $this->input->post('mixtape_title'),
			'desc' => $this->input->post('description'),
			'dj' => $this->input->post('dj_name'),
			'artist' => $this->input->post('artist_name'),
			'permalink' => $permalink,
			'tags' => $this->input->post('tags'),
			'poster_id' => $this->session->userdata('admin_user_id'),
			'status' => '2',
			'date_created' => $date
		);

		// insert the mixtape
		$this->db->insert('mixtapes',$data);
		$mixtape_id = $this->db->insert_id();

		// create the mixtape hash
		$upd['hash'] = md5($mixtape_id);
		$this->db->where('id',$mixtape_id);
		$this->db->update('mixtapes',$upd);
		
		// log this activity.
		$this->log_activity('mixtape',$mixtape_id,'create');

		return $mixtape_id;
	}

	function updateMixtape($mixtape_id,$new = '')
	{
		$status = $this->input->post('status');
		$date = $this->input->post('date');

		if($status == '4')
			$status = '1';
		
		$data = array(
			'title' => $this->input->post('mixtape_title'),
	        'dj' => $this->input->post('dj_name'),
	        'desc' => $this->input->post('description'),
	        'artist' => $this->input->post('artist_name'),
			'permalink' => clean_url_slug($this->input->post('artist_name').'-'.$this->input->post('mixtape_title'), $mixtape_id),
			'tags' => $this->input->post('tags'),
			'status' => $this->input->post('status')
	    );

	    if($new == '')
	    	$data['date_edited'] = date('Y-m-d H:i:s');

		$this->db->where('id', $mixtape_id);
		$this->db->update('mixtapes', $data); 
		$this->log_activity('mixtape',$mixtape_id,'update');
	}

	function updateTracksOrder()
	{
		$i = 1;

		foreach ($_POST['post'] as $value) {
		    // Execute statement:
		    $sql =  "UPDATE mixtape_tracks SET track_num = $i WHERE id = $value";
		    $this->db->query($sql);

		    $i++;
		}
	}
/*
	function getAllMixtapes()
	{
		$sql = "SELECT * FROM mixtapes where status != 0";
		$query = $this->db->query($sql);
		return $query->result();
	}
*/
	function new_hotmixtape($id,$slot)
	{

		$date_created = date('Y-m-d H:i:s');

		$sql = "INSERT INTO feat_items (type,feat_type,type_id,slot,date_created) VALUES ('mixtape','hot_mixtape',{$id},{$slot},{$date_created});";
		$this->db->query($sql);
		$this->log_activity('hot_mixtape',$id,'create','Slot $slot');
	}

	


	############################### GBAAM MOBILE APPs API SUPPORT ########################
	
}

?>