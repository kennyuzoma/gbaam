<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class Email_model extends CI_model {

	function __construct() {
		parent::__construct();
	}

	function welcome($user_id)
	{
		// message variables
		$message['subject'] = 'Welcome to Gbaam!';

		// get the user
		$user = $this->User_model->get_user($user_id);

		// to
		$message['to'][0]['email'] = $user->email;
		$message['to'][0]['name'] = $user->display_name;
		$message['to'][0]['type'] = 'to';

		// template stuff
		$template_name = 'Welcome';
		$template_content = array(
		    array(
		        'name' => 'main',
		        'content' => 'Thanks for signing up.'
		    )
		);

		$message['merge_vars'][] = array(
		        'rcpt' => $message['to'][0]['email'],
		        'vars' => array(
		        	array(
		                'name' => 'SUBJECT',
		                'content' => $message['subject']
		            ),
		            array(
		                'name' => 'DISPLAY_NAME',
		                'content' => $message['to'][0]['name']
		            )
				)
    		);

		// send the email
		sendthatemail($template_name, $template_content, $message, $user);
	}

	function forgot_pwd($email)
	{
		// message variables
		$message['subject'] = 'Password Reset';
 
		//get the reset key
		$sql = "SELECT * FROM reset_pw where email = '{$email}' order by id desc limit 1";
		$query = $this->db->query($sql);
		$rp = $query->row();

		//prepare the message
		$email = $email;
		$key = $rp->reset_key;

		// get the user
		$user = $this->User_model->get_user_by_email($email);

		$reset_url = base_url() .'main/resetpw?email='.$email.'&key='.$key;

		// to
		$message['to'][0]['email'] = $user->email;
		$message['to'][0]['name'] = $user->display_name;
		$message['to'][0]['type'] = 'to';

		// template stuff
		$template_name = 'Forgot Password';
		$template_content = '';

		$message['merge_vars'][] = array(
		        'rcpt' => $message['to'][0]['email'],
		        'vars' => array(
		        	array(
		                'name' => 'SUBJECT',
		                'content' => $message['subject']
		            ),
		            array(
		                'name' => 'DISPLAY_NAME',
		                'content' => $message['to'][0]['name']
		            ),
		            array(
		            	'name' => 'RESET_URL',
		            	'content' => $reset_url

		            )
				)
    		);

		// send the email
		sendthatemail($template_name, $template_content, $message);
	}

	function confirm_account($user_id)
	{
		// message variables
		$message['subject'] = 'Confirm Your Account';

		// get the user
		$user = $this->User_model->get_user($user_id);

		//prepare the message
		$user_id_hash = hashids_encrypt($user_id);

		$confirm_url = base_url().'users/confirm_account/'.$user_id_hash;

		// to
		$message['to'][0]['email'] = $user->email;
		$message['to'][0]['name'] = $user->display_name;
		$message['to'][0]['type'] = 'to';

		// template stuff
		$template_name = 'Confirm Account';
		$template_content = '';

		$message['merge_vars'][] = array(
		        'rcpt' => $message['to'][0]['email'],
		        'vars' => array(
		        	array(
		                'name' => 'SUBJECT',
		                'content' => $message['subject']
		            ),
		            array(
		                'name' => 'DISPLAY_NAME',
		                'content' => $message['to'][0]['name']
		            ),
		            array(
		            	'name' => 'CONFIRM_URL',
		            	'content' => $confirm_url

		            )
				)
    		);

		// send the email
		sendthatemail($template_name, $template_content, $message);
	}

	function deactivated($user_id)
	{

	}

	function banned($user_id)
	{
		// message variables
		$message['subject'] = 'Your Account has been banned';

		// get the user
		$user = $this->User_model->get_user($user_id);

		// to
		$message['to'][0]['email'] = $user->email;
		$message['to'][0]['name'] = $user->display_name;
		$message['to'][0]['type'] = 'to';

		// template stuff
		$template_name = 'Banned Account';
		$template_content = '';

		$message['merge_vars'][] = array(
		        'rcpt' => $message['to'][0]['email'],
		        'vars' => array(
		        	array(
		                'name' => 'SUBJECT',
		                'content' => $message['subject']
		            ),
		            array(
		                'name' => 'DISPLAY_NAME',
		                'content' => $message['to'][0]['name']
		            )
				)
    		);

		// send the email
		sendthatemail($template_name, $template_content, $message, $user);
	}

	function contact_us_email($ins)
	{

		// message variables
		$message['subject'] = 'New Gbaam Contact Us Msg';

		// get the user
		//$user = $this->User_model->get_user($user_id);

		// to
		$message['to'][0]['email'] ='kennyuzoma@gmail.com';
		$message['to'][0]['name'] = 'Kenny Uzoma';
		$message['to'][0]['type'] = 'to';

		// template stuff
		$template_name = 'Contact Us';
		$template_content = array(
		    array(
		        'name' => 'from',
		        'content' => $ins['person_name'].' ('.$ins['email'].')'
		    ),
		    array(
		        'name' => 'subject',
		        'content' => $ins['subject']
		    ),
		    array(
		        'name' => 'date_sent',
		        'content' => date('F j, Y g:i:s A')
		    ),
		    array(
		        'name' => 'message',
		        'content' => nl2br($ins['message'])
		    )
		);

		$message['merge_vars'][] = array(
		        'rcpt' => $message['to'][0]['email'],
		        'vars' => array(
		        	array(
		                'name' => 'SUBJECT',
		                'content' => $message['subject']
		            )
				)
    		);

		// send the email
		sendthatemail($template_name, $template_content, $message);
	}

	function article_reviewed($article_id,$action)
	{
		// get the article
		$article = $this->Site_model->getArticleById($article_id);	
		$link = base_url().'editorial/'.$article->permalink;

		// message variables
		if($action == 'approved')
		{
			$message['subject'] = 'Your Gbaam Article was approved!';

			$themessage = '<h2>'.$message['subject'].'</h2><br />';
			$themessage .= 'Congrats, Your article submission was approved!<br><br>
							You can View it live here - <a href="'.$link.'">'.$link.'</a><Br><br>
							Thanks!';
		}

		if($action == 'deny')
		{
			$message['subject'] = 'Your Gbaam Article was not approved.';
				
			$deny_reasons = explode(',', $article->deny_reasons);
			$deny_reasons_other = $article->deny_reasons_other;

			$themessage = '<h2>'.$message['subject'].'</h2><br />';

			if(($deny_reasons_other == '') || empty($deny_reasons))
			{
				$themessage .= 'We were not able to approve your article at this time.<br><br>';
			}
			else
			{
				$themessage .= 'We were not able to approve your article because it contained the following...<br><br>';
			
			
				if(!empty($info->deny_reasons))
				{
					$deny_reasons = explode(',', $info->deny_reasons);

					foreach($deny_reasons as $k)
					{
						$r = $this->Admin_model->get_reason($k,'deny');
						$themessage .= '<b> - '. $r->display_name.'</b><br>';
					}
				}
			
				$themessage .= '<br>Other Reasons: <b>'.$deny_reasons_other.'</b><Br><br>';
				$themessage .= 'If you believe this was an error, please email us at <a href="mailto:gbaamtv@gmail.com">Gbaamtv@gmail.com</a>';
			}
		}

		// get the user
		$user = $this->Admin_model->get_user($article->poster_id);

		// to
		$message['to'][0]['email'] = $user->email;
		$message['to'][0]['name'] = $user->firstname.' '.$user->lastname;
		$message['to'][0]['type'] = 'to';

		

		// template stuff
		$template_name = 'Blank';
		$template_content = array(
		    array(
		        'name' => 'main',
		        'content' => $themessage
		    )
		);

		$message['merge_vars'][] = array(
		        'rcpt' => $message['to'][0]['email'],
		        'vars' => array(
		        	array(
		                'name' => 'SUBJECT',
		                'content' => $message['subject']
		            ),
		            array(
		                'name' => 'DISPLAY_NAME',
		                'content' => $message['to'][0]['name']
		            )
				)
    		);

		// send the email
		sendthatemail($template_name, $template_content, $message);

		
	}

	function new_admin($user_id)
	{
		// message variables
		$message['subject'] = 'New Admin Created!';

		// get the user
		$admin = $this->Admin_model->get_user($user_id);

		// to
		$message['to'][0]['email'] = 'Kennyuzoma@gmail.com';
		$message['to'][0]['name'] = 'Kenny Uzoma';
		$message['to'][0]['type'] = 'to';

		// the message
		$themessage = 'Just emailing to let you know that a New admin account was created. Here are some details...<br><br>
					Username: <b>'.$admin->username.'</b><br><br>
					First Name: <b>'.ucwords($admin->firstname).'</b><br><br>
					Last Name: <b>'.ucwords($admin->lastname).'</b><br><br>
					Admin Type: <b>'.$this->Admin_model->get_admin_type($admin->admin_type).'</b><br><br>';

		// template stuff
		$template_name = 'Blank';
		$template_content = array(
		    array(
		        'name' => 'main',
		        'content' => $themessage
		    )
		);

		$message['merge_vars'][] = array(
		        'rcpt' => $message['to'][0]['email'],
		        'vars' => array(
		        	array(
		                'name' => 'SUBJECT',
		                'content' => $message['subject']
		            ),
		            array(
		                'name' => 'DISPLAY_NAME',
		                'content' => $message['to'][0]['name']
		            )
				)
    		);

		// send the email
		sendthatemail($template_name, $template_content, $message);

		
	}

	function pending_result_email($id,$new_video_id)
	{
		// get the video
		$info = $this->Admin_model->getPendingVideo($id);

		

		// Gbaam TV - Video link
		if($info->gtv == '1' && $info->chall == '0')
			$videolink = 'tv/v/'.hashids_encrypt($new_video_id); 
		
		// Challenge - video link
		elseif($info->gtv == '0' && $info->chall == '1')
			$videolink = 'challenge/v/'.hashids_encrypt($new_video_id);

		// Video - video link
		else
			$videolink = 'video/'.hashids_encrypt($new_video_id); 	
			
		// set the subject and denial reasons
		if($info->status == 'deny')
		{
			$subject = 'We did not approve your video submission.';

			
			$themessage = '<h2>'.$subject.'</h2><br />';

			if(($info->deny_reasons_other == '') || empty($info->deny_reasons))
			{
				$themessage .= 'Sorry '. ucwords($info->firstname.' '.$info->lastname).', <br>
						We were not able to approve your video at this time. <br><br>';
			}
			else
			{
				$themessage .= 'Sorry '. $info->firstname.' '.$info->lastname.', <br>
						We were not able to approve your video at this time because your video contained the following...<br><br>';
			
				if(!empty($info->deny_reasons))
				{
					$deny_reasons = explode(',', $info->deny_reasons);

					foreach($deny_reasons as $k)
					{
						$r = $this->Admin_model->get_reason($k,'deny');
						$themessage .= '<b> - '. $r->display_name.'</b><br>';
					}
				}

				$deny_reasons_other = $info->deny_reasons_other;
				$themessage .= '<br> Other Reasons: '.$deny_reasons_other.'</b><Br><br>
							If you believe this was an error, please email us at <a href="mailto:gbaamtv@gmail.com">Gbaamtv@gmail.com</a>';
			}
			
		}
		// set the approval message
		elseif($info->status == 'approved')
		{
			$subject = 'Your video has been approved!';

			$themessage = '<h2>'.$subject.'</h2><br />';
			$themessage .= 'Congratulations ' .ucwords($info->firstname.' '.$info->lastname).'!
						<br>
						Your video has been <span style="color:green;font-weight:bold;">APPROVED</span> and your video link is...<br> <br>

						'.base_url().$videolink.'<br><br>
						
						Thank you for submiting your video with us and Don\'t forget to share it!';
			
		}

		// message variables
		$message['subject'] = $subject;

		// to
		$message['to'][0]['email'] = $info->email;
		$message['to'][0]['name'] = $info->firstname.' '.$info->lastname;
		$message['to'][0]['type'] = 'to';

		// template stuff
		$template_name = 'Blank';
		$template_content = array(
		    array(
		        'name' => 'main',
		        'content' => $themessage
		    )
		);

		$message['merge_vars'][] = array(
	        'rcpt' => $message['to'][0]['email'],
	        'vars' => array(
	        	array(
	                'name' => 'SUBJECT',
	                'content' => $message['subject']
	            ),
	            array(
	                'name' => 'DISPLAY_NAME',
	                'content' => $message['to'][0]['name']
	            )
			)
		);

		// send the email
		sendthatemail($template_name, $template_content, $message);

	}

	function submit_email($id,$type)
	{

		switch ($type) {
			case 'gbaamtv':
				$thetypeword = 'GbaamTV';
				$thetype = $thetypeword;
				
				break;

			case 'video':
				$thetypeword = 'Video';
				$thetype = $thetypeword;
				
				break;

			case 'challenge':
				$thetypeword = 'Challenge';
				$thetype = 'Gbaam Challenge';
				
				break;
			
			default:
				break;
		}

		$d['submission'] = TRUE;

		//get me
		$info = $this->Admin_model->getPendingVideo($id);
		$newDate = date('F j, Y g:i:s A', strtotime($info->date_submitted));

		// prepare user email.... store in DB later
		$user_subject = 'We received your '.$thetype.' Submission!';
		$user_message = '<h2>We received your submission!</h2><br />';
		$user_message .= ucwords($info->firstname.' '.$info->lastname).', we are emailing you to let you know we received your <b>'.$thetypeword.'</b> submission and it\'s currently in review! <br><br>You should recieve a follow up email after our decision in 1-3 business days. Thank you for choosing Gbaam!</p>';

		// prepare admin email ...
		$admin_subject = 'Gbaam! New '.$thetype.' Submission!';
		$admin_message = '<h2>New Submission!</h2><br />';
		$admin_message .= 'There is a new <b>'. $thetypeword .'</b> submission!<br>
							Here is some quick info<br>
							Person Name: <b>'. $info->firstname.' '.$info->lastname.'</b><Br>
							Type: <b>'. $thetypeword.'</b><br>
							Title: <b>'. $info->title.'</b><br>';

		if($info->artist_name != ''):
			$admin_message .= 'Artist Name: <b>'. $info->artist_name.'</b><br>';
		endif;

		$admin_message .= 'Video Source: <b><a href="'. $info->video_src.'">'. $info->video_src.'</a></b><br>
								Submitted Date: <b>'. $newDate.'</b><Br><br>

								<a href="'.base_url(). $this->config->item('admin_location') .'/"><u>Login Now</u></a> to your admin area to review this video!';


		// message variables
		$message['subject'] = '*|SUBJECT|*';

		// to the user
		$message['to'][0]['email'] = $info->email;
		$message['to'][0]['name'] = ucwords($info->firstname.' '.$info->lastname);
		$message['to'][0]['type'] = 'to';

		// to the admin
		$message['to'][1]['email'] = 'kennyuzoma@gmail.com';
		$message['to'][1]['name'] = 'Kenny Uzoma';
		$message['to'][1]['type'] = 'to';

		// template stuff
		$template_name = 'submitpending';
		$template_content = array(
		    array(
		        'name' => 'main',
		        'content' => ''
		    )
		);

		$message['merge_vars'][] = array(
	        'rcpt' => $message['to'][0]['email'],
	        'vars' => array(
		        	array(
		                'name' => 'SUBJECT',
		                'content' => $user_subject
		            ),
		            array(
		                'name' => 'CONTENT',
		                'content' => $user_message
		            )
			)
		);

		$message['merge_vars'][] =  array(
	        'rcpt' => $message['to'][1]['email'],
	        'vars' => array(
		        	array(
		                'name' => 'SUBJECT',
		                'content' => $admin_subject
		            ),
		            array(
		                'name' => 'CONTENT',
		                'content' => $admin_message
		            )
				)
		);

		// send the email
		sendthatemail($template_name, $template_content, $message);
	}

	function isEmailSubscribed($email)
	{
		$sql = "SELECT * FROM subscribers where email = '{$email}' LIMIT 1";

		$q = $this->db->query($sql);

		if($q->num_rows() == 1)
			return TRUE;
		else
			return FALSE;
	}

	function subscribe_me($type = '')
	{
		// if a logged in user clicked the "subscribe" button which doesnt require
		// a form fill out, OR if a user subscribed to the email thru the settings
		if($type == 'button' || $type == 'settings')
		{	
			// get the user (Helper method)
			$user_id = loggedin_user_id();
			$user = get_user($user_id);

			// get this users info
			$email = $user->email;
			$firstname = $user->firstname;
			$lastname = $user->lastname;
			$displayname = $user->display_name;

			// if the first name is blank then set it to the display name.
			if($firstname == '')
				$firstname = $displayname;


			// insert into subscribe table
			$ins['lastname'] = $lastname;
			$ins['displayname']  = $displayname;
			$ins['user_id'] = $user_id;



			// update email_gbaam_newletter option in user settings
			$this->User_model->user_setting($user_id, 'email_gbaam_news', '1');
		}
		// a user that was not logged in (submitted a regular signup form)
		else 
		{
			// get the post vars from the form
			$firstname = $this->input->post('name');
			$email = $this->input->post('email');

			//set this to blank right now.
			$displayname = ''; 

			// lets check if this user already is a gbaam user
			$user = $this->User_model->get_user_by_email($email);
			if($user)
			{
				// get the user info
				$displayname = $user->display_name;
				$lastname = $user->lastname;
				$user_id = $user->id;

				// add some data to the insert array that is going to be inserted
				$ins['lastname'] = $lastname;
				$ins['displayname']  = $displayname;
				$ins['user_id'] = $user_id;

				// then lets update the users setting
				$this->User_model->user_setting($user_id, 'email_gbaam_news', '1');
			}

		}

		// insert data into the subscribe table
		$ins['email'] = $email;
		$ins['firstname'] = $firstname;
		$ins['list'] = '1';
		$ins['date_subbed'] = current_time();

		$this->db->insert('subscribers',$ins);


		// subscribe the user to the Gbaam NewsLetter 
		$this->load->library('mailchimp_library');
		
		$result = $this->mailchimp_library->call('lists/subscribe', array(
			'id'                => '56de05bbdf', // store this in a variable
			'email'             => array('email' => $email),
			'merge_vars'        => array('FNAME' => $firstname, 'LNAME' => $lastname, 'DNAME' => $displayname),
			'double_optin'      => false,
			'update_existing'   => true,
			'replace_interests' => false,
			'send_welcome'      => false,
		));

	}

	/**
	 * Unsubscribe from the email.
	 * For now, this is unsubscribing from just one list (Gbaam Newsletter)
	 * 
	 * Todo
	 * - Add list argument so that we can unsubscribe by list. 
	 * - Unsubscribe by Email?
	 * @return [type] [description]
	 */
	function unsubscribe_me()
	{
		if(is_loggedin())
		{
			// get the user
			$user_id = loggedin_user_id();
			$user = get_user($user_id);

			// update the email_gbaam_news setting
			$this->User_model->user_setting($user_id, 'email_gbaam_news', '0');
		}

		// remove the record from the unsubscribe area
		$sql = "DELETE FROM subscribers WHERE user_id = '{$user_id}'";
		$this->db->query($sql);

		// unsubscribe the user from mailchimp
		$this->load->library('mailchimp_library');

		$result = $this->mailchimp_library->call('lists/unsubscribe', array(
			'id'                => '56de05bbdf', // store this in a variable
			'email'             => array('email' => $user->email),
			'send_goodbye'      => false,
			'send_notify'  		=> false
		));

	}




}