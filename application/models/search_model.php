<?php 

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class Search_model extends CI_Model {
	
	function __construct()
    {
        parent::__construct();
    }
	
	function getSearch($term , $type, $limit ='', $offset = '')
	{
		if(strstr($term,'%20'))
			$goodterm = str_replace('%20',' ',$term);
		else
			$goodterm = $term;
		
		if($type == 'video')
		{
			if(strlen($term) < 5)
			{
				$sql = "SELECT * FROM `video` WHERE (title LIKE '%{$term}%' or artist_name LIKE '%{$term}%' or tags LIKE '%{$term}%') and gtv = 0 and chall = 0 and video.status = 1 order by id DESC";
			}
			else
			{
				$sql = "select * FROM video WHERE MATCH (title,artist_name,tags) AGAINST";
			  	$sql .= " ('+{$goodterm}' IN BOOLEAN MODE) and gtv = 0 and chall = 0 and video.status = 1 order by id DESC";
			}
		}

		if($type == 'tv')
		{
			if(strlen($term) < 5)
			{
				$sql = "SELECT * FROM `video` WHERE (title LIKE '%{$term}%' or artist_name LIKE '%{$term}%' or tags LIKE '%{$term}%') and gtv = 1 and chall = 0 and video.status = 1 order by id DESC";
			}
			else
			{
				$sql = "select * FROM video WHERE MATCH (title,artist_name,tags) AGAINST";
			  	$sql .= " ('+{$goodterm}' IN BOOLEAN MODE) and gtv = 1 and chall = 0 and video.status = 1 order by id DESC";
			}
		}
		if($type == 'chal')
		{
			if(strlen($term) < 5)
			{
				$sql = "SELECT * FROM `video` WHERE (title LIKE '%{$term}%' or artist_name LIKE '%{$term}%' or tags LIKE '%{$term}%') and gtv = 0 and chall = 1  and video.status = 1 order by id DESC";
			}
			else
			{
				$sql = "select * FROM video WHERE MATCH (title,artist_name,tags) AGAINST";
			  	$sql .= " ('+{$goodterm}' IN BOOLEAN MODE) and gtv = 0 and chall = 1 and video.status = 1 order by id DESC";
			}
		}

		if($type == 'mixtape')
		{
			$sql = "SELECT *  FROM `mixtapes` WHERE `title` LIKE '%{$term}%' OR `desc` LIKE '%{$term}%' OR `artist` LIKE '%{$term}%' OR `dj` LIKE '%{$term}%'  or tags LIKE '%{$term}%' and mixtapes.status = 1 order by id DESC";
		}

		if($type == 'song')
		{
			$sql = "SELECT *  FROM `songs` WHERE `title` LIKE '%{$term}%' OR `desc` LIKE '%{$term}%' OR `artist` LIKE '%{$term}%' or tags LIKE '%{$term}%' and songs.status = 1 order by id DESC";
		}

		if($type == 'articles')
		{
			$sql = "SELECT *  FROM `articles` WHERE `title` LIKE '%{$term}%' OR `body` LIKE '%{$term}%' or tags LIKE '%{$term}%' and articles.status = 1 order by id DESC";
		}

		if($limit != '')
			{
				$sql .= " LIMIT";

				if($offset != '')
					$sql .= " {$offset},";

				$sql .= " {$limit}";
			}
		
		/*if($type == 'artist')
		{
			$sql = "SELECT * FROM artist WHERE name LIKE '%{$term}%' or label LIKE '%{$term}%'";
			if($limit != '')
				$sql .= " LIMIT {$limit}";
		}
		
		if($type == 'store')
		{
			$sql = "SELECT * FROM products WHERE name LIKE '%{$term}%'";
			if($limit != '')
				$sql .= " LIMIT {$limit}";
		}*/
		
		
		$table = $this->db->query($sql);
		if ($table->num_rows() > 0)
		{
			foreach ($table->result() as $row)
			{
			  $data[] = $row;
			}
			return $data;
		}
		else {
			return FALSE;
		}	
	}

	function getSearch_count($term , $type)
	{
		if(strstr($term,'%20'))
			$goodterm = str_replace('%20',' ',$term);
		else
			$goodterm = $term;
		
		if($type == 'video')
		{
			if(strlen($term) < 5)
			{
				$sql = "SELECT * FROM `video` WHERE (title LIKE '%{$term}%' or artist_name LIKE '%{$term}%' or tags LIKE '%{$term}%') and gtv = 0 and chall = 0 and video.status = 1 ";
			}
			else
			{
				$sql = "select * FROM video WHERE MATCH (title,artist_name,tags) AGAINST";
			  	$sql .= " ('+{$goodterm}' IN BOOLEAN MODE) and gtv = 0 and chall = 0 and video.status = 1 ";
			}
		}
		if($type == 'tv')
		{
			if(strlen($term) < 5)
			{
				$sql = "SELECT * FROM `video` WHERE (title LIKE '%{$term}%' or artist_name LIKE '%{$term}%' or tags LIKE '%{$term}%') and gtv = 1 and chall = 0 and video.status = 1 ";
			}
			else
			{
				$sql = "select * FROM video WHERE MATCH (title,artist_name,tags) AGAINST";
			  	$sql .= " ('+{$goodterm}' IN BOOLEAN MODE) and gtv = 1 and chall = 0 and video.status = 1 ";
			}
		}
		if($type == 'chal')
		{
			if(strlen($term) < 5)
			{
				$sql = "SELECT * FROM `video` WHERE (title LIKE '%{$term}%' or artist_name LIKE '%{$term}%' or tags LIKE '%{$term}%') and gtv = 0 and chall = 1 and video.status = 1 ";
			}
			else
			{
				$sql = "select * FROM video WHERE MATCH (title,artist_name,tags) AGAINST";
			  	$sql .= " ('+{$goodterm}' IN BOOLEAN MODE) and gtv = 0 and chall = 1 and video.status = 1 ";
			}
		}
		if($type == 'mixtape')
		{
			$sql = "SELECT *  FROM `mixtapes` WHERE `title` LIKE '%{$term}%' OR `desc` LIKE '%{$term}%' OR `artist` LIKE '%{$term}%' OR `dj` LIKE '%{$term}%' or tags LIKE '%{$term}%' and mixtapes.status = 1 ";
		}
		if($type == 'song')
		{
			$sql = "SELECT *  FROM `songs` WHERE `title` LIKE '%{$term}%' OR `desc` LIKE '%{$term}%' OR `artist` LIKE '%{$term}%' or tags LIKE '%{$term}%' and songs.status = 1 ";
		}
		if($type == 'articles')
		{
			$sql = "SELECT *  FROM `articles` WHERE `title` LIKE '%{$term}%' OR `body` LIKE '%{$term}%' or tags LIKE '%{$term}%' and articles.status = 1 ";
		}
		
		/*if($type == 'artist')
		{
			$sql = "SELECT * FROM artist WHERE name LIKE '%{$term}%' or label LIKE '%{$term}%'";
			if($limit != '')
				$sql .= " LIMIT {$limit}";
		}
		
		if($type == 'store')
		{
			$sql = "SELECT * FROM products WHERE name LIKE '%{$term}%'";
			if($limit != '')
				$sql .= " LIMIT {$limit}";
		}*/
		
		
		$table = $this->db->query($sql);
		return $table->num_rows();
		
	}
	
}

?>