<?php

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class Blog_model extends CI_Model {

    function __construct()
    {
        parent::__construct();

    }

	function getArticles($limit = '', $offset = '')
	{
		$sql = "SELECT * FROM blog_posts Where status = 1 order by id DESC";

		if($limit != '')
			{
				$sql .= " LIMIT";

				if($offset != '')
					$sql .= " {$offset},";

				$sql .= " {$limit}";
			}
        $query = $this->db->query($sql);
        return $query->result();
	}

	function getArticles_admin($limit = '', $offset = '')
	{
		$sql = "SELECT * FROM blog_posts where status != 0 order by id DESC";

		if($limit != '')
			{
				$sql .= " LIMIT";

				if($offset != '')
					$sql .= " {$offset},";

				$sql .= " {$limit}";
			}
        $query = $this->db->query($sql);
        return $query->result();
	}

	function getArticlesCount()
	{
		$sql = "SELECT * FROM blog_posts Where status = 1 order by id DESC";
        $query = $this->db->query($sql);
        return $query->num_rows();
	}


	function getArticle($slug)
	{
		if($slug != '')
		{
			if(is_numeric($slug))
				$sql = "SELECT * FROM blog_posts WHERE status = 1 and id = $slug";
			else
				$sql = "SELECT * FROM blog_posts WHERE status = 1 and permalink = '$slug'";

	        $query = $this->db->query($sql);
        	if($query->num_rows() > 0)
				return $query->result();
			else
				return FALSE;
		}
		else
		{
			return FALSE;
		}
	}

	function getArticle_admin($slug)
	{
		if($slug != '')
		{
			if(is_numeric($slug))
				$sql = "SELECT * FROM blog_posts WHERE status != 0 and id = $slug";
			else
				$sql = "SELECT * FROM blog_posts WHERE status != 0 and permalink = '$slug'";

	        $query = $this->db->query($sql);
        	if($query->num_rows() > 0)
				return $query->result();
			else
				return FALSE;
		}
		else
		{
			return FALSE;
		}
	}

	function getArticleById($id)
	{
		$sql = "SELECT * FROM blog_posts where status = 1 and id = $id";
		$query = $this->db->query($sql);

		return $query->row();
	}

	
	function insertBlogPost($title, $body, $author, $permalink, $tags)
	{
		$date_created = date('Y-m-d H:i:s');

		$sql = "INSERT INTO	blog_posts (title,body, author, permalink, date_created, tags)
				VALUES ('".$this->db->escape_str($title)."','".$this->db->escape_str($body)."','".$this->db->escape_str($author)."','".$this->db->escape_str($permalink)."','".$date_created."','".$tags."')";
		$this->db->query($sql);

		$this->log_activity('blog',$this->db->insert_id(),'create');
	}

	function getBlogPost()
	{
		$sql = "SELECT * FROM blog_posts where status != 0";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function updateBlogPost($Articleid,$title, $body, $author, $permalink, $tags, $status)
	{
		$sql = "UPDATE blog_posts
  				SET 
	  				title='".$this->db->escape_str($title)."', 
	  				body = '".$this->db->escape_str($body)."',  
	  				author = '".$this->db->escape_str($author)."', 
	  				permalink = '".$this->db->escape_str($permalink)."', 
	  				tags = '{$tags}', 
	  				date_edited = '".date('Y-m-d H:i:s')."',
	  				status = '{$status}' 
  				WHERE id = {$Articleid}";

		//$sql = "INSERT INTO	Articles (Article_id,image,href)
				//VALUES ({$Articleid},'{$image}','{$href}');";
		$this->db->query($sql);
		$this->log_activity('blog',$Articleid,'update');
	}
	
	function deleteBlogPosts($Article_id)
	{
		$article = $this->Blog_model->getArticleById($Article_id);	
		// unlink any images
		// ... 

		$upd['status'] = '0';
		$upd['date_deleted'] = date('Y-m-d H:i:s');

		// delete article
		$this->db->update('blog_posts', $upd, array('id' => $Article_id));

		// delete disqus theread
		$this->delete_disqus_thread();
		$this->log_activity('blog',$Article_id,'delete');
	}


}