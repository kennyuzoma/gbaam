<?php

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class Main_model extends CI_Model {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


	function submit_contact()
	{
		$ins = array(
			'person_name' => $this->input->post('person_name'),
			'email' => $this->input->post('email'),
			'subject' => $this->input->post('subject'),
			'message' => $this->input->post('message'),
			'date_created' => date('Y-m-d H:i:s')
		);

		$this->db->insert('contact_us',$ins);
		
		$this->Email_model->contact_us_email($ins);
		
		return $this->db->insert_id();
	}


}