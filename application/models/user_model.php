<?php

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class User_model extends CI_Model {


	public function __construct() {
		parent::__construct();
	}

	//edit
	public $tbl_users = 'users';

	function logUserIn($email,$password,$next = '')
	{
		//$this->session->sess_destroy();

		$sql = "SELECT id,u_status,email,username,pass FROM ".$this->tbl_users." WHERE (email = '{$email}' or username = '{$email}') LIMIT 1;";

		$query = $this->db->query($sql);


		//if user is found, set session vars
		if($query->num_rows() == 1)
		{
			$row = $query->row();

			if ($this->bcrypt->check_password($password, $row->pass))
			{

				//Update the users status if the user was deactivated
				if($row->u_status == 'deact')
				{
					$update['u_status'] = 'active';

					$this->db->where('id',$row->id);
					$this->db->update('users',$update);

				}

				// log user in
				$sess_data = array(
					'user_id' => $row->id,
					'username' => $row->username,
					'email' => $row->email,
					'u_status' => $row->u_status,
					'logged_in' => true
				);

	            $this->session->set_userdata($sess_data);
	        }
	        else
	        	return FALSE;

			//Checks user status and redirects
			//$this->checkUserStatus($next);
		
		}
	}

	function login_facebook( $user, $signup = '' )
	{
		//$this->session->sess_destroy();

		//get userdata
		$sql = "SELECT * FROM users WHERE facebook_id = " .$user['id'] ." LIMIT 1";
		$query = $this->db->query($sql);

		$row = $query->row();

		$sess_data = array(
			'user_id' => $row->id,
			'logged_in' => TRUE,
			'provider' => 'facebook',
			'u_status' => $row->u_status,
		);

		$this->session->set_userdata($sess_data);

		// update the database with the latest user info
		// if this user signed up with facebook, Or this user already had a 
		// facebook image as the profile picture....
		if (($signup == 'signup') || strpos($row->profile_img,'facebook') !== false)
		{
			$data = array(
				//'display_name' => $user['name'],
				'profile_img' => 'https://graph.facebook.com/' . $user['id'] . '/picture',
				'birthday' => date("Y-m-d", strtotime($user['birthday'])),
				'gender' => $user['gender']
			);
			$this->db->where('id',$row->id);
			$this->db->update('users',$data);
		}

	}

	function login_twitter($user,$signup = '')
	{
		//get the user
		$sql = "SELECT * FROM users WHERE twitter_id = " .$user['user_id']." LIMIT 1";
		$query = $this->db->query($sql);
		$row = $query->row();

		// store the session
		$sess_data = array(
			'user_id' => $row->id,
			'logged_in' => TRUE,
			'provider' => 'twitter',
			'u_status' => $row->u_status,
		);

		$this->session->set_userdata($sess_data);

		// if this user signed up with twitter, Or this user already had a 
		// twitter image as the profile picture....
		if (($signup == 'signup') || strpos($row->profile_img,'twimg') !== false)
		{
			// get the latest profile image
			$this->twconnect->twaccount_verify_credentials();
			$usr = $this->twconnect->tw_user_info;

			$data['profile_img'] = $usr->profile_image_url_https;
			$this->db->where('id',$row->id);
			$this->db->update('users',$data);
		}
	}

	function signUp($name,$username,$email,$pass)
	{
		$u_pass = $this->bcrypt->hash_password($pass);

		$signUpVars = array(
			'username' => $username,
			'display_name' => $name,
			'email' => $email,
			'pass' => $u_pass,
			//'u_status' => 's1'
			'u_status' => 'active',
			'date' => date('Y-m-d H:i:s')
		);

		$insert = $this->db->insert($this->tbl_users, $signUpVars);

		$user_id = $this->db->insert_id();

		// insert the beginning user data
		$this->insert_user_setting($user_id, 'display_name', $name);
		$this->insert_user_setting($user_id, 'bio', '');
		$this->insert_user_setting($user_id, 'birthday', '');
		$this->insert_user_setting($user_id, 'location', '');
		$this->insert_user_setting($user_id, 'website', '');
		$this->insert_user_setting($user_id, 'email_gbaam_news', '1');

		// send confirmation email
		$this->Email_model->confirm_account($user_id);

		return TRUE;
	}

	function finish_twitter_signup($email, $user_id)
	{
		$upd['email'] = $email;
		$upd['u_status'] = '1';

		// modify session
		$this->session->set_userdata('u_status', '1');

		$this->db->where('id',$user_id);
		$this->db->update($this->tbl_users, $upd);

		$this->insert_user_setting($user_id, 'email_gbaam_news', '1');

		$this->Email_model->welcome($user_id);

		return TRUE;
	}

	function social_signup($service, $user)
	{
		$ins['date'] = date('Y-m-d H:i:s');
		$ins['u_status'] = '1';

		if($service == 'facebook')
		{
			// store the facebook id in data array
			$ins['facebook_id'] = $user['id'];
			$ins['email'] = $user['email'];
			$ins['birthday'] = date("Y-m-d", strtotime($user['birthday']));
			$ins['gender'] = $user['gender'];
			$ins['signup_method'] = 'facebook';
			$ins['profile_img'] = 'https://graph.facebook.com/' . $user['id'] . '/picture';

			//insert into database
			$this->db->insert('users',$ins);

			// the insert id
			$user_id = $this->db->insert_id();

			// the insert data
			$this->insert_user_setting($user_id, 'display_name', $user['name']);
			$this->insert_user_setting($user_id, 'bio', '');
			$this->insert_user_setting($user_id, 'birthday', '');
			$this->insert_user_setting($user_id, 'location', '');
			$this->insert_user_setting($user_id, 'website', '');
			$this->insert_user_setting($user_id, 'facebook_id', $ins['facebook_id'] );
			$this->insert_user_setting($user_id, 'email_gbaam_news', '1');

			// send welcome email
			$this->Email_model->welcome($user_id);

			//log this user in
			$this->login_facebook($user,'signup');
		}

		elseif($service == 'twitter')
		{
			// get the user info
			$this->twconnect->twaccount_verify_credentials();
			$usr = $this->twconnect->tw_user_info;

			// store the twitter id in ins array
			$ins['twitter_id'] = $user['user_id'];
			$ins['u_status'] = '4';
			$ins['signup_method'] = 'twitter';
			$ins['username'] = next_avail_username($user['screen_name']);
			$ins['profile_img'] = $usr->profile_image_url_https;

			//insert into database
			$this->db->insert('users',$ins);

			// the insert id
			$user_id = $this->db->insert_id();

			// the insert data
			$this->insert_user_setting($user_id, 'display_name', $usr->name);
			$this->insert_user_setting($user_id, 'bio', '');
			$this->insert_user_setting($user_id, 'birthday', '');
			$this->insert_user_setting($user_id, 'location', '');
			$this->insert_user_setting($user_id, 'website', '');
			$this->insert_user_setting($user_id, 'twitter_id', $ins['twitter_id'] );
			
			$this->session->set_flashdata('finish_twitter', 'TRUE');
			//redirect('main/twitter_finish');

			//log this user in
			$this->login_twitter($user);
		}
		
	}

	function checkUserStatus($next = '')
	{
		$us = $this->session->userdata('u_status');
		
		// if this user is banned destroy the session
		if($us == 3) 
		{
			$this->session->sess_destroy();
			session_destroy();
			//$this->session->set_flashdata('user_banned', 'TRUE');
			redirect('main/logout/banned');
		}

		//if($this->session->flashdata('finish_twitter') == 'TRUE')
		if($us == 4)
		{
			redirect('main/twitter_finish');
		}
		else
		{
			// redirect the user to where they came from.
			redirect($next);
		}
			
	}


	function password_reset_req($email)
	{
		// does request exist?
		$sql = "SELECT * FROM reset_pw WHERE email = '{$email}' and resolved = 0 LIMIT 1";
		$query = $this->db->query($sql);

		if($query->num_rows() == 0)
		{
			$data = array(
				'email' => $email,
				'reset_key' => random_string('alnum', 32)
			);
			$this->db->insert('reset_pw',$data);
		}
	}

	function check_password_request($email,$key)
	{
		$sql = "SELECT * FROM reset_pw WHERE email = '{$email}' and reset_key = '{$key}' and resolved = 0 LIMIT 1";
		$query = $this->db->query($sql);

		if($query->num_rows() == 1)
		{
			//$this->db->update('users', array('user_status' => 'active'));
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	function reset_password($email,$key)
	{
		$sql = "SELECT * FROM reset_pw WHERE email='{$email}' AND reset_key = '{$key}' LIMIT 1;";
		$query = $this->db->query($sql);

		if($query->num_rows() == 1)
		{
			//$this->db->update('users', array('user_status' => 'active'));
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	function resolve_password($email,$key)
	{
		$data = array(
			'resolved' => 1
		);

		$this->db->where('email',$email);
		$this->db->where('reset_key',$key);
		$this->db->update('reset_pw',$data);
	}


	function isMember_facebook($user)
	{
		$fb_id = $user['id'];

		$sql = "SELECT * FROM users WHERE facebook_id = " . $fb_id . " LIMIT 1";
		$query = $this->db->query($sql);

		if($query->num_rows() == 0)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	function isMember_twitter($user)
	{
		$sql = "SELECT * FROM users WHERE twitter_id = " . $user['user_id'] . " LIMIT 1";
		$query = $this->db->query($sql);

		if($query->num_rows() == 0)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	function get_user($user_id = '')
	{
		if($user_id == '')
			$user_id = $this->session->userdata('user_id');

		$sql = "SELECT * FROM users
				LEFT JOIN photos ON photos.photo_id = users.profile_img where";

		if(is_numeric($user_id))
			$sql .= " id = {$user_id} ";
		else
			$sql .= " username = '{$user_id}'";

		$sql .= " LIMIT 1";

		$query = $this->db->query($sql);

		if($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row;
		}
		else
		{
				return FALSE;
		}
	}

	function update_user($user_id = '',$type = '')
	{
		if($user_id == '')
			$user_id = $this->session->userdata('user_id');


		if($type == 'home')
		{
			/*
			$upd['firstname'] = $this->input->post('firstname');
			$upd['lastname'] = $this->input->post('lastname');
			*/
		
			$upd['display_name'] = $this->input->post('display_name');
			$upd['email'] = $this->input->post('email');
			$upd['username'] = $this->input->post('username');

			// update the user
			$this->db->where('id',$user_id);
			$this->db->update('users',$upd);

			// update Bio
			$bio = $this->input->post('bio');
			$this->user_setting($user_id, 'bio', $bio);

			// update displayname
			$display_name = $this->input->post('display_name');
			$this->user_setting($user_id, 'display_name', $display_name);
		}

		if($type == 'password')
		{
			// password
			$pass = $this->input->post('password2');
			$upd['pass'] = $this->bcrypt->hash_password($pass);

			// update the user
			$this->db->where('id',$user_id);
			$this->db->update('users',$upd);
		}

		if($type == 'notifications')
		{
			$email = $this->input->post('user');
			foreach($email as $k=>$v)
			{
				$this->user_setting($user_id, $k, $v);

				// unsubscribe this user from the gbaam emails
				if($k == 'email_gbaam_news' && $v == '0')
				{
					$this->Email_model->unsubscribe_me();
				}
				elseif($k == 'email_gbaam_news' && $v == '1')
				{
					$this->Email_model->subscribe_me('settings');
				}
			}
		}
	}

	function add_user_view($username)
	{
		$sql = "UPDATE users SET views = views + 1 where ";

		if(is_numeric($username))
			$sql .= " id = '{$username}' ";
		else
			$sql .= " username = '{$username}' ";

		$sql .= " LIMIT 1";

		$this->db->query($sql);
	}

	function get_user_by_email($email)
	{
		$sql = "SELECT * FROM users where email = '{$email}' LIMIT 1";
		$query = $this->db->query($sql);
		//$row = $query->row();

		if($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row;
		}
		else
		{
				return FALSE;
		}
	}

	function get_user_by_email_or_username($email)
	{
		$sql = "SELECT * FROM users where email = '{$email}' or username = '{$email}' LIMIT 1";
		$query = $this->db->query($sql);
		$row = $query->row();

		return $row;
	}

	function get_user_social($user_id)
	{
		$sql = "SELECT * FROM users where id = {$user_id} LIMIT 1";
		$query = $this->db->query($sql);
		$row = $query->row();

		return $row;
	}

	function get_users($order = '')
	{
		$sql = "SELECT * FROM users";

		if($order != '')
			$sql .= ' ORDER BY id '.$order;

		$query = $this->db->query($sql);
		
		$a['count'] = $query->num_rows();
		$a['result'] = $query->result();

		return $a;
	}


	/*
	* Checks if the User exists
	*/
	function doesUserExist($email,$password)
	{

		$sql = "SELECT * FROM ".$this->tbl_users."
				WHERE (users.email = '{$email}' or users.username = '{$email}') LIMIT 1";

		$query = $this->db->query($sql);

		// a user was found.
		if($query->num_rows() == 1)
		{
			$row = $query->row();

			if ($this->bcrypt->check_password($password, $row->pass)) 
				return 'good_password'; // if password matches
			else
				return 'bad_password'; // this is a bas password
		}
		else
			return 'doesnt_exist'; // no user was found
	}

	function doesUserExist_username($username, $user_id = '')
	{
		if($user_id == '')
			$user_id = $this->session->userdata('user_id');

		$sql = "SELECT * FROM users WHERE username = '{$username}' LIMIT 1";

		$query = $this->db->query($sql);
		$row = $query->row();

		if(is_this_a_reserved_username($username) == TRUE)
			return TRUE; // this reserved username can not be used.

		// if username is found
		if($query->num_rows() == 1)
		{
			if($user_id != '')
			{
				// if this is mine
				if($row->id == $user_id)
				{
					return FALSE; // if this is my username its cool. pass
				}
				else
				{
		            return TRUE; // this username is taken
				}
			}
			else
			{
				return TRUE;
			}
			
		}
		else
		{
			return FALSE; // this username is free. pass
		}
	}

	function doesUserExist_forgot($email)
	{

		$sql = "SELECT * FROM ".$this->tbl_users."
				WHERE username = '{$email}' or email = '{$email}'
						LIMIT 1";

		$query = $this->db->query($sql);
		
		if($query->num_rows() == 1)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	function existing_email($email)
	{
		$sql = "SELECT * FROM users WHERE email='{$email}' LIMIT 1;";
		$query = $this->db->query($sql);

		if($query->num_rows() == 1)
		{
			if($this->session->userdata('logged_in') == TRUE)
			{
				$myid = $this->session->userdata('user_id');

			// is this my email?
			$sql2 = "SELECT * FROM users where email='{$email}' and id = $myid LIMIT 1";
			$q2 = $this->db->query($sql2);

			if($q2->num_rows() == 1)
				return FALSE; // this is my email and i "TOOK" it so this is cool
			else
				return TRUE; // this is not my email so this email is taken
			}
			else
				return TRUE; // this email is not available;
			
		}
		else{
			return FALSE; // this email is available
		}
	}

	function confirm_account($hash,$stay = '')
	{
		if($stay != '')
			redirect('main/logout/confirm/'.$hash);

		// decrypt the id
		$user_id = hashids_decrypt($hash);
		

		$sql = "SELECT * FROM users WHERE id = '{$user_id}' LIMIT 1";
		$query = $this->db->query($sql);

		if($query->num_rows() == 1)
		{
			$row = $query->row();
			$u_status = $row->u_status;

			// if this user is banned
			if($u_status == '3')
			{
				$this->session->sess_destroy(); 
				session_destroy();
				//$this->session->set_flashdata('user_banned', 'TRUE');
				redirect('main/logout/banned');
			}
			else
			{
				// if already confirmed
				if($u_status == '1' ||  $u_status == '2')
				{
					// show message here that says already confirmed
					redirect('/'); 
				}
				elseif($u_status == '0')
				{
					$sql = "UPDATE users SET u_status = 1 WHERE id = $user_id";
					$this->db->query($sql);


					// Log the user in
					$sess_data = array(
						'user_id' => $row->id,
						'username' => $row->username,
						'email' => $row->email,
						'u_status' => 1,
						'logged_in' => true,

					);

		            $this->session->set_userdata($sess_data);
		            $this->session->set_flashdata('user_confirm', 'TRUE');

		            // send welcome email
		            $this->Email_model->welcome($row->id);

					redirect('/');
				}
			}
		}
		else
			redirect('/');
	}

	function insert_favorite($type,$type_id)
	{
		$ins = array(
			'type' => $type,
			'type_id' => $type_id,
			'user_id' => $this->session->userdata('user_id'),
		);

		$this->db->insert('users_favorites',$ins);
	}


	function get_favorite($favorite_id, $user_id)
	{
		$sql = "SELECT * FROM users_favorites where id = {$favorite_id} and user_id = {$user_id}";
		$query = $this->db->query($sql);

		return $query->row();
	}

	function get_favorites($user_id)
	{
		$sql = "SELECT * FROM users_favorites where id = {$favorite_id} and user_id = {$user_id}";
		$query = $this->db->query($sql);

		return $query->result();
	}

	function insert_like($type,$type_id)
	{
		$ins = array(
			'type' => $type,
			'type_id' => $type_id,
			'user_id' => $this->session->userdata('user_id'),
		);

		$this->db->insert('users_likes',$ins);
	}


	function get_like($like_id, $user_id)
	{
		$sql = "SELECT * FROM users_likes where id = {$like_id} and user_id = {$user_id}";
		$query = $this->db->query($sql);

		return $query->row();
	}

	function get_likes($user_id)
	{
		$sql = "SELECT * FROM users_likes where id = {$like_id} and user_id = {$user_id}";
		$query = $this->db->query($sql);

		return $query->result();
	}

	function get_user_setting($setting,$user_id = '')
	{
		if($user_id == '')
			$user_id = loggedin_user_id();

		$sql = "SELECT * FROM users_settings where user_id = '{$user_id}' and name = '{$setting}' LIMIT 1";
		$query = $this->db->query($sql);

		

		if($query->num_rows() == 0)
			return NULL;
		else
		{
			$row = $query->row();
			return $row->value;
		}
		
	}

	function insert_user_setting($user_id, $setting, $value)
	{
		$ins['user_id'] = $user_id;
		$ins['name'] = $setting;
		$ins['value'] = $value;

		$this->db->insert('users_settings', $ins);
	}

	function update_user_setting($user_id, $setting, $value)
	{
		$upd['user_id'] = $user_id;
		$upd['name'] = $setting;
		$upd['value'] = $value;

		$sql = "UPDATE users_settings 
				SET value = '{$value}'
				WHERE user_id = '{$user_id}' and name = '{$setting}'";

		$this->db->query($sql);
	}

	function user_setting($user_id, $setting, $value)
	{
		// check if this exists in the database
		if($this->does_user_setting_exist($user_id, $setting) == TRUE)
		{
			$this->update_user_setting($user_id, $setting, $value);
		}
		else
		{
			$this->insert_user_setting($user_id, $setting, $value);
		}
	}

	function does_user_setting_exist($user_id,$setting)
	{
		$sql = "SELECT * FROM users_settings where user_id = '{$user_id}' and name = '{$setting}' LIMIT 1";
		$query = $this->db->query($sql);

		if($query->num_rows() == 1)
			return TRUE;
		else
			return FALSE;
	}

	function get_all_user_settings($user_id)
	{
		$sql = "SELECT * FROM users_settings where user_id = '{$user_id}' ";
		$query = $this->db->query($sql);

		foreach($query->result() as $g)
		{
			$array[$g->name] = $g->value;
		}

		return $array;
	}

	function get_all_user_settings_array($user_id)
	{
		$sql = "SELECT * FROM users_settings where user_id = '{$user_id}' ";
		$query = $this->db->query($sql);

		return $query->result();
		
	}



	function get_twitter_screen_name($twitter_user_id)
	{
		$a['user_id'] = $twitter_user_id;

		$this->load->library('twconnect');

		$user = $this->twconnect->tw_get('users/show', $a);

		return $user->screen_name;

	}

	function get_facebook_name($facebook_id)
	{
		echo json_decode(file_get_contents('http://graph.facebook.com/'.$facebook_id))->name;
	}

	function disconnect_socialnetwork_account($network)
	{
		// check for conditions.... if password is set... etc etc
		// 
		
		$user_id = $this->session->userdata('user_id');

		$sql = "DELETE FROM users_settings WHERE";

		if($network == 'facebook')
			$sql .= " name = 'facebook_id'";
		
		if($network == 'twitter')
			$sql .= " name = 'twitter_id'";

		$sql .= " and user_id = '{$user_id}'";

		$this->db->query($sql);
	}

	function deleteUser($id)
	{
		// get the user
		$user = $this->User_model->get_user($id);
		$this->log_activity('user',$id,'delete');

		// delete user from reset_pw
		$sql = "DELETE FROM reset_pw WHERE email = {$user->email}";
		$this->db->query($sql);

		// delete user
		$sql = "DELETE FROM users WHERE id = {$id}";
		$this->db->query($sql);

		// delete user images
		// ....
	}


}