<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email_model extends CI_model {

	function __construct() {
		parent::__construct();
		$config = Array(
    	'mailtype' => 'html',
		);
		$this->load->library('Email',$config);
	}

	function get_email_template($template)
	{
		$query = $this->db->get_where('static_pages',array('page_code'=>$template));

		return $query->row(0);  //Return template

		return FALSE;
	}

	function new_account()
	{

	}

	function forgot_password($email)
	{

		$d['title'] = 'Gbaam - Password Reset';
		$d['sub_title'] = 'Password Reset';

		//get me
		$me = $this->User_model->get_user_by_email($email);

		//get the reset key
		$sql = "SELECT * FROM reset_pw where email = '{$email}'";
		$query = $this->db->query($sql);
		$rp = $query->row();

		//prepare the message
		$d['name'] = $me->username;
		$d['email'] = $email;
		$d['key'] = $rp->reset_key;

		// added TRUE so that it will send to email
		$message = $this->load->view('email/forgot_password', $d, TRUE);

		//send the message
		$this->email->from('noreply@gbaam.com', 'Gbaam');
		$this->email->to($email);
		$this->email->subject('Reset Your Password');
		$this->email->message($message);
		$this->email->send();
	}


	function contact_us_email($ins)
	{

		// added TRUE so that it will send to email
		$message = '
			<b>New Gbaam Contact Us message</b><br><Br>
			From: <b>'.$ins['person_name'].'</b> <'.$ins['email'].'><br>
			Subject: <b>'.$ins['subject'].'</b><br><br>
			Date Sent: <b>'.date('F j, Y g:i:s A').'</b><br><br>

			<b>Message</b>:<br>
			'.$ins['message'].'
		';

		//send the message
		$this->email->from('noreply@gbaam.com', 'Gbaam');
		$this->email->to('kennyuzoma@gmail.com');
		$this->email->subject('New Gbaam Contact Us Message');
		$this->email->message($message);
		$this->email->send();
	}




	function submit_email($id,$type)
	{
		;

		switch ($type) {
			case 'gbaamtv':
				$thetypeword = 'GbaamTV';
				$thetype = $thetypeword;
				
				break;

			case 'video':
				$thetypeword = 'Video';
				$thetype = $thetypeword;
				
				break;

			case 'challenge':
				$thetypeword = 'Challenge';
				$thetype = 'Gbaam Challenge';
				
				break;
			
			default:
				break;
		}

		$d['submission'] = TRUE;
		$d['thetype'] = $thetype;
		$d['thetypeword'] = $thetypeword;
		$d['title'] = 'Your '.$thetype.' submission';

		//get me
		$d['info'] = $this->Admin_model->getPendingVideo($id);
		$d['newDate'] = date('F j, Y g:i:s A', strtotime($d['info']->date_submitted));

		// added TRUE so that it will send to email
		$message = $this->load->view('email/pending', $d, TRUE);
		$message_admin = $this->load->view('email/pending_admin', $d, TRUE);

		//send the message
		$this->email->from('noreply@gbaam.com', 'Gbaam');
		$this->email->to($d['info']->email);
		$this->email->subject('We received your '.$thetype.' Submission!');
		$this->email->message($message);
		$this->email->send();

		$this->email->clear();

		//send the message
		$this->email->from('noreply@gbaam.com', 'Gbaam');
		$this->email->to('kennyuzoma@gmail.com');
		$this->email->subject('Gbaam! New '.$thetype.' Submission!');
		$this->email->message($message_admin);
		$this->email->send();

	}


	function pending_result_email($id,$new_video_id = '')
	{
		// load the amin model
		;


		// just a place holder
		if($new_video_id == '')
			$new_video_id = 0;


		/// get the video
		$d['info'] = $this->Admin_model->getPendingVideo($id);
		$d['newDate'] = date('F j, Y g:i:s A', strtotime($d['info']->date_submitted));


		// set vars
		$info = $d['info'];
		$email = $info->email;


		// set the subject and denial reasons
		if($info->status == 'deny')
		{
			$subject = 'We did not approve your video submission.';
			$d['deny_reasons'] = explode(',', $info->deny_reasons);
			$d['deny_reasons_other'] = $info->deny_reasons_other;
		}
		elseif($info->status == 'approved')
		{
			$subject = 'Your video has been approved!';
		}


		// set the video type
		if($info->gtv == '1' && $info->chall == '0')
			$type = 'gbaamtv';
		elseif($info->gtv == '0' && $info->chall == '1')
			$type = 'challenge';
		else
			$type = 'video';


		// certain vars depending on type
		switch ($type) {
			case 'gbaamtv':
				$thetypeword = 'GbaamTV';
				$thetype = $thetypeword;
				$videolink = 'gbaamtv/v/'.hashids_encrypt($new_video_id); 
				
				break;

			case 'video':
				$thetypeword = 'Video';
				$thetype = $thetypeword;
				$videolink = 'video/'.hashids_encrypt($new_video_id); 
				
				break;

			case 'challenge':
				$thetypeword = 'Challenge';
				$thetype = 'Gbaam Challenge';
				$videolink = 'challenge/v/'.hashids_encrypt($new_video_id);
				
				break;
			
			default:
				break;
		}

		// set more vars...
		$d['submission'] = TRUE;
		$d['thetype'] = $thetype;
		$d['thetypeword'] = $thetypeword;
		$d['title'] = 'Your '.$thetype.' submission';
		$d['videolink'] = $videolink;

		


		// added TRUE so that it will send to email
		$message = $this->load->view('email/'.$info->status, $d, TRUE);
		//$message_admin = $this->load->view('email/pending_admin', $d, TRUE);

		//send the message
		$this->email->from('noreply@gbaam.com', 'Gbaam');
		$this->email->to($d['info']->email);
		$this->email->subject($subject);
		$this->email->message($message);
		$this->email->send();

		//$this->email->clear();

		//send the message
		/*$this->email->from('noreply@gbaam.com', 'Gbaam');
		$this->email->to('kennyuzoma@gmail.com');
		$this->email->subject('Your video has been approved!');
		$this->email->message($message);
		$this->email->send();*/

	}

	function account_deactivated($user_id)
	{
		$d['title'] = 'Your Account has been deactivated';
		$d['sub_title'] = 'Deactivated';

		//get me
		$me = $this->User_model->get_user($user_id);

		//prepare the message
		$d['name'] = $me->username;
		$email = $me->email;
		$d['email_message'] = 'Your account is now successfully deactivated. If you wish to reactivate your account, just simply log back in! Hope to see you soon!';

		// added TRUE so that it will send to email
		$message = $this->load->view('email/reg_template', $d, TRUE);

		//send the message
		$this->email->from('noreply@gbaam.com', 'Gbaam');
		$this->email->to($email);
		$this->email->subject('Your Account has been deactivated');
		$this->email->message($message);
		$this->email->send();
	}

	function account_reactivated($user_id)
	{
		$d['title'] = 'Welcome back!';
		$d['sub_title'] = 'Account Reactivated';

		//get me
		$me = $this->User_model->get_user($user_id);

		//prepare the message
		$d['name'] = $me->username;
		$email = $me->email;
		$d['email_message'] = 'We are emailing you to let you know your account has been successfully reactivated! Glad to have you back... We missed you!';

		// added TRUE so that it will send to email
		$message = $this->load->view('email/reg_template', $d, TRUE);

		//send the message
		$this->email->from('noreply@gbaam.com', 'Gbaam');
		$this->email->to($email);
		$this->email->subject('Welcome back!');
		$this->email->message($message);
		$this->email->send();
	}

	function account_banned($user_id)
	{
		$d['title'] = 'Welcome back!';
		$d['sub_title'] = '';

		//get me
		$me = $this->User_model->get_user($user_id);

		//prepare the message
		$d['name'] = $me->username;
		$email = $me->email;
		$d['email_message'] = '<b>'.$d['name'].'</b>,<br>Your account has been banned for the following reasons.';

		// added TRUE so that it will send to email
		$message = $this->load->view('email/reg_template', $d, TRUE);

		//send the message
		$this->email->from('noreply@gbaam.com', 'Gbaam');
		$this->email->to($email);
		$this->email->subject('Your account has been Banned');
		$this->email->message($message);
		$this->email->send();
	}

	function confirm_account($user_id)
	{
		$d['title'] = 'Please confirm your Account';
		$d['sub_title'] = '';

		//get me
		$me = $this->User_model->get_user($user_id);

		//prepare the message
		$d['name'] = $me->username;
		$email = $me->email;
		$user_id_hash = hashids_encrypt($me->id);
		
		$link = base_url().'users/confirm_account/'.$user_id_hash;

		$d['email_message'] = '<b>'.$d['name'].'</b>,<br>
							Please confirm your account by clicking this link
							<a href="'.$link.'"">'.$link.'</a>';

		// added TRUE so that it will send to email
		$message = $this->load->view('email/reg_template', $d, TRUE);

		//send the message
		$this->email->from('noreply@gbaam.com', 'Gbaam');
		$this->email->to($email);
		$this->email->subject('Confirm your account');
		$this->email->message($message);
		$this->email->send();
	}

	function account_welcome($user_id)
	{
		$d['title'] = 'Welcome to Gbaam!';
		$d['sub_title'] = '';

		//get me
		$me = $this->User_model->get_user($user_id);

		//prepare the message
		$d['name'] = $me->username;
		$email = $me->email;
		$d['email_message'] = '<b>'.$d['name'].'</b>,<br>Thank you for confirming your account! Welcome to gbaam!';

		// added TRUE so that it will send to email
		$message = $this->load->view('email/reg_template', $d, TRUE);

		//send the message
		$this->email->from('noreply@gbaam.com', 'Gbaam');
		$this->email->to($email);
		$this->email->subject('Welcome to Gbaam');
		$this->email->message($message);
		$this->email->send();
	}

	
	function new_message($email)
	{

	}

	function new_admin($new_admin_id)
	{
		
	}

	function new_comments($email)
	{

	}

	function new_friend_request($email)
	{

	}

	function new_wall_post($email)
	{

	}

	function event_invite($email)
	{

	}

	function event_changes($email)
	{

	}

	function monthly_newsletter($email)
	{

	}

}