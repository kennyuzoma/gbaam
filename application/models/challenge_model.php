<?php 

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class Challenge_model extends CI_Model {
	
	function __construct()
    {
        parent::__construct();
        
    }

    function challenge_list()
	{
		$sql = "SELECT * FROM categories WHERE type = 'challenge'";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function sing_challenge_info($id)
	{
		$sql = "SELECT * FROM categories WHERE type = 'challenge' and id = {$id}";
		$query = $this->db->query($sql);
		return $query->row();
	}

	function challenge_info($id)
	{
		$sql = "SELECT * FROM categories WHERE type = 'challenge' and id = {$id}";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function get_video($id)
	{
		if($id != '')
		{
			$sql = "SELECT * FROM video where id = {$id} and chall = 1 ";
			
			$query = $this->db->query($sql);
		
			if($query->num_rows() > 0)
				return $query->result();
			else
				return FALSE;
		}
		else
		{
			return FALSE;
		}
		
	}
	
	function get_videos_by_cat($cat = '')
	{
		$sql = "SELECT * FROM video where chall_cat = {$cat} and chall = 1  ORDER BY id DESC";
		
		$query = $this->db->query($sql);
		
		return $query->result();
	}

	function get_videos_by_cat_count($cat = '')
	{
		$sql = "SELECT * FROM video where chall_cat = {$cat} and chall = 1  ORDER BY id DESC";
		
		$query = $this->db->query($sql);
		
		return $query->num_rows();
	}
	
	function first_vid_in_cat($cat = '')
	{
		$sql = "SELECT * FROM video where chall_cat = {$cat} and chall = 1  ORDER BY id DESC LIMIT 1;";
		$query = $this->db->query($sql);
		
		return $query->result();
	}
	function first_vid_in_cat_count($cat = '')
	{
		$sql = "SELECT * FROM video where chall_cat = {$cat} and chall = 1  ORDER BY id DESC LIMIT 1;";
		$query = $this->db->query($sql);
		
		if($query->num_rows == 0)
		{
			return FALSE;
		}
		else
			return $query->num_rows();
	}

	function add_vote($videoid,$choice)
	{
		$sql = "INSERT INTO  video_votes ( video_id  ,  choice , v_type ) 
				VALUES ('{$videoid}',  '{$choice}', 'chal');";
		$this->db->query($sql);
	}
	
	function get_vote($videoid)
	{
		$sql = "SELECT choice FROM votes WHERE type_id = {$videoid} and type = 'video' ";
		
		$query = $this->db->query($sql);

		$bl = array();
		foreach($query->result() as $b) 
		{
			$bl[] = $b->choice;	
		}
		 
		$rows = $query->num_rows();
		$d['num_votes'] = count($bl);
		$d['total_score'] = $rows*4; 
		$d['my_score'] = array_sum($bl);
		
		return $d; 
	}
	
	
	function get_vote_count($videoid)
	{
		$sql = "SELECT choice FROM video_votes WHERE type_id = {$videoid} and type = 'video' ";
		
		$query = $this->db->query($sql);

		$bl = array();
		foreach($query->result() as $b) 
		{
			$bl[] = $b->choice;	
		}
		 
		return count($bl);
		 
	}
	
	function get_votes($videoid,$choice)
	{
		//Save For later.
		//SELECT COUNT(choice) FROM `video_votes` WHERE choice = 1 or choice = 2 or choice = 3 or choice = 4 or choice = 5 GROUP BY choice;
		$sql = "SElECT COUNT(choice) as choice FROM votes WHERE type_id = '{$videoid}' and choice = '{$choice}' and type='video' ;";
		$query = $this->db->query($sql);
		foreach($query->result() as $q)
			$choi =  $q->choice;	
		
		if($choi != '0')
			echo $choi;
	}
	
	function addview($id) 
	{
		$view = 1;//rand(1, 3);
		$sql = "UPDATE video_challenge SET v_count=v_count+{$view} WHERE id={$id}";
		$this->db->query($sql);		
	}
	
	public function scores($cat = '')
	{
		$sql = "SELECT * FROM video";
		
		if($cat != '')
			$sql .= ' WHERE chall_cat = '.$cat.'';
		
		$query = $this->db->query($sql);
		$fruits[] = array(); 

		foreach($query->result() as $b)
		{
			$getmix = $this->get_vote($b->id);
				$my_score = $getmix['my_score'];
				$total_score = $getmix['total_score'];
				$num_votes = $getmix['num_votes'];
				//echo $total_score;
				//echo $my_score.'/'.$total_score;
				if($total_score == 0 ) 
				{
					$d['isScore'] = FALSE;
				}
				else
				{
					$d['isScore'] = TRUE;
					$percent = ($my_score / $total_score) * 100;
					$percent = round($percent);
					$d['final_score'] = $percent;
					//sort($fruits);
					//echo 'video '.$b->id.' score = '. $d['final_score'].'<br>';
					//$fruits = array($d['final_score']);
					// array_push($fruits,$d['final_score']);

					$array[$b->id] = $d['final_score'];
					//echo '<pre>'.print_r($fruits).'</pre>';
				}
				
		}
		//if($this->get_vote_count())
		
		if(empty($array))
		{
			return FALSE;
		}
		else
		{
			arsort($array);
			$i = 0;
			foreach($array as $key => $val)
			{
				$newar[$key] = $val;
				if($i++ == 4) break;
				
				/*foreach($this->Challenge_model->get_video($key) as $g)
				{?>
				  <li>
	                	<a href="<?php echo base_url(); ?>challenge/view/<?php echo $g->id; ?>">
							<?php if($g->thumb == ''){ ?>
								<img style="height:80px;width:120px;" src="<?php echo base_url(); ?>thumbs/Gbaam3.jpg">
							<?php } else { ?>
								<img style="height:80px;width:120px;" src="<?php echo base_url(); ?>thumbs/<?php echo $g->thumb;?>" />
							<?php } ?>
							<?php echo $g->title; ?>
						</a>
	                </li>
					
				<? }
				 * 
				 */
			}
			return $newar;
		}
		
	}
	
	public function get_top_score($cat = '')
	{
		$sql = "SELECT * FROM video";
		
		if($cat != '')
			$sql .= ' WHERE chall_cat = '.$cat.'';
		
		$query = $this->db->query($sql);
		$fruits[] = array(); 
		foreach($query->result() as $b)
		{
			$getmix = $this->get_vote($b->id);
				$my_score = $getmix['my_score'];
				$total_score = $getmix['total_score'];
				
				//echo $total_score;
				//echo $my_score.'/'.$total_score;
				if($total_score == 0 ) 
				{
					$d['isScore'] = FALSE;
				}
				else
				{
					$d['isScore'] = TRUE;
					$percent = ($my_score / $total_score) * 100;
					$percent = round($percent);
					$d['final_score'] = $percent;
					//sort($fruits);
					//echo 'video '.$b->id.' score = '. $d['final_score'].'<br>';
					//$fruits = array($d['final_score']);
					// array_push($fruits,$d['final_score']);

					$array[$b->id] = $d['final_score'];
					//echo '<pre>'.print_r($fruits).'</pre>';
				}
				
		}
		
		
		if($query->num_rows() == 0)
		{
			return '';
		}
		else
		{
			arsort($array);
			$i = 0;
			foreach($array as $key => $val)
			{
				if($this->get_vote_count($key) > 9)
				{
					$newar[$key] = $val;
					if($i++ == 0) break;
					
				}
				
			}
			if($this->get_vote_count($key) > 9)
				return $newar;
			else
				return '';

			
		}
		
	}

	function newChallenge($status,$name,$display_name)
	{
		$data = array(
			'name' => $name,
			'active' => $status,
			'type' => 'challenge',
			'display_name' => $display_name,
			'date_created' => date('Y-m-d H:i:s')
		);

		$this->db->insert('categories', $data);
	}

	function editChallenge($chal_id, $status, $name, $display_name)
	{
		$data = array(
			'name' => $name,
			'display_name' => $display_name,
			'active' => $status
		);

		$this->db->where('id',$chal_id);
		$this->db->where('type','challenge');
		$this->db->update('categories', $data);
	}

	function deleteChallenge($challenge_id)
	{
		$this->db->delete('categories', array('id' => $challenge_id,'type' => 'challenge'));
	}
	############################### GBAAM MOBILE APPs API SUPPORT ########################
	
	public function getChallengeCategories($catId = 0)
	{
		// return all challenge categories 
		$sql = "SELECT * FROM categories";
		if ($catId != 0)
			$sql .= " WHERE id = $catId";
		
		$query = $this->db->query($sql);
		
		return $query->result();
	}
	
	public function getChallengeVideos($cat = '')
	{
		// return all challenge videos with cat >= 1, sorted by category in ascending order 
		$sql = "SELECT * FROM video_challenge WHERE cat >= 1";

		if ($cat != '')
			$sql .= " AND cat = $cat";
		else
			$sql .= " ORDER BY cat ASC";
		
		$query = $this->db->query($sql);
		
		return $query->result();
	}
	
	public function getChallengeVideo($id)
	{
		$query = $this->db->query("SELECT * FROM video_challenge WHERE id = $id AND cat >= 1");
		
		if($query->num_rows() > 0)
			return $query->result();
		else
			return FALSE;
	}
}

	