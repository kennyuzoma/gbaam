<?php

/**
* 						
*/
class Slide_model extends CI_Model
{

	function createSlide($type,$name,$program_id)
	{
		$sql = "INSERT INTO	slides (type,name,program_id)
				VALUES ('{$type}','{$name}',{$program_id});";
		$this->db->query($sql);
		$this->log_activity('slide',$this->db->insert_id(),'create');
		return $this->db->insert_id();
	}

	function insertSlide($href='', $alt_text='')
	{
		$this->log_activity('slide',$this->db->insert_id(),'create');

		$poster_id = $this->session->userdata('admin_user_id');

		$sql = "INSERT INTO	slides (href,alt_text,poster_id)
				VALUES ('{$href}','{$this->db->escape_str($alt_text)}','{$poster_id}')";
		$this->db->query($sql);
	}

	function getSlides()
	{
		$sql = "SELECT * FROM slides where status != 0";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function updateSlide($slideid,$href='', $alt_text='',$status)
	{
		$sql = "UPDATE slides
  				SET 
  					href='{$href}', 
  					alt_text = '{$this->db->escape_str($alt_text)}', 
  					date_edited = '".date('Y-m-d H:i:s')."',
  					status = '{$status}' 
  				WHERE id = {$slideid}";

		//$sql = "INSERT INTO	slides (slide_id,image,href)
				//VALUES ({$slideid},'{$image}','{$href}');";
		$this->db->query($sql);
		$this->log_activity('slide',$slideid,'update');
	}

	function updateSlideOrder()
	{
		$i = 1;

		foreach ($_POST['post'] as $value) {
		    // Execute statement:
		    $sql =  "UPDATE slides SET position = $i WHERE id = $value";
		    $this->db->query($sql);

		    $i++;
		}
		$this->log_activity('slideorder','0','update');
	}

	function deleteSlide($slide_id)
	{
		// unlink image
		// ....
		
		// delete the slide
		$upd['status'] = '0';
		$upd['date_deleted'] = date('Y-m-d H:i:s');

		$this->db->update('slides', $upd, array('id' => $slide_id)); 
		$this->log_activity('slide',$slide_id,'delete');
	}

}