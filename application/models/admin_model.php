<?php
/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class Admin_model extends CI_Model {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

	function validate()
	{
		if($this->input->post('submit_login'))
		{
			$e = $this->input->post('username');
			$p = $this->input->post('password');
		}

		$sql = "SELECT * FROM admin WHERE (email = '{$e}' or username = '{$e}') LIMIT 1;";
		$query = $this->db->query($sql);

		if($query->num_rows() == 1)
		{
			$row = $query->row();

			if($this->bcrypt->check_password($p, $row->password))
			{

				$admin_type = $row->admin_type;

				$sess_data = array(
					'admin_user_id' => $row->id,
					'admin_username' => $row->username,
					'admin_type' => $row->admin_type,
					'admin_email' => $row->email,
					'admin_logged_in' => true
				);
	            $this->session->set_userdata($sess_data);

	            // Author
	            if($admin_type == '5')
	            	redirect($this->config->item('admin_location').'/articles/list');

	            else
	            {
	            	$this->log_activity('login','0','logged in');
					redirect('admin/home/home2');
	            }
			}
		}
		else
	        return FALSE;

	}

	function checklogin($e, $p)
	{
		$sql = "SELECT * FROM admin WHERE (email = '{$e}' or username = '{$e}') LIMIT 1;";
		$query = $this->db->query($sql);

		if($query->num_rows() == 1)
		{
			$row = $query->row();

			if($this->bcrypt->check_password($p, $row->password))
			{
				return TRUE;
			}
			else
				return FALSE;
		}
	}

	function doesUserExist_username($username,$user_id = '')
	{
		if($user_id == '')
			$user_id = $this->session->userdata('admin_user_id');

		$sql = "SELECT * FROM admin WHERE username = '{$username}' LIMIT 1";

		$query = $this->db->query($sql);
		$row = $query->row();

		if(is_this_a_reserved_username($username,'admin') == TRUE)
		{
			if($this->session->userdata('admin_type') == '1')
				return FALSE;
			else
				//echo 'username is reserved';
				return 'reserved'; // this reserved username can not be used.
		}

		// if username is found
		if($query->num_rows() == 1)
		{
			// if this is mine
			if($row->id == $user_id)
			{
				//echo 'this is my username';
				return FALSE; // if this is my username its cool. pass
			}
			else
			{
				//echo 'this username is taken';
	            return TRUE; // this username is taken
			}
		}
		else
		{
			return FALSE; // this username is free. pass
		}
	}

	function doesUserExist_email($email,$user_id = '')
	{
		if($user_id == '')
			$user_id = $this->session->userdata('admin_user_id');

		$sql = "SELECT * FROM admin WHERE email = '{$email}' LIMIT 1";

		$query = $this->db->query($sql);
		$row = $query->row();

		// if username is found
		if($query->num_rows() == 1)
		{
			// if this is mine
			if($row->id == $user_id)
			{
				return FALSE; // email is mine so it will ignore this
			}
			else
			{
	            return TRUE; // email exists
			}
		}
		else
		{
			return FALSE; // this email address does not exist
		}
	}

	function getAdmin( $id )
	{
		$sql = "SELECT * FROM admin 
				LEFT JOIN photos ON photos.photo_id = admin.profile_img  
				WHERE id = {$id}";
		$query = $this->db->query($sql);

		return $query->row(); 
	}

	function get_user($id)
	{
		$sql = "SELECT * FROM admin 
				LEFT JOIN admin_types on admin_types.id = admin.admin_type
				LEFT JOIN photos ON photos.photo_id = admin.profile_img 
				WHERE admin.id = {$id}";
		$query = $this->db->query($sql);

		return $query->row(); 
	}

	function get_users($guest = '',$order = '')
	{
		$sql = "SELECT *, admin.id as aid FROM admin 
				LEFT JOIN admin_types on admin_types.id = admin.admin_type
				LEFT JOIN photos ON photos.photo_id = admin.profile_img where admin_type != 0 ";

		if($guest == 'noguest')
			$sql .= "and admin_type != '6' ";

		if($guest == 'guest')
			$sql .= "and admin_type = '6' ";

		if($order == 'byalpha')
			$sql .= "ORDER BY firstname ASC";
		if($order == '')
			$sql .= "ORDER BY admin.id DESC";
		$query = $this->db->query($sql);

		return $query->result(); 
	}

	function get_user_by_username($username)
	{
		$sql = "SELECT *, admin.id as aid FROM admin 
				LEFT JOIN admin_types on admin_types.id = admin.admin_type
				LEFT JOIN photos ON photos.photo_id = admin.profile_img 
				WHERE admin.username = '{$username}'";


		$query = $this->db->query($sql);

		return $query->row();
	}

	function insert_user()
	{
		$firstname = $this->input->post('firstname');
		$lastname = $this->input->post('lastname');
		$username = $this->input->post('username');
		$password2 = $this->input->post('password2');

		if($password2 == '')
			$password2 = '';
		else
			$password2 = $this->bcrypt->hash_password($password2);

		//username stuff
		if($username == '')
		{
			if($this->doesUserExist_username($firstname.$lastname) == TRUE)
			{
				$username = $firstname.$lastname;
			}
			else
			{
				// find an available username...
				for ($i = 1; $i < 100;  $i++) 
				{
			    	if($this->doesUserExist_username($firstname.$lastname.$i) == TRUE)
			    	{
			    		$username = $firstname.$lastname.$i;
			    		break;
			    	}
			    }
			}
		}

		$access = $this->input->post('access');

		$ins = array(
			'admin_type' => $this->input->post('admin_type'),
			'access' => $access,
			'username' => $username,
			'email' => $this->input->post('email'),
			'firstname' => $firstname,
			'lastname' => $lastname,
			'display_name' => $firstname.' '.$lastname,
			'password' => $password2,
			'created_by' => $this->session->userdata('admin_user_id'),
			'date_created' => date('Y-m-d H:i:s')
		);

		

		if($access != '')
		{
			$ins['access'] = implode(',', $access);
		}

		// admin access stuff for "ADMINS and SUPER ADMINS"
		switch($this->input->post('admin_type'))
		{
			case '1': // super admin
				$ins['access'] = '1,2,3,4,5,6,7';

			case '2': // admin
				$ins['access'] = '1,2,3,4,5,6,7';

		}

		$this->db->insert('admin',$ins);
		$user_id = $this->db->insert_id();
		$this->log_activity('user',$user_id,'create');
		$this->Email_model->new_admin($user_id);
	}

	function update_user($id)
	{
		$upd = array(
			'admin_type' => $this->input->post('admin_type'),
			'username' => $this->input->post('username'),
			'email' => $this->input->post('email'),
			'firstname' => $this->input->post('firstname'),
			'lastname' => $this->input->post('lastname'),
			'display_name' => $this->input->post('firstname').' '.$this->input->post('lastname'),
			'bio' => $this->input->post('bio'),
			'website' => $this->input->post('website'),
			'facebook' => $this->input->post('facebook'),
			'twitter' => $this->input->post('twitter'),
			'googleplus' => $this->input->post('googleplus'),
			'instagram' => $this->input->post('instagram')
		);

		$access = $this->input->post('access');

		if($access != '')
		{
			$upd['access'] = implode(',', $access);
		}

		/*
		$me_id = $this->session->userdata('admin_user_id');

		if(isset($access))
		{
			$access = implode(',', $this->input->post('deny_reasons'));

			/*foreach($access as $k=>$v)
			{
				$sql = "select * from admin_access where admin_id = ".$v." and type_id = ".$me_id;
				$query = $this->db->query($sql);

			}
		}*/

		$pass = $this->input->post('password2');

		if($pass != '')
			$upd['password'] = $this->bcrypt->hash_password($pass);

		$this->db->where('id',$id);
		$this->db->update('admin',$upd);
		$this->log_activity('user',$id,'update');
	}

	function get_admin_types($type = '')
	{
		if($type == 'access')
			$sql = "SELECT * FROM admin_types_access";
		else
			$sql = "SELECT * FROM admin_types";

		$query = $this->db->query($sql);

		return $query->result(); 
	}

	function get_admin_type($id)
	{
		$sql = "SELECT * FROM admin_types where id = {$id}";
		$query = $this->db->query($sql);

		$row = $query->row();

		return $row->type_name; 
	}

	function get_user_access($user_id, $type)
	{

		// quick and dirty method
		switch($type)
		{
			case 'editorial':
				$type = 'articles';
				break;

			case 'video':
				$type = 'videos';
				break;

			case 'tv':
				$type = 'gbaamtv';
				break;

			case 'homepage':
				$type = 'slides';
				break;

		}
		// get the admin
		$sql = "SELECT * FROM admin where id = ".$user_id;
		$query = $this->db->query($sql);
		$row = $query->row();

		// get the access ids and turn them into arrays
		$access = $row->access;
		$access = explode(",",$access);

		// if type is all letters fetch the ID
		if(ctype_alpha($type))
		{
			$sql2 = "SELECT * FROM admin_types_access where type = '".$type."'";
			$query2 = $this->db->query($sql2);
			$row2 = $query2->row();

			$type = $row2->id;
		}

		$admin_type = $this->session->userdata('admin_type');

		// if a user has the admin type OR if this user is an admin by default
		if((in_array($type,$access)) OR ($admin_type <= '2')) 
			return TRUE;
		else
			return FALSE;
	}

	/*
	Combine this method and the previeous method... this method was used in the admin2/users/edit/234 page...
	 */
	function get_user_access_other($user_id, $type)
	{

		// quick and dirty method
		switch($type)
		{
			case 'editorial':
				$type = 'articles';
				break;

			case 'video':
				$type = 'videos';
				break;

			case 'tv':
				$type = 'gbaamtv';
				break;

			case 'homepage':
				$type = 'slides';
				break;

		}
		// get the admin
		$sql = "SELECT * FROM admin where id = ".$user_id;
		$query = $this->db->query($sql);
		$row = $query->row();

		// get the access ids and turn them into arrays
		$access = $row->access;
		$access = explode(",",$access);

		// if type is all letters fetch the ID
		if(ctype_alpha($type))
		{
			$sql2 = "SELECT * FROM admin_types_access where type = '".$type."'";
			$query2 = $this->db->query($sql2);
			$row2 = $query2->row();

			$type = $row2->id;
		}

		// if a user has the admin type OR if this user is an admin by default
		if(in_array($type,$access)) 
			return TRUE;
		else
			return FALSE;
	}

	function processpending($status,$id,$new_video_id = '')
	{
		if($status == 'deny')
		{	
			$r = $this->input->post('deny_reasons');
			
			if(empty($r))
				$deny_reasons = '';
			else
				$deny_reasons = implode(',',$r);

			$deny_reasons_other = $this->input->post('deny_reasons_other');
			$upd['deny_reasons'] = $deny_reasons;
			$upd['deny_reasons_other'] = $deny_reasons_other;
		}
		

		$upd['status'] = $status;
		$this->db->where('id',$id);
		$this->db->update('video_pending',$upd);


		// send email
		$this->Email_model->pending_result_email($id,$new_video_id);
	}

	function deleteAdmin($id)
	{
		// delete admin
		$sql = "DELETE FROM admin WHERE id = {$id}";
		$this->db->query($sql);
		$this->log_activity('admin',$id,'delete');
	}
	
	function delete_disqus_thread()
	{
	}

	function get_reasons($type)
	{
		$sql = "SELECT * FROM admin_reasons WHERE type = '{$type}'";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function get_reason($id,$type)
	{
		$sql = "SELECT * FROM admin_reasons WHERE id = '$id' and type = '{$type}' ";
		$query = $this->db->query($sql);
		return $query->row();
	}

	function is_this_my_post($type, $post_id)
	{
		// set vars to use
		$my_type = $this->session->userdata('admin_type');
		$my_id = $this->session->userdata('admin_user_id');

		// if this is a regular user who is not an admin 
		if($my_type != '1' && $my_type != '2' && $my_type != '4')
		{
			$sql = "SELECT * FROM {$type} where id = '{$post_id}' and poster_id = '{$my_id}' LIMIT 1";
			$query = $this->db->query($sql);
			$num_rows = $query->num_rows();

			if($num_rows > 0)
				return TRUE; // this is my post so i have access to it
			else
				return FALSE; // this is NOT my post so i DONT have access to it.
		}
		else // this is an admin
		{
			return TRUE; // this admin has access to this post
		}
	}

	function update_dn()
	{

		foreach($this->get_users() as $g)
		{
			$sql = "Update admin SET display_name = '".$g->firstname." ".$g->lastname."' where id = {$g->aid}";
			$this->db->query($sql);

		}
	}

	function log_activity($type,$type_id,$action,$other_info = '')
	{
		$ins['user_id'] = $this->session->userdata('admin_user_id');
		$ins['type'] = $type;
		$ins['type_id'] = $type_id;
		$ins['action'] = $action;
		$ins['other_info'] = $other_info;
		$ins['date_created'] = date('Y-m-d H:i:s');

		$this->db->insert('admin_activity',$ins);
	}

	function monthly_unlink($type)
	{

		// Mixtapes
		$sql = "Select * from ".$type." where status = 0";
		//$sql = "Select * from slides where id = 46";
		$query = $this->db->query($sql);

		foreach($query->result() as $g)
		{
			// get the date
			$date_deleted = strtotime($g->date_deleted);

			// get the last month
			$last_month =  strtotime('- 1 month');

			// if a month or more has passed.
			if($last_month >= $date_deleted)
			{

				if($type == 'mixtapes')
				{
					$sql2 = "SELECT * FROM mixtape_tracks where mixtape_id = ".$g->id;
					$query2 = $this->db->query($sql2);

					// delete the mixtape Tracks
					foreach($query2->result() as $g2)
					{
						$audio_file = "./".$this->config->item('mixtape_audio_dir').$g2->hash.$g2->ext;
						if(file_exists($audio_file))
							unlink($audio_file);
					}

					// delete the mixtape covers
					$this->Photo_model->delete_image('mixtape',$g->front_cover);
					$this->Photo_model->delete_image('mixtape',$g->back_cover);

					// delete the mixtape Tracks
					$zip_file = "./".$this->config->item('mixtape_zip_dir').$g->zip_file;
					if(file_exists($zip_file))
						unlink($zip_file);
				}

				if($type == 'video')
				{
					// delete the video file
					/*$video_file = "./".$this->config->item('mixtape_audio_dir').$g2->hash.$g2->ext;
					if(file_exists($video_file))
						unlink($video_file);*/

					// delete the thumbnail file
					$this->Photo_model->delete_image('video',$thumb);
				}

				if($type == 'slides')
				{
					// delete the Slide image
					$slide_image = "./".$g->image;

					if(file_exists($slide_image))
						unlink($slide_image);
				}

				if($type == 'articles')
				{
					// delete the Article image
					$this->Photo_model->delete_image('article',$g->image);
				}

				if($type == 'blog_posts')
				{
					// delete the Article image
					$this->Photo_model->delete_image('blog_posts',$g->image);
				}

			}

		}
	}

}