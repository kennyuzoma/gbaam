<?php 

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class Songs_model extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
        
    }

    function getSotw()
    {
		//arange by date
        $sql = "SELECT *, songs.id AS sid FROM feat_items 
        		LEFT JOIN songs ON songs.id = feat_items.type_id 
				LEFT JOIN photos ON photos.photo_id = songs.front_cover 
        		WHERE songs.status = 1 and feat_type = 'sotw' and active = 1 order by feat_items.type_id DESC LIMIT 1 ";
        $query = $this->db->query($sql); 
        return $query->result();
    }

    function makeSotw($song_id)
	{
		$date_created = date('Y-m-d H:i:s');

		$sql = "INSERT INTO  songs_sotw ( song_id, date_created ) 
				VALUES ('{$song_id}',  '{$date_created}');";  
		$this->db->query($sql);
	}

	function AddToSlot($slot, $feat_type, $song_id)
	{
		$sql = "UPDATE feat_items SET active=0 WHERE slot={$slot} and feat_type = '{$feat_type}' and type='song'";
		$this->db->query($sql);

		$ins['feat_type'] = $feat_type;
		$ins['slot'] = $slot;
		$ins['type_id'] = $song_id;
		$ins['type'] = 'song';
		$ins['active'] = 1;
		$ins['datestart'] = date('Y-m-d H:i:s');

		$this->db->insert('feat_items',$ins);

		$this->Admin_model->log_activity('feat_song',$this->db->insert_id(),'slot','Feat Type: '.$feat_type.' Slot '.$slot);
	}

	function isSOTWavail()
	{
		$sql = "SELECT * FROM feat_items LEFT JOIN songs ON songs.id = feat_items.type_id WHERE feat_type = 'sotw' and songs.status = 1 and feat_items.active = 1 ORDER BY feat_items.id DESC LIMIT 1";
        $query = $this->db->query($sql); 
        $count = $query->num_rows();

        if($count >= 1)
        {
        	return TRUE;
        }
        else
        {
        	return FALSE;
        }
	}

	
	function getSong($sname,$permalink)
	{ 
		if($sname != '' || $permalink != '')
		{
			$sql = "SELECT *, songs.status as songstatus from songs LEFT JOIN photos ON photos.photo_id = songs.front_cover where id = ".$sname." and permalink = '".$permalink."' and songs.status != 0";	
			$query = $this->db->query($sql);
			if($query->num_rows() > 0)
				return $query->result();
			else
				return FALSE;
		}
		else
		{
			return FALSE;
		}
	}

	function getSong_new($sname)
	{ 
		$sql = "SELECT * from songs LEFT JOIN photos ON photos.photo_id = songs.front_cover where songs.status != 0 and id = ".$sname;	
		$query = $this->db->query($sql);
		return $query->row();
	}

	function getSong_by_hash($hash)
	{ 
		$sql = "SELECT * from songs LEFT JOIN photos ON photos.photo_id = songs.front_cover where songs.status != 0 and song_hash = '".$hash."'";	
		$query = $this->db->query($sql);
		return $query->row();
	}
	
	function getSong_new_admin($song_id)
	{ 
		$sql = "SELECT *, songs.status as songstatus, songs.date_created as sdate_created from songs LEFT JOIN photos ON photos.photo_id = songs.front_cover where id = ".$song_id;	

		$query = $this->db->query($sql);
		return $query->row();
	}
	
	
	function getSongUrl($sname)
	{
		$sql = "SELECT * from songs where id = ".$sname;	
		$query = $this->db->query($sql); 
		foreach($query->result() as $b)
		{
			$artist = clean_url_slug($b->artist);
			$title = clean_url_slug($b->title); 
			$id = $b->id;
		}
		
		//$url = $artist.'-'.$title.'-_-'.$id;
		$url = $artist.'-'.$title;
		return $url;
	}
	
	function getAllSongs($limit = '', $offset = '')
	{
		$sql = "SELECT * from songs  LEFT JOIN photos ON photos.photo_id = songs.front_cover where songs.status = 1 order by id desc";
			
			if($limit != '')
			{
				$sql .= " LIMIT";

				if($offset != '')
					$sql .= " {$offset},";

				$sql .= " {$limit}";
			}
				

			

		$query = $this->db->query($sql);
		return $query->result();	

	}

	function getAllSongs_admin($limit = '', $offset = '')
	{
		$sql = "SELECT * from songs  LEFT JOIN photos ON photos.photo_id = songs.front_cover WHERE songs.status != 0  order by id desc";
			
			if($limit != '')
			{
				$sql .= " LIMIT";

				if($offset != '')
					$sql .= " {$offset},";

				$sql .= " {$limit}";
			}
				

			

		$query = $this->db->query($sql);
		return $query->result();	

	}

	function getAllSongs_2013($limit = '')
	{
		$sql = "SELECT * from songs  LEFT JOIN photos ON photos.photo_id = songs.front_cover Where songs.status = 1 order by id desc";
			if($limit != '')
				$sql .= " LIMIT {$limit}";
		$query = $this->db->query($sql);

		$d['result'] = $query->result();	
		$d['count'] = $query->num_rows;
		return $d;
	}

	function addview($d = '',$id)
	{
		$view = 1;//rand(1, 3);
		if ($d == '')
		{
			$sql = "UPDATE songs SET v_count=v_count+{$view} WHERE id={$id}";
		}
		elseif($d == 'd')
			$sql = "UPDATE songs SET d_count=d_count+{$view} WHERE id={$id}";
			
		$this->db->query($sql);		
	}

	function add_vote($song_id,$choice)
	{
		$user_id = $this->session->userdata('user_id');

		if( $this->session->userdata('logged_in') == FALSE )
		{
			return FALSE;
		}
		else
		{
			$sql1 = "SELECT * From votes WHERE type_id = {$song_id} and voter_id = {$user_id}  and type = 'song' order by id desc limit 1";

			$query = $this->db->query($sql1);

			if($query->num_rows() == 1)
			{
				$sql = "UPDATE votes SET choice='{$choice}' WHERE type_id = {$song_id} and voter_id = {$user_id} and type = 'song' ";
			}
			else
			{
				$sql = "INSERT INTO  votes ( type_id  ,  choice, voter_id, type )
				VALUES ('{$song_id}',  '{$choice}', {$user_id}, 'song');";
			}
			
			$this->db->query($sql);

			return TRUE;
		}
		
	}

	function did_i_vote($song_id)
	{
		$user_id = $this->session->userdata('user_id');

		$sql = "SELECT * From votes WHERE type_id = {$song_id} and voter_id = {$user_id} and type = 'song' order by id desc limit 1";

		$query = $this->db->query($sql);

		$q['num_rows'] = $query->num_rows();
		$q['row'] = $query->row();

		return $q;

	}
	
	function get_vote($song_id)
	{
		$sql = "SELECT choice FROM votes WHERE type_id = {$song_id} and type = 'song' ";
		
		$query = $this->db->query($sql);

		$bl = array();
		foreach($query->result() as $b) 
		{
			$bl[] = $b->choice;	
		}
		
		$rows = $query->num_rows();
		$d['total_score'] = $rows*4;
		$d['my_score'] = array_sum($bl);
		
		return $d; 
	}

	function get_vote_final($song_id)
	{
		$sql = "SELECT choice FROM votes WHERE type_id = {$song_id} and type = 'song' ";
		
		$query = $this->db->query($sql);

		$bl = array();
		foreach($query->result() as $b) 
		{
			$bl[] = $b->choice;	
		}
		
		$rows = $query->num_rows();
		$getmix['total_score'] = $rows*4;
		$getmix['my_score'] = array_sum($bl);
		
		$my_score = $getmix['my_score'];
		$total_score = $getmix['total_score'];
			
		//echo $my_score.'/'.$total_score;
		if($total_score == 0)
		{
			$d['isScore'] = FALSE;
			$d['final_score'] = FALSE;
		}
		else
		{
			$d['isScore'] = TRUE;
			$percent = ($my_score / $total_score) * 10;
			$percent = round($percent);
			$d['final_score'] = $percent; 
		}

		return $d; 
	}

	function insertSong()
	{
		$date = date("Y-m-d H:i:s", strtotime($this->input->post('date')));

		$data = array(
			'title' => $this->input->post('song_title'),
			'desc' => $this->input->post('description'),
			'artist' => $this->input->post('artist_name'),
			'permalink' => clean_url_slug($this->input->post('artist_name').'-'.$this->input->post('song_title')),
			'tags' => $this->input->post('tags'),
			'author' => $this->input->post('author'),
			'author_id' => $this->input->post('author_id'),
			'poster_id' => $this->session->userdata('admin_user_id'),
			'status' => '1',
			'date_created' => date("Y-m-d H:i:s")
		);

		$this->db->insert('songs',$data);
		$song_id = $this->db->insert_id();
		
		$this->log_activity('song',$song_id,'create');

		return $song_id;
	}

	function updateSong($song_id)
	{
		$status = $this->input->post('status');
		
		$date = date("Y-m-d H:i:s", strtotime($this->input->post('date')));

		if($status == '4')
			$status = '1';
		
		$data = array(
			'title' => $this->input->post('song_title'),
	        'desc' => $this->input->post('description'),
	        'date_created' => $date,
	        'artist' => $this->input->post('artist_name'),
			'permalink' => clean_url_slug($this->input->post('artist_name').'-'.$this->input->post('mixtape_title'), $song_id),
			'author' => $this->input->post('author'),
			'author_id' => $this->input->post('author_id'),
			'tags' => $this->input->post('tags'),
			'status' => $this->input->post('status'),
			'date_edited' => date('Y-m-d H:i:s')
	    );

		$this->db->where('id', $song_id);
		$this->db->update('songs', $data); 
		$this->log_activity('song',$song_id,'update');
	}
	

	function deleteSong($song_id)
	{

		$this->Admin_model->log_activity('song',$song_id,'delete');

		$upd1['status'] = '0';
		$upd1['date_deleted'] = date('Y-m-d H:i:s');
		$this->db->update('songs', $upd1, array('id' => $song_id)); 

		$upd['status'] = '0';
		$this->db->update('votes', $upd, array('type_id' => $song_id)); 
		$this->db->update('feat_items', $upd, array('type_id' => $song_id,'type' => 'songs'));

	}

	

	
}

?>