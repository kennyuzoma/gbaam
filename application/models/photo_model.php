<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class Photo_model extends CI_model {

	public $tbl_users = 'users';

	function get_photo($photo_id)
	{
		$sql = "SELECT * FROM photos WHERE photo_id = '{$photo_id}' LIMIT 1";
		$query = $this->db->query($sql);
		return $query->row();
	}

	function new_photo_sandbox($type,$type_id = '')
  	{
		$date = date('Y-m-d H:i:s');

		$data = array(
			'user_id' => $this->session->userdata('user_id'),
			'date' => $date,
			'type' => $type,
			'type_id' => $type_id
		);

	    $this->db->insert('photos', $data);

		return $this->db->insert_id();
  	}

	function actual_upload($type, $type_id='', $photodata)
	{
		//load image manipulation
		$this->load->library('image_lib');

		$upload_path = "./".$this->config->item('uploaded_images_folder').'/'.$type.'/';

		//insert the new photo into the database
		$this->new_photo_sandbox($type,$type_id);

		//prepare the new image to be renames
		$id = $this->db->insert_id();
		$img_hash = md5($id);

/*
		$dir_1 = substr($img_hash, 0, 3);
		$dir_2 = substr($img_hash, 3, 3);
		$dir_3 = substr($img_hash, 6, 3);

		$img_node[0] = $upload_path . $dir_1 . '/';
		$img_node[1] = $img_node[0] . $dir_2 . '/';
		$img_node[2] = $img_node[1] . $dir_3 . '/';

		foreach ($img_node as $this_dir) {

			if (!is_dir($this_dir)) { // directory doesn't exist

				if (!mkdir($this_dir, 0777)) { // attempt to make it with read, write, execute permissions
					return false; // bug out if it can't be created
				}
			}
		}

		$new_img_path = $upload_path. $dir_1 . '/' . $dir_2 . '/'. $dir_3 . '/' ;
		
*/

		$new_img_path = $upload_path . '/' ;

		$without_jpg = $new_img_path .  $id . '_' . $img_hash ;
		$new_name_img =  $new_img_path .  $id . '_' . $img_hash . '.jpg';

		//rename the image
		rename($photodata['upload_data']['full_path'],$new_name_img);
		//chmod($new_name_img,0777);

		$this->add_details_to_photo_sandbox($id,$img_hash);

		
		//Create post var to set it to default
		$_POST['set_picture'] = $id;

		//Get photo information
		$imageSize = $this->image_lib->get_image_properties($new_name_img, TRUE);

		if($type == 'article')
			$ref_size = 640;
		else
			$ref_size = 960;

		//prepare to reduce if the with or height > 800px
		if($imageSize['width'] > $ref_size || $imageSize['height'] > $ref_size)
		{
			$config['source_image'] = $new_name_img;
			$config['new_image'] = $new_name_img;
			$config['create_thumb'] = FALSE;
			$config['maintain_ratio'] = TRUE;
			//if Witdh is greater then height
			if($imageSize['width'] > $imageSize['height'])
			{
				if($type == 'article')
				{
					$config['width'] = 640;
					$config['height'] = 480;
				}
				else
				{
					$config['width'] = 960;
					$config['height'] = 720;
				}
				
			}
			//if height is greater then width
			elseif($imageSize['height'] > $imageSize['width'])
			{
				if($type == 'article')
				{
					$config['height'] = 640;
					$config['width'] = 480;
				}
				else
				{
					$config['height'] = 960;
					$config['width'] = 720;
				}
				
			}

			else
			{
				$config['height'] = $ref_size;
				$config['width'] = $ref_size;
			}
			//Quality 100 percent
			$config['quality'] = 100;

			//Get ready then .... resisze
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
		}
		else
		{
			$config['source_image'] = $new_name_img;
			$config['new_image'] = $new_name_img;
			$config['create_thumb'] = FALSE;
			$config['maintain_ratio'] = TRUE;
			$config['width'] = $imageSize['width'];
			$config['height'] = $imageSize['height'];
			$config['quality'] = 100; // this sets the resizer to default to height when preserving the ratio
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
		}
		$this->image_lib->clear();

		//create thumbnails
		$this->create_thumbs($type,$without_jpg);

		return $id;
	}

	function create_thumbs($type, $photo_without_ext)
	{	
		// create temp img to use
		$this->create_temp_image($photo_without_ext);

		if($type == 'mixtape')
		{
			// create the thumbnails
			$this->crop_photo($photo_without_ext, 50, 50, '_50');
			$this->crop_photo($photo_without_ext, 125, 125, '_125');
			$this->crop_photo($photo_without_ext, 200, 200, '_200');
			$this->crop_photo($photo_without_ext, 500, 500, '_500');
		}

		if($type == 'song')
		{
			// create the thumbnails
			$this->crop_photo($photo_without_ext, 50, 50, '_50');
			$this->crop_photo($photo_without_ext, 125, 125, '_125');
			$this->crop_photo($photo_without_ext, 200, 200, '_200');
			$this->crop_photo($photo_without_ext, 500, 500, '_500');
		}

		if($type == 'article')
		{
			// create the thumbnails
			$this->crop_photo($photo_without_ext, 60, 60, '_60');
			$this->crop_photo($photo_without_ext, 150, '', '_150');
		}

		if($type == 'user')
		{
			$this->crop_photo($photo_without_ext, 50, 50, '_50');
			$this->crop_photo($photo_without_ext, 125, 125, '_125');
			$this->crop_photo($photo_without_ext, 200, 200, '_200');
		}

		if($type == 'admin_user' || $type == 'user')
		{
			$this->crop_photo($photo_without_ext, 50, 50, '_50');
			$this->crop_photo($photo_without_ext, 125, 125, '_125');
			$this->crop_photo($photo_without_ext, 200, 200, '_200');
		}

		if($type == 'video')
		{
			$this->crop_photo($photo_without_ext, 50, '', '_50');
			$this->crop_photo($photo_without_ext, 125, '', '_125');
			$this->crop_photo($photo_without_ext, 200, '', '_200');
		}

		

		// delete the temp image, we are done with it.
		unlink($photo_without_ext.'_temp.jpg');
	}

  	function create_temp_image($photo_without_ext)
  	{
  		// load library
  		$this->load->library('image_lib');

  		$image = $photo_without_ext.'.jpg';
  		$temp_img = $photo_without_ext.'_temp.jpg';

		//Load Settings
		$config['source_image'] = $image;

		//Create this so that the original photo isnt altered
		$config['new_image'] = $temp_img; 

		$config['maintain_ratio'] = FALSE;
		$config['create_thumb'] = FALSE;

		//Get Image Properties for Cropping
		$imageSize = $this->image_lib->get_image_properties($image, TRUE);

		//If width is larger. Prep for square crop.
		if ($imageSize['width'] > $imageSize['height'])
		{
			$config['width'] = $imageSize['height'];
			$config['height'] = $imageSize['height'];
			$config['x_axis'] = (($imageSize['width'] / 2) - ($config['width']/ 2));
		}
		//If height is larger. Prep for square crop.
		elseif($imageSize['width'] < $imageSize['height'])
		{
			$config['width'] = $imageSize['width'];
			$config['height'] = $imageSize['width'];
			$config['y_axis'] = (($imageSize['height'] / 2) - ($config['height'])/ 2);
		}

		//Quality is 100percent
		$config['quality'] = 100;

		//Get ready set....Crop!
		$this->image_lib->initialize($config);
		
		if(!$this->image_lib->crop())
			echo $this->image_lib->display_errors();

		return $temp_img;
  	}

	function crop_photo($photo_without_ext, $width = '', $height = '', $marker)
	{
		$image = $photo_without_ext.'.jpg';
  		$temp_img = $photo_without_ext.'_temp.jpg';

		// load library
		$this->load->library('image_lib');

		//Config for the thumb
		$config2['width']        = $width;

		if($height == '')
		{
			$config2['height'] = $width-1;
			$config2['source_image'] = $image;
			$config2['maintain_ratio'] = true;
		}
		else
		{
			$config2['height'] = $height;
			$config2['source_image'] = $temp_img;
			$config2['maintain_ratio'] = false;
		}

		
		$config2['new_image'] = $image;
		$config2['create_thumb'] = TRUE;

		// if false then the empty value will be filled automatically
		

		$config2['thumb_marker']    = $marker;
		
		$config2['quality'] = 100;

		// create the thumbnail
		$this->image_lib->initialize($config2);
		$this->image_lib->resize();
		$this->image_lib->clear($config2);
	}

	function add_details_to_photo_sandbox($photo_id,$img_hash)
  	{
		$data = array(
			'img_hash' => $img_hash,
		);
		$this->db->where('photo_id',$photo_id);
	    $this->db->update('photos', $data);
  	}

	function new_photo()
  	{
		$my_id = $this->session->userdata('user_id');
		$date = date('Y-m-d H:i:s');
		$sql = "INSERT INTO photos (user_id,date) VALUES ({$my_id},'{$date}')";

		$this->db->query($sql);

		return $this->db->insert_id();
  	}

  	



	function set_default($photo_id)
	{
		$session_id = $this->session->userdata('user_id');
		$profile_img = $photo_id.'_'.$session_id;
		$user = array(
			'profile_img' => $profile_img
		);

		$this->db->where('id',$session_id);

		$this->db->update($this->tbl_users,$user);
		return TRUE;
	}

	function remove_default_image()
	{
		//retrieve information
		foreach($this->Home_model->get_profile() as $g)
		{
			$id = $g->id;
			$img = $g->profile_img;
		}

		//set Var to be changed
		$user['profile_img'] = '';

		//Remove From database
		$this->db->where('id',$id);
		$this->db->update($this->tbl_users,$user);
	}

	//in the future
	function delete_photo($image_id,$type = '')
	{
		//delete the photo
		$images_folder = $this->config->item('user_images_folder');

		//find faster method
		$sql = "SELECT * FROM photos where photo_id = {$image_id}";
		$query = $this->db->query($sql);
		$r = $query->row();

		$image = 'ug2'.'/'.$r->dir_1 . '/' . $r->dir_2 . '/' . $r->dir_3 . '/'.$image_id.'_'.$r->img_hash;

		//Find the Images

		$reg_img = $image.'.jpg';
		$tiny_img = $image.'_t.jpg';
		$small_img = $image.'_sm.jpg';
		$large_img = $image.'_lg.jpg';
		$bg_img = $reg_img;

		//Finally Delete the photos
		if($type == 'bg' || $type  == 'background')
		{
			unlink($bg_img);
		}
		else
		{
			unlink($tiny_img);
			unlink($small_img);
			unlink($large_img);
			unlink($reg_img);
		}

	}

	function show_image($item_type, $image_id, $img_hash = '', $image_size = '', $path = '')
	{
		// return full path if path variable is 'return_path'
		if($path == 'return_path')
			$base = './';
		else
			$base = base_url();


		// If image ID is blank or not numeric... then we are gonna return a gholder image....
		if(($image_id == '') || (!is_numeric($image_id)))
		{
			if($image_size > 125 && $image_size < 200)
				$image_size = 125;

			if(($image_size < 50) || (($image_size > 50 && $image_size < 100)))
				$image_size = 50;

			if($image_size > 200)
				$image_size = 200;

			return $base.'assets/img/gholder_'.$image_size.'.gif';
		}
		else 
		{
			// get the image folder depending on the item type.
			$images_folder = $this->config->item('uploaded_images_folder').$item_type;

			// if there is no image hash then get the info from the dB
			if($img_hash == '')
			{
				$photo = $this->get_photo($image_id);
				$img_hash = $photo->img_hash;
			}

	
			$image = $base.$images_folder.'/'.$image_id.'_'.$img_hash;

			if($image_size == 'bg' || $image_size == 'background')
			{
				$sql = "SELECT * FROM photos where photo_id = {$image_id}";
				$query = $this->db->query($sql);
				$r = $query->row();

				$image = $base.$images_folder.'/'.$image_id.'/'.$image_id.'_'.$r->img_hash.'.jpg';

				return $image;
			}
			elseif($image_size == '' || $image_size == 'original')
			{
				return $image.'.jpg';
			}
			else
				return $image.'_'.$image_size.'.jpg';
		}
	}

	function delete_image($item_type,$image_id)
	{
		// images folder
		$images_folder = './'.$this->config->item('uploaded_images_folder').$item_type.'/';

		// Open a known directory, and proceed to read its contents
		foreach(glob($images_folder.$image_id."_*") as $file) 
		{
			unlink($file);
		}

	}


	function show_profile_image($item_type, $image_id, $img_hash = '', $image_size = '', $path = '')
	{
		// return full path if path variable is 'return_path'
		if($path == 'return_path')
			$base = './';
		else
			$base = base_url();

		
		// If image ID is blank or not numeric... then we are gonna return a gholder image....
		if(($image_id == ''))
		{
			if($image_size > 125 && $image_size < 200)
				$image_size = 125;

			if(($image_size < 50) || (($image_size > 50 && $image_size < 100)))
				$image_size = 50;

			if($image_size > 200)
				$image_size = 200;

			return $base.'assets/img/gholder_'.$image_size.'.gif';
		}
		else 
		{
			if( (strpos($image_id,'twimg') !== false) || (strpos($image_id,'facebook') !== false))
			{
				if($image_size == '50')
					$image = $image_id;
				else
					$image = str_replace('_normal','',$image_id); // for twitter

				return $image;
			}
			else
			{
				// get the image folder depending on the item type.
				$images_folder = $this->config->item('uploaded_images_folder').$item_type;

				// if there is no image hash then get the info from the dB
				if($img_hash == '')
				{
					$photo = $this->get_photo($image_id);
					$img_hash = $photo->img_hash;
				}

		
				$image = $base.$images_folder.'/'.$image_id.'_'.$img_hash;

				if($image_size == 'bg' || $image_size == 'background')
				{
					$sql = "SELECT * FROM photos where photo_id = {$image_id}";
					$query = $this->db->query($sql);
					$r = $query->row();

					$image = $base.$images_folder.'/'.$image_id.'/'.$image_id.'_'.$r->img_hash.'.jpg';

					return $image;
				}
				elseif($image_size == '' || $image_size == 'original')
				{
					return $image.'.jpg';
				}
				else
					return $image.'_'.$image_size.'.jpg';
			}
		}
	}

}