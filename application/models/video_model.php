<?php

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class Video_model extends CI_Model {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

	function getVideo($videoid,$api = '')
	{
		if(!$videoid == '')
		{
			$sql = "SELECT *, video.status as videostatus, video.date_created as vdate_created FROM video 
					LEFT JOIN photos ON photos.photo_id = video.id 
					where video.id = ".$videoid." and video.status != 0;";
			$query = $this->db->query($sql);

			if($query->num_rows() > 0)
				return $query->result();
			else
				return FALSE;
		}
		else
		{
			return FALSE;
		}
	}

	function getVideo_row($videoid)
	{
		$sql = "SELECT * FROM video 
				LEFT JOIN photos ON photos.photo_id = video.id 
				where id = ".$videoid." and video.status != 0 ;";
		$query = $this->db->query($sql);
		
		return $query->row();
	}

	function getVideos_count()
	{
		$sql = "SELECT * FROM video 
				LEFT JOIN photos ON photos.photo_id = video.id 
				where video.status = 1 and video.gtv = 0 and video.id != 3 and video.chall = 0";
		$query = $this->db->query($sql);
		
		return $query->num_rows();
	}

	function get_chall_video($videoid)
	{
		$sql = "SELECT * FROM video_challenge 
				LEFT JOIN photos ON photos.photo_id = video.id 
				where video.status = 1 and video.id = ".$videoid.";";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
			return $query->result();
		else
			return FALSE;
	}

	function getAllVideos($type='', $limit = '', $order_by = 'id DESC')
	{
		$sql = "SELECT *, video.date_created as vdate_created  FROM video 
				LEFT JOIN photos ON photos.photo_id = video.id 
				WHERE video.status = 1 ";

		if($type == '')
			$sql .= "and video.gtv = 0 and video.chall = 0";
		if($type == 'gbaamtv')
			$sql .= "and gtv = 1 and chall = 0";
		if($type == 'challenge')
			$sql .= "and gtv = 0 and chall = 1";

		if($limit != '')
		  $sql .= " ORDER by ".$order_by." LIMIT {$limit};";

	  	$query = $this->db->query($sql);

		return $query->result();
	}


	function getFeatVideo_slot_count($type,$slot)
	{
		$sql = "Select * FROM feat_items 
				LEFT JOIN video ON video.id = feat_items.type_id 
				WHERE  video.status = 1 and feat_items.active = 1 and slot = {$slot} and feat_type = '{$type}' ORDER BY feat_items.id DESC LIMIT 1";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

	function getFeatVideo_slot($type,$slot)
	{
		$sql = "Select * FROM feat_items LEFT JOIN video ON video.id = feat_items.type_id WHERE  video.status = 1 and feat_items.active = 1 and slot = {$slot} and feat_type='{$type}' LIMIT 1";
		$query = $this->db->query($sql);
		return $query->row();
	}

	function getFeatVideoTypeCount($type)
	{
		$sql = "Select * FROM feat_items WHERE feat_items.active = 1 and feat_type='{$type}'";
		$query = $this->db->query($sql);

		return $query->num_rows();
	}

	function get_votes($videoid,$choice)
	{
		//Save For later.
		$sql = "SElECT COUNT(choice) as choice FROM votes WHERE type_id = '{$videoid}' and choice = '{$choice}' and type = 'video' ;";
		$query = $this->db->query($sql);
		foreach($query->result() as $q)
			$choi =  $q->choice;

		if($choi != '0')
			echo $choi;
	}

	function add_vote($videoid,$choice)
	{
		$user_id = $this->session->userdata('user_id');

		if( $this->session->userdata('logged_in') == FALSE )
		{
			return FALSE;
		}
		else
		{
			$sql1 = "SELECT * From votes WHERE type_id = {$videoid} and voter_id = {$user_id}  and type = 'video' order by id desc limit 1";

			$query = $this->db->query($sql1);

			if($query->num_rows() == 1)
			{
				$sql = "UPDATE votes SET choice='{$choice}' WHERE type_id = {$videoid} and voter_id = {$user_id} and type = 'video' ";
			}
			else
			{
				$sql = "INSERT INTO  votes ( type_id  ,  choice, voter_id,type )
				VALUES ('{$videoid}',  '{$choice}', {$user_id}, 'video' );";
			}
			
			$this->db->query($sql);

			return TRUE;
		}
		
	}

	function get_vote($videoid)
	{
		$sql = "SELECT choice FROM votes WHERE type_id = {$videoid} and type = 'video'";

		$query = $this->db->query($sql);

		$bl = array();
		foreach($query->result() as $b)
		{
			$bl[] = $b->choice;
		}

		$rows = $query->num_rows();
		$d['total_score'] = $rows*4;
		$d['my_score'] = array_sum($bl);

		return $d;
	}

	function getLatestGbaamVids($page = '', $limit = '')
	{
		/*
		$sql = "SELECT *
				FROM  video WHERE gtv = 1 ";
		if($page != '')
			$sql .= " and gtv_cat = '{$page}'";

		$sql .= " ORDER BY id DESC LIMIT 8";
		*/
	
		$sql = "SELECT *, video.id as id FROM video 
				LEFT JOIN photos ON photos.photo_id = video.id 
				LEFT JOIN categories ON categories.id = video.gtv_cat WHERE video.status = 1 and gtv = 1 ";

		if($page != '')
		{
			if(is_numeric($page) )
				$sql .= "and categories.id = '{$page}' ";

			else
				$sql .= "and categories.name = '{$page}' ";
		}

		$sql .= "ORDER BY video.id DESC LIMIT 8"; 

		$query = $this->db->query($sql);
		return $query->result();
	}

	function ajax_load_gtv_videos($page_num, $page = '')
	{
		/*$sql = "SELECT *
				FROM  video WHERE gtv = 1 ";
		if($page != '')
			$sql .= " and gtv_cat = '{$page}'";

		if($page_num == 0)
		{
			$sql .= " ORDER BY id DESC LIMIT 8";
		}
		else
		{
			$page_num = $page_num - 1;
			$new_page_num = $page_num*8;
			$sql .= " ORDER BY id DESC LIMIT {$new_page_num},8";
		}*/

		$sql = "SELECT *, video.id as id FROM video 
				LEFT JOIN categories ON categories.id = video.gtv_cat 
				LEFT JOIN photos ON photos.photo_id = video.id 
				WHERE video.status = 1 and gtv = 1 ";

		if($page != '')
			$sql .= "and categories.name = '{$page}' ";

		if($page_num == 0)
		{
			$sql .= "ORDER BY video.id DESC LIMIT 8"; 
		}
		else
		{
			$page_num = $page_num - 1;
			$new_page_num = $page_num*8;
			$sql .= " ORDER BY video.id DESC LIMIT {$new_page_num},8";
		}



		$query = $this->db->query($sql);
		return $query->result();
	}

	public function create_ajax_pagination_links($page = '')
	{
		/*$sql = "SELECT * FROM  video WHERE gtv = 1 ";

		if($page != '')
			$sql .= " and gtv_cat = '{$page}'";*/

		$sql = "SELECT * FROM video 
				LEFT JOIN categories ON categories.id = video.gtv_cat 
				LEFT JOIN photos ON photos.photo_id = video.id 
				WHERE video.status = 1 and gtv = 1 ";

		if($page != '')
			$sql .= "and categories.name = '{$page}' ";

		$q = $this->db->query($sql);
		$count = $q->num_rows();


		//$num_of_links = (floor($count/8));
		
		$num_of_links = ($count/8);

		//if($num_of_links != 1)
		//	$num_of_links = $num_of_links+1;
		
		if($count > 8)
		{
			// if this is a fraction
			if(is_int($num_of_links) == FALSE)
			{
				$num_of_links = floor($num_of_links);
				$num_of_links = $num_of_links+1;
			}
			
		}
		

		return $num_of_links;
	}

	

	function getGbaamTVByCat($cat = '', $limit = '', $offset = '')
	{
		/*
		$sql = "SELECT *
				FROM  video WHERE gtv = 1 ";
		if($cat != '')
			$sql .= "and gtv_cat = '{$cat}' ";
		*/

		$sql = "SELECT *, video.id as vid, categories.id as cid FROM video 
				LEFT JOIN categories ON categories.id = video.gtv_cat 
				LEFT JOIN photos ON photos.photo_id = video.id 
				WHERE video.status = 1 and gtv = 1 ";

		if($cat != '')
			$sql .= "and categories.name = '{$cat}' ";


		$sql .= "ORDER BY video.id DESC";

		if($limit != '')
		{
			$sql .= " LIMIT";
				if($offset != '')
					$sql .= " {$offset},";

				$sql .= " {$limit}";
			}

		$query = $this->db->query($sql);
		return $query->result();
	}

	function getGbaamTVByCat_count($cat = '')
	{
		/*$sql = "SELECT *
				FROM  video WHERE gtv = 1 ";
		if($cat != '')
			$sql .= "and gtv_cat = '{$cat}' ";
			*/
		
		$sql = "SELECT * FROM video 
				LEFT JOIN categories ON categories.id = video.gtv_cat 
				LEFT JOIN photos ON photos.photo_id = video.id 
				WHERE video.status = 1 and gtv = 1 ";

		if($cat != '')
			$sql .= "and categories.name = '{$cat}' ";

		$sql .= "ORDER BY video.id DESC";

	

		$query = $this->db->query($sql);
		return $query->num_rows();
	}

	function addview($id)
	{
		$view = 1;//rand(1, 3);
		$sql = "UPDATE video SET v_count=v_count+{$view} WHERE id={$id}";
		$this->db->query($sql);
	}

	function fetchSideVideos($date)
	{
		$sql = "SELECT * FROM video WHERE status = 1 and date_created = '".$date."'";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function fsv_dategroup($date)
	{
		$date = date('Y-m-d', strtotime($date));
		echo $date;

		$sql = "Select * FROM video where status = 1 and gtv = 0 and date_created LIKE '{$date}%' and video.id != 3  and chall = 0 order by id DESC";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function get_dates_new($limit = '',$offset = '')
	{
		$sql = 'SELECT id, SUBSTR(date_created, 1, 11) AS n_date_created
				FROM video 
				WHERE status = 1 and gtv =0 and video.id != 3 and chall= 0
				GROUP BY n_date_created
				ORDER BY date_created DESC ';

		if($limit != '')
		{
			$sql .= " LIMIT";
				if($offset != '')
					$sql .= " {$offset},";

				$sql .= " {$limit}";
			}

		$query = $this->db->query($sql);
		return $query->result();
	}

	function get_dates_new_count()
	{
		$sql = 'SELECT id, date_created
				FROM video
				WHERE status = 1 and gtv =0 and video.id != 3 and chall= 0
				GROUP BY date_created
				ORDER BY date_created DESC ';


		$query = $this->db->query($sql);
		return $query->num_rows();
	}
 
	public function get_g_ch_cat()
	{
		$sql = "SELECT * FROM categories WHERE type = 'challenge' and active = 1 ORDER BY date_created DESC";
		$query = $this->db->query($sql);
		$result = $query->result();

		return $result;
	}

	public function get_latest_challenge()
	{
		$sql = "SELECT * FROM categories WHERE type = 'challenge' and active = 1 ORDER BY date_created DESC LIMIT 1";
		$query = $this->db->query($sql);
		$row = $query->row(); 

		return $row->id;
	}
	public function get_latest_challenge_info($id='')
	{
		if($id == '')
		{
			$sql = "SELECT * FROM categories WHERE type = 'challenge' and  active = 1 ORDER BY date_created DESC LIMIT 1";
		}
		else
		{
			$sql = "SELECT * FROM categories WHERE type = 'challenge' and  id='{$id}' ORDER BY date_created DESC LIMIT 1";
		}
		
		$query = $this->db->query($sql);
		$row = $query->row(); 

		return $row;
	}

	public function get_gtv_cat()
	{
		$sql = "SELECT * FROM categories where type = 'gbaamtv'";
		$query = $this->db->query($sql);
		$result = $query->result();

		return $result;
	}

	function AddToSlot($type,$slot,$video_id)
	{
		$sql = "UPDATE feat_items SET active=0 WHERE slot={$slot} and feat_type = '{$type}' and type='video'";
		$this->db->query($sql);

		$ins['feat_type'] = $type;
		$ins['slot'] = $slot;
		$ins['type_id'] = $video_id;
		$ins['type'] = 'video';
		$ins['active'] = 1;
		$ins['datestart'] = date('Y-m-d H:i:s');

		$this->db->insert('feat_items',$ins);

		;
		$this->Admin_model->log_activity('feat_video',$this->db->insert_id(),'slot','Feat Type: '.$type.' Slot '.$slot);
	}


	function did_i_vote($video_id)
	{
		$user_id = $this->session->userdata('user_id');

		$sql = "SELECT * From votes WHERE type_id = {$video_id} and voter_id = {$user_id} and type = 'video' order by id desc limit 1";

		$query = $this->db->query($sql);

		$q['num_rows'] = $query->num_rows();
		$q['row'] = $query->row();

		return $q;

	}

	function api_getScore($videoid)
	{
			$getmix = $this->Video_model->get_vote($videoid);
			$my_score = $getmix['my_score'];
			$total_score = $getmix['total_score'];
			//echo $total_score;
			//echo $my_score.'/'.$total_score;
			if($total_score == 0 )
			{
				$score = 'None';
			}
			else
			{
				$d['isScore'] = TRUE;
				$percent = ($my_score / $total_score) * 100;
				$percent = round($percent);
				$d['final_score'] = $percent;
				$score =  $d['final_score'];
			}
			return $score;
	}

	function deleteVideo($videoId)
	{
		// get the video
		$video = $this->Video_model->getVideo_row($videoId);

		// delete the votes
		//$sql3 = "DELETE FROM votes where type_id = {$videoId} and type = 'video'";
		$sql3 = "Update votes SET status = 0 where type_id = {$videoId} and type = 'video'";
		$this->db->query($sql3);

		// delete the featured item
		//$sql3 = "DELETE FROM feat_items where type_id = {$videoId} and type = 'video'";
		$sql3 = "Update feat_items set status = 0 where type_id = {$videoId} and type = 'video'";
		$this->db->query($sql3);

		// delete disqus theread
		$this->delete_disqus_thread();

		// unlink video file here
		// ....
		
		// delete the video
		//$sql = "DELETE FROM video WHERE id = {$videoId}";
		$sql = "Update video set status = 0, date_deleted = '".date('Y-m-d H:i:s')."' WHERE id = {$videoId}";
		$this->db->query($sql);

		$this->log_activity('video',$videoId,'delete');
	}


	function updateThumb($videoid, $thumb)
	{
		$sql = "UPDATE video
				SET thumb='{$thumb}'
				WHERE id={$videoid}";

		$this->db->query($sql);
	}

	function new_featvideo($id)
	{
		$sql = 	"INSERT INTO feat_items (type_id,type) VALUES ({$id},'video');";
		$this->db->query($sql);
		$this->log_activity('feat_video',$id,'create');
	}

	function getAllVideos_Admin($type = '')
	{
		$sql = "SELECT *, video.id as vid FROM video";

		if($type == 'gbaamtv')
			$sql .= ' LEFT JOIN categories ON categories.id = video.gtv_cat WHERE gtv = 1 and video.status != 0';

		if($type == 'challenge')
			$sql .= ' LEFT JOIN categories ON categories.id = video.chall_cat WHERE chall = 1 and video.status != 0';

		if($type == 'mv')
			$sql .= ' WHERE chall = 0 and gtv = 0 and video.status != 0';

		$sql .= " ORDER by video.id DESC";

		$query = $this->db->query($sql);
		return $query->result();
	}

	function getOnlyMVideos()
	{
		$sql = "SELECT * FROM video";
			$sql .= ' WHERE gtv = 0 order by id DESC';


		$query = $this->db->query($sql);
		return $query->result();
	}

	function getAllChalVideos_Admin()
	{
		$sql = "SELECT * FROM video_challenge";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function insertVideo($title,$artist,$prod,$dir,$desc,$video_src,$date='',$gbaamtv,$gtv_cat,$chall,$chall_cat, $tags)
	{
		$poster_id = $this->session->userdata('admin_user_id');


		$sql = "INSERT 
					INTO video (
						title, 
						artist_name, 
						prod, 
						dir, 
						description, 
						video_src,
						tags,
						poster_id,
						date_created
					)
					VALUES(
						'{$this->db->escape_str($title)}', 
						'{$this->db->escape_str($artist)}', 
						'{$this->db->escape_str($prod)}',
						'{$dir}', 
						'{$this->db->escape_str($desc)}', 
						'{$video_src}',
						'{$tags}', 
						{$poster_id},
						'".date('Y-m-d H:i:s')."'
				);";

		 $this->db->query($sql);

		 $ins_id = $this->db->insert_id();

		 if($gbaamtv == 1)
		 {
			$sql2 = "UPDATE  video SET  `gtv` =  '1', gtv_cat = '{$this->db->escape_str($gtv_cat)}' WHERE  `video`.`id` = {$ins_id}; ";
			$this->db->query($sql2);
		 }

		if($chall == 1)
		{
			$sql2 = "UPDATE  video SET  chall =  '1', chall_cat =  '{$chall_cat}' WHERE  id = {$ins_id};";

			$this->db->query($sql2);
		}

		 if (strpos($video_src,'youtu') !== false) {
		 	$sql3 = "UPDATE  video SET  `yt` =  '1' WHERE  `video`.`id` = {$ins_id}; ";
			$this->db->query($sql3);
		 }

		 $this->log_activity('video',$ins_id,'create');

		 return $ins_id;
		//return $query->result();
	}

	function updateVideo($videoid, $title,$artist,$prod,$dir,$desc,$video_src,$videothumb,$date='',$gbaamtv,$gtv_cat,$chall,$chall_cat,$tags,$status)
	{
		if($date == '')
			$date = date('Y-m-d H:i:s');
		$sql = "UPDATE video
					SET 
					title='{$this->db->escape_str($title)}',
					artist_name='{$this->db->escape_str($artist)}', 
					prod='{$this->db->escape_str($prod)}',
					dir='{$dir}',
					description ='{$this->db->escape_str($desc)}', 
					video_src='{$this->db->escape_str($video_src)}', 
					tags='{$tags}', 
					status = '{$status}',
					date_edited = '".date('Y-m-d H:i:s')."'
				WHERE id={$videoid}";

		$this->db->query($sql);

		 if($gbaamtv == 1)
		 {
			$sql2 = "UPDATE  video SET  `gtv` =  '1', gtv_cat = '{$this->db->escape_str($gtv_cat)}' WHERE  `video`.`id` = {$videoid}; ";
			$this->db->query($sql2);
		 }

		if($chall == 1)
		{
			$sql2 = "UPDATE  video SET  chall =  '1',
				chall_cat =  '{$chall_cat}' WHERE  id = {$videoid};";

			$this->db->query($sql2);
		}

		if (strpos($video_src,'youtu') == TRUE) {
		 	$sql3 = "UPDATE  video SET  `yt` =  '1' WHERE  `video`.`id` = {$videoid}; ";
			$this->db->query($sql3);
		 } else {
		 	$sql3 = "UPDATE  video SET  `yt` =  '0' WHERE  `video`.`id` = {$videoid}; ";
			$this->db->query($sql3);
		 }

		 $this->log_activity('video',$videoid,'update');
	}

	function insertPendingVideo($page,$email,$fname,$lname,$title,$artist='',$prod='',$dir='',$editor='',$company='',$desc,$video_src,$video_src_other,$date_release='',$gbaamtv,$gtv_cat,$chall,$chall_cat)
	{

		$ins = array(
			'email' => $email,
			'firstname' => $fname,
			'lastname' => $lname,
			'title' => $title,
			'artist_name' => $artist,
			'prod' => $prod,
			'dir' => $dir,
			'editor' => $editor,
			'company' => $company,
			'description' => $desc,
			'video_src' => $video_src,
			'video_src_other' => $video_src_other,
			'date_release' => $date_release,
			'gtv' => $gbaamtv,
			'gtv_cat' => $gtv_cat,
			'chall' => $chall,
			'chall_cat' => $chall_cat,
			'date_submitted' => date('Y-m-d H:i:s')
		);

		$this->db->insert('video_pending',$ins);

		$ins_id = $this->db->insert_id();

		if($gbaamtv == 1)
		{
			$sql2 = "UPDATE  video_pending SET  gtv =  '1', gtv_cat = '{$this->db->escape_str($gtv_cat)}' WHERE  id = {$ins_id}; ";
			$this->db->query($sql2);
		}

		if($chall == 1)
		{
			$sql2 = "UPDATE  video_pending SET  chall =  '1',
					chall_cat =  '{$chall_cat}' WHERE  id = {$ins_id};";

			$this->db->query($sql2);
		}

		 if (strpos($video_src,'youtu') !== false) {
		 	$sql3 = "UPDATE  video_pending SET  yt =  '1' WHERE  id = {$ins_id}; ";
			$this->db->query($sql3);
		 }

		 // Send email to user
		 $this->Email_model->submit_email($ins_id,$page);

		 return $ins_id;
	}

	function getPendingVideo($id)
	{
		$sql = "SELECT * FROM video_pending where id = $id";
		$query = $this->db->query($sql);
		return $query->row();
	}

	function getPendingVideos()
	{
		$sql = "SELECT * FROM video_pending where status = 'pending' Order by id DESC";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function getPendingVideos_count()
	{
		$sql = "SELECT * FROM video_pending where status = 'pending' Order by id DESC";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

}

?>