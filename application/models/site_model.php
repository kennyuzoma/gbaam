<?php

/**
 *	Gbaam 
 *	Copyright 2014, Kenny Uzoma.
 * 
 *  @author Kenny Uzoma <kennyuzoma@gmail.com> 
 *  @version 1.0
 */

class Site_model extends CI_Model {

    function __construct()
    {
        parent::__construct();

    }

	function getSlides()
	{
		$sql = "SELECT * FROM slides WHERE status = 1 order by position asc";
        $query = $this->db->query($sql);
        return $query->result();
	}

	function getSlides_admin()
	{
		$sql = "SELECT * FROM slides where status != 0 order by position asc";
        $query = $this->db->query($sql);
        return $query->result();
	}

	function getSlide($id)
	{
		$sql = "SELECT * FROM slides WHERE id = $id";
        $query = $this->db->query($sql);
        return $query->result();
	}

	function getSlides_count()
	{
		$sql = "SELECT * FROM slides WHERE status = 1";
        $query = $this->db->query($sql);
        return $query->num_rows();
	}

	function getFeatCat($id)
	{
		$sql = "SELECT id,name FROM feat_cat WHERE id = {$id}";
		$query = $this->db->query($sql);
		foreach($query->result() as $b)
			return $b->name;
	}

	function getFpVideos()
	{
		$sql = "SELECT * FROM feat_items LEFT JOIN video on video.id = feat_items.type_id ORDER by feat_items.id DESC";
		$query = $this->db->query($sql);
        return $query->result();
	}

	function getArticles($limit = '', $offset = '')
	{
		$sql = "SELECT * FROM articles Where status = 1 order by id DESC";

		if($limit != '')
			{
				$sql .= " LIMIT";

				if($offset != '')
					$sql .= " {$offset},";

				$sql .= " {$limit}";
			}
        $query = $this->db->query($sql);
        return $query->result();
	}

	function getArticlesByAuthorId($author_id, $limit = '', $offset = '')
	{
		$sql = "SELECT * FROM articles Where author_id = {$author_id} and status = 1  order by id DESC";

		if($limit != '')
			{
				$sql .= " LIMIT";

				if($offset != '')
					$sql .= " {$offset},";

				$sql .= " {$limit}";
			}
        $query = $this->db->query($sql);
        return $query->result();
	}

	function getArticles_admin($limit = '', $offset = '')
	{
		$admin_type = $this->session->userdata('admin_type');
		$admin_id = $this->session->userdata('admin_user_id');
		
		//if(($admin_type == '5') || ($admin_type == '6'))
		//	$sql = "SELECT * FROM articles where author_id = '".$admin_id."' and status != '5' and status != '0' ORDER by id DESC";
		
		//else
			$sql = "SELECT * FROM articles where status != '4' and status != '5' and status != '0' order by id DESC";

		if($limit != '')
			{
				$sql .= " LIMIT";

				if($offset != '')
					$sql .= " {$offset},";

				$sql .= " {$limit}";
			}
        $query = $this->db->query($sql);
        return $query->result();
	}

	function getArticlesToReview_admin($limit = '', $offset = '')
	{
		$sql = "SELECT * FROM articles where status = '4' order by id DESC";

		if($limit != '')
			{
				$sql .= " LIMIT";

				if($offset != '')
					$sql .= " {$offset},";

				$sql .= " {$limit}";
			}
        $query = $this->db->query($sql);
        return $query->result();
	}

	function getArticlesCount()
	{
		$sql = "SELECT * FROM articles Where status = 1 order by id DESC";
        $query = $this->db->query($sql);
        return $query->num_rows();
	}


	function getArticle($slug)
	{
		if($slug != '')
		{
			if(is_numeric($slug))
				$sql = "SELECT * FROM articles WHERE status != '0' and id = $slug";
			else
				$sql = "SELECT * FROM articles WHERE status != '0' and permalink = '$slug'";

	        $query = $this->db->query($sql);
        	if($query->num_rows() > 0)
				return $query->result();
			else
				return FALSE;
		}
		else
		{
			return FALSE;
		}
	}

	function getArticle_admin($slug)
	{
		if($slug != '')
		{
			if(is_numeric($slug))
				$sql = "SELECT * FROM articles WHERE status != 0 and id = $slug";
			else
				$sql = "SELECT * FROM articles WHERE status != 0 and permalink = '$slug'";

	        $query = $this->db->query($sql);
        	if($query->num_rows() > 0)
				return $query->result();
			else
				return FALSE;
		}
		else
		{
			return FALSE;
		}
	}

	function getArticleById($id)
	{
		$sql = "SELECT * FROM articles where id = $id";
		$query = $this->db->query($sql);

		return $query->row();
	}

	function getAuthor($id,$type = 'article')
	{
		if($type == 'article')
		{
			$d = $this->getArticleById($id);

			if($d->author != '') // if author field is filled out
			{
				$author =  $d->author; 
			}
			if($d->author == '')
			{
				if($d->author_id == '0' )
				{
					$author = 'Gbaam';
				}
				else
				{
					$sql = "SELECT * FROM admin where id = ".$d->author_id." Limit 1";
					$query = $this->db->query($sql);
					$na = $query->row();

					$author =  $na->display_name;
				}
				
			}
		}

		if($type == 'mixtape')
		{
			$d = $this->Mixtape_model->getMixtape_new($id);
		}

		if($type == 'song')
		{
			$d = $this->Songs_model->getSong_new($id);
		}

		if($type == 'mixtape' || $type == 'song')
		{
			
			$sql = "SELECT * FROM admin where id = ".$d->poster_id." Limit 1";
			$query = $this->db->query($sql);
			$na = $query->row();

			$author =  $na->display_name;

		}


	

		
    	return $author;
	}

	function getAuthorLink($id,$type = 'article')
	{
		if($type == 'article')
			$d = $this->getArticleById($id);

		if($type == 'mixtape')
			$d = $this->Mixtape_model->getMixtape_new($id);

		if($type == 'song')
			$d = $this->Songs_model->getSong_new($id);

		if($d->author != '') // if author field is filled out
		{
			$author =  'gbaam'; 
		}

		if($d->author == '')
		{
			if($d->author_id == '0')
			{
				$author = 'gbaam';
			}
			else
			{
				$sql = "SELECT * FROM admin where id = ".$d->author_id." Limit 1";
				$query = $this->db->query($sql);
				$na = $query->row();

				$author =  $na->username;
			}
			
		}
    	return $author;
	}

	function AddToSlot($type,$slot,$video_id)
	{
		$sql = "UPDATE feat_items SET active=0 WHERE slot={$slot} and feat_type = '{$type}' and type = 'article'";
		$this->db->query($sql);

		$ins['feat_type'] = $type;
		$ins['slot'] = $slot;
		$ins['type_id'] = $video_id;
		$ins['type'] = 'article';
		$ins['active'] = 1;
		$ins['datestart'] = date('Y-m-d H:i:s');

		$this->db->insert('feat_items',$ins);

		;
		$this->Admin_model->log_activity('feat_article',$this->db->insert_id(),'slot','Feat Type: '.$type.' Slot '.$slot);
	}

	function getFeatArticle_slot_count($type,$slot)
	{
		$sql = "Select * FROM feat_items LEFT JOIN articles ON articles.id = feat_items.type_id WHERE articles.status = 1 and active = 1 and slot = {$slot} and feat_type = '{$type}' and type = 'article' ORDER BY feat_items.id DESC LIMIT 1";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

	function getFeatArticle_slot($type,$slot)
	{
		$sql = "Select * FROM feat_items 
				LEFT JOIN articles on articles.id = feat_items.type_id 
				WHERE articles.status = 1 and feat_items.active = 1 and slot = {$slot} and feat_type='{$type}' and type = 'article' LIMIT 1";
		$query = $this->db->query($sql);
		return $query->row();
	}

	function addview($id)
	{
		$view = 1;//rand(1, 3);
		$sql = "UPDATE articles SET v_count=v_count+{$view} WHERE id={$id}";
		$this->db->query($sql);
	}

	function get_all_config()
	{
		$sql = "SELECT * FROM site_settings";
		$query = $this->db->query($sql);
        return $query->result();
	}

	function get_config_item($setting)
	{
		$sql = "SELECT * FROM site_settings WHERE setting = '{$setting}'";
		$query = $this->db->query($sql);
        return $query->row();
	}

	function update_config($data)
	{
		$success = true;
		foreach($data as $key=>$value)
		{
			if(!$this->save($key,$value))
			{
				$success=false;
				break;  
			}
		}
		return $success;
	}

	function save_config($setting,$value)
	{
		$config_data=array(
			'setting'=>$setting,
			'value'=>$value
			);
		$this->db->where('setting', $setting);
		return $this->db->update('site_settings',$config_data); 
	}

	function get_author($id)
	{
		$sql = "SELECT * FROM admin 
				LEFT JOIN admin_types on admin_types.id = admin.admin_type
				LEFT JOIN photos ON photos.photo_id = admin.profile_img 
				WHERE admin.id = {$id}";
				
		$query = $this->db->query($sql);

		return $query->row(); 
	}

	function get_author_by_username($username)
	{
		$sql = "SELECT * FROM admin 
				LEFT JOIN admin_types on admin_types.id = admin.admin_type
				LEFT JOIN photos ON photos.photo_id = admin.profile_img 
				WHERE admin.username = '{$username}'";
				
		$query = $this->db->query($sql);

		return $query->row(); 
	}


	function get_page($slug)
	{
		$sql = "SELECT * FROM pages where ";
		
		if(is_numeric($slug))
			$sql .= " id = ".$slug;
		else
			$sql .= " permalink = '".$slug."'";

		$query = $this->db->query($sql);

		
		return $query->row();

	}

	function insertArticle($title, $body, $author_id, $author, $permalink, $tags, $posted_by_gbaam)
	{
		$date_created = date('Y-m-d H:i:s');
		$poster_id = $this->session->userdata('admin_user_id');
		$guest = '0';

		if($posted_by_gbaam == 'on')
			$author_id = '0';


		if(($this->session->userdata('admin_type') == '5') || ($this->session->userdata('admin_type') == '6'))
			$status = '4'; //review
		elseif($this->session->userdata('admin_type') <= '4')
			$status = '1'; // active if admin


		if($this->session->userdata('admin_type') == '6')
			$guest = '1';

		$sql = "INSERT INTO	articles (title,body, author, author_id, poster_id, permalink, date_created, tags, status, guest)
				VALUES (
						'".$this->db->escape_str($title)."',
						'".$this->db->escape_str($body)."',
						'".$this->db->escape_str($author)."',
						'".$this->db->escape_str($author_id)."',
						'".$this->db->escape_str($poster_id)."',
						'".$this->db->escape_str($permalink)."',
						'".$date_created."',
						'".$tags."',
						'".$status."',
						'".$guest."'
				)";
				
		$this->db->query($sql);

		$this->log_activity('article',$this->db->insert_id(),'create');
	}
/*
	function getArticles()
	{
		$sql = "SELECT * FROM articles where status != 0";
		$query = $this->db->query($sql);
		return $query->result();
	}
*/
	function updateArticle($Articleid,$title, $body, $author, $author_id, $permalink, $tags, $status, $posted_by_gbaam)
	{
		// get the original author id
		$article = $this->Site_model->getArticleById($Articleid);	
		$original_author_id = $article->author_id;

		// if posted by gbaam was checked.
		if($posted_by_gbaam == 'on')
		{
			$author_id = '0';
			$author = '';
		}
		
		// 
		$sql2 = "UPDATE articles
  				SET title='".$this->db->escape_str($title)."', 
  					body = '".$this->db->escape_str($body)."',  
  					author = '".$this->db->escape_str($author)."', 
  					author_id = '".$this->db->escape_str($author_id)."', 
  					permalink = '".$this->db->escape_str($permalink)."', 
  					tags = '{$tags}', 
  					status = '{$status}',
  					date_edited = '".date('Y-m-d H:i:s')."'
  					WHERE id = {$Articleid}";
		$this->db->query($sql2);
		$this->log_activity('article',$Articleid,'update');

		if($this->session->userdata('admin_type') <= '4' && $status == '4')
		{
			if($this->input->post('submit'))
			{
				$status = '1';
				$action = 'approved';
				$this->log_activity('article',$Articleid,'approve');
			}
			elseif($this->input->post('submit_deny'))
			{
				$status = '5';
				$action = 'deny';
				$this->log_activity('article',$Articleid,'deny');

				$deny_reasons = implode(',', $this->input->post('deny_reasons'));
				$deny_reasons_other = $this->input->post('deny_reasons_other');
				$upd['deny_reasons'] = $deny_reasons;
				$upd['deny_reasons_other'] = $deny_reasons_other;

			}

			$upd['reviewed_by'] = $this->session->userdata('admin_user_id');
			$upd['status'] = $status;

			$this->db->where('id',$Articleid);
			$this->db->update('articles',$upd);

  			// send the descision email.
  			$this->Email_model->article_reviewed($Articleid, $action);

		}
	}


	function deleteArticle($Article_id)
	{
		$article = $this->Site_model->getArticleById($Article_id);	
		// unlink any images
		// ... 

		// delete feat item
		//$sql3 = "DELETE FROM feat_items where type_id = {$Article_id} and type = 'article'";
		$sql3 = "UPDATE feat_items SET status = 0 where type_id = {$Article_id} and type = 'article'";
		$this->db->query($sql3);

		$upd['status'] = '0';
		$upd['date_deleted'] = date('Y-m-d H:i:s');

		// delete article
		$this->db->update('articles', $upd, array('id' => $Article_id));

		// delete disqus theread
		$this->delete_disqus_thread();
		$this->log_activity('article',$Article_id,'delete');
	}


	function getPendingArticles_count()
	{
		$sql = "SELECT * FROM articles where status = '4' Order by id DESC";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	


}

?>